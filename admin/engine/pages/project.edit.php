<section id="project_edit">

    <div class="container">
        <div class="row">
            <div class="col-md-8"><p class="h2 mt-none"><?= $page->heading ?></p></div>
            <div class="col-md-4 text-right">
                <?php if (isset($project)) { ?>
                    <a href="/works/<?= $project->id ?>" class="btn btn-default" target="_blank">Просмотреть</a>
                <?php } ?>
            </div>
        </div>

        <hr>

        <form action="/admin/index.php" method="post">
            <?php if (isset($project)) { ?>
                <input type="text" name="action" value="update_project" hidden>
                <input type="text" name="id" value="<?= $project->id ?>" hidden>
            <?php } else { ?>
                <input type="text" name="action" value="add_project" hidden>
            <?php } ?>

            <div class="row">
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="">Название проекта</label>
                        <input type="text" class="form-control" name="name" value="<?php if (isset($project->name)) { print htmlspecialchars($project->name); } ?>">
                    </div>

                     <div class="form-group">
                        <label for="">H1</label>
                        <input type="text" class="form-control" name="seo_h1" value="<?php if (isset($project->seo_h1)) { print htmlspecialchars($project->seo_h1); } ?>">
                    </div>

                     <div class="form-group">
                        <label for="">Title</label>
                        <input type="text" class="form-control" name="seo_title" value="<?php if (isset($project->seo_title)) { print htmlspecialchars($project->seo_title); } ?>">
                    </div>

                    <div class="form-group">
                        <label for="">SEO Description</label>
                        <input type="text" class="form-control" name="seo_description" value="<?php if (isset($project->seo_description)) { print htmlspecialchars($project->seo_description); } ?>">
                    </div>

                    <div class="form-group">
                        <label for="">SEO Keywords</label>
                        <input type="text" class="form-control" name="seo_keywords" value="<?php if (isset($project->seo_keywords)) { print htmlspecialchars($project->seo_keywords); } ?>">
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">URL проекта</label>
                                <input type="text" class="form-control" name="url" value="<?php if (isset($project->url)) { print $project->url; } ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">SEO URL</label>
                                <input type="text" class="form-control" name="seo_url" value="<?php if (isset($project->seo_url)) { print $project->seo_url; } ?>">
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Бюджет</label>
                                <input type="text" class="form-control" name="budget" value="<?php if (isset($project->budget)) { print $project->budget; } ?>">
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="">Клиент</label>
                                <input type="text" class="form-control" name="client" value="<?php if (isset($project->client)) { print htmlspecialchars($project->client); } ?>">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-2">
                    <label for="">Раздел проекта</label>
                    <select name="category_id" class="form-control" id="">
                        <?php foreach($categories as $item){?>
                            <option
                                <?php if (isset($project)) { if ($project->category_id == $item->id){ ?> selected <?php } } ?>

                                value="<?= $item->id ?>"><?= $item->name ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="">Изображения проекта</label>
                        <?php if (isset($project->image)) { ?> <img class="img-responsive" src="/img/projects/<?= $project->image ?>" alt=""> <?php } ?>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="form-group">
                        <label for="">Описание проекта</label>
                        <textarea name="content" class="form-control"  id="ckeditor" cols="30" rows="10"><?php if (isset($project->content)) { print htmlspecialchars($project->content); } ?></textarea>
                    </div>
                </div>
            </div>
            <hr>
            <div class="row">
                <div class="col-md-12">
                    <button type="submit" class="btn btn-primary">Сохранить изменения</button>
                </div>
            </div>

        </form>
    </div>

</section>