<section id="projects_list">
<div class="container">
    <div class="row">
        <div class="col-md-6">
            <p class="h2 mt-none">Проекты</p>
        </div>
        <div class="col-md-6 text-right">
            <a class="btn btn-success" href="/admin/project/add">Добавить новый</a>
        </div>
    </div>
    <hr>

    <?php foreach ($categories as $item) { ?>

        <p class="h3"> <?= $item->name ?></p>
        <div class="row">
            <div class="well">
        <?php foreach ($projects as $project) { ?>
            <?php if ($project->category_id == $item->id) {?>

                <div class="col-md-3">
                    <div class="item">
                        <a href="/admin/project/edit/<?= $project->id ?>" class="h4"><?= $project->name ?></a>
                    </div>
                </div>
            <?php } ?>
        <?php } ?>
                <div class="clearfix"></div>
            </div>
        </div>
    <?php } ?>

</div>
</section>
