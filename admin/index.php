<?php

include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");
include_once($_SERVER['DOCUMENT_ROOT']."/admin/engine/classes/Simpleimage.php");

$app = new App();

$page = new stdClass();
$page->title = "";
$page->heading = "";

// Form processing
if (isset($_POST['action'])){
    $post_action = $_POST['action'];

    switch ($post_action){
        case "add_project":
            $app->db->add(
                array('name','url','budget','client','category_id','content','seo_url','seo_keywords','seo_h1','seo_title','seo_description'),
                array(
                    $_POST['name'],
                    $_POST['url'],
                    $_POST['budget'],
                    $_POST['client'],
                    $_POST['category_id'],
                    $_POST['content'],
                    $_POST['seo_url'],
                    $_POST['seo_keywords'],
                    $_POST['seo_h1'],
                    $_POST['seo_title'],
                    htmlspecialchars($_POST['seo_description'])
                ),
                "projects");
            header('Location: /admin/project/edit/'.$app->db->getLastId());
            break;
        case "update_project":
            $app->db->set(
                array('name','url','budget','client','category_id','content','seo_url','seo_keywords','seo_h1','seo_title','seo_description'),
                array(
                    $_POST['name'],
                    $_POST['url'],
                    $_POST['budget'],
                    $_POST['client'],
                    $_POST['category_id'],
                    $_POST['content'],
                    $_POST['seo_url'],
                    $_POST['seo_keywords'],
                    $_POST['seo_h1'],
                    $_POST['seo_title'],
                    htmlspecialchars($_POST['seo_description'])
                ),
                "projects",
                "WHERE `id` = '".$_POST['id']."'");

           // $App->db->uploadFiles( $_POST['project_id'] );
            header('Location: /admin/project/edit/'.$_POST['id']);
            break;
        default:
            break;
    }
}

// routing based on GET
if (isset($_GET['path'])){

    // if (isset($_SESSION['user_auth']) && ($_SESSION['user_auth'] == 1) ){  }

    $path = $_GET['path'];
    switch ($path){
        /*case "edit_project":
            $categories = $App->db->get('*','categories');
            $project = $App->db->get('*','projects', "WHERE `id` = '".$_GET['id']."'");
            // get images
            $images = $App->db->get("*", 'project_images', "WHERE `project_id` = '".$project->id."'");
            $project->images = array();
            is_object($images) ? array_push($project->images, $images) :  $project->images = $images;
            $includable = array("/admin/engine/pages/project.edit.php");
            break;*/

        case "projects_list":
            $categories = $app->db->get('*','projects_categories');
            $projects = $app->db->get('*','projects');
            $includable = array("/admin/engine/pages/projects.list.php");
            break;

        case "project_add":
            $categories = $app->db->get('*','projects_categories');

            $page->title = "Редактирование проекта";
            $page->heading = "Добавление проекта";

            $includable = array("/admin/engine/pages/project.edit.php");
            break;

        case "project_edit":
            $project = $app->db->get('*','projects', "WHERE `id` = '".$_GET['id']."'");
            $categories = $app->db->get('*','projects_categories');

            $page->title = "Редактирование проекта";
            $page->heading = "Редактирование проекта <strong>".$project->name."</strong>";

            $includable = array("/admin/engine/pages/project.edit.php");
            break;
        default:
            break;
    }
} else {
    //default action
    /* $projects = $App->db->get("*","projects", " ORDER BY `id` DESC");
    foreach ($projects as &$item){
        $images = $App->db->get("*", 'project_images', "WHERE `project_id` = '".$item->id."'");
        $item->images = array();
        is_object($images) ? array_push($item->images, $images) :  $item->images = $images;
    }*/

    $includable = array("/admin/engine/pages/home.php");
}



include_once($_SERVER["DOCUMENT_ROOT"]."/admin/engine/parts/template.php");
?>