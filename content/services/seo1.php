<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.01.2016
 * Time: 15:24
 */

include_once($_SERVER['DOCUMENT_ROOT']."/engine/App.php"); ?>
<!DOCTYPE html>
<html>
<head>

	<!-- Basic -->
	<meta charset="utf-8">
	<meta name="google-site-verification" content="roI4WhDGEBeE6qTZ8QqUF1LCAb2Ck_xvxCopHxZjl3c" />
	<meta name="keywords" content="одностраничный, лендинг, landing page, лендинг пейдж, лендингпейдж, одностроничник, сайт в одну страницу, одностраничный сайт, создать одностраничный сайт, создать лендингпейдж, сделать сайт, создать сайт, разработать одностраничник, создать одностраничник, сайт, сайтов, сайты, сайта, интернет магазин, интернетмагазин, интернет-магазин, создать, сделать, разработать, спроектировать, прототип, купить, цена, стоимость, продающий сайт, продающего сайта"/>
	<meta name="description" content="Создание одностраничного сайта. Создание продающего сайта для контекстной рекламы." />

	<title>Создание лендинг пейдж. Одностраничник. Продающий сайт.</title>

	<!-- Favicon -->
	<link rel="apple-touch-icon" sizes="57x57" href="../../img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="../../img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="../../img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="../../img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="../../img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="../../img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="../../img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="../../img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="../../img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="../../img/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="../../img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="../../img/favicon/favicon-16x16.png">
	<link rel="manifest" href="/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="../../img/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<!-- Mobile Metas -->
	<meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

	<!-- Web Fonts  -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

	<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/css.php"); ?>

	<!-- Current Page CSS -->
	<link rel="stylesheet" href="/vendor/rs-plugin/css/settings.css" media="screen">
	<link rel="stylesheet" href="/vendor/rs-plugin/css/layers.css" media="screen">
	<link rel="stylesheet" href="/vendor/rs-plugin/css/navigation.css" media="screen">
	<link rel="stylesheet" href="/vendor/circle-flip-slideshow/css/component.css" media="screen">
	<!-- Head Libs -->

	<style>
		#promo{background:#ff6400; margin-top: 3.6em; color: #fff;}
		#promo p, #promo strong{color: #fff}

		#promo .h1{font-size: 1.5em}
		#promo .col-md-5 {padding: 4em 5em;}
		#promo img{margin: 0 -0.2em;width: 15.6em;}
		#promo .path6{}

		#promo #case-link{
			height: 15.6em;
			border: solid 1px;
			padding: 7em 3.5em;
			width: 15.5em;
			margin: 0 0 0 -0.2em;
		}
		#promo #case-link > a{
			text-decoration: none;color: #fff;font-size: 1.5em;
		}
		#promo #case-link > a::before{
			content: "ознакомиться с кейсами";
			height: 10.4em;
			opacity: 0;
			background: #ff7500;
			display: block;
			position: absolute;
			width: 10.3em;
			margin: -4.7em -2.4em;
			padding: 4.7em 2em;
		}
		#promo #case-link:hover > a::before{
			opacity: 1;
		}

		#call-back{}
		#call-back .row{margin: 0.5em 0}
		#call-back .btn-warning,
		#services .btn-warning,
		#strategy .btn-warning,
		#our-templates .btn-warning{background: #ff6400;}

		#services{color: grey;}
		#services .block > a{ color: grey}
		#services .block > a:hover{text-decoration: none;}
		#services .block > a:hover .text{color: grey}
		#services .block > a:hover .h3{color: #ff6400}
		#services .block > a:hover button{color: #fff;background: #ff6400}
		#services .other-service{padding: 1em}

		#pluse{color: grey}
		#pluse .row{padding-bottom: 0.5em}
		#pluse span{font-size: 5em;}
		#pluse .block:hover{color: #ff6400}

		#strategy{background: darkgrey;}
		#strategy .text-center{padding: 1em 0 0 0;}
		#strategy .btn_active{}
		#strategy .btn1{background: rgba(255, 100, 0, 1);}
		#strategy .btn2{background: rgba(255, 100, 0, 0.8);}
		#strategy .btn3{background: rgba(255, 100, 0, 0.6);}
		#strategy .btn4{background: rgba(255, 100, 0, 0.4);}
		#strategy .btn5{background: rgba(255, 100, 0, 0.2);}
		#strategy ul {list-style: none;}
		#strategy ul span{font-weight: bold;}


		#myCarousel{}
		#myCarousel .item{
			margin-top: 2em;
			min-height: 23em;
		}

		#myCarousel label{
			padding: 0.5em 3.5em;
		}
		#myCarousel label:hover{
			background: burlywood;
		}

		#myCarousel .btn1{background: rgba(235, 147, 22, 1);}
		#myCarousel .btn2{background: rgba(235, 147, 22, 0.8);}
		#myCarousel .btn3{background: rgba(235, 147, 22, 0.6);}
		#myCarousel .btn4{background: rgba(235, 147, 22, 0.4);}
		#myCarousel .btn5{background: rgba(235, 147, 22, 0.2);}
		#myCarousel .btn6{background: rgba(235, 147, 22, 0);}

		#myCarousel h1 p{color: #000}

		#myCarousel .carousel-caption{
			top: 0;
			right: 0;
			left: 0;
			padding-bottom: 0;
			text-align: left;
		}

		#myCarousel .carousel-inner{
		}

		#myCarousel .seo-carusel{
			list-style: none;
		}

		label::after{
			content: '';
			position: absolute;
			top: 2.4em;
			width: 10px;
			height: 10px;
			background: red;
			border-top: solid 10px #EB9316;
			border-left: solid 10px darkgrey;
			border-right: solid 10px darkgrey;
			display: none;

		}

		input:checked + label{
			color: red;
		}
		#myCarousel .seo-carusel li{
			display: inline-block;
		}

		#myCarousel .seo-list{
			margin-left: -2em;
		}

		#myCarousel .seo-list > li{
			display: inline-block;
			margin: 0 1em;
			font-size: 1.5em;
			text-shadow: none;
		}

		#myCarousel .seo-list > li > p{
			color:#fff;
		}

		#our-templates .our-templates{background: url('../../img/services/templates.jpg');}
		#our-templates .icon{
			width: 128px;
			height: 128px;
			position: relative;
			display: block;
			background-repeat: no-repeat;
			background-size: cover;
		}
		#our-templates .icon1{background: url('../../img/services/');}
		#our-templates .icon2{background: url('../../img/services/');}
		#our-templates .icon3{background: url('../../img/services/');}
		#our-templates .icon4{background: url('../../img/services/');}
		#our-templates .our-templates-title{padding-top: 2em;}
		#our-templates .our-templates-title .h1{padding: 0 0;margin: 0;color: #fff}
		#our-templates .our-templates-title .h2{padding: 0 0;margin: 0;color: #fff}
		#our-templates .our-templates-about span{}
		#our-templates .our-templates-about p{color:#fff;}
		#our-templates .our-templates-btn{padding-left:4em; margin-top: 1em;}

		#adventages{}
		#partners{}


	</style>
</head>
<body>
<section id="promo">
    <div class="container">
        <div class="row">
            <div class="col-md-5">
                <p class="h2">Создаем и <strong class="h1">продвигаем</strong> сайты для бизнеса</p>
            </div>
            <div class="col-md-7">

				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4"><img src="/img/services/1.jpg"></div>
						<div class="col-md-4"><img src="/img/services/2.jpg"></div>
						<div class="col-md-4"><img src="/img/services/3.jpg"></div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="col-md-4"><img src="/img/services/4.jpg"></div>
						<div class="col-md-4"><img src="/img/services/5.jpg"></div>
						<div class="col-md-4"><div id="case-link"><a href="#">кейсы по продвижению</a></div></div>
					</div>
				</div>
				   
			</div>
		</div>
		<!--обратный звонок-->
    </div>
</section>

<section id="call-back">
	<div class="row">
		<div class="col-md-4 col-md-offset-8">
			<button class="btn btn-default">обратный звонок</button>
			<button class="btn btn-warning">отправить заявку</button>
		</div>
	</div>
</section>

<section id="services">
	<div class="container">
		<div class="row">
			<div>
				<p class="h3 text-center">
					услуги интернет-маркетинга
				</p>
			</div>
		</div>
	
		<div class="row text-center">
			<div class="col-md-3 block">
				<a href="#">
					<img src="" id="service-img1">
					<p class="h3">Классническое продвижение в ТОП</p>
					<p class="text">Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
					<button class="btn btn-block">подробнее</button>
				</a>
			</div>
			<div class="col-md-3 block">
				<a href="">
					<img src="" id="service-img2">
					<p class="h3">Классническое продвижение в ТОП</p>
					<p class="text">Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
					<button class="btn btn-block">подробнее</button>
				</a>
			</div>
			<div class="col-md-3 block">
				<a href="#">
					<img src="" id="service-img3">
					<p class="h3">Классническое продвижение в ТОП</p>
					<p class="text">Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
					<button class="btn btn-block">подробнее</button>
				</a>
			</div>
			<div class="col-md-3 block">
				<a href="#">
					<img src="" id="service-img4">
					<p class="h3">Классническое продвижение в ТОП</p>
					<p class="text">Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
					<button class="btn btn-block">подробнее</button>
				</a>
			</div>
		</div>
		
		<!--другие услуги-->
		<div class="row other-service">
			<div class="col-md-2 col-md-offset-5">
				<button class="btn btn-warning">другие услуги <span class="caret"></span></button>
			</div>
		</div>
		
	</div>
</section>

				<!--
				glyphicon glyphicon-link
				glyphicon glyphicon-refresh
				glyphicon glyphicon-list-alt
				glyphicon glyphicon-th
				-->
<section id="pluse">
	<div class="container">
		<div class="row text-center"><p class="h1">Фишки нашего продвижения</p></div>
		
		<div class="row text-center">
			<div class="col-md-3 block">
				<span class="glyphicon glyphicon-link"></span>
				<p class="h3">Ссылочная масса</p>
				<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
			</div>
			<div class="col-md-3 block">
				<span class="glyphicon glyphicon-list-alt"></span>
				<p class="h3">Наши гарантии</p>
				<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
			</div>
			<div class="col-md-3 block">
				<span class="glyphicon glyphicon-th"></span>
				<p class="h3">Ключевые фразы</p>
				<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
			</div>
			<div class="col-md-3 block">
				<span class="glyphicon glyphicon-refresh"></span>
				<p class="h3">Наполнение сайта</p>
				<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
			</div>
			
		</div>
	</div>
</section>

<section id="cases"></section>

<section id="strategy">
	<div class="container">
		<div class="row">
			<p class="h1 text-center">Оптимальная стратегия	продвижения</p>
		</div>

		<div id="myCarousel" class="carousel slide">
			<!-- Indicators -->
			<ol class="seo-carusel">
				<li data-target="#myCarousel" data-slide-to="0" class="active"> <label for="bt1" class="btn atc btn-warning btn1">Оптимизация<input type="radio" name="btn" style="display: none" id="bt1" checked></label></li>
				<li data-target="#myCarousel" data-slide-to="1">				<label for="bt2" class="btn btn-warning btn2">Продвижение<input type="radio" name="btn" style="display: none" id="bt2"></label></li>
				<li data-target="#myCarousel" data-slide-to="2">				<label for="bt3" class="btn btn-warning btn3">Трафик<input type="radio" name="btn" style="display: none" id="bt3"></label></li>
				<li data-target="#myCarousel" data-slide-to="3">				<label for="bt4" class="btn btn-warning btn4">Юзабилити<input type="radio" name="btn" style="display: none" id="bt4"></label></li>
				<li data-target="#myCarousel" data-slide-to="4">				<label for="bt5" class="btn btn-warning btn5">Конверсия<input type="radio" name="btn" style="display: none" id="bt5"></label></li>
				<li data-target="#myCarousel" data-slide-to="5">				<label for="bt6" class="btn btn-warning btn6">Продажи<input type="radio" name="btn" style="display: none" id="bt6"></label></li>
			</ol>

			<div class="carousel-inner" role="listbox">

				<!--path1-->
				<div class="item active">

						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>1.</span> Проверяем историю сайта и<br>
										проводим мониторинг конкурентной среды,<br>
										определяем стратегию продвижения
									</p>
									<p><span>2.</span> Проводим подробный SEO-аудит и<br>
										технический анализ сайта и сервера, в том числе<br>
										на наличие вредоносного ПО и вирусов
									</p>
									<p><span>3.</span> Оптимизируем работу сервера,<br>
										в том числе файлы .htaccess и robots.txt
									</p>
									<p><span>4.</span> Оптимизируем микроразметку сайта<br>
										и ЧПУ, настраиваем автогенерацию url-ов
									</p>
								</li>
								<li>
									<p><span>5.</span> Оптимизируем навигацию и структуру сайта,<br>
										в том числе карты сайта форматов html и xml,<br>
										исправляем битые ссылки
									</p>
									<p><span>6.</span> Оптимизируем скорость работы сайта<br>
										и обработку основных ошибок
									</p>
									<p><span>7.</span> Распределяем семантические группы и<br>
										оптимизируем метаданные
									</p>
									<p><span>8.</span> Устраняем дубли, анализируем уникальность<br>
										и качество контента сайта, пишем необходимый<br>
										и уникализируем существующие тексты
									</p>
								</li>
							</ul>
						</div>


				</div>

				<!--path2-->
				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>9.</span> Регистрируем локальный бизнес<br>
										и продвигаем в картах поисковых систем
									</p>
									<p><span>10.</span> Проводим внешнюю оптимизацию сайта:<br>
										увеличиваем число упоминаний и отзывов,<br>
										наращиваем качественную ссылочную массу
									</p>
								</li>
								<li>
									<p><span>11.</span> В течение продвижения систематически<br>
										расширяем и корректируем семантическое ядро
									</p>
									<p><span>12.</span> Продвигаем сайт социальными факторами:<br>
										создаем и наполняем контентом группу в<br>
										Google+, репостим сайт в аккаунтах соцсетей
									</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<!--path3-->
				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>13.</span> При необходимости создаем и ведем <br>
										рекламные компании в поисковой сети<br>
										и КМС, настраиваем ремаркетинг
									</p>
								</li>
								<li>
									<p><span>14.</span> Рекомендуем и ведем работы по<br>
										контент-маркетингу, наращивая трафик<br>
										по длинному хвосту запросов (long tail)
									</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<!--path4-->
				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>15.</span> Проверяем кроссбраузерность,<br>
										проводим тестирование функционала сайта,<br>
										анализируем видимость актуальной информации
									</p>
								</li>
								<li>
									<p><span>16.</span> Тестируем процессы и работу форм<br>
										онлайн-заказов и обратной связи, вносим<br>
										рекомендации по повышению юзабилити
									</p>
								</li>
						</div>
					</div>
				</div>

				<!--path5-->
				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>17.</span> Исследуем карту поведения и страницы выхода,<br>
										карту кликов, увеличиваем глубину просмотров<br>
										и время, проводимое пользователями на сайте,<br>
										уменьшаем показатель отказов
									</p>
								</li>
								<li>
									<p><span>18.</span> Устанавливаем сервисы аналитики и<br>
										настраиваем цели, конверсии, на базе<br>
										данных которых вносим рекомендации<br>
										по повышению конверсий сайта
									</p>
								</li>
							</ul>
						</div>
					</div>
				</div>

				<!--path6-->
				<div class="item">
					<div class="container">
						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>19.</span> Исследуем отзывы и упоминания о компании,<br>
										доступные в интернете через поиск,<br>
										а при выявлении негатива рекомендуем<br>
										и проводим репутационный маркетинг (SERM)
									</p>
								</li>
								<li>
									<p><span>20.</span> Совершаем тайную покупку,<br>
										а также заказ по телефону, фиксируем ошибки<br>
										и упущения контактного персонала,<br>
										вносим рекомендации по оптимизации сервиса
									</p>
								</li>
							</ul>
						</div>
					</div>
				</div>


			</div>

		</div><!-- /.carousel -->
	</div>
</section>

<section id="our-templates">
	<div class="our-templates">
	<div class="container">
		<div class="row">
			<div class="col-md-12 text-center our-templates-title">
				<p class="h1">Продвижение на базе готовых решений</p>
				<p class="h2">Для начинающего онлайн-бизнеса с малым бюджетом</p>
			</div>
			<div class="col-md-12 text-center our-templates-title">
				<p class="h1">Заказав продвижение на 1 год</p>
				<p class="h2">Вы получаете пакетное решение</p>
			</div>
		</div>
		<div class="row our-templates-about">
				<div class="col-md-3">
					<span class="icon icon1"></span>
					<p>
						Разработка адаптируемого
						под мобильные устройства
						современного сайта с учетом
						требований продвижения
					</p>
				</div>
				<div class="col-md-3">
					<span class="icon icon2"></span>
					<p>
						Продвижение сайта в ТОП
						по утвержденному списку
						тематических поисковых
						запросов в течении года
					</p>
				</div>
				<div class="col-md-3">
					<span class="icon icon3"></span>
					<p>
						Возможность бесплатного
						наполнения нового сайта
						текстовым и медиа
						контентом в течении года
					</p>
				</div>
				<div class="col-md-3">
					<span class="icon icon4"></span>
					<p>
						Возможность бесплатной
						модернизации сайта
						(развитие функционала и
						редизайн) в течении года
					</p>
				</div>

		</div>
	</div>
	</div>

	<div class="row">
		<div class="col-md-3 col-md-offset-4 our-templates-btn">
			<button class="btn btn-warning btn-block">Заказать комерческоп предложение</button>
		</div>
	</div>
</section>
<section id="adventages"></section>

<section id="partners"></section>



<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/footer.php"); ?>
</div>

<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/scripts.php"); ?>



</body>
</html>