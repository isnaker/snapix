<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 09.01.2016
 * Time: 15:24
 */

include_once($_SERVER['DOCUMENT_ROOT']."/engine/App.php"); ?>
<!DOCTYPE html>
<html lang="ru-RU">
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta name="google-site-verification" content="roI4WhDGEBeE6qTZ8QqUF1LCAb2Ck_xvxCopHxZjl3c" />
        <meta name="keywords" content="вебстудия создать сайт цена создание для бизнеса лендинг визитка продающий одностраничник интернет магазин"/>
        <meta name="description" content="Создание сайтов для бизнеса. Создание продающих сайтов и готовых решений для бизнеса в Коломне и Москве." />

        <?php include_once($_SERVER['DOCUMENT_ROOT']."/mtags.php"); ?>

        <title>Создание лендинг пейдж. Вебстудия Snapix</title>

        <!-- Favicon -->
        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="../../img/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="../../img/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="../../img/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="../../img/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="../../img/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="../../img/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="../../img/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="../../img/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="../../img/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="../../img/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="../../img/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="../../img/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="../../img/favicon/favicon-16x16.png">
        <link rel="manifest" href="/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="../../img/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/parts/css.php"); ?>

        <!-- Current Page CSS -->
        <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css" media="screen">
        <link rel="stylesheet" href="vendor/rs-plugin/css/layers.css" media="screen">
        <link rel="stylesheet" href="vendor/rs-plugin/css/navigation.css" media="screen">
        <link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css" media="screen">
        <!-- Head Libs -->
        <script src="vendor/modernizr/modernizr.min.js"></script>
        <style>
            .tabs.tabs-simple .nav-tabs.small > li a {
                padding: 0px 11px 5px 11px;
                font-size: 14px;
            }
            #howwework {position: relative}
            #howwework p.heading {
                position: absolute;top: -14px;margin-left: 15%;
            }
            .mockup-landing-page {margin-top: -60px !important;}
        </style>
    </head>
    <body>

    <link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
    <script type="text/javascript" src="https://cdn.callbackkiller.com/widget/cbk.js?wcb_code=fc70afff955b43b13d5ae1622fd5e25c" charset="UTF-8" async></script>


    <div class="body">
            <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/parts/header.php'); ?>
        <div role="main" class="main"></div>

        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/parts/footer.php"); ?>
    </div>

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/parts/scripts.php"); ?>
    </body>
</html>