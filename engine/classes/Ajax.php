<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 30.01.2016
 * Time: 17:05
 */

require ($_SERVER['DOCUMENT_ROOT'].'/engine/phpmailer/PHPMailerAutoload.php');

$input = $_POST;
$response = array();

if (isset($input['action'])) {
    switch ($input['action']){
        case "callback":
            $input['heading'] = "Заявка на обратный звонок";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "support_order":
            $input['heading'] = "Заявка на поддержку сайта";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "service":
            $input['heading'] = "Заказ услуги (нижняя форма)";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "landing_callback_headform":
            $input['heading'] = "Заявка на консультацию по лендингам (верхняя форма)";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "landing_order":
            $input['heading'] = "Заказ на лендинг";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "landing_consult":
            $input['heading'] = "Заказ консультации на лендинг";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "shop_order":
            $input['heading'] = "Заказ интернет-магазина";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "shop_consult_sidebar":
            $input['heading'] = "Заказ консультации по интернет-магазину";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "orderShop1CSyncModal":
            $input['heading'] = "Заказ синхронизации 1С с интернет-магазином";
            $input['message'] = $input['data'];
            $input['result'] = sendMail($input);
            $response = $input;
            break;
        case "remont":
            $input['heading'] = "Вызов специалиста по ремонту компов";
            $input['message'] = $input['data'];
            $input['to'] = "repair@snapix.ru";
            $input['result'] = sendMail($input);

            $response = $input;
            break;
        default:
            $response['message'] = "не отправлено сообщение.";
            $response['result'] = "error";
            break;
    }

    print json_encode($response);
}


// set up mailer

function sendMail($data){
//Create a new PHPMailer instance
    $mail = new PHPMailer;
    $mail->CharSet="utf-8";
//Set who the message is to be sent from
    $mail->setFrom('robot@snapix.ru');
//Set an alternative reply-to address
    $mail->addReplyTo('mail@snapix.ru', 'Snapix');
//Set who the message is to be sent to
    if (isset($data['to'])) {
        $mail->addAddress('repair@snapix.ru', 'Snapix');
    } else {
        $mail->addAddress('mail@snapix.ru', 'Snapix');
    }
    $mail->addAddress('steamrus89@mail.ru', 'Snapix');

//Set the subject line
    if (isset($data['heading'])){$mail->Subject = $data['heading'];} else {
        $mail->Subject = 'Новая заявка!';
    }

//Read an HTML message body from an external file, convert referenced images to embedded,
//convert HTML into a basic plain-text alternative body
    $mail->msgHTML($data['message']);
//Replace the plain text body with one created manually
//$mail->AltBody = 'This is a plain-text message body';

    if(!$mail->send()) {
        return 'Message could not be sent.'.$mail->ErrorInfo;
    } else {
        return 'ok';
    }
}