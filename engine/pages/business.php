<!DOCTYPE html>
<html>
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta name="google-site-verification" content="roI4WhDGEBeE6qTZ8QqUF1LCAb2Ck_xvxCopHxZjl3c" />
    <meta name="keywords" content="вебстудия готовые решения разработка создать сайт цена создание для бизнеса лендинг визитка продающий одностраничник интернет магазин"/>
    <meta name="description" content="Готовые решения для бизнеса. Разработка готовых решений в Коломне и Москве." />

    <title>Готовые решения для бизнеса. Разработка готовых решений. Вебстудия Snapix</title>

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <!-- Vendor CSS -->
    <link rel="stylesheet" href="vendor/bootstrap/bootstrap.css">
    <link rel="stylesheet" href="vendor/fontawesome/css/font-awesome.css">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.carousel.css" media="screen">
    <link rel="stylesheet" href="vendor/owlcarousel/owl.theme.css" media="screen">
    <link rel="stylesheet" href="vendor/magnific-popup/magnific-popup.css" media="screen">

    <!-- Theme CSS -->
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/theme-elements.css">
    <link rel="stylesheet" href="css/theme-blog.css">
    <link rel="stylesheet" href="css/theme-shop.css">
    <link rel="stylesheet" href="css/theme-animate.css">

    <!-- Current Page CSS -->
    <link rel="stylesheet" href="vendor/rs-plugin/css/settings.css" media="screen">
    <link rel="stylesheet" href="vendor/circle-flip-slideshow/css/component.css" media="screen">

    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- Skin CSS -->
    <link rel="stylesheet" href="css/skins/default.css">

    <!-- Theme Custom CSS -->
    <link rel="stylesheet" href="css/custom.css">

    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.js"></script>

    <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css">
    <![endif]-->

    <!--[if lte IE 8]>
    <script src="vendor/respond/respond.js"></script>
    <script src="vendor/excanvas/excanvas.js"></script>
    <![endif]-->

</head>
<body>

<div class="body">
<?php include_once("header.php"); ?>
<div role="main" class="main">
    <section class="page-top">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="index.php">Главная</a></li>
                        <li class="active">Бизнесу</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="text-head">Готовые решения для бизнеса</div>
                </div>
            </div>
        </div>
    </section>

    <div class="container">
        <div class="col-md-12 center">
            <h2 class="push-top" style="font-size: 5em; font-weight: bold">MasterFood</h2>
            <h2 class="appear-animation fadeInDown appear-animation-visible">готовая система ведения бизнеса в сфере доставки питания.</h2>
        </div>
        <div class="row">
            <div class="col-md-6 text-right">
                 <p>Если Вы владелец пиццерии, кафе или любого другого заведения питания, то Вы должны понимать,
                что без автоматизации бизнеса Вы в среднем теряете до 60% потенциальных клиентов, если не имеете в
                арсенале качественной системы по представлению Вас в интернете.</p>
                <p>Но разработка качественной системы под ключ - долгий, сложный и дорогой процесс, а главное -
                никто не гарантирует реального результата от вложенных в проект средств. В итоге ниша
                    интернет-деятельньности остается незаполненной, а Вы теряете клиентов и прибыль.</p>
            </div>
            <div class="col-md-6">
                <img class="img-responsive img-thumbnail" src="img/products/sushi.jpg" alt="MasterFood">
            </div>
    </div>
    <hr class="small clearfix">
    <div class="container">

        <div class="col-md-10 col-md-offset-1 center">
            <p class="featured center">К счастью, есть <strong class="alternative-font">MasterFood</strong> - полнофункциональная система автоматизации, включающая в себя всё
                необходимое для ведения бизнеса в интернете - веб-сайт, мобильную версию, онлайн-заказы, административную панель,
                автоматическое продвижение в итернете, ведение статистики и многое другое!</p>
            <a href="http://pizzademo.snapix.ru" target="_blank" class="btn btn-lg btn-primary appear-animation bounceIn appear-animation-visible" data-appear-animation="bounceIn">Демонстрация системы</a>
        </div>
    </div>
    <hr class="tall">
    <div class="container">
        <div class="col-md-12 center">
            <h2 class="appear-animation pull-left fadeInDown appear-animation-visible"><strong>АвтоКат</strong> - специализированный модуль каталога товаров для сайта автомагазина.</h2>
        </div>
    </div>
    <div class="container">

            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="feature-box secundary">
                                <div class="feature-box-icon">
                                    <i class="fa fa-group"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="shorter">Бесконечные категории</h4>
                                    <p class="tall">В каталоге может быть неограниченное число категорий любой глубины
                                        вложенности. Каким бы ни был каталог - для всего хватит места.</p>
                                </div>
                            </div>
                            <div class="feature-box secundary">
                                <div class="feature-box-icon">
                                    <i class="fa fa-file"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="shorter">Гибкие товары</h4>
                                    <p class="tall">Описания, артикулы, наличие на складе, возможности доставки -
                                        все данные о товарах легко настраиваются.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-box secundary">
                                <div class="feature-box-icon">
                                    <i class="fa fa-film"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="shorter">SEO-продвижение в комплекте</h4>
                                    <p class="tall">Система использует последние тренды в продвижении сайтов, позволяя Вам
                                    не беспокоиться о своих позициях в интернете. Товары и категории продвигаются и появляются в
                                    интернете автоматически.</p>
                                </div>
                            </div>
                            <div class="feature-box secundary">
                                <div class="feature-box-icon">
                                    <i class="fa fa-check"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="shorter">Для любого сайта</h4>
                                    <p class="tall">Вам <span class="alternative-font">не нужно</span> переделывать
                                    свой сайт для того, чтобы подключить каталог. Мы встроим его в любой дизайн и настроим
                                    под Ваши нужды.</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="feature-box secundary">
                                <div class="feature-box-icon">
                                    <i class="fa fa-bars"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="shorter">Понятная панель управления</h4>
                                    <p class="tall">Минимум настроек. Максимум пользы. Занимайтесь только теми делами,
                                    которые действительно необходимы - товары, заказы и прибыль. Остальное - забота самой системы.</p>
                                </div>
                            </div>
                            <div class="feature-box secundary">
                                <div class="feature-box-icon">
                                    <i class="fa fa-desktop"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="shorter">Поддержка и обновления</h4>
                                    <p class="tall">Круглосуточная поддержка по электронной почте и телефону.
                                        Система постоянно развивается и поддерживается командой
                                        высококвалифицированных специалистов. </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        <div class="col-md-8">
            <h2><strong>Отзывы</strong> клиентов</h2>
            <div class="row">
                <div class="owl-carousel push-bottom" data-plugin-options='{"items": 1, "autoHeight": true}'>
                                <div>
                                    <div class="col-md-12">
                                        <blockquote class="testimonial">
                                            <p>После того, как к сайту прицепили каталог и пришлось потратить много
                                                времени на его заполнение сайт резко стал выходить на первые строчки
                                                в поисковиках. Пошли заказы, работы существенно прибавилось. В общем в
                                            своем городе я теперь почти как монопилист - другие опоздали, видимо, с
                                            сайтом.</p>
                                        </blockquote>
                                        <div class="testimonial-arrow-down"></div>
                                        <div class="testimonial-author">
                                            <div class="img-thumbnail img-thumbnail-small">
                                                <img src="img/clients/client-1.jpg" alt="">
                                            </div>
                                            <p><strong>Алексей</strong><span>Корея-Авто</span></p>
                                        </div>
                                    </div>
                                </div>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <h2>Как <strong>заказать</strong> этот модуль?</h2>
            <p>Позвоните нам по телефону <strong>8 (496) 612-23-35</strong> и наш менеджер подробно расскажет Вам о
                всех деталях установки и использования этой системы.</p>
        </div>

    </div>



</div>
<?php include_once("footer.php"); ?>
</div>

<!-- Vendor -->
<script src="vendor/jquery/jquery.js"></script>
<script src="vendor/jquery.appear/jquery.appear.js"></script>
<script src="vendor/jquery.easing/jquery.easing.js"></script>
<script src="vendor/jquery-cookie/jquery-cookie.js"></script>
<script src="vendor/bootstrap/bootstrap.js"></script>
<script src="vendor/common/common.js"></script>
<script src="vendor/jquery.validation/jquery.validation.js"></script>
<script src="vendor/jquery.stellar/jquery.stellar.js"></script>
<script src="vendor/jquery.easy-pie-chart/jquery.easy-pie-chart.js"></script>
<script src="vendor/jquery.gmap/jquery.gmap.js"></script>
<script src="vendor/twitterjs/twitter.js"></script>
<script src="vendor/isotope/jquery.isotope.js"></script>
<script src="vendor/owlcarousel/owl.carousel.js"></script>
<script src="vendor/jflickrfeed/jflickrfeed.js"></script>
<script src="vendor/magnific-popup/jquery.magnific-popup.js"></script>
<script src="vendor/vide/jquery.vide.js"></script>

<!-- Theme Base, Components and Settings -->
<script src="js/theme.js"></script>

<!-- Specific Page Vendor and Views -->
<script src="vendor/rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script src="vendor/rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="vendor/circle-flip-slideshow/js/jquery.flipshow.js"></script>
<script src="js/views/view.home.js"></script>

<!-- Theme Custom -->
<script src="js/custom.js"></script>
<script src="js/views/view.contact.js"></script>
<!-- Theme Initialization Files -->
<script src="js/theme.init.js"></script>

</body>
</html>