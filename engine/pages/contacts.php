        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li class="active">Контакты</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section class="mt-lg">
            <div class="container">
                <div class="row">
                <div class="col-md-7">
                    <h2>Snapix - <strong>Контакты</strong></h2>
                    <p class="lead">Маркетинговое агентство Snapix занимается комплексным развитием
                    бизнеса в интернете, разработкой веб-сайтов, интернет-магазинов, продвижением, рекламой и IT-аутсорсингом.</p>

                    <p>Наша команда состоит из маркетологов, специалистов по продвижению сайтов и контекстной рекламе,
                        программистов и дизайнеров. И мы предлагаем многое из тех инструментов развития бизнеса, о
                        чем другие даже не подозревают!</p>

                    <div class="row mt-xlg mb-xl">
                        <div class="col-md-6">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-star"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="heading-primary mb-xs">7 лет на рынке</h4>
                                    <p>Мы работаем для Вас уже много лет. За это время мы
                                        постоянно развивались и улучшали качество услуг. Десятки клиентов по всей России
                                        и сотни проектов!</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="feature-box feature-box-style-2">
                                <div class="feature-box-icon">
                                    <i class="fa fa-heart"></i>
                                </div>
                                <div class="feature-box-info">
                                    <h4 class="heading-primary mb-xs">Кратко о команде</h4>
                                    <p>Мы молоды, креативны, доброжелательны и всегда стараемся дать больше,
                                    чем написано в техзадании или договоре.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <h4>Мы есть <a target="_blank" href="https://vk.com/snapix"><i class="fa fa-vk"></i> ВКонтакте </a> - добавляйтесь в <strong>друзья</strong>! </h4>

                    </div>
                </div>
                <div class="col-md-5" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name" style="display: none;">Snapix</span>
                    <h4>Мы на <strong>карте</strong></h4>

                    <div id="map" style="height: 350px">
                        <script type="text/javascript" charset="utf-8" async src="https://api-maps.yandex.ru/services/constructor/1.0/js/?sid=4r5HOmDVdEgsteU64w1RBBhIjdKxlsPJ&amp;width=100%25&amp;height=350&amp;lang=ru_RU&amp;sourceType=constructor&amp;scroll=true"></script>
                    </div>


                    <ul class="list list-icons list-icons-style-3 mt-xlg">
                        <li  class="adr"><i class="fa fa-map-marker"></i> <strong>Офис:</strong> <span class="adr" itemprop="streetAddress">г. Москва, ул. Ферганская, д. 2</span></li>
                        <li  class="adr"><i class="fa fa-map-marker"></i> <strong>Офис:</strong> <span class="adr" itemprop="streetAddress"><?php App::address(); ?></span></li>
                        <li class="tel"><i class="fa fa-phone"></i> <strong>Телефон:</strong> <span class="tel" itemprop="tel"><?php App::phone(); ?></span></li>
                        <li class="email"><i class="fa fa-envelope"></i> <strong>Email:</strong> <a href="mailto:mail@snapix.ru" class="email" itemprop="email">mail@snapix.ru</a></li>
                    </ul>

                    <hr>

                    <h4>Часы работы <strong>офиса</strong></h4>
                    <ul class="list list-icons list-dark mt-xlg">
                        <li><i class="fa fa-clock-o"></i> <span class="workhours" itemprop="workhours">Понедельник - пятница - с 10:00 до 18:00</span></li>
                        <li><i class="fa fa-clock-o"></i> <span class="workhours" itemprop="workhours">Суббота - с 11:00 до 14:00</span></li>
                        <li><i class="fa fa-clock-o"></i> <span class="workhours" itemprop="workhours">Воскресенье - Выходной</span></li>
                    </ul>
                </div>
            </div>
            </div>
        </section>
