<?php
/**
 * Created by PhpStorm.
 * User: Alex
 * Date: 11.03.2015
 * Time: 14:38
 */ ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="/favicon.ico">

    <title>Заказать директ</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link href="vendor/bootstrap/bootstrap.css" rel="stylesheet">
    <link href="css/direct.css" rel="stylesheet">


    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>

<body>

<div class="container">

<header>
    <div class="row">
        <div class="col-md-4">
            <h3 class="text-muted">Привлечение
                клиентов по всей
                России и СНГ
                c 2010 года.</h3>
        </div>
        <div class="col-md-4"></div>
        <div class="col-md-4 text-right">
            <p class="heading"> 8-916-190-51-08</p>
            <p class="subheading"> Skype: AlexSteam89</p>
        </div>
    </div>
</header>

<!-- Jumbotron -->
<div class="jumbotron">
    <h1 class=" text-center">Профессиональная настройка <strong>Яндекс.Директ</strong>
        с гарантией результата за 48 часов.</h1>
    <hr style="border: none; border-top: white solid 1px">
    <p class="lead  text-center"> 100% ГАРАНТИЯ ПРИВЛЕЧЕНИЯ КЛИЕНТОВ ИЛИ ВОЗВРАТ ДЕНЕГ!</p>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p><strong>Настройка Яндекс Директ:</strong></p>
                <ol type="1">
                    <li>Увеличение Ваших продаж.</li>
                    <li>Снижение стоимости одной продажи.</li>
                    <li>Получение оптимального соотношения Цена / Качество.</li>
                    <li>Оптимизирование расходов на контекстную рекламу.</li>
                    <li>Старт в любой нише с любым предложением.</li>
                </ol>
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-4">
                <div>Начните получать от 2 заявок в день с помощью профессионально настроенной контекстной рекламы</div>

                <div class="form-group">
                    <label>Введите Ваше имя</label>
                    <input type="text" class="form-control" placeholder="Ваше имя">
                </div>
                <button class="btn btn-primary btn-block">Оставить заявку</button>
            </div>
        </div>
    </div>
    <p class="text-center push-top">Тест кампании: без предоплаты, запуск 1-3 дня, цена клика 3-7 рублей.</p>
</div>

<div class="container" id="features">
    <div class="row text-center">
        <p><strong class="yaback heading"> ИСПЫТЫВАЕТЕ ПРОБЛЕМЫ С КОНТЕКСТНОЙ РЕКЛАМОЙ?</strong></p>
        <div class="col-md-4 item"><p>Кампания с низким CTR- как следствие дорогие клики</p></div>
        <div class="col-md-4 item"><p>Мало ключевых слов, или использованы только дорогие ключи</p></div>
        <div class="col-md-4 item"><p>Исполнитель не обслуживает компанию - поэтому её бюджет увеличивается.</p></div>

        <div class="col-md-4 item"><p>Не можете разобраться, как работает контекстная реклама</p></div>
        <div class="col-md-4 item"><p>Большие расходы на рекламу, не дают продаж.</p></div>
        <div class="col-md-4 item"><p>С вашим бюджетом не хотят работать агенства.</p></div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p class="text-center"><strong class="yaback heading"> КАК МЫ ПРИВОДИМ КЛИЕНТОВ НА ВАШ САЙТ</strong></p>

        <div class="col-md-8" id="grid">
            <div class="item">
                <div class="col-md-4">Сейчас <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Вы оставляете заявку бесплатно</div>
            </div>

            <div class="item">
                <div class="col-md-4">Через 2 часа 	 <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Мы оцениваем бюджет проводим анализ</div>
            </div>

            <div class="item">
                <div class="col-md-4">Через 6 часов <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Показываем Вам ключевые слова</div>
            </div>

            <div class="item">
                <div class="col-md-4">Через 7 часов <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Бесплатно устанавливаем Яндекс Метрику на ваш сайт</div>
            </div>

            <div class="item">
                <div class="col-md-4">Через 8 часов <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Мы создаем и запускаем рекламную компанию </div>
            </div>

            <div class="item">
                <div class="col-md-4">Через 8 часов <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Вы получаете клиентов на сайт </div>
            </div>

            <div class="item">
                <div class="col-md-4">Через 8 часов <i class="fa fa-arrow-down"></i> </div>
                <div class="col-md-8">Оплата работы </div>
            </div>
        </div>

        <div class="col-md-4">
            <p class="subheading">Оставьте заявку сейчас!</p>
            <div>Введите свой номер телефона и получите расчет своей рекламной компании через 2 часа <strong>БЕСПЛАТНО!</strong></div>

            <div class="form-group">
                <label>Введите Ваш телефон</label>
                <input type="text" class="form-control" placeholder="Ваш телефон">
            </div>
            <button class="btn btn-primary btn-block">Оставить заявку</button>
            <small>Мы заботимся о вашей конфиденциальности!
                Ваши данные НЕ будут переданы третьим лицам!</small>
        </div>
    </div>
</div>

<div class="container" id="whatisincluded">
    <div class="row">
        <p class="heading text-center">ЧТО ВХОДИТ В НАСТРОЙКУ РЕКЛАМНОЙ КАМПАНИИ</p>
        <div class="col-md-2"></div>
        <div class="col-md-5">
            <ul class="list-unstyled">
                <li><i class="fa fa-check"></i> Настройка кампании на поиске</li>
                <li><i class="fa fa-check"></i> Настройка кампании РСЯ</li>
                <li><i class="fa fa-check"></i> Настройка отдельных кампаний для регионов</li>
                <li><i class="fa fa-check"></i> Настройка сервиса "Яндекс.Метрика"</li>
                <li><i class="fa fa-check"></i> Яндекс-визитка</li>
                <li><i class="fa fa-check"></i> Быстрые ссылки</li>
            </ul>
        </div>
        <div class="col-md-4">
            <ul class="list-unstyled">
                <li><i class="fa fa-check"></i> Ретаргетинг</li>
                <li><i class="fa fa-check"></i> UTM-метки</li>
                <li><i class="fa fa-check"></i> Аудит сайта</li>
                <li><i class="fa fa-check"></i> Доступ к аккаунту</li>
                <li><i class="fa fa-check"></i> Отчет по ходу работы</li>
                <li><i class="fa fa-check"></i> Телефонная поддержка</li>
            </ul>
        </div>
    </div>
</div>

<div class="container push-top" id="whatisincluded">
    <div class="row">
        <p class="subheading text-center">10 причин, почему стоит заказать у нас настройку Яндекс Директ</p>
        <p class="small-subheading text-center">Почему мы даем 100% гарантию привлечения Вашей целевой аудитории</p>

        <div class="push-top col-md-9 col-md-offset-1">
            <ol>
                <li>До 72 часов на настройку и запуск рекламной кампании.</li>
                <li>Возможность ведения кампании на вашем аккаунте.</li>
                <li>У Вас есть полный доступ к любой статистике кампании.</li>
                Даже к ключевым фразам и минус словам!
                <li>24 часа / 7 дней в неделю - бесплатная поддержка.</li>
                <li>100% гарантия увеличения CTR.</li>
                <li>Анализ Яндекс.Метрики на сайте.</li>
                <li>Ручной подбор ключевиков.
                    Только профессионал может в полной мере оценить, подходит то или иное ключевое слово для сайта, или нет.</li>
                <li>Для интернет магазинов мы делаем ссылки на КАЖДЫЙ товар!
                    Если у вас несколько товаров - На конкретный товар создаём отдельные объявления
                    с ссылкой на этот товар!</li>
                <li>Установка UTM-меток
                    С помощью этих меток Мы точно определяем, с каких объявлений клиент перешел и купил.
                    С помощью них Мы тестируем заголовки и тексты объявлений.</li>
                <li> Создаем изображения для кампании в Рекламной Сети Яндекса</li>
                <li>Создаём отдельные рекламные кампании под РСЯ и уникальные продающие изображения</li>
            </ol>
        </div>
    </div>
</div>

<div class="container">
    <div class="row">
        <p class="heading text-center">ПРИМЕРЫ НАШИХ РЕКЛАМНЫХ КАМПАНИЙ</p>
    </div>
</div>

<div class="container">
    <div class="row">
        <p class="heading text-center">ОТЗЫВЫ</p>
    </div>
</div>

<div class="container" id="prices">
    <div class="row">
        <p class="heading text-center">Стоимость настройки Яндекс Директа</p>
        <table class="table table-responsive table-bordered">
            <thead>
            <th>Что входит?</th>
            <th>ТЕСТ</th>
            <th>СТАНДАРТ</th>
            <th>ЛУЧШИЙ</th>
            <th>VIP</th>
            </thead>
            <tbody>
            <tr>
                <td>Количество объявлений</td>
                <td>до 200</td>
                <td>до 500</td>
                <td>1000</td>
                <td>от 1500 и более</td>
            </tr>
            <tr>
                <td>Время создания</td>
                <td>3 часа</td>
                <td>2 дня</td>
                <td>4 дня</td>
                <td>от 1 недели</td>
            </tr>
            <tr>
                <td>Ведение компании</td>
                <td>Нет</td>
                <td>1 неделя</td>
                <td>2 недели</td>
                <td>1 месяц</td>
            </tr>
            <tr>
                <td>Аудит сайта</td>
                <td>Нет</td>
                <td>Нет</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            <tr>
                <td>Яндекс.Визитка</td>
                <td>Нет</td>
                <td>По требованию</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            <tr>
                <td>UTM-метки</td>
                <td>Нет</td>
                <td>По требованию</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            <tr>
                <td>Быстрые ссылки</td>
                <td>Нет</td>
                <td>По требованию</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            <tr>
                <td>РСЯ</td>
                <td>Нет</td>
                <td>Да</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            <tr>
                <td>Ретаргетинг</td>
                <td>Нет</td>
                <td>Да</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            <tr>
                <td><strong>Яндекс.Метрика</strong> <br><small>Установка счетчика на сайт</small></td>
                <td>Нет</td>
                <td>Да</td>
                <td>Да</td>
                <td>Да</td>
            </tr>
            </tbody>
            <tfoot>

            </tfoot>
        </table>
    </div>
</div>
</div> <!-- /container -->

<!-- Site footer -->
<footer class="footer">
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <p>&copy; ИП Барышев А.Н. <br>
                    502240000009455 <br>
                    ОГРН:302556647889</p>
            </div>
            <div class="col-md-4 text-center">
                <a href="#policy">Политика конфиденциальности</a>
            </div>
            <div class="col-md-4 text-right">
                <div>8 (916) 190-51-08</div>
                <div>Skype: AlexSteam89</div>
            </div>
        </div>
    </div>
</footer>


<script src="vendor/bootstrap/bootstrap.js"></script>
</body>
</html>
