<div class="slider-container rev_slider_wrapper" style="height: 500px;">
    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"gridwidth": 960, "gridheight": 800}'>
        <ul>
            <li data-transition="fade">
                <img src="/img/backgrounds/mount_compressed.jpg"
                     alt="разработка сайтов"
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat"
                     class="rev-slidebg">
                <div class="rs-background-video-layer hidden-xs"
                     data-forcerewind="on"
                     data-volume="mute"
                     data-videowidth="100%"
                     data-videoheight="100%"
                     data-videomp4="/video/firework.mp4"
                     data-videopreload="preload"
                     data-videoloop="none"
                     data-forceCover="1"
                     data-aspectratio="16:9"
                     data-autoplay="true"
                     data-autoplayonlyfirsttime="false"
                     data-nextslideatend="true"
                ></div>
                <div class="tp-caption"
                     data-x="center" data-hoffset="-160"
                     data-y="center" data-voffset="-85"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="создание сайтов"></div>
                <div class="tp-caption top-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-85"
                     data-start="500"
                     style="z-index: 5"
                     data-transform_in="y:[-300%];opacity:0;s:500;">Мы - Snapix. И мы</div>
                <div class="tp-caption"
                     data-x="center" data-hoffset="160"
                     data-y="center" data-voffset="-85"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="продвижение сайтов"></div>
                <div class="tp-caption main-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-10"
                     data-start="1500"
                     data-whitespace="nowrap"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;"
                     style="z-index: 5"
                     data-mask_in="x:0px;y:0px;">УВЕЛИЧИВАЕМ ПРОДАЖИ</div>
                <div class="tp-caption bottom-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="60"
                     data-start="2000"
                     style="z-index: 5"
                     data-transform_in="y:[100%];opacity:0;s:500;">минимум в 3 раза с помощью интернета</div>
                <div style="opacity: 0.7"
                     class="tp-dottedoverlay tp-opacity-overlay visible-xs"></div>
            </li>
            <li data-transition="fade">
                <img src="img/backgrounds/rocket.jpg"
                     alt="разработка сайтов"
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat"
                     class="rev-slidebg">
                <div class="tp-caption"
                     data-x="center" data-hoffset="-150"
                     data-y="center" data-voffset="-85"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="реклама сайта"></div>
                <div class="tp-caption top-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-85"
                     data-start="500"
                     style="z-index: 5"
                     data-transform_in="y:[-300%];opacity:0;s:500;">профессиональная</div>
                <div class="tp-caption"
                     data-x="center" data-hoffset="150"
                     data-y="center" data-voffset="-85"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="создание интернет магазина"></div>
                <div class="tp-caption main-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-10"
                     data-start="1500"
                     data-whitespace="nowrap"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;"
                     style="z-index: 5"
                     data-mask_in="x:0px;y:0px;">РАЗРАБОТКА LANDING PAGE</div>
                <a class="tp-caption btn btn-lg btn-primary btn-slider-action"
                   data-hash
                   data-hash-offset="85"
                   href="/services/landing"
                   data-x="center" data-hoffset="0"
                   data-y="center" data-voffset="90"
                   data-start="2200"
                   data-whitespace="nowrap"
                   data-transform_in="y:[100%];s:500;"
                   data-transform_out="opacity:0;s:500;"
                   style="z-index: 5"
                   data-mask_in="x:0px;y:0px;">Узнать подробнее</a>
                <div style="opacity: 0.7"
                     class="tp-dottedoverlay tp-opacity-overlay"></div>
            </li>
        </ul>
    </div>
</div>

<section id="home-promo">
    <div class="container">
        <div class="row">
            <div class="col-md-12 center">
                <h2 class="word-rotator-title mb-sm mt-xlg"><strong>Самые
                                    <span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
                                        <span class="word-rotate-items">
                                            <span>работающие</span>
                                            <span>качественные</span>
                                            <span>инновационные</span>
                                        </span>
							        </span>
                    </strong> услуги в сфере развития бизнеса в Интернете.</h2>
                <p class="lead">С 2009 года мы предоставляем услуги по разработке и продвижению сайтов,
                    комплексному развитию бизнеса в интернете и техническому сопровождению.</p>
            </div>
        </div>
        <div class="row mt-xl">
            <div class="counters counters-text-dark">
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
                        <i class="fa fa-user"></i>
                        <strong data-to="90" data-append="+">0</strong>
                        <label>Довольных клиентов</label>
                        <p class="text-color-primary mb-xl">Они сделали верный выбор.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
                        <i class="fa fa-desktop"></i>
                        <strong data-to="250" data-append="+">0</strong>
                        <label>Выполненных проектов</label>
                        <p class="text-color-primary mb-xl">И будет еще!</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
                        <i class="fa fa-ticket"></i>
                        <strong data-to="326" data-append="%">0</strong>
                        <label>Повышение продаж</label>
                        <p class="text-color-primary mb-xl">Да, так бывает.</p>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6">
                    <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
                        <i class="fa fa-clock-o"></i>
                        <strong data-to="4000" data-append="+">0</strong>
                        <label>Привлекли лидов</label>
                        <p class="text-color-primary mb-xl">Для наших друзей - самых горячих!</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

<section id="home-works">
    <div class="container">
        <div class="row mt-xl">
            <div class="col-md-12 center">
                <p class="h1 mt-sm mb-none">Некоторые <strong>наши работы</strong></p>
                <div class="divider divider-primary divider-small divider-small-center mb-xl">
                    <hr>
                </div>
            </div>
        </div>

        <div class="slick-slider dots">
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <img src="/img/testimonials/palla.png" class="img-responsive" alt="">
                </div>
                <div class="col-md-6 col-sm-6">
                    <span class="badge">Комплексное сопровождение</span>
                    <p class="h1 mb-none strong">Palla</p>
                    <noindex><a href="//palla.su" rel="nofollow">palla.su</a></noindex>

                    <p>Интернет-магазин оптового производителя женской одежды</p>
                    <p class="mb-none">Было сделано:</p>
                    <ul class="mt-none">
                        <li>Вывод всех оптовых фраз-ключей в топ-10</li>
                        <li>Улучшение видимости в Поисковых системах</li>
                        <li>Продвижение по РФ</li>
                    </ul>

                    <p class="mb-none">Результат:</p>
                    <ul class="mt-none">
                        <li>До нас - Было 3-5 заявок в сутки</li>
                        <li>С нами - Стало 10-15 заявок в сутки</li>
                        <li>Увеличение оборота компании с 450 тыс. до 1,2 млн. рублей</li>
                    </ul>

                    <a class="btn btn-primary" href="/img/testimonials/palla.jpg" data-fancybox data-caption="Отзыв о работе компании Palla">
                       Отзыв о работе
                    </a>

                    <a class="btn btn-primary" href="/img/testimonials/palla-2.jpg" data-fancybox data-caption="Результаты работы компании Palla">
                        Результаты работы
                    </a>

                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <img src="/img/testimonials/osago52.jpg" class="img-responsive" alt="">
                </div>
                <div class="col-md-6 col-sm-6">
                    <span class="badge">Настройка Яндекс.Директ</span>
                    <p class="h1 mb-none strong">1-й Центр Страхования</p>
                    <noindex><a href="http://osago52.ru" rel="nofollow">osago52.ru</a></noindex>

                    <p>Портал о страховании в Нижнем Новгороде.</p>
                    <p class="mb-none">Было сделано:</p>
                    <ul class="mt-none">
                        <li>Сбор семантического ядра</li>
                        <li>Написание продающих объявлений</li>
                        <li>Ведение рекламных кампаний</li>
                    </ul>

                    <p class="mb-none">Результат:</p>
                    <ul class="mt-none">
                        <li>Входящие обращения (лиды): 550 лидов.</li>
                        <li>Стоимость лида: 72 рубля.</li>
                        <li>Заработано (прибыль): 1 500 000 рублей.</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <img src="/img/testimonials/elfa.jpg" class="img-responsive" alt="">
                </div>
                <div class="col-md-6 col-sm-6">
                    <span class="badge">Разработка интернет-магазина</span>
                    <p class="h1 mb-none strong">Elfa-Room</p>
                    <noindex><a href="//elfa-room.ru" rel="nofollow">elfa-room.ru</a></noindex>

                    <p>Интернет-магазин шведских модульных гардеробных комнат.</p>
                    <p class="mb-none">Было сделано:</p>
                    <ul class="mt-none">
                        <li>Программирование</li>
                        <li>Дизайн и верстка</li>
                        <li>Подключение систем оплаты</li>
                        <li>Подключение и ведение аналитики</li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 col-sm-6">
                    <img src="/img/testimonials/8burgers.jpg" class="img-responsive" alt="">
                </div>
                <div class="col-md-6 col-sm-6">
                    <span class="badge">Разработка сайта</span>
                    <p class="h1 mb-none strong">8Burgers</p>
                    <noindex><a href="//8Burgers.ru" rel="nofollow">8Burgers.ru</a></noindex>

                    <p>Разработка сайта сервиса по доставке бургеров.</p>
                    <p class="mb-none">Было сделано:</p>
                    <ul class="mt-none">
                        <li>Программирование</li>
                        <li>Дизайн и верстка</li>
                        <li>Административная часть для менеджеров компании</li>
                        <li>Подключение систем рассылки смс, аналитики и интеграция с соц. сетями</li>
                    </ul>
                </div>
            </div>
        </div>

    </div>

    <div class="text-center">
        <a href="/works" class="btn btn-primary btn-lg mt-xlg">Посмотреть все работы</a>
    </div>
</section>


<section id="home-services">

    <div class="container">
        <div class="row mt-xl">
            <div class="col-md-12 center">
                <p class="h1 mt-sm mb-none">Наши <strong>Услуги</strong></p>
                <div class="divider divider-primary divider-small divider-small-center mb-xl">
                    <hr>
                </div>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <a href="/landing" class="item" style="background-image: url('/img/backgrounds/rocket.jpg')">
                    <span class="gradient"></span>
                    <span class="border"></span>
                    <span class="title">Создание высококонверсионных Landing Page
                        <span class="price">от 24 999 рублей</span>
                        <button class="btn">Подробнее</button>
                    </span>

                </a>
            </div>
            <div class="col-md-6">
                <a href="/shop" class="item" style="background-image: url('/img/backgrounds/ecommerce_compressed.jpg')">
                    <span class="gradient"></span>
                    <span class="border"></span>
                    <span class="title">Создание интернет-магазинов
                        <span class="price">от 32 999 рублей</span>
                        <button class="btn">Подробнее</button>
                    </span>
                </a>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <a href="/seo" class="item" style="background-image: url('/img/seo_2.png')">
                    <span class="gradient"></span>
                    <span class="border"></span>
                    <span class="title">SEO-продвижение сайтов
                        <span class="price">от 14 999 рублей</span>
                        <button class="btn">Подробнее</button>
                    </span>
                </a>
            </div>
            <div class="col-md-6">
                <a href="/context" class="item" style="background-image: url('/img/backgrounds/context_compressed.jpg')">
                    <span class="gradient"></span>
                    <span class="border"></span>
                    <span class="title">Настройка Яндекс.Директ и Google.AdWords
                        <span class="price">от 14 999 рублей</span>
                        <button class="btn">Подробнее</button>
                    </span>
                </a>
            </div>
            <div class="col-md-6">
                <a href="/support" class="item" style="background-image: url('/img/services/development.png')">
                    <span class="gradient"></span>
                    <span class="border"></span>
                    <span class="title">Техническая поддержка и сопровождение сайтов
                        <span class="price">от 4999 рублей</span>
                        <button class="btn">Подробнее</button>
                    </span>
                </a>
            </div>
            <div class="col-md-6">
                <a href="/#" class="item" style="background-image: url('/img/services/support.png')">
                    <span class="gradient"></span>
                    <span class="border"></span>
                    <span class="title">Комплексное маркетинговое сопровождение бизнеса
                        <span class="price">Индивидуально</span>
                        <button class="btn">Подробнее</button>
                    </span>
                </a>
            </div>
        </div>
    </div>
</section>

<section class="call-to-action call-to-action-primary call-to-action-front mb-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action-content align-left pb-md mb-xl ml-none">
                    <h2 class="text-color-light mb-none mt-none">
                        Поднимаем <strong>продажи</strong> Вашего сайта, приводим в бизнес новых <strong>клиентов</strong>,
                        а также возвращаем старых!
                    </h2>
                </div>
                <div class="call-to-action-btn">
                    <button data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary btn-primary-scale-2 mr-md">Хочу этого!</button>
                    <span class="mr-md text-color-light hidden-xs">не откладывай!<span class="arrow arrow-light hlb" style="top: -88px; right: -47px;"></span></span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-default mt-none mb-xl" id="howwework">
    <div class="container">
        <div class="row">
            <p class="h2 mt-none mb-xl text-center">Почему наши сайты так хороши?</p>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="feature-box feature-box-style-2 reverse mb-xl appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="300">
                    <div class="feature-box-icon">
                        <i class="fa fa-bolt"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Современный дизайн</h4>
                        <p class="mb-lg">Используем все современые наработки и приемы в сфере дизайна
                            и следим за новыми тенденциями.</p>
                    </div>
                </div>
                <div class="feature-box feature-box-style-2 reverse mt-xl appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="600">
                    <div class="feature-box-icon">
                        <i class="fa fa-heart"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Ориентиры на клиента</h4>
                        <p class="mb-lg">С трепетом подходим к требованиям наших клиентов. Даем гарантию и особые условия работы.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <img src="img/services/development.png"
                     class="img-responsive mockup-landing-page mb-xl mt-xl appear-animation hidden-xs"
                     alt="Snapix" data-appear-animation="fadeInDown" data-appear-animation-delay="300">
            </div>
            <div class="col-md-4">
                <div class="feature-box feature-box-style-2 mb-xl appear-animation" data-appear-animation="fadeInRight" data-appear-animation-delay="300">
                    <div class="feature-box-icon">
                        <i class="fa fa-puzzle-piece"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Умные технологии</h4>
                        <p class="mb-lg">Делаем свою работу по современным методикам и
                            постоянно внедряем инновации, оставляя конкурентов позади. </p>
                    </div>
                </div>
                <div class="feature-box feature-box-style-2 mt-xl appear-animation" data-appear-animation="fadeInRight" data-appear-animation-delay="600">
                    <div class="feature-box-icon">
                        <i class="fa fa-cloud-download"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Постоянное развитие</h4>
                        <p class="mb-lg">Осуществляем постоянную поддержку и развитие проектов.
                            Оперативно предлагаем свежие решения.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12 center">
            <p class="h2 mt-xl mb-none">Оптимизация под все <strong>мобильные устройства</strong></p>
            <p class="lead mb-xl">63% пользователей посещают интернет с помощью<br> мобильных телефонов или планшетов.</p>
            <hr class="invisible">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6 reverse mb-none mt-xl appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="300">
                <div class="feature-box-icon">
                    <i class="fa fa-mobile text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="mb-sm">100% Адаптивные сайты</h4>
                    <p class="mb-lg">Мы разрабатываем сайты, идеально выглядящие на всех возможных устройствах.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <img alt="Responsive" data-appear-animation="fadeInUp" class="hidden-xs img-responsive appear-animation fadeInUp" src="img/responsive-ready.png" style="margin-bottom: -1px;">
        </div>
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6 mb-none mt-xl appear-animation" data-appear-animation="fadeInRight" data-appear-animation-delay="300">
                <div class="feature-box-icon">
                    <i class="fa fa-eye text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="mb-sm">Дизайн и удобство</h4>
                    <p class="mb-lg">Мы продумываем дизайн и все элементы с учетом мобильных устройств.
                        Сайтом всегда удобно пользоваться.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="section section-default mt-none">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <p class="h2 text-right">Опытная <strong>команда</strong> разработчиков и маркетологов</p>
                <p class="text-right">
                    Мы занимаемся разработкой и продвижением сайтов с 2009 года.
                    С 2012 года мы занимаемся комплексным маркетингом в сфере интернет и развитием бизнеса.
                    Наши сотрудники - профессионалы с многолетним стажем работы, имеющие за плечами
                    учебу в престижных ВУЗах России, сертификаты ведущих на рынке компаний
                    (Яндекс, 1С, Бизнес Молодость) и опыт работы в самых горячих проектах.
                </p>
            </div>
            <div class="col-md-4">
                <img class="hidden-xs img-responsive appear-animation" src="img/services/crew.png" alt="Snapix" data-appear-animation="fadeInRight">
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row mt-none mb-xl">
        <div class="col-md-4 col-md-offset-1">
            <img class="img-responsive mt-xl appear-animation" src="img/services/support.png" alt="поисковое продвижение сайтов" data-appear-animation="fadeInLeft">
        </div>
        <div class="col-md-7">
            <p class="h2 mt-xl">Качественная и компетентная <strong>поддержка</strong></p>
            <p>Наши клиенты всегда могут рассчитывать на поддержку в решении любых проблем с
                их проектами - от простой консультации о работе сайта или сервиса до глобального
                сопровождения всего бизнеса!<br>Мы своих не бросаем. </p>
        </div>
    </div>
</div>
<section class="call-to-action call-to-action-primary mb-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action-content align-left pb-md mb-xl ml-none">
                    <h2 class="text-color-light mb-none mt-xl">Все еще сомневаетесь? <strong>Свяжитесь с нами.</strong></h2>
                    <p class="lead mb-xl">Нам есть, что Вам предложить уже сейчас!</p>
                </div>
                <div class="call-to-action-btn">
                    <button data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary btn-primary-scale-2 mr-md">Связаться!</button>
                    <span class="arrow arrow-light hlb" style="top: -88px; right: -87px;"></span>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="section section-text-light section-center mt-none mb-none"  style="background-image: url(img/slides/slide-bg-medium.jpg); background-repeat: no-repeat; background-position: center center; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs"></i><br><strong>Отзывы о нас</strong></h2>
                <div class="owl-carousel owl-theme nav-bottom rounded-nav mb-none" data-plugin-options='{"items": 1, "loop": false}'>
                    <div>
                        <div class="col-md-12">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                <blockquote>
                                    <p>Выражаем свою благодарность Агентству интернет маркетинга Snapix
                                        за качественную настройку рекламных кампаний Яндекс.Директ. В первый день запуска
                                        получили 40 заявок на свои услуги, при этом потратив 1500 рублей из бюджета.</p>
                                    <p>Надеемся на продолжение успешного сотрудничества, а также на дольейшее увеличение
                                        достигнутых показателей совместной работы.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>ООО Осаго52</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-md-12">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                <blockquote>
                                    <p>Спасибо за быстро и качественно выполненный лендинг!
                                        Меньше, чем за неделю ребята создали отличный продающий сайт, который
                                        уже начал окупаться! Правда думал что цена будет чуть меньше, а потом
                                        посмотрел - по качеству если, то цена даже ниже, чем могла бы быть. Не пожалел.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>Владимир Кудряшов</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-md-12">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                <blockquote>
                                    <p>После того, как к сайту прицепили каталог и пришлось потратить много
                                        времени на его заполнение сайт резко стал выходить на первые строчки
                                        в поисковиках. Пошли заказы, работы существенно прибавилось. В общем в
                                        своем городе я теперь почти как монопилист - другие опоздали, видимо, с
                                        сайтом.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>Алексей, "Корея-Авто"</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="call-to-action call-to-action-default call-to-action-in-footer mt-none no-top-arrow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action-content align-left pb-md mb-xl ml-none">
                    <h2 class="mb-xs mt-xl">Присоединяйтесь к успеху вместе с командой <strong>Snapix!</strong></h2>

                </div>
                <div class="call-to-action-btn">
                    <a data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary popup-with-zoom-anim"><i class="fa fa-cark mr-xs"></i>Начать сотрудничество</a>
                    <p><span class="alternative-font font-size-sm mt-xs text-color-primary">У нас много бонусов для новых клиентов :)</span></p>
                </div>
            </div>
        </div>
    </div>
</section>