        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li class="active">Калькулятор контекста</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Калькулятор контекста</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <div class="col-md-12">
                <div class="featured-box featured-box-primary featured-box-text-left">
                    <div class="box-content">
                        <div class="row">
                            <div class="col-md-4">
                                <h3>Введите значения</h3>

                                <div class="inputdiv">Сколько вы хотите в месяц продаж от рекламы?
                                <input  id="ex123" type="number" min="1" max="1000" data-slider-min="1" data-slider-max="1000" data-slider-step="1" data-slider-value="1" data-slider-id="slider"/>
                                <span id="ex1CurrentSliderValLabel"> <span id="ex1SliderVal"></span></div>

                                 <div class="inputdiv">Какую прибыль вы получаете от одной продажи?<br>
                                <input  id="ex223" min="100" max="1000000" type="number" data-slider-min="100" data-slider-max="1000000" data-slider-step="100" data-slider-value="1000" data-slider-id="slider"/>
                                <span id="ex2CurrentSliderValLabel"> <span id="ex2SliderVal"></span></span><br></div>
                            </div>
                            <div class="col-md-4">
                                <h3>Доп. значения</h3>

                                <div class="inputdiv">Конверсия сайта: <span id="ex3CurrentSliderValLabel"> <span id="ex3SliderVal">1</span></span>%<br>
                                    <input  id="ex3" type="text" data-slider-min="1" data-slider-max="100" data-slider-step="1" data-slider-value="1" data-slider-id="slider"/>
                                    </div>
                                <div class="inputdiv">Средняя цена за клик: <span id="ex4CurrentSliderValLabel"> <span id="ex4SliderVal">3</span></span> руб.<br>
                                    <input  id="ex4" type="text" data-slider-min="3" data-slider-max="2000" data-slider-step="1" data-slider-value="3" data-slider-id="slider"/>
                                </div>
                                <div class="inputdiv">Конверсия оператора: <span id="ex5CurrentSliderValLabel"> <span id="ex5SliderVal">1</span></span>%<br>
                                    <input  id="ex5" type="text" data-slider-min="1" data-slider-max="100" data-slider-step="1" data-slider-value="1" data-slider-id="slider"/>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <h3>Расходы и доходы</h3>
                                <p><strong><span id="pribil">0</span> руб.</strong> - общая прибыль от продаж в месяц</p>
                                <p><strong><span id="vloj">0</span> руб.</strong> - объем вложений в рекламу в месяц</p>
                                <p><strong><span id="pribil_chist">0</span> руб.</strong> - чистая прибыль от рекламы в месяц</p>
                                <h3>ROI</h3>
                                <p><strong><span id="days">0</span> дней</strong> - время, за которое вы вернете средства вложенные в рекламу</p>
                                <p><strong><span id="prozent">0</span> %</strong> - процент возвратности вложений в рекламу</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-4">
                                <button id="calc" class="btn btn-lg btn-secondary mr-xs mb-lg" type="button">Расcчитать</button>
                            </div>
                            <div class="col-md-4">
                                <div class="btn-secondary btn-lg mr-xs mb-lg">
                                    <span>Целевые заявки в месяц: <span id="cz"></span></span><br>
                                    <span>Посетителей сайта в целом: <span id="ps"></span></span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>



