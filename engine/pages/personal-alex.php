
<section id="me">
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <p class="h2 mb-none mt-xlg">WEB-Разработчик</p>
                <p class="h1 mt-none strongest">Александр Гуськов</p>
                <hr>
                <div class="row">
                    <div class="col-md-6">
                        <div class="text-center">
                            <i class="fa fa-graduation-cap fa-3x mb-md"></i>

                            <p class="h5">Бакалавр Информационных технологий, инженер.</p>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <p class="h5">Окончил Рязанский Радиотехнический Университет по специальности "Информационные технологии" в 2013 году</p>
                    </div>
                </div>
                <hr>




                <p class="h5">Опыт активной разработки: более 8 лет</p>
                <p class="h6">Деятельность: Landing-page, корпоративные сайты, порталы, интернет-магазины, сложные проекты, системная интеграция</p>
            </div>
        </div>
    </div>
</section>


<hr>

<section id="technics">
    <div class="container">
        <p class="h2 strongest">Какими технологиями я пользуюсь в своей работе</p>
        <div class="row">
            <?php for ($i=2; $i < count($logos); $i++) { $item = $logos[$i]; ?>
                <div class="col-md-3 text-center">
                    <img class="" height="80" src="/img/personal-alex/logos/<?= $item ?>" alt="">
                </div>
            <?php } ?>

        </div>
    </div>
</section>

<hr>
<section id="offer">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <p class="h4 text-center mt-xlg">На данный момент я открыт предложениям об участии в проектах</p>
                <div class="text-center">
                    <button class="btn btn-default">Связаться со мной</button>
                </div>
            </div>
        </div>
    </div>
</section>