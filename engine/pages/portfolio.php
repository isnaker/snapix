            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="breadcrumb">
                                <li><a href="#">Главная</a></li>
                                <li class="active">Наши работы</li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Наши работы</h1>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <ul class="nav nav-pills sort-source mt-md" data-sort-id="portfolio" data-option-key="filter">
                    <li data-option-value="*" class="active"><a href="#">Все</a></li>
                    <?php foreach ($categories as $item) {?>
                        <li data-option-value=".<?php print $item->id ?>"><a href="#"><?php print $item->name ?></a></li>
                    <?php } ?>
                </ul>

                <hr>

                <div class="row">

                    <ul class="portfolio-list sort-destination" data-sort-id="portfolio">
                        <?php foreach ($projects as $project){
                        if (is_null($project->image)){$project->image = "project.png";} ?>
                            <li class="col-md-4 col-sm-6 col-xs-6 isotope-item <?php print $project->category_id ?>">
                                <div class="portfolio-item">
                                    <a href="/works/<?php echo $project->id; ?>">
                                            <span class="thumb-info">
                                                <span class="thumb-info-wrapper">
                                                    <img src="img/projects/miniatures/<?php echo $project->image ?>" class="img-responsive" alt="">
                                                    <span class="thumb-info-title">
                                                        <span class="thumb-info-inner"><?php echo $project->name ?></span>
                                                        <span class="thumb-info-type"><?= $project->category_name ?></span>
                                                    </span>
                                                    <span class="thumb-info-action">
                                                        <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                                    </span>
                                                </span>
                                            </span>
                                    </a>
                                </div>
                            </li>
                        <?php } ?>
                    </ul>
                </div>

            </div>
