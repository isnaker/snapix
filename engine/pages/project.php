    <div role="main" class="main">
        <section class="page-header">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <ul class="breadcrumb">
                        <li><a href="/">Главная</a></li>
                        <li><a href="/works">Наши работы</a></li>
                        <li class="active"><?php print $project->name; ?></li>
                    </ul>
                </div>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <h1><?php print $project->seo_h1; ?></h1>
                </div>
            </div>
        </div>
    </section>
        <div class="container">
            <div class="row">
            <div class="col-md-12">
                <div class="portfolio-title mt-xl">
                    <div class="row">
                        <div class="portfolio-nav-all col-md-1">
                            <a href="/works" data-tooltip data-original-title="Все работы"><i class="fa fa-th"></i></a>
                        </div>
                        <div class="col-md-10 center">
                            <h2 class="mb-none strong"><?= $project->name; ?></h2>
                        </div>
                    </div>
                </div>
                <hr class="tall">
            </div>
        </div>
            <div class="row">
            <div class="col-md-7">
                <div class="owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10}'>
                    <div>
                        <span class="img-thumbnail">
                            <img alt="" class="img-responsive" src="/img/projects/miniatures/<?php print $project->image; ?>">
                        </span>
                    </div>
                </div>
                <div class="text-center">
                    <a target="_blank" href="http://www.<?php print $project->url; ?>" class="btn btn-primary btn-icon">
                        <i class="fa fa-external-link"></i>Просмотреть в новом окне</a>
                 </div>
            </div>
                <div class="col-md-5">
                    <div class="portfolio-info">
                    <div class="row">
                        <div class="col-md-12 center">
                            <ul>
                                <li>
                                    <a href="#" data-tooltip data-original-title="Like">
                                        <i class="fa fa-heart"></i>
                                        <?php print $project->likes; ?></a>
                                </li>
                                <li>
                                    <i class="fa fa-calendar"></i><?php print $project->deployed; ?>
                                </li>
                                <li class="hidden">
                                    <i class="fa fa-tags"></i>
                                    <a href="#">Brand</a>,
                                    <a href="#">Design</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                    <h4 class="heading-primary">Описание <strong>проекта</strong></h4>
                <p class="mt-xlg"><?php print $project->content; ?></p>
                    <button data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary mt-xlg">Заказать похожий проект</button>
                <span class="arrow hlb appear-animation" data-appear-animation="rotateInUpLeft" data-appear-animation-delay="800"></span>
                    <ul class="portfolio-details">
                    <li>
                        <p><strong>Что мы делали:</strong></p>

                        <ul class="list list-inline list-icons">
                            <li><i class="fa fa-check-circle"></i> Design</li>
                            <li><i class="fa fa-check-circle"></i> HTML/CSS</li>
                            <li><i class="fa fa-check-circle"></i> Javascript</li>
                            <li><i class="fa fa-check-circle"></i> Backend</li>
                        </ul>
                    </li>
                    <li>
                        <p><strong>Клиент:</strong></p>
                        <p><?php print $project->client ?></p>
                    </li>
                </ul>

            </div>
        </div>

            <div class="row text-center">
                <hr>
                <p class="h1 mt-lg mb-lg text-center">Проект в высоком разрешении:</p>
                <img alt="" class="img-responsive img-thumbnail" src="/img/projects/<?php print $project->image; ?>">
            </div>

            <div class="row">
                <div class="col-md-12">
                    <hr class="tall">
                    <h4 class="mb-md text-uppercase">Последние  <strong>работы</strong></h4>
                    <div class="row">
                        <?php

                            $related = $app->db->get('*', 'projects', ' ORDER BY `id` DESC LIMIT 8');
                        ?>
                        <ul class="portfolio-list">
                            <?php foreach ($related as $item){ ?>
                            <li class="col-md-3 col-sm-6 col-xs-12">
                                <div class="portfolio-item">
                                    <a href="/works/<?php echo $item->id; ?>">
                                        <span class="thumb-info">
                                            <span class="thumb-info-wrapper">
                                                <img src="/img/projects/miniatures/<?php echo $item->image; ?>" class="img-responsive" alt="">
                                                <span class="thumb-info-title">
                                                    <span class="thumb-info-inner"><?php echo $item->name; ?></span>
                                                    <span class="thumb-info-type"><?php echo $item->url; ?></span>
                                                </span>
                                                <span class="thumb-info-action">
                                                    <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                                </span>
                                            </span>
                                        </span>
                                    </a>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    var currentLink = "#link-portfolio";
</script>
