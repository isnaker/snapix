<?php

$testimonials= array();

$t = new stdClass();
// data bindings
$t->customer = "Александр";
$t->img = "2-55x55.jpg";
$t->content = "Поймал вирус &quot;отправьте смс на номер&quot;. Приехали в тот же день, починили довольно быстро. Человек разговаривал вежливо и на понятном языке. Никаких &quot;разводов&quot;, говорил только по делу. Доволен, рекомендую всем.";
$t->size = 6;
array_push($testimonials, $t);

$t = new stdClass();
$t->customer = "Елена Сергеевна";
$t->img = "1-55x55.jpg";
$t->content = "Вызвала мастера чтобы подключили роутер. Приехали быстро, настроили качественно, еще помогли решить проблемы с ноутбуком. Рекомендую....";
$t->size = 6;
array_push($testimonials, $t);

$t = new stdClass();
$t->customer = "Сергей";
$t->img = "3-55x55.jpg";
$t->content = "У меня старенький ноутбук acer, в нем сгорела видео карта. Обращался в несколько сервисов, мне объявляли приличную сумму за ремонт такой неисправность. Как случайно в интернете я наткнулся на ваш сайт, узнав стоимость решения моей проблемы - я был приятно удивлен, правда пришлось подождать пару дней так как не было комплектующих для моего ноутбука, но это не смертельно. Спасибо вам! вчера приезжал мастер Алексей, помог, настроил все отлично - мастер на все руки очень хороший специалист. Теперь ни что мне не помешает готовиться, комп стал работать лучше чем раньше";
$t->size = 8;
array_push($testimonials, $t);

$t = new stdClass();
$t->customer = "Никита";
$t->img = "4-55x55.jpg";
$t->content = "Спасибо вам! вчера приезжал мастер Алексей, помог, настроил все отлично - мастер на все руки очень хороший специалист. Теперь ни что мне не помешает готовиться, комп стал работать лучше чем раньше.";
$t->size = 4;
array_push($testimonials, $t);

$t = new stdClass();
$t->customer = "Наташа";
$t->img = "6-55x55.jpg";
$t->content = "Упал внешний жеский диск и перестал запускаться. Отдала на восстановление, всю информацию восстановили но правда пришлось подождать 1 неделю. Но в целом результатом я довольна.";
$t->size = 4;
array_push($testimonials, $t);

$t = new stdClass();
$t->customer = "Геннадий Дмитриевич";
$t->img = "5-55x55.jpg";
$t->content = "Накрылся моноблок, хотел отдать в ремонт но мне нужен был жеский диск для работы, обзвонил десяток сервисов, но во всех сервисах мне сказали что снять можно только в сервисе и ремонт от одной недели. Позвонил в Снапикс, мастер приехал ко мне на дом, снял жеский и забрал аппарат в ремонт. Уже через день мне сообщили результаты диагностики, а еще через два дня привезли отремонтированный компьютер. Огромное спасибо за качество и скорость. Мне очень понравилась квалификация мастеров и операторов.";
$t->size = 8;
array_push($testimonials, $t);

function generate_testimonials($testimonials){
    $result = "";
    $counter = 0;
    foreach ($testimonials as $item){
        $result.= "<div class='col-md-$item->size'>
                                <div class='testimonial testimonial-style-3'>
                                    <blockquote>
                                        <p>$item->content</p>
                                    </blockquote>
                                    <div class='testimonial-arrow-down'></div>
                                    <div class='testimonial-author'>
                                        <div class='testimonial-author-thumbnail'>
                                            <img src='/img/clients/$item->img' class='img-responsive img-circle' alt=''>
                                        </div>
                                        <p><strong>$item->customer</strong></p>
                                    </div>
							    </div>
							</div>";
        $counter++;
        $counter % 2 == 0 ? $result.='<div class="clearfix"></div>' : "";
    }
    return $result;
}
?>

    <style>
        html #header.header-semi-transparent, html #header.header-semi-transparent-light {
            min-height: 85px !important;
            width: 100%;
            background: rgba(0, 0, 0, 0.89);
            position: absolute;
            top: 0;
        }
        .page-header {margin-bottom: 0;     border-bottom: 5px solid #333333;}
        div.main { margin-top: 85px}
        #head-block {
            margin: 0;
            padding: 2em 0 2em 0;
            background-size: 380px;
            background-position: center 30%;
            background-repeat: no-repeat;
            background-attachment: fixed;
        }
        #head-block .h1 { font-size: 3em; font-weight: bold; padding: 0.18em 0.5em}

        #head-block .h4 span {background: #4d5fbc; padding: 0.4em 0.8em}
        #head-block p {
            color: #ffffff;
        }
        #head-block .form {
            background: rgba(0,0,0,0.66);
            padding: 1.5em 3em;
        }
        #head-block .list li {padding-left: 3em}
        #footer {margin-top: 0}
    </style>
        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li>Наши услуги</li>
                            <li class="active">Ремонт компьютеров</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>

        <section id="head-block" class="section">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="heading h1 mb-none bg text-center appear-animation" data-appear-animation="fadeInUp">Ремонт компьютеров в Коломне с выездом на дом</h1>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <h2 class="text-center short">Сломался компьютер, срочно требуется ремонт или настройка?</h2>
                    <p class="featured lead text-center">Устраним любые возникшие проблемы с компьютерной техникой у Вас дома!</p>
                    <div class="text-center"><strong class="alternative-font">Популярные услуги:</strong></div>
                </div>
            </div>
            <div class="container mt-xlg">
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-group"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Ремонт компьютеров</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-group"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Ремонт ноутбуков</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-film"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Удаление вирусов и баннеров</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-check"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Настройка Wi-Fi и Интернет</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-bars"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Диагностика компьютера</h4>
                            </div>
                        </div>
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-desktop"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Модернизация и апгрейд</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-bars"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Установка программ</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-desktop"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Восстановление данных</h4>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box secundary">
                            <div class="feature-box-icon">
                                <i class="fa fa-file"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="shorter">Установка Windows</h4>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>


        <div class="container mt-xlg mb-xlg">
            <div content="row">
                <div class="col-md-6">
                    <img class="img-responsive img-thumbnail" src="/img/remont-wallpaper540x360.jpg" alt="Ремонт компьютеров в Коломне. Компьютерная помощь. С выездом на дом.">
                </div>
                <div class="col-md-6">
                    <div class="alert alert-success hidden" id="contactSuccess">
                        <strong>Успех!</strong> Ваше сообщение было отправлено!
                    </div>

                    <div class="alert alert-danger hidden" id="contactError">
                        <strong>Ошибка!</strong> Что-то пошло не так и Ваше сообщение не отправлено
                    </div>
                    <h2 class="short push-top">Оставьте заявку на <strong>БЕСПЛАТНЫЙ</strong> вызов мастера прямо сейчас!</h2>
                    <div class="form" id="remont_inline">
                        <div class="row">
                            <div class="form-group">
                                <div class="col-md-5">
                                    <label>Ваш телефон *</label>
                                    <input type="tel" value="" data-msg-required="Пожалуйста, введите Ваш телефон." maxlength="100" class="form-control" name="phone" id="phone" required="" aria-required="true">
                                </div>
                                <div class="col-md-7">
                                    <p class="h5 mt-lg mb-none"><i class="fa fa-check"></i> Мастер приедет в течение 1 часа!</p>
                                    <p class="h5 mt-none mb-none"><i class="fa fa-check"></i> Диагностика бесплатно!</p>
                                    <p class="h5"><i class="fa fa-check"></i> Гарантия 3 месяца!</p>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <input type="submit" value="Вызвать мастера" class="btn mt-none btn-primary btn-lg sender" data-loading-text="Loading...">
                                <div class="alert text-center hidden mt-md"></div>
                            </div>
                            <div class="col-md-6">
                                или по телефону
                                <p class="h3">8 (916) 416-32-64</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    <section class="section section-default section-with-divider">
        <div class="divider divider-solid divider-style-4">
            <i class="fa fa-chevron-down"></i>
        </div>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <h2 class="text-center">Вам <strong>не придется</strong> долго ждать!</h2>
                    <p class="text-center mb-none">Высококвалифицированный мастер для оказания компьютерной помощи срочно прибудет в течение 1 часа в любой день недели или в любое другое удобное для Вас время! Наш центр компьютерной помощи готов заключить долгосрочные договоры на обслуживание компьютеров, как на дому, так и в офисе, предоставить взаимовыгодные условия сотрудничества, обеспечить Вам бесперебойность работы и сохранность информации.
                    </p>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="col-md-4 text-center">
            <img class="img-responsive" src="/img/itoutsource_1.png" alt="">
        </div>
        <div class="col-md-4 text-center">
            <hr class="tall">
            <h2>Выезд и диагностика - <strong>бесплатно!</strong></h2>
        </div>
        <div class="col-md-4 text-center">
            <hr class="tall clearfix">
            <div>Закажите услугу по телефону</div>
            <h3 class="short">8 (916) 416-32-64</h3>
            <div>или оставьте</div>
            <button class="btn btn-primary btn-lg push-bottom short" data-toggle="modal" data-target="#remontModal">
                Заявку онлайн
            </button>
        </div>
    </div>
    <hr class="tall">
    <div class="container">
        <h2 class="text-center">6 причин <strong>обратиться к нам!</strong></h2>
        <div class="row">
            <div class="col-md-4 text-center">
                <img class="push-bottom" src="/img/icons/speedometer.png" alt="">
                <h4>Бесплатный срочный выезд и диагностика</h4>
            </div>
            <div class="col-md-4 text-center">
                <img class="push-bottom" src="/img/icons/trophy.png" alt="">
                <h4>Квалифицированные специалисты</h4>
            </div>
            <div class="col-md-4 text-center">
                <img class="push-bottom" src="/img/icons/clock.png" alt="">
                <h4>Более <strong>10</strong> лет мы ремонтируем компьютеры и ноутбуки по всей Коломне</h4>
            </div>
            <div class="col-md-4 text-center">
                <img class="push-bottom" src="/img/icons/settings.png" alt="">
                <h4>За это время мы установили более <strong>15000</strong> программ</h4>
            </div>
            <div class="col-md-4 text-center">
                <img class="push-bottom" src="/img/icons/tools.png" alt="">
                <h4>Нашли и уничтожили более <strong>6000</strong> вирусных угроз</h4>
            </div>
            <div class="col-md-4 text-center">
                <img class="push-bottom" src="/img/icons/check.png" alt="">
                <h4>В общей сложности починили <strong>6300</strong> компьютеров</h4>
            </div>
        </div>
    </div>
    <section id="testimonials" class="mb-none mt-xlg">
        <div class="container">
            <h2 class="text-center taller"><strong>Отзывы</strong></h2>
            <?php print generate_testimonials($testimonials); ?>
        </div>
    </section>
    <div class="container">
        <section class="call-to-action with-borders call-to-action-quaternary button-centered mb-xl">
            <div class="call-to-action-content">
                <h3><strong>Специальные условия</strong> для организаций:</h3>
            </div>
            <div class="call-to-action-btn">
                <a class="btn btn-primary btn-lg" href="/files/kommercheskoe_predlojenie_snapix.doc">Скачать коммерческое предложение</a>
            </div>
        </section>
    </div>
    <div class="container mt-xlg">
        <h2 class="text-center">После проведения работ мы <strong>гарантируем:</strong></h2>
        <div class="col-md-8 col-md-offset-2">
            <div class="row clearfix">
                <div class="col-md-6 text-center">
                    <img src="/img/icons/garant1.png" alt="">
                    <h4>Быструю загрузку Вашего компьютера или ноутбука при включении</h4>
                </div>
                <div class="col-md-6 text-center">
                    <img src="/img/icons/garant2.png" alt="">
                    <h4>Бесперебойную работу установленных программ</h4>
                </div>
            </div>
            <div class="row clearfix">
                <div class="col-md-6 text-center">
                    <img src="/img/icons/garant3.png" alt="">
                    <h4>Безопасную работу пользователей в сети Интернет</h4>
                </div>
                <div class="col-md-6 text-center">
                    <img src="/img/icons/garant4.png" alt="">
                    <h4>Полное восстановление работы компьютеров после вирусов</h4>
                </div>
            </div>
        </div>
    </div>
    <section class="mt-xlg">
        <div class="container">
            <div class="row">
                <div class="center">
                    <h3>Вам необходима <strong>помощь</strong> IT-специалиста?
                        <button class="btn btn-primary btn-lg push-top push-bottom" data-toggle="modal" data-target="#remontModal">
                            Вызвать мастера!
                        </button>
                        <span class="arrow hlb hidden-xs hidden-sm hidden-md appear-animation rotateInUpLeft appear-animation-visible" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>
                    <img style="position: relative; margin-bottom: -30px" class="img-responsive center-block" src="/img/girl.png" alt="">
                </div>
            </div>
        </div>
    </section>
