	<section id="promo">
    	<div class="container">
    	    <div class="row">
    	        <div class="col-md-5">
    	            <p class="h2 heading">Создаем и <strong class="h1">продвигаем</strong> сайты для бизнеса</p>
    	        </div>
    	        <div class="col-md-7">
					<div class="row">
							<div class="col-md-4">
								<div class="promoBlock promoBlock1">
									<i class="fa fa-bullseye"></i>
									<p>За 7 лет продвинули 15 крупных проектов</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="promoBlock promoBlock2">
									<i class="fa fa-commenting-o"></i>
									<p>Каждый 3-й клиент приходит к нам по рекомендации</p>
								</div>
							</div>
							<div class="col-md-4">
								<div class="promoBlock promoBlock3">
									<i class="fa fa-thumbs-o-up"></i>
									<p>7 из 10 клиентов сотрудничают к нам дольше 3-х лет</p>
								</div>
							</div>
					</div>
					<div class="row">
							<div class="col-md-4">
								<div class="promoBlock promoBlock4">
									<i class="fa fa-font"></i>
									<p>За 7 лет наши специалисты прошли более 20 курсов повышения квалификациию</p>
								</div>
							</div>
						<div class="col-md-4">
							<div class="promoBlock promoBlock5">
								<i class="fa fa-key"></i>
								<p>Всем клиентам даются рекомендации для дальнейшего роста и дополнительной прибыли</p>
							</div>
						</div>
						<div class="col-md-4">
							<div class="promoBlock promoBlock6">
								<i class="fa fa-bar-chart-o anim"></i>
								<p><a href="#cases">Наши кейсы по продвижению</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
    	</div>
	</section>
	<!--обратный звонок-->
	<section id="call-back">
		<div class="row">
			<div class="col-md-4 col-md-offset-10">
				<button class="btn btn-warning " data-toggle="modal" data-target="#callbackModal">обратный звонок</button>
			</div>
		</div>
	</section>
	<section id="services">
		<div class="container">
			<div class="row">
				<div>
					<p class="h3 text-center">
						услуги интернет-маркетинга
					</p>
				</div>
			</div>
			<!--строка 1-->
			<div class="row text-center">
				<div class="col-md-3 block">
					<a href="#">
						<span class="glyphicon glyphicon-indent-left span-TOP span-services"></span>
						<p class="h3">Классническое продвижение в ТОП</p>
						<p class="text">Продвижение сайтов в поисковой системе без покупок ссылок</p>
						<button class="btn btn-block about">подробнее</button>
					</a>
				</div>
				<div class="col-md-3 block">
					<a href="">
						<span class="glyphicon glyphicon-thumbs-up span-services"></span>
						<p class="h3">Продвижение в социальных сетях</p>
						<p class="text">Пиар бренда на узнаваемость может повысить поток клиентов до 40%</p>
						<button class="btn btn-block about">подробнее</button>
					</a>
				</div>
				<div class="col-md-3 block">
					<a href="#">
						<span class="glyphicon glyphicon-record span-services"></span>
						<p class="h3">Контекстная медийная реклама</p>
						<p class="text">Реклама в интернете, позволяющая увеличить количество клиентов уже в первый день работы</p>
						<button class="btn btn-block about">подробнее</button>
					</a>
				</div>
				<div class="col-md-3 block">
					<a href="#">
						<span class="glyphicon glyphicon-wrench span-services"></span>
						<p class="h3">Разработка сайта для бизнеса</p>
						<p class="text">Создание продающего сайта для повышения конверсии</p>
						<button class="btn btn-block about">подробнее</button>
					</a>
				</div>
			</div>

		</div>
	</section>
	<section id="pluse">
		<div class="container">
			<div class="row text-center"><p class="h1">Фишки нашего продвижения</p></div>
			<div class="row text-center">
				<div class="col-md-3 block">
					<span class="glyphicon glyphicon-link"></span>
					<p class="h3">Ссылочная масса</p>
					<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
				</div>
				<div class="col-md-3 block">
					<span class="glyphicon glyphicon-list-alt"></span>
					<p class="h3">Наши гарантии</p>
					<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
				</div>
				<div class="col-md-3 block">
					<span class="glyphicon glyphicon-th"></span>
					<p class="h3">Ключевые фразы</p>
					<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
				</div>
				<div class="col-md-3 block">
					<span class="glyphicon glyphicon-refresh"></span>
					<p class="h3">Наполнение сайта</p>
					<p>Используйте их в кнопках, группах кнопок для панели инструментов, панели навигации, или в приставках элементов формы.</p>
				</div>
			</div>
		</div>
	</section>

	<section id="cases">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="h1 text-center">Наши кейсы</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<img src="http://turboseo.net.ua/images/case-item-1.png" class="img-responsive">
					<p class="h4 title-1">Интернет-магазин электроники</p>
					<span></span>
					<p>Мультибрендовый интернет-магазин. Занимается мелкооптовой и розничной торговлей японской электроникой, аксессуарами и товарами народного потребления на всей территории Украины.</p>
					<p><strong>Результат:</strong></p>
					<ul>
						<li>Посещаемость ресурса увеличилась в 20 раз и превысила 430 000 уникальных пользователей в месяц</li>
						<li>Количество продаж увеличилось в 10 раз, более 170 продаж в сутки</li>
						<li>Возврат инвестиций (ROI) увеличился в 2,5 раза и составил 264% с момента начала работы над проектом</li>
						<li>Временные затраты по проекту составили 500 часов</li>
					</ul>
				</div>
				<div class="col-md-4">
					<img src="http://turboseo.net.ua/images/case-item-2.png" class="img-responsive">
					<p class="h4 title-2">Интернет-магазин сумок</p>
					<span></span>
					<p>Интернет-магазин специализируется на розничной торговле сумками и аксессуарами из различных материалов и от разных производителей.</p>
					<p><strong>Результат:</strong></p>
					<ul>
						<li>Посещаемость ресурса превысила 16000 уникальных пользователей в месяц</li>
						<li>Возврат инвестиций (ROI) увеличился на 77% и равен 179%</li>
						<li>Конверсия увеличилась в полтора раза и превысила 3,7%</li>
					</ul>
				</div>
				<div class="col-md-4">
					<img src="http://turboseo.net.ua/images/case-item-3.png" class="img-responsive">
					<p class="h4">Интернет-магазин фильтров для воды</p>
					<span></span>
					<p>Компания работает на рынке более 10 лет и является одним из ведущих поставщиков оборудования и комплектующих для систем очистки воды.</p>
					<p><strong>Результат:</strong></p>
					<ul>
						<li>Посещаемость ресурса увеличилась на 111%, до 14 295 уникальных пользователей в месяц</li>
						<li>Возврат инвестиций (ROI) увеличился на 130%, до 207%</li>
						<li>Количество продаж увеличилось на 83%, до 208 в месяц</li>
					</ul>
				</div>
			</div>
		</div>
	</section>

	<section id="strategy">
		<div class="container">
			<div class="row">
				<p class="h1 text-center">Оптимальная стратегия	продвижения</p>
			</div>

			<div id="myCarousel" class="carousel slide">
				<!-- Indicators -->
				<ol class="seo-carusel text-center">
					<li data-target="#myCarousel" data-slide-to="0" class="active"> <label for="bt1" class="btn atc btn-warning btn1">Продажи<input type="radio" name="btn" style="display: none" id="bt1" checked></label></li>
					<li data-target="#myCarousel" data-slide-to="1">				<label for="bt2" class="btn btn-warning btn2">Продвижение<input type="radio" name="btn" style="display: none" id="bt2"></label></li>
					<li data-target="#myCarousel" data-slide-to="2">				<label for="bt3" class="btn btn-warning btn3">Трафик<input type="radio" name="btn" style="display: none" id="bt3"></label></li>
					<li data-target="#myCarousel" data-slide-to="3">				<label for="bt4" class="btn btn-warning btn4">Оптимизация<input type="radio" name="btn" style="display: none" id="bt4"></label></li>
					<li data-target="#myCarousel" data-slide-to="4">				<label for="bt5" class="btn btn-warning btn5">Конверсия<input type="radio" name="btn" style="display: none" id="bt5"></label></li>
					<li data-target="#myCarousel" data-slide-to="5">				<label for="bt6" class="btn btn-warning btn6">Юзабилити<input type="radio" name="btn" style="display: none" id="bt6"></label></li>
				</ol>

				<div class="carousel-inner" role="listbox">

					<!--продажи-->
					<div class="item">
						<div class="container">
							<div class="carousel-caption">
								<ul class="seo-list">
									<li>
										<p><span>19.</span> Исследуем отзывы и упоминания о компании,<br>
											доступные в интернете через поиск,<br>
											а при выявлении негатива рекомендуем<br>
											и проводим репутационный маркетинг (SERM)
										</p>
									</li>
									<li>
										<p><span>20.</span> Совершаем тайную покупку,<br>
											а также заказ по телефону, фиксируем ошибки<br>
											и упущения контактного персонала,<br>
											вносим рекомендации по оптимизации сервиса
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<!--продвижение-->
					<div class="item">
						<div class="container">
							<div class="carousel-caption">
								<ul class="seo-list">
									<li>
										<p><span>9.</span> Регистрируем локальный бизнес<br>
											и продвигаем в картах поисковых систем
										</p>
										<p><span>10.</span> Проводим внешнюю оптимизацию сайта:<br>
											увеличиваем число упоминаний и отзывов,<br>
											наращиваем качественную ссылочную массу
										</p>
									</li>
									<li>
										<p><span>11.</span> В течение продвижения систематически<br>
											расширяем и корректируем семантическое ядро
										</p>
										<p><span>12.</span> Продвигаем сайт социальными факторами:<br>
											создаем и наполняем контентом группу в<br>
											Google+, репостим сайт в аккаунтах соцсетей
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<!--трафик-->
					<div class="item">
						<div class="container">
							<div class="carousel-caption">
								<ul class="seo-list">
									<li>
										<p><span>13.</span> При необходимости создаем и ведем <br>
											рекламные компании в поисковой сети<br>
											и КМС, настраиваем ремаркетинг
										</p>
									</li>
									<li>
										<p><span>14.</span> Рекомендуем и ведем работы по<br>
											контент-маркетингу, наращивая трафик<br>
											по длинному хвосту запросов (long tail)
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<!--оптимизация-->
					<div class="item active">

						<div class="carousel-caption">
							<ul class="seo-list">
								<li>
									<p><span>1.</span> Проверяем историю сайта и<br>
										проводим мониторинг конкурентной среды,<br>
										определяем стратегию продвижения
									</p>
									<p><span>2.</span> Проводим подробный SEO-аудит и<br>
										технический анализ сайта и сервера, в том числе<br>
										на наличие вредоносного ПО и вирусов
									</p>
									<p><span>3.</span> Оптимизируем работу сервера,<br>
										в том числе файлы .htaccess и robots.txt
									</p>
									<p><span>4.</span> Оптимизируем микроразметку сайта<br>
										и ЧПУ, настраиваем автогенерацию url-ов
									</p>
								</li>
								<li>
									<p><span>5.</span> Оптимизируем навигацию и структуру сайта,<br>
										в том числе карты сайта форматов html и xml,<br>
										исправляем битые ссылки
									</p>
									<p><span>6.</span> Оптимизируем скорость работы сайта<br>
										и обработку основных ошибок
									</p>
									<p><span>7.</span> Распределяем семантические группы и<br>
										оптимизируем метаданные
									</p>
									<p><span>8.</span> Устраняем дубли, анализируем уникальность<br>
										и качество контента сайта, пишем необходимый<br>
										и уникализируем существующие тексты
									</p>
								</li>
							</ul>
						</div>


					</div>

					<!--конверсия-->
					<div class="item">
						<div class="container">
							<div class="carousel-caption">
								<ul class="seo-list">
									<li>
										<p><span>17.</span> Исследуем карту поведения и страницы выхода,<br>
											карту кликов, увеличиваем глубину просмотров<br>
											и время, проводимое пользователями на сайте,<br>
											уменьшаем показатель отказов
										</p>
									</li>
									<li>
										<p><span>18.</span> Устанавливаем сервисы аналитики и<br>
											настраиваем цели, конверсии, на базе<br>
											данных которых вносим рекомендации<br>
											по повышению конверсий сайта
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>

					<!--юз-->
					<div class="item">
						<div class="container">
							<div class="carousel-caption">
								<ul class="seo-list">
									<li>
										<p><span>15.</span> Проверяем кроссбраузерность,<br>
											проводим тестирование функционала сайта,<br>
											анализируем видимость актуальной информации
										</p>
									</li>
									<li>
										<p><span>16.</span> Тестируем процессы и работу форм<br>
											онлайн-заказов и обратной связи, вносим<br>
											рекомендации по повышению юзабилити
										</p>
									</li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div><!-- /.carousel -->
		</div>
	</section>
	<section id="our-templates">
		<div class="container-fluid our-templates">
			<div class="container">
				<div class="row">
					<div class="col-md-12 text-center our-templates-title">
						<p class="h1">Продвижение на базе готовых решений</p>
						<p class="h2">Для начинающего онлайн-бизнеса с малым бюджетом</p>
					</div>
					<div class="col-md-12 text-center our-templates-title">
						<p class="h1">Заказав продвижение на 1 год</p>
						<p class="h2">Вы получаете пакетное решение</p>
					</div>
				</div>
				<div class="row our-templates-about text-center">
					<div class="col-md-3">
						<span class="glyphicon glyphicon-phone icon"></span>
						<p>
							Разработка адаптируемого
							под мобильные устройства
							современного сайта с учетом
							требований продвижения
						</p>
					</div>
					<div class="col-md-3">
						<span class="fa fa-external-link-square icon"></span>
						<p>
							Продвижение сайта в ТОП
							по утвержденному списку
							тематических поисковых
							запросов в течении года
						</p>
					</div>
					<div class="col-md-3">
						<span class="glyphicon glyphicon-pencil icon"></span>
						<p>
							Возможность бесплатного
							наполнения нового сайта
							текстовым и медиа
							контентом в течении года
						</p>
					</div>
					<div class="col-md-3">
						<span class="glyphicon glyphicon-wrench icon"></span>
						<p>
							Возможность бесплатной
							модернизации сайта
							(развитие функционала и
							редизайн) в течении года
						</p>
					</div>
				</div>
			</div>
		</div>
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-md-offset-4 our-templates-btn">
					<button class="btn btn-warning btn-block" data-toggle="modal" data-target="#callbackModal">Заказать коммерческое предложение</button>
				</div>
			</div>
		</div>
	</section>
	<section id="adventages">
		<div class="container">
			<div class="row">
				<div class="col-md-12 text-center">
					<p class="h2">Преимущества пакетного расширения</p>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4 text-center">
					<img src="https://seosolution.ua/images/advantages-1.png">
					<p class="adventages-title1">Во-первых</p>
					<p class="adventages-content1">Обратившись за продвижением, вы
					получаете весь комплекс услуг,
					необходимых для результативного
					онлайн-бизнеса в современных условиях.</p>
				</div>
				<div class="col-md-4 text-center">
					<img src="https://seosolution.ua/images/advantages-2.png">
					<p class="adventages-title2">Во-вторых</p>
					<p class="adventages-content2">Оплачивая лишь продвижение, вы
							существенно экономите на всем
							комплексе дополнительных услуг,
							предоставляемых нами бесплатно
				</div>
				<div class="col-md-4 text-center">
					<img src="https://seosolution.ua/images/advantages-3.png">
					<p class="adventages-title3">В-третьих</p>
					<p class="adventages-content3">Обращаясь за услугами по разработке,
							наполнению и продвижению сайта к
							1 исполнителю, вы избегаете лишних
							хлопот и экономите свое время</p>
				</div>
			</div>
		</div>
	</section>

	<section id="partners">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
					<p class="h1 text-center">Среди наших клиентов</p>
				</div>
			</div>
			<div class="row text-center">
				<div class="col-md-4">
					<img src="/img/seo-grafics/l-cbi.png" class="img-responsive" title="Palla" alt="">
				</div>
				<div class="col-md-4">
					<img src="/img/seo-grafics/l-sushi.png" class="img-responsive" title="Lengy" alt="" style="margin-top: -1em;">
				</div>
				<div class="col-md-4">
					<img src="/img/seo-grafics/l-elfa.png" class="img-responsive" title="ЦБИ 3" alt="">
				</div>
			</div>
			<div class="row col-md-offset-3">
				<div class="col-md-4">
					<img src="/img/seo-grafics/l-palla.png" class="img-responsive" title="Elfa" alt="">
				</div>
				<div class="col-md-4">
					<img src="/img/seo-grafics/l-lengy.png" class="img-responsive" title="Центр Суши" alt="" style="margin: 3em 0;">
				</div>
			</div>
			<h2>Продвижение сайта в поисковых системах</h2>
			<p>На сегодняшний день среди самых востребованных и распространенных инструментов, использующихся для увеличения объемов продаж, очевидное преимущество занимает <strong>продвижение сайта в поисковых системах</strong>. 
			Высокий уровень конкуренции и кризис приводят к необходимости поиска новых решений, которые позволят обрести новых клиентов и удержать уже имеющихся.</p> 
			<h2>Сео продвижение сайта</h2> 
			<p>дает возможность <strong>продвинуть сайт</strong> на более высокую позицию в поисковике, а наша компания является тем «спутником», который знает, как сделать это правильно и эффективно. 
			Мы осуществляем комплексную работу, которая позволяет <strong>раскрутить сайт</strong> и привести его в желаемый заказчику вид.</p>
			<h3>Продвижение сайта в Яндексе</h3> 
			<p>и не только предполагает, что проведенная <strong>SEO оптимизация сайта</strong> позволит сократить расходы, выделяемые на рекламу услуг и товаров, и получить максимальный доход.</p>
            <p>Благодаря целому комплексу мер, <strong>поисковая оптимизация сайта</strong> предоставляет возможность достичь потрясающего эффекта, увеличивая посещаемость, продвигая сайт в поисковых системах, привлекая новых потребителей продукции компании и повышая ее конкурентоспособность.</p>
            <p>В век информационных технологий наличие веб-сайта у каждой серьезной компании не просто прихоть, а одна из составляющих успешного ведения бизнеса. Наша компания индивидуально подбирает и создает интерфейс, наиболее полно соответствующий стилю и индивидуальности компании.</p>
            <p>Качественная <strong>оптимизация сайта</strong> позволяет не только привлечь новых клиентов и отображаться в первых строках поисковых систем, но и за счет размещения рекламы на страницах сайта получать немалый доход.</p>
            <p>Квалифицированные специалисты, работающие в нашей компании, уже не первый год реализовывают успешную стратегию <strong>продвижения сайтов в Москве</strong> предлагает большой выбор фирм, предоставляющих подобные услуги, однако наши клиенты, обращаясь к нам, получают неоспоримые преимущества, которые позволяют им существенно увеличить количество продаж.</p> 
            <p>Наша стратегия включает необходимые для успешного достижения цели элементы:</p>
            <ul>
            <li>проведение анализа конкурентной среды;</li>
            <li>изучение целевой аудитории сайта;</li>
            <li>сбор и обработка данных о продукте и рынке;</li>
            <li>отбор списка запросов;</li>
            <li>технический анализ сайта;</li>
            <li>анализ юзабилити.</li>
            <p>Все, что требуется от заказчика для успешного продвижения сайта – это информация о деятельности компании и о том, что он хочет получить в результате, а мы уже позаботимся о составлении эффективного плана по реализации проекта.</p>
 
		</div>
	</section>