        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li> Наши услуги</li>
                            <li class="active">Создание интернет-магазинов</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section id="head-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center mb-none">
                            <i class="fa fa-shopping-basket fa-5x color-green promo-cart animated zoomIn appear-animation"
                               data-appear-animation="fadeInUp"
                            ></i>
                        </p>
                        <h1 class="heading h1 text-center appear-animation" data-appear-animation="fadeInUp">

                            <span>Создание интернет-магазина </span><br>
                            <span class="sub">под ключ от профессионалов!</span>
                        </h1>
                        <p class="h4 text-center appear-animation" data-appear-animation="fadeInDown">

                            <span>Получите <strong>мощный интернет-магазин</strong> для Вашего бизнеса!</span></p>
                    </div>
                </div>
            </div>
        </section>
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-5 p-none">
                    <img class="center-block mt-xlg" src="/img/services/shops.jpg" alt="">
                </div>
                <div class="col-md-6 p-none">
                    <h2 class="mt-xl">Мы знаем, а главное - <strong>можем создать интернет-магазин</strong>,
                        который будет привлекательным
                        и удобным для ваших покупателей.</h2>
                    <p>Мы предлагаем Вам действительно качественный продукт, который
                        будет привлекать посетителей и потенциальных клиентов. Наша команда с радостью
                        познакомит, продемонстрирует и расскажет о всех особенностях нашего продукта.</p>
                    <p>Наши магазины разработаны не для специалистов, а для простых пользователей.
                        Управлять структурой сайта и редактировать страницы, добавлять товар максимально
                        просто и удобно.
                    </p>
                </div>
            </div>
        </div>
        <section id="features">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <h2 class="h1 text-center mt-xlg"><strong>Особенности</strong> наших интернет-магазинов</h2>
                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#adaptive" data-toggle="tab" aria-expanded="true">Адаптивный дизайн</a>
                                </li>
                                <li>
                                    <a href="#catalog" data-toggle="tab">Удобный каталог</a>
                                </li>
                                <li>
                                    <a href="#product" data-toggle="tab">Страница товара</a>
                                </li>
                                <li>
                                    <a href="#cart" data-toggle="tab">Удобная корзина</a>
                                </li>
                                <li>
                                    <a href="#order" data-toggle="tab">Подтверждение заказа</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane active" id="adaptive">
                                    <img src="/img/content/adaptive1.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="tab-pane" id="catalog">
                                    <img src="/img/content/catalog.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="tab-pane" id="product">
                                    <img src="/img/content/product.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="tab-pane" id="cart">
                                    <img src="/img/content/cart.jpg" class="img-responsive" alt="">
                                </div>
                                <div class="tab-pane" id="order">
                                    <img src="/img/content/order.jpg" class="img-responsive" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="ourclients">
            <div class="container">
                <div class="row">
                    <div class="heading heading-border heading-middle-border heading-middle-border-center mt-xl">
                        <h3 class="h1">Примеры наших <strong>интернет-магазинов</strong></h3>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <p class="h4 text-center mb-xlg">Первый интернет-магазин мы сделали в 2009 году.
                            Среди наших клиентов - крупные российские и иностранные компании, среди которых как именитые,
                            давно работающие на рынке, так и молодые амбициозные участники рынка.</p>
                    </div>
                </div>
                <div class="row lightbox"  data-plugin-options='{"delegate": "a", "type": "image", "gallery": {"enabled": true}}'>
                    <ul class="portfolio-list sort-destination" data-sort-id="portfolio">
                        <li class="col-md-4 col-sm-6 col-xs-12 isotope-item websites">
                            <div class="portfolio-item">
                                <a href="/img/projects/palla.jpg">
                                    <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="/img/projects/palla545x267.jpg" class="img-responsive" alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner">Palla.su</span>
                                                <span class="thumb-info-type">Интернет-магазин</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-12 isotope-item websites">
                            <div class="portfolio-item">
                                <a href="/img/projects/elfaonline.jpg">
                                    <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="/img/projects/elfaonline545x267.jpg" class="img-responsive" alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner">ElfaOnline.ru</span>
                                                <span class="thumb-info-type">Интернет-магазин</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-12 isotope-item logos">
                            <div class="portfolio-item">
                                <a href="/img/projects/centersushi.jpg">
                                    <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="/img/projects/centersushi350x172.jpg" class="img-responsive" alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner">Center-Sushi.ru</span>
                                                <span class="thumb-info-type">Интернет-магазин</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-12 isotope-item brands">
                            <div class="portfolio-item">
                                <a href="/img/projects/elfa.jpg">
                                    <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="/img/projects/elfa350x172.jpg" class="img-responsive" alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner">Elfa-room.ru</span>
                                                <span class="thumb-info-type">Интернет-магазин</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </li>
                        <li class="col-md-4 col-sm-6 col-xs-12 isotope-item brands">
                            <div class="portfolio-item">
                                <a href="/img/projects/lengy_new.jpg">
                                    <span class="thumb-info">
                                        <span class="thumb-info-wrapper">
                                            <img src="/img/projects/lengy_new350x172.jpg" class="img-responsive" alt="">
                                            <span class="thumb-info-title">
                                                <span class="thumb-info-inner">Lengy.ru</span>
                                                <span class="thumb-info-type">Интернет-магазин</span>
                                            </span>
                                            <span class="thumb-info-action">
                                                <span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
                                            </span>
                                        </span>
                                    </span>
                                </a>
                            </div>
                        </li>
                    </ul>
                </div>
            </div>
        </section>
        <section id="shop_pricing_table" class="mt-none section section-default">
            <div class="container">
                <div class="row mt-none mb-xlg">
                    <div class="col-md-12">
                        <div class="heading mt-none heading-border heading-middle-border heading-middle-border-center">
                            <h4 class="h1 mt-none">Выберите <strong>свой</strong> интернет-магазин</h4>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="item" data-value="basic">
                            <div class="heading heading-border heading-bottom-double-border">
                                <h4> <strong>Базовый</strong> магазин</h4>
                            </div>
                            <ul class="list list-icons list-primary list-borders">
                                <li><i class="fa fa-check"></i>Срок создания сайта - 3-5 дней*</li>
                                <li><i class="fa fa-check"></i>Доменное имя .RU / .РФ (1 год)</li>
                                <li><i class="fa fa-check"></i>Хостинг (3 месяца)</li>
                                <li><i class="fa fa-check"></i>Неограниченное количество страниц</li>
                                <li><i class="fa fa-check"></i>Неограниченное количество товаров</li>
                                <li><i class="fa fa-check"></i>Готовый дизайн сайта</li>
                                <li><i class="fa fa-check"></i>Система управления сайтом</li>
                            </ul>
                            <hr>
                            <p class="h2 mb-sm text-center">
                                <strong>24 990</strong> руб.
                            </p>
                        </div>
                        <button data-value="basic" data-toggle="modal" data-target="#orderShopModal" class="btn btn-primary btn-3d btn-lg center-block"><strong>Заказать</strong></button>
                    </div>
                    <div class="col-md-4">
                        <div class="item" data-value="optimal">
                            <div class="heading heading-border heading-bottom-double-border">
                                <h4> <strong>Оптимальный</strong> магазин</h4>
                            </div>
                            <ul class="list list-icons list-primary list-borders">
                                <li><i class="fa fa-check"></i>Срок создания сайта - 10-15 дней*</li>
                                <li><i class="fa fa-check"></i>Доменное имя .RU / .РФ (1 год)</li>
                                <li><i class="fa fa-check"></i>Хостинг (3 месяца)</li>
                                <li><i class="fa fa-check"></i>Неограниченное количество страниц</li>
                                <li><i class="fa fa-check"></i>Неограниченное количество товаров</li>
                                <li><i class="fa fa-check"></i>Шаблонный дизайн сайта</li>
                                <li><i class="fa fa-check"></i>Система управления сайтом</li>
                            </ul>
                            <hr>
                            <p class="h2 mb-sm text-center">
                                <strong>43 990</strong> руб.
                            </p>
                        </div>
                        <button data-value="optimal" data-toggle="modal" data-target="#orderShopModal" class="btn btn-primary btn-3d btn-lg center-block"><strong>Заказать</strong></button>
                    </div>
                    <div class="col-md-4">
                        <div class="item" data-value="extended">
                            <div class="heading heading-border heading-bottom-double-border">
                                <h4> <strong>Расширенный</strong> магазин</h4>
                            </div>
                            <ul class="list list-icons list-primary list-borders">
                                <li><i class="fa fa-check"></i>Срок создания сайта - 14-30 дней*</li>
                                <li><i class="fa fa-check"></i>Доменное имя .RU / .РФ (1 год)</li>
                                <li><i class="fa fa-check"></i>Хостинг (3 месяца)</li>
                                <li><i class="fa fa-check"></i>Неограниченное количество страниц</li>
                                <li><i class="fa fa-check"></i>Неограниченное количество товаров</li>
                                <li><i class="fa fa-check"></i>Уникальный дизайн сайта</li>
                                <li><i class="fa fa-check"></i>Возможность синхронизации с 1С</li>
                            </ul>
                            <hr>
                            <p class="h2 mb-sm text-center">
                                <strong>69 990</strong> руб.
                            </p>
                        </div>
                        <button data-value="extended" data-toggle="modal" data-target="#orderShopModal" class="btn btn-primary btn-3d btn-lg center-block"><strong>Заказать</strong></button>

                    </div>
                </div>
                </div>
        </section>
        <section>
                <div class="container sticky-container">
                    <div class="row">
                        <div class="">
                            <div class="col-md-9">
                                <p class="h2 mt-xlg mb-md">Пакеты включают:</p>
                                <table class="table table-responsive table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <td>Возможности</td>
                                            <td><strong>Базовый</strong> </td>
                                            <td><strong>Оптимальный</strong> </td>
                                            <td><strong>Расширенный</strong> </td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Регистрация и кабинет покупателя</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Расширенная карточка товара</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Корзина товаров</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Удобное перемещение товаров</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Массовая загрузка товаров</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Уведомление о смене статуса заказа</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Оплата: банковские карты, терминалы, эл. валюта</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Расширенный фильтр товаров</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Социальные кнопки</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Модуль “Новости сайта”</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Модуль Форма обратной связи</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Слайд-шоу</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Выпадающее меню</td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Спецпредложения</td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Модуль "Быстрый Обратный Звонок"</td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Модуль "Похожие товары"</td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Модуль "Ранее просмотренные"</td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Карусель товаров</td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Сравнение товаров</td>
                                            <td></td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Отзывы о товарах</td>
                                            <td></td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Рейтинг товаров</td>
                                            <td></td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Доработка специальных возможностей под ваши нужды</td>
                                            <td></td>
                                            <td></td>
                                            <td><i class="fa fa-check"></i> </td>
                                        </tr>
                                        <tr>
                                            <td>Общая цена продукта:</td>
                                            <td>
                                                <p class="h4 text-center mb-sm"><strong>24 990</strong> руб.</p>
                                                <button data-link="basic" class="btn btn-primary btn-block mt-none link-sender">Заказать</button>
                                            </td>
                                            <td>
                                                <p class="h4 text-center mb-sm"><strong>43 990</strong> руб.</p>
                                                <button data-link="optimal" class="btn btn-primary btn-block mt-none link-sender">Заказать</button>
                                            </td>
                                            <td>
                                                <p class="h4 text-center mb-sm"><strong>69 990</strong> руб.</p>
                                                <button  data-link="extended" class="btn btn-primary btn-block mt-none link-sender">Заказать</button>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-md-3">
                                <div id="shop_consult_sidebar" style="background: #ffd829; padding: 1em 1.5em" class="center hidden-xs" data-plugin-sticky data-plugin-options='{"minWidth": 291, "containerSelector": ".sticky-container", "padding": {"top": 110, "bottom": 20}}'>
                                    <div class="form">
                                        <div class="heading heading-border heading-bottom-border text-left">
                                            <p class="h4 heading"><strong>Бесплатная консультация</strong>
                                                по выбору тарифа для Вашего интернет-магазина</p>
                                        </div>
                                        <div class="form-group">
                                            <input type="tel" class="form-control" placeholder="Введите Ваш телефон">
                                        </div>
                                        <button class="btn btn-3d btn-sm btn-secondary mr-xs mb-sm btn-lg btn-block sender">
                                            <strong class="text-uppercase">Получить консультацию</strong></button>
                                        <div class="text-center">
                                            <small><i class="fa fa-shield"></i> Ваши данные защищены</small>
                                        </div>
                                        <div class="alert text-center hidden"></div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
             <!--   <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-bordered table-responsive">
                                <thead>
                                    <tr>
                                        <td colspan="3">
                                            <div class="heading">
                                                <h5 class="h2 mt-md mb-md text-center">
                                                    Синхронизация интернет-магазина с 1С-предприятие
                                                </h5>
                                            </div>
                                        </td>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>Выгрузка товаров на сайт</td>
                                        <td rowspan="6" class="td-about">
                                            <p>Интеграция с 1С – это настройка обмена данными между Вашим интернет-магазином и
                                            программой 1С.</p>
                                            <p>Интеграция интернет-магазина с программой 1С становится необходимым условием
                                            эффективной работы и прибыльности интернет-магазина.</p>
                                            <p>Позволяет автоматически загружать товары из 1С на сайт в комплекте с
                                            изображениями, описанием и характеристиками.</p>
                                            <p>Существенно экономит время и деньги, так как заменяет ручное внесение
                                            информации о товарах в каталог на сайте.</p>
                                            <p>Позволяет выгружать заказы ваших клиентов сразу в программу 1С и
                                            оперативно их обрабатывать.</p>
                                        </td>
                                        <td rowspan="6" class="td-count">
                                            <div class="text-center">
                                                Стоимость модуля синхронизации:
                                                <p class="h2"><strong>15 000</strong> руб.</p>
                                            </div>
                                            <img src="/img/1c.jpg" alt="">
                                            <button data-toggle="modal" data-target="#orderShop1CSyncModal" class="btn btn-secondary btn-block btn-lg"><strong>Заказать синхронизацию</strong></button>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Выгрузка информации об остатках на складе</td>
                                    </tr>
                                    <tr>
                                        <td>Загрузка новых контрагентов с сайта</td>
                                    </tr>
                                    <tr>
                                        <td>Загрузка новых заказов с сайта (по товарам)</td>
                                    </tr>
                                    <tr>
                                        <td>Первоначальная загрузка товаров с сайта в 1С</td>
                                    </tr>
                                    <tr>
                                        <td>Срок настройки модуля интеграции - 5 дней</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div> -->
        </section>
        <section id="garanty">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <p class="h1 text-center heading heading-border">С нами <strong>надежно.</strong></p>
                    </div>
                </div>
                <div class="row mt-xl">
                    <div class="col-md-4">
                        <div class="feature-box feature-box-tertiary feature-box-style-5">
                            <div class="feature-box-icon">
                                <i class="icon-trophy icons"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-sm">Работа по договору</h4>
                                <p class="mb-lg">Предоставляем все документы на работы и четко следим на исполнением
                                    обязательст. Мы ценим своих клиентов и всегда находимся на связи.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box feature-box-tertiary feature-box-style-5">
                            <div class="feature-box-icon">
                                <i class="icon-speedometer icons"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-sm">Соблюдение сроков</h4>
                                <p class="mb-lg">Мы всегда исполняем свои обещания и делаем работу в срок.
                                    За срыв сроков и качество работы несем полную ответственность.</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="feature-box feature-box-tertiary feature-box-style-5">
                            <div class="feature-box-icon">
                                <i class="icon-cloud-upload icons"></i>
                            </div>
                            <div class="feature-box-info">
                                <h4 class="mb-sm">Гибкая оплата</h4>
                                <p class="mb-lg">Начинаем работу над Вашим проектом по предоплате. Предоставляем возможности
                                    рассрочки и оплаты только за результат.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="why">
            <div class="container">
                <div class="row">
                    <div class="heading heading-border heading-middle-border heading-middle-border-center mt-xl">
                        <h6 class="h1 title-h6">Интернет-магазин. </h6><strong class="h1 strong-h1">Выгодно ли?</strong>
                    </div>
                </div>
                <div class="featured-boxes featured-boxes-style-3">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 167px;">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-user"></i>
                                    <p class="h2"><strong>799</strong> +</p>
                                    <p>Товаров покупается в России
                                        каждую минуту через интернет</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="featured-box featured-box-secondary featured-box-effect-3" style="height: 167px;">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-book"></i>
                                    <p class="h2"><strong>36</strong> %</p>
                                    <p>Покупателей используют
                                        интернет для заказа услуг или товаров</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="featured-box featured-box-tertiary featured-box-effect-3" style="height: 167px;">
                                <div class="box-content">
                                    <i class="icon-featured fa fa-trophy"></i>
                                    <p class="h2"><strong>39.5</strong> $</p>
                                    <p>Миллиардов покупок было
                                        совершенно российскими
                                        покупателями в интернете в 2015г.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <hr class="mb-xlg">
                <div class="row">
                    <div class="col-md-7 col-md-offset-1">
                        <p class="h2">Будущее - <strong>за интернетом!</strong></p>

                        <ul class="list list-icons list-icons-lg">
                            <li><i class="fa fa-check"></i> Непрекращаемый рост интернет аудитории и все большее проникновение в регионы</li>
                            <li><i class="fa fa-check"></i> 81% аудитории интернета — это люди со средним доходом и выше;</li>
                            <li><i class="fa fa-check"></i> Даже в кризис рост объема рынка интернет рекламы продолжается;</li>
                            <li><i class="fa fa-check"></i> Интернет — самый измеряемый канал;</li>
                            <li><i class="fa fa-check"></i> Тренд: интернет опережает печатные издания, телевещание и радио.</li>
                        </ul>

                    </div>
                    <div class="col-md-4 text-left">
                        <img src="/img/services/shops.jpg" class="img-responsive appear-animation" data-appear-animation="fadeInRight" alt="">
                    </div>
                </div>
                 <h2>Создание интернет магазина под ключ</h2>
                 <p>– одно из основных направлений деятельности компании. При этом заказчик получает именно то, что хочет. В большинстве случаев, у продавцов возникает потребность <strong>создать сайт интернет магазин</strong> с целью увеличения продаж и, желательно, с минимальными затратами на его разработку.
                 <h2>Заказать интернет магазин в нашей студии</h2> 
                 <p>– значит получить современный и качественный продукт, обеспечивающий удобство при его эксплуатации; внедрение и использование всех сервисов, обеспечивающих поступление платежей за товар; оптимальные условия поиска товара в базе магазина и быстрое предоставление полной информации о нем вместе с его изображением. Открытый код дает возможность любому веб-мастеру  менять параметры интернет – магазина по желанию заказчика. 
                 <h3>Разработка интернет магазина</h3> 
                 <p>предполагает также интеграцию с программой 1С и Яндекс маркетом, оптимизацию с популярными поисковыми системами, при этом возможна доработка интернет - продукта с учетом желаний заказчика.</p>

                <h3>Создание интернет магазина с нуля</h3> 
                <p>нашими специалистами обеспечивает формирование сложного каталога предлагаемых товаров (при этом их количество не ограничено), возможность найти  интересующий товар при помощи всевозможных фильтров, деление товаров на определенные категории (акционные, новинки, хиты). Также предусмотрена возможность предоставления разных артиклей для модификаций одного товара и соответствующей цены для каждой такой модификации. Сохраняется история посещения интернет – магазина (просмотренные и избранные товары), предусмотрена функция их сравнения, а также предлагается перечень похожих и сопутствующих товаров с отзывами о них. Есть возможность поделиться своим мнением о товаре и магазине с друзьями по средствам популярных сервисов и соцсетей и многие  другие функции.
                При этом заказчик получает необходимый ему интернет продукт, который полностью готов к работе. Наполнение магазина товаром происходит автоматически. Также организация обеспечивает продвижение интернет – магазина, что приводит к его популярности среди клиентов.
                Сотрудники нашей компании обеспечивают дальнейшее обслуживание сайта, отслеживают его рейтинг и оперативно сообщают о возникающих ошибках, консультируют при возникновении проблем.
                Если вы решились на <strong>создание интернет магазина цена</strong> оговаривается в каждом конкретном случае. Будем рады сотрудничеству. </p>
            </div>
        </section>
        <section id="contact_form" class="section section-default mb-none">
            <div class="container">
                <div class="row">
                    <div class="col-md-7">
                        <p class="h2">Свяжитесь с нами прямо сейчас!</p>
                        <p class="h5"><strong>Напишите нам на почту:</strong></p>
                        <p class="h4"><strong>mail@snapix.ru</strong></p>
                        <p class="h5 mt-xlg"><strong>Или позвоните по телефонам:</strong></p>
                        <p class="h4 mb-none"><strong><?php App::phone() ?></strong></p>
                    </div>
                    <div class="col-md-5">
                        <p class="h2">Заказать <strong>бесплатную</strong> консультацию</p>
                        <div class="col-md-10">
                            <div class="form">
                                <div class="row">
                                    <div class="col-md-12">
                                        <button data-toggle="modal" data-target="#callbackModal" class="btn btn-primary btn-block btn-lg center-block">Оставить заявку</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
