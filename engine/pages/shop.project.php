<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">Главная</a></li>
                    <li class=""><a href="/store">Купить готовый сайт</a></li>
                    <li class="active"><?php print $product->name ?></li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>Купить "<?php print $product->name ?>"</h1>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-md-6">
                <div class="owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10}'>
                    <div>
                        <img alt="" height="300" class="img-responsive" src="/img/projects/buildmasterz.jpg">
                    </div>
                    <div>
                        <img alt="" height="300" class="img-responsive" src="/img/projects/centersushi.jpg">
                    </div>
                    <div>
                        <img alt="" height="300" class="img-responsive" src="/img/projects/cgz.jpg">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                Описание:
                <p class="h4"><?php print $product->description ?></p>


                    Стоимость:
                    <small><del><?php print $product->price*1.3 ?> <i class="fa fa-rouble"></i> </del> </small>
                <p class="h2">
                     <strong><?php print $product->price ?></strong> <i class="fa fa-rouble"></i>
                </p>
                <div>
                    <button class="btn btn-primary btn-lg text-uppercase">заказать</button>
                </div>

                <hr>
                Сделать заказ:
                <p>Вы можете приобрести этот проект, связавшись с нами по телефону или по e-mail</p>

            </div>
        </div>
    </div>

</section>