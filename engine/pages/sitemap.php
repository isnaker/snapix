<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="/">Главная</a></li>
                    <li class="active">Карта сайта</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>Карта сайта</h1>
            </div>
        </div>
    </div>
</section>

<div class="container">
    <div class="row">
        <div class="col-md-8">
            <table>
                <tr valign="top">
                    <td class="lpart" colspan="100">
                        <p class="h2 mt-xlg">Разделы</p>
                        <table cellpadding="0" cellspacing="0" border="0" width="100%">

                            <tr><td class="lpage"><a href="http://snapix.ru/" title="Создание сайтов и интернет магазинов. Реклама и продвижение сайта">Создание сайтов и интернет магазинов. Реклама и продвижение сайта</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works" title="Наши работы. Примеры работ. Портфолио сайтов. Агентство Snapix.">Наши работы. Примеры работ. Портфолио сайтов. Агентство Snapix.</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/kalkulyator-konteksta" title="Калькулятор контекстной рекламы - рассчитать бюджет!">Калькулятор контекстной рекламы - рассчитать бюджет!</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/contacts" title="Snapix - Контакты">Snapix - Контакты</a></td></tr>
                        </table>
                    </td>
                </tr>

                <tr valign="top">
                    <td class="lbullet">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td class="lpart" colspan="99">
                        <p class="h2 mt-xlg">Услуги</p>

                        <table cellpadding="0" cellspacing="0" border="0" width="100%">

                            <tr><td class="lpage"><a href="http://snapix.ru/landing" title="Заказать лендинг пейдж (landing page) под ключ: создание, разработка, создать, купить!">Заказать лендинг пейдж (landing page) под ключ: создание, разработка, создать, купить!</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/shop" title="Создать интернет магазин под ключ: создание, сделать, заказать, цена, с нуля! - Snapix.ru">Создать интернет магазин под ключ: создание, сделать, заказать, цена, с нуля! - Snapix.ru</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/support" title="Заказать поддержку сайтов.">Заказать поддержку сайтов.</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/seo" title="Заказать seo продвижение сайта в топ 10. Seo оптимизация сайтов.">Заказать seo продвижение сайта в топ 10. Seo оптимизация сайтов.</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/context" title="Заказать яндекс директ - настроить контекстную рекламу у профессионалов! Опыт - 5 лет!">Заказать яндекс директ - настроить контекстную рекламу у профессионалов! Опыт - 5 лет!</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/videonablyudenie-v-kolomne" title="Заказать видеонаблюдение в Коломне">Заказать видеонаблюдение в Коломне</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/remont" title="Ремонт компьютеров с выездом на дом в Коломне">Ремонт компьютеров с выездом на дом в Коломне</a></td></tr>

                        </table>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-md-4">
            <table cellpadding="0" cellspacing="0" border="0" width="100%">
               <tr valign="top">
                      <td class="lpart" colspan="99">
                                        <p class="h2 mt-xlg">Наши работы</p>

                        <table cellpadding="0" cellspacing="0" border="0" width="100%">

                            <tr><td class="lpage"><a href="http://snapix.ru/works/51" title="Проект Кухни-Милан">Проект Кухни-Милан</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/30" title="Проект Корпорация бетона">Проект Корпорация бетона</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/29" title="Проект НеРудСтрой">Проект НеРудСтрой</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/28" title="Проект Nadosayt">Проект Nadosayt</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/27" title="Проект Palla.su">Проект Palla.su</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/22" title="Проект Sound Bros">Проект Sound Bros</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/24" title="Проект Lengy">Проект Lengy</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/25" title="Проект Технокомплекс">Проект Технокомплекс</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/26" title="Проект Pneumoshop">Проект Pneumoshop</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/20" title="Проект Хорошие Люди">Проект Хорошие Люди</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/21" title="Проект Palla.su">Проект Palla.su</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/18" title="Проект Laitovo">Проект Laitovo</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/16" title="Проект &amp;laquo;Строймастерс&amp;raquo;">Проект &laquo;Строймастерс&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/9" title="Проект &amp;laquo;Коломенский Кремль&amp;raquo;">Проект &laquo;Коломенский Кремль&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/19" title="Проект Центр Суши">Проект Центр Суши</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/17" title="Проект &amp;laquo;Такси Независимость&amp;raquo;">Проект &laquo;Такси Независимость&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/13" title="Проект &amp;laquo;Уют-Стиль&amp;raquo;">Проект &laquo;Уют-Стиль&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/14" title="Проект Эльфарум">Проект Эльфарум</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/15" title="Проект &amp;laquo;Ракета&amp;raquo;">Проект &laquo;Ракета&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/12" title="Проект &amp;laquo;Трезвый водитель&amp;raquo;">Проект &laquo;Трезвый водитель&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/4" title="Проект Студия &amp;laquo;Декоратор&amp;raquo;">Проект Студия &laquo;Декоратор&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/5" title="Проект &amp;laquo;Золотые Руки &amp;raquo;">Проект &laquo;Золотые Руки &raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/3" title="Проект ТК &amp;laquo;ГИГАНТ&amp;raquo;&quot;">Проект ТК &laquo;ГИГАНТ&raquo;"</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/7" title="Проект &amp;laquo;Корея-Авто&amp;raquo;">Проект &laquo;Корея-Авто&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/8" title="Проект &amp;laquo;Дом Озерова&amp;raquo;">Проект &laquo;Дом Озерова&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/10" title="Проект &amp;laquo;Видеолюкс&amp;raquo;">Проект &laquo;Видеолюкс&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/1" title="Проект Сайт Е. Лобышевой">Проект Сайт Е. Лобышевой</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/11" title="Проект &amp;laquo;Первая Творческая Контора&amp;raquo;">Проект &laquo;Первая Творческая Контора&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/6" title="Проект &amp;laquo;Кит-Авто&amp;raquo;">Проект &laquo;Кит-Авто&raquo;</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/2" title="Проект &quot;Центргирозем+&quot;">Проект "Центргирозем+"</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/31" title="Проект Luminus">Проект Luminus</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/32" title="Проект Каркасные дома">Проект Каркасные дома</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/33" title="Проект &quot;Творец&quot;">Проект "Творец"</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/34" title="Проект Elfaonline.ru">Проект Elfaonline.ru</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/35" title="Проект СваиТехн">Проект СваиТехн</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/36" title="Проект Сппром">Проект Сппром</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/37" title="Проект ОзонПромТех">Проект ОзонПромТех</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/38" title="Проект Империя Доставки">Проект Империя Доставки</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/39" title="Проект 8BURGERS">Проект 8BURGERS</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/41" title="Проект Грузоперевозки24">Проект Грузоперевозки24</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/42" title="Проект Солидный Возраст">Проект Солидный Возраст</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/43" title="Проект Mazal-Shop.ru">Проект Mazal-Shop.ru</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/44" title="Проект a-osago.ru">Проект a-osago.ru</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/45" title="Проект Смета-Онлайн">Проект Смета-Онлайн</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/46" title="Проект Елегант-Лайф">Проект Елегант-Лайф</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/47" title="Проект Франшиза автомоек самообслуживания">Проект Франшиза автомоек самообслуживания</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/48" title="Проект Часы Вашерон Константин">Проект Часы Вашерон Константин</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/49" title="Проект Кукольный Чудо-домик для девочки">Проект Кукольный Чудо-домик для девочки</a></td></tr>
                            <tr><td class="lpage"><a href="http://snapix.ru/works/50" title="Проект Прометей-Топливо">Проект Прометей-Топливо</a></td></tr>
                        </table>
                    </td>
                </tr>

            </table>
        </div>

    </div>
</div>

