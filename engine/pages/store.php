<section class="page-header">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <ul class="breadcrumb">
                    <li><a href="#">Главная</a></li>
                    <li class="active">Купить готовый сайт</li>
                </ul>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <h1>Готовые решения по привлекательной цене</h1>
            </div>
        </div>
    </div>
</section>


<div class="container">
    <hr>
    <div class="row">
        <div class="col-md-3">
            <div data-plugin-sticky data-plugin-options='{"minWidth": 991, "containerSelector": ".container", "padding": {"top": 110}}'>
                <p class="h3">Категории</p>
                <ul class="list-group sort-source" data-sort-id="portfolio" data-option-key="filter">
                    <li class="list-group-item active" data-option-value="*"><a href="#">Все</a></li>
                    <?php foreach ($categories as $item){ ?>
                        <li class="list-group-item" data-option-value=".<?php print $item->id ?>"><a href="#"><?php print $item->name; ?></a></li>
                    <?php } ?>
                </ul>
            </div>
        </div>
        <div class="shop col-md-9">
            <ul class="products product-thumb-info-list sort-destination" data-sort-id="portfolio"  data-plugin-masonry data-plugin-options='{"layoutMode": "fitRows"}'>
                <?php foreach ($products as $item){ ?>
                    <li class="col-md-4 col-sm-6 col-xs-12 product <?php print $item->category_id ?>">
										<span class="product-thumb-info">
											<a href="shop-cart.html" class="add-to-cart-product">
												<span><i class="fa fa-shopping-cart"></i> Заказать</span>
											</a>
											<a href="shop-product-sidebar.html">
												<span class="product-thumb-info-image">
													<span class="product-thumb-info-act">
														<span class="product-thumb-info-act-left"><em>посмотреть</em></span>
														<span class="product-thumb-info-act-right"><em><i class="fa fa-plus"></i> подробнее</em></span>
													</span>
													<img alt="" class="img-responsive" src="img/products/product-1.jpg">
												</span>
											</a>
											<span class="product-thumb-info-content">
												<a href="/store/product/<?php print $item->id ?>">
													<h4 class="heading-primary"><?php print $item->name ?></h4>
													<span class="price">
														<del><span class="amount"><?php print $item->price * 1.3 ?> <i class="fa fa-rouble"></i> </span></del>
														<ins><span class="amount"><?php print $item->price ?> <i class="fa fa-rouble"></i></span></ins>
													</span>
												</a>
											</span>
										</span>
            </li>
                <?php } ?>
            </ul>
        </div>
    </div>

</div>

