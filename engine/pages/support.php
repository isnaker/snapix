        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li>Наши услуги</li>
                            <li class="active">Поддержка сайтов</li>
                        </ul>
                    </div>
                </div>
            </div>
        </section>
        <section id="head-block">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <img src="/img/services/support.png" class="center-block mt-xl" alt="">
                    </div>
                    <div class="col-md-8">
                        <div class="heading heading-border heading-middle-border heading-middle-border-center mb-none mt-lg">
                            <h1 class="mb-none">Поддержка и сопровождение <strong>сайтов</strong></h1>
                        </div>
                        <p class="text-center">Берем на себя <strong>решение всех проблем</strong> с Вашим сайтом всего за 4999 руб. в месяц*</p>
                        <div class="col-md-10 col-md-offset-1">
                            <p>Существуют люди, искренне полагающие, что созданный ранее сайт совершенно
                                не требует обслуживания. А средства, потраченные на информационную и техническую
                                поддержку, это пустая трата денег. </p>
                            <p class="mb-xlg">Бытует мнение, что запущенный в работу ресурс,
                                который обладает привлекательным дизайном и достаточно удобной системой
                                администрирования может существовать практически автономно. Зачастую это приводит
                                к появлению в интернете огромного множества web-страниц, которые не могут заинтересовать
                                ровным счетом никого. И соответственно не приносит никакого дохода своему владельцу.</p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section id="tarifs"
                 class="parallax section section-text-light section-parallax mt-none mb-none"
                 data-stellar-background-ratio="0.5" style="background-image: url('/img/backgrounds/mount.jpg');
                  background-position: 0% -27px;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                            <p class="h2 text-center">Стоимость <strong>поддержки</strong> сайтов</p>
                    </div>
                </div>
                <div class="row">
                    <div class="pricing-table princig-table-flat spaced no-borders">
                        <div class="col-md-3 col-sm-6">
                            <div class="plan">
                                <h3 data-value="basic">Базовая поддержка<span>4999 <i class="fa fa-rouble"></i> <small>/ месяц</small></span></h3>
                                <ul>
                                    <li><strong>Мониторинг</strong> работы сайта</li>
                                    <li><strong>Оповещения</strong> о сроках оплаты<br> домена и хостинга</li>
                                    <li><strong>Резервное</strong> копирование</li>
                                    <li><strong>Внесение</strong> изменений <br>в страницы сайта (2 рабочих часа)</li>
                                    <li><strong>Время реакции</strong> на запрос изменений - 1 рабочий день.</li>
                                    <li><button data-toggle="modal" data-target="#orderSupportModal" data-value="basic" class="btn btn-default">Заказать</button></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6 center">
                            <div class="plan most-popular">
                                <h3 data-value="extended">Расширенная поддержка
                                    <em class="desc">Самое популярное</em>
                                    <span>8999 <i class="fa fa-rouble"></i>
                                        <small>/ месяц</small>
                                    </span>
                                </h3>
                                <ul>
                                    <li><strong>Мониторинг</strong> работы сайта</li>
                                    <li><strong>Оповещения</strong> о сроках оплаты<br> домена и хостинга</li>
                                    <li><strong>Резервное</strong> копирование</li>
                                    <li><strong>Доработка структуры</strong> сайта</li>
                                    <li><strong>Внесение</strong> изменений в <br>страницы сайта (5 рабочих часов)</li>
                                    <li><strong>Время реакции</strong> - 2-4 часа.</li>
                                    <li><button data-toggle="modal" data-target="#orderSupportModal" data-value="extended" class="btn btn-primary"><strong>Заказать сейчас!</strong></button></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="plan">
                                <h3 data-value="premium">Премиальная поддержка<span>19 999 <i class="fa fa-rouble"></i> <small>/ месяц</small></span></h3>
                                <ul>
                                    <li><strong>Все опции базовых пакетов</strong> <i class="fa fa-plus"></i> </li>
                                    <li><strong>Базовая </strong>SEO-оптимизация</li>
                                    <li><strong>Рекомендации </strong> по наполнению сайта</li>
                                    <li><strong>Проработка </strong> дизайна сайта</li>
                                    <li><strong>Внесение</strong> изменений в <br>страницы сайта (8 рабочих часов)</li>
                                    <li><strong>Время реакции</strong> - 0,5-2 часа.</li>
                                    <li><button data-toggle="modal" data-target="#orderSupportModal" data-value="premium" class="btn btn-default"><strong>Заказать сейчас!</strong></button></li>
                                 </ul>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-6">
                            <div class="plan">
                                <h3 data-value="business">Сопровождение бизнеса<span>от 24 999 <i class="fa fa-rouble"></i> <small>/ месяц</small></span></h3>
                                <ul>
                                    <li><strong>Полное</strong> вовлечение в <br>бизнес-процесс.
                                        <strong>Доработка</strong>, <strong>оптимизация</strong> и продвижение в интернете и соцсетях.</li>
                                    <li><strong>Консультации</strong> маркетологов, программистов и
                                        специалистов по продвижению.</li>
                                    <li><strong>Личный</strong> менеджер. <br><strong>Время реации</strong> - Вы в приоритете.</li>
                                    <li><button data-toggle="modal" data-target="#orderSupportModal" data-value="business" class="btn btn-default"><strong>Заказать сейчас!</strong></button></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <p class="h1 text-center mt-xlg mb-lg">Вы получаете:</p>
        <div class="featured-boxes-full">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-group"></i>
                            <h4>Мониторинг доменного имени и хостинга:</h4>
                            <p>Сайт всегда будет доступен и вовремя оплачен. Мы следим за балансом и своевремено оповещаем Вас
                                о необходимости продления.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-film"></i>
                            <h4>Резервное копирование ресурса:</h4>
                            <p>Мы производим ежедневное создание резервных копий вашего сайта и всех данных.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-trophy"></i>
                            <h4>Оперативное устранение ошибок:</h4>
                            <p>Мы устраняем возникающие ошибки в работе сайта. Ваш сайт будет всегда работоспособен.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-cogs"></i>
                            <h4>Диагностика и лечение вирусов:</h4>
                            <p>Мы имеем большой опыт в защите и профилактике сайтов от вирусных и хакерских атак.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <div class="featured-boxes-full">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-group"></i>
                            <h4>Единый сервис обслуживания:</h4>
                            <p>Мы самостоятельно привлекаем необходимых специалистов для решения возникающих проблем.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-film"></i>
                            <h4>Доработка функционала</h4>
                            <p>В нашем штате программисты высокого уровня, способные решить любые задачи по доработке сайта.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-trophy"></i>
                            <h4>Продвижение</h4>
                            <p>Имея огромный опыт продвижения и раскрутки сайтов любого уровня, мы следим за позициями вашего сайта
                                в поисковой выдаче и выводим его в топ.</p>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="featured-box featured-box-primary featured-box-effect-3" style="height: 363px;">
                            <i class="icon-featured fa fa-cogs"></i>
                            <h4>Развитие</h4>
                            <p>Мы самостоятельно анализируем и предлагаем пути максимально продуктивного развития вашего бизнеса -
                                увеличение продаж, возврат и удержание клиента.</p>
                        </div>
                    </div>
                </div>
            </div>

        </div>

    <div class="container">
         <h2>Поддержка сайта</h2>
<p>Запуск сайта – это только первый шаг. Чтобы в дальнейшем интернет-ресурс приносил постоянную прибыль, а бизнес в сети оставался успешным, необходимо заниматься его развитием. И важной составляющей этого процесса является <strong>поддержка сайта</strong>. </p>
<h2>Техподдержка сайта</h2>
<p>Как показывает практика, многие сайты время от времени прекращают свою работу. Такая неприятность происходит по многим причинам:</p>
<ul>
<li>владелец сайта просто забыл продлить домен или своевременно оплатить услуги хостинга;</li>
<li>владелец доменного имени забыл доступ к панели регистратора;</li>
<li>владелец по ошибке проставил не те галочки;</li>
<li>сайт заражен вирусами или взломан.</li>
</ul>
<p>Сотрудники компании «Snapix», полагаясь на свой богатый опыт, предлагают решать возникающие проблемы самым простым способом – обратиться за помощью в указанную компанию. Ведь <strong>техническая поддержка сайта</strong> под руководством профессионалов обеспечит постоянную доступность и слаженную работу ресурса. В результате, владелец сайта сэкономит денежные средства и время.</p>
<h3>Информационная поддержка сайта</h3>
<p>Для того чтобы у потенциальных клиентов возникло доверие к сайту, он должен соответствовать многим параметрам:</p>
<ul>
<li>всегда быть актуальным. Для достижения данной цели следует регулярно обновлять на сайте информацию: размещать интересные статьи, новости, анонсы и так далее;</li>
<li>поддерживать работу сайта на планшетах и смартфонах, поскольку мобильный интернет развивается очень активно;</li>
<li>содержимое сайта должно легко читаться. Поэтому стоит размещать контент, составленный хорошим слогом. Не помешает также следить за размером шрифта и фоном;</li>
<li>сайт должен выглядеть аккуратно и привлекательно. Существует мнение, что в раскрутке сайта большая часть успеха зависит именно от дизайна.</li>
</ul>
<p>Понятно, что у предпринимателя вряд ли найдется время для освоения новых компьютерных знаний  и навыков для обеспечения информационной поддержки своего ресурса. Замечено, что владелец, как правило, многого не знает. На самом деле, обладать узкоспециализированными знаниями тем, кто сосредоточен на бизнесе, не обязательно. Поэтому <strong>разработка и техническая поддержка сайтов</strong> - дело профессиональных мастеров.
Интересует услуга «<strong>поддержка сайтов в Москве</strong>»? В таком случае, воспользуйтесь комплексным обслуживанием веб сайтов, обратившись в компанию «Snapix». Здесь всегда смогут устранить все технические неполадки.</p>
</p>
        <div class="row">
            <section class="call-to-action with-borders mb-xl">
                <div class="col-md-12">
                    <div class="call-to-action-content">
                        <h3>Остались вопросы? <strong>Мы поможем</strong> </h3>
                        <p>в выборе тарифа поддержки и ответим на все вопросы!</p>
                    </div>
                    <div class="call-to-action-btn">
                        <button data-toggle="modal" data-target="#callbackModal"
                                class="btn btn-lg btn-primary">Заказать звонок</button>
                    </div>
                </div>
            </section>
            </div>
        </div>
