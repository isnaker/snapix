        <div id="first_container">
            <div id="wallcam" class="visible-md visible-lg" style="
    background-image: url('/img/video_security/wallcamera.png');
    position: absolute;
    background-repeat: no-repeat;
    background-position: top left;
    background-size: contain;"></div>

            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <div class="with-borders white-opacity-bg padding10px push-top border-radius5px">
                            <h1 class="black-title del-margin-bot left-line">Установка и монтаж систем видеонаблюдения в Коломне!</h1>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-5 col-md-offset-3">
                        <div class="push-top10em">
                            <p class="text-center h2 white-title text-uppercase">
                                акция!
                            </p>
                            <p class="h2 text-center white-title">
                                Хороший Wi-Fi в Подарок
                                Он не "зависнет" НИКОГДА!
                            </p>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="form_top-bot_margin">
                            <div class="padding10px black-opacity-bg border-primary border-radius5px">
                                <p class="text-center white-text font-size1_2em">
                                    <strong>Получите проект  системы видеонаблюдения  для вашего коттеджа, дома  или отеля бесплатно!</strong>
                                </p>

                                <form class="form-horizontal" id="first_form">
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <i class="fa fa-user fa-2x form_icon"></i>
                                            <input type="text" id="get_project_name" class="form-control input-lg border-primary" placeholder="Ваше Имя">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-12">
                                            <i class="fa fa-phone fa-2x form_icon"></i>
                                            <input type="tel" id="get_project_tel" class="form-control input-lg border-primary" placeholder="Ваш контактный телефон">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-12">
                                            <button type="button" id="get_project_button" class="btn btn-default btn-block btn-lg btn-primary">Получить проект бесплатно</button>
                                        </div>
                                    </div>
                                    <p class="text-center white-text push-bot0_3em"><small>Ваши данные в безопасности и не передаются третьим лицам.</small></p>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="for">
            <div class="container">
                <p class="h1 text-black bold text-center font-size3em push-top">Видео докажет, что случилось на самом деле</p>
                <div class="row appear-animation" data-appear-animation="fadeIn">
                    <div class="text-center push-top">
                        <div class="col-md-4 col-sm-4">
                            <div class="">
                                <img src="/img/video_security/foricons/section-02-clock.png" alt=""/>
                                <p class="bold text-black h4">Для коттеджей и квартир</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="">
                                <img src="/img/video_security/foricons/section-02-calendar.png" alt=""/>
                                <p class="bold h4 text-black">Для ресторанов и кафе</p>
                            </div>
                        </div>
                        <div class="col-md-4 col-sm-4">
                            <div class="">
                                <img src="/img/video_security/foricons/section-02-eye.png" alt=""/>
                                <p class="bold h4 text-black">Для отелей и магазинов</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row appear-animation" data-appear-animation="fadeIn">
                    <div class="col-md-6 col-md-offset-3">
                        <p class="bold text-center text-black grey-opacity-bg push-top4em angle-border h4 padding10px">
                            Наши охранные системы не оставляют места воображению. Поэтому наши клиенты всегда уверены в своей безопасности и сохранности личного имущества.
                        </p>
                    </div>
                </div>
            </div>
            <div class="container">
                <h3 class="h1 white-text bold text-center font-size3em push-top2_5em push-bottom">«Snapix» – это:</h3>

                <div class="row push-bottom">
                    <div class="text-center">
                        <div class="col-md-4  col-sm-4">
                            <img src="/img/video_security/what_icons/section-03-lock.png" alt=""/>
                            <p class="text-center h3 white-text">
                                Цена точно по смете
                            </p>
                        </div>
                        <div class="col-md-4  col-sm-4">
                            <img src="/img/video_security/what_icons/section-03-camera.png" alt=""/>
                            <p class="text-center h3 white-text">
                                Разработка фото проекта
                            </p>
                        </div>
                        <div class="col-md-4  col-sm-4">
                            <img src="/img/video_security/what_icons/section-03-balaklava.png" alt=""/>
                            <p class="text-center h3 white-text">
                                Современное оборудование
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="watchman">
            <div class="container">
                <h5 class="h1 push-bottom push-top text-center">Электронный сторож от «Snapix» –
                    <span class="text-primary">Чего НЕ ждут грабители?</span></h5>

                <div class="push-top">
                    <div class="row">
                        <div class="col-md-2  col-sm-4"><img src="/img/video_security/watchman/watchman_icon/1.png" alt=""/></div>
                        <div class="col-md-6  col-sm-8">
                            <p class="h3 push-top2em">Сработала сигнализация ?</p>
                        </div>
                        <div class="col-md-4">
                            <img src="/img/video_security/watchman/reaver.png" class="img-responsive position-absolute visible-lg visible-md appear-animation" data-appear-animation="fadeIn" alt=""/>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-4 col-md-offset-1"><img src="/img/video_security/watchman/watchman_icon/2.png" alt=""/></div>
                        <div class="col-md-9  col-sm-8">
                            <p class="h3 push-top2em">И что сделает видеонаблюдение ?</p>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-2 col-sm-4 col-md-offset-2"><img src="/img/video_security/watchman/watchman_icon/3.png" alt=""/></div>
                        <div class="col-md-8  col-sm-8">
                            <p class="h3 push-top2em">Обычная система просто запишет видео...</p>
                        </div>
                    </div>
                    <div class="row push-bottom7em">
                        <div class="col-md-2  col-sm-4 col-md-offset-3"><img src="/img/video_security/watchman/watchman_icon/4.png" alt=""/></div>
                        <div class="col-md-7  col-sm-8">
                            <p class="h3 push-top2em">Необходима более продуманная система!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div>
            <div class="container">
                <div class="col-md-10 col-md-offset-1">
                    <h6 class="h1 text-center head-push-top-1em_bordered">
                        Стандартная сигнализация здесь не поможет
                    </h6>
                </div>
                <div class="row">
                    <div class="col-md-5 visible-lg visible-md"><img src="/img/video_security/man.png" class="img-responsive appear-animation" data-appear-animation="fadeInLeft" alt=""/></div>
                    <div class="col-md-7">

                        <p class="h4 left-line push-top">Мы создали уникальную систему!</p>

                        <p class="h4 left-line push-top">Датчики охраны периметра + камеры видеонаблюдения + светодиодное освещение + сирена</p>

                        <p class="h4 left-line push-top">Злоумышленник будет в шоке если решит пересечь периметр!</p>

                        <p class="h4 left-line push-top">При сработке системы Вам на телефон сразу придёт письмо с фото места нарушения</p>

                        <p class="h4 left-line push-top">Вы увидите на что сработала сигнализация!</p>

                        <p class="h3 push-top text-center angle-border padding2em">«Snapix»: <span class="text-primary">Выставьте параметры безопасности – «на максимум»!</span></p>
                    </div>
                </div>
            </div>
        </div>
        <div id="smart_system">
            <div class="container">
                <h5 class="h1 text-center white-text push-top4em">Интеллектуальные системы охраны
                    <span class="text-primary">и другие выгоды вашего сотрудничества с нами:</span></h5>
                <div class="row">
                    <div class="push-top push-bottom">
                        <div class="col-md-6  col-sm-6">
                            <div class="feature-box feature-box-style-5">
                                <div class="col-md-4 col-sm-3">
                                    <img class="img-responsive" src="/img/video_security/smart_system/icons/1.png" alt=""/>
                                </div>
                                <div class="feature-box-info col-md-8  col-sm-9" style="padding-left: 0">
                                    <p class="h4 mb-sm text-uppercase text-primary">Высокая детализация видеосъемки</p>
                                    <p class="del-margin-bot white-text">Мы настоятельно рекомендуем цифровые IP камеры. Почему цифровые?</p>
                                    <ul class="white-text">
                                        <li>Качество видео на 200% выше</li>
                                        <li>Нет помех и шумов в изображении</li>
                                        <li>Стабильная работа</li>
                                        <li>Меньше проводки</li>
                                    </ul>
                                    <p class="white-text">Дороже всего на 14%</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="feature-box feature-box-style-5">
                                <div class="col-md-4  col-sm-3">
                                    <img class="img-responsive" src="/img/video_security/smart_system/icons/2.png" alt=""/>
                                </div>
                                <div class="feature-box-info col-md-8 col-sm-9" style="padding-left: 0">
                                    <p class="mb-sm h4 text-primary text-uppercase">Качественное оборудование</p>
                                    <p class="mb-lg white-text">Используем проверенное оборудование.
                                        10 лет мы подбираем только лучшее по соотношению
                                        ЦЕНА / КАЧЕСТВО</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="push-top">
                        <div class="col-md-6 col-sm-6">
                            <div class="feature-box feature-box-style-5">
                                <div class="col-md-4  col-sm-3">
                                    <img class="img-responsive" src="/img/video_security/smart_system/icons/3.png" alt=""/>
                                </div>
                                <div class="feature-box-info col-md-8  col-sm-9" style="padding-left: 0">
                                    <p class="h4 text-primary mb-sm text-uppercase">Оперативность монтажа</p>
                                    <p class="mb-lg white-text del-margin-bot">ПРОЕКТ – 1 день</p>
                                    <p class="mb-lg white-text">РАБОТЫ – 7 – 14 дней</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="feature-box feature-box-style-5">
                                <div class="col-md-4  col-sm-3">
                                    <img class="img-responsive" src="/img/video_security/smart_system/icons/4.png" alt=""/>
                                </div>
                                <div class="feature-box-info col-md-8 col-sm-9" style="padding-left: 0">
                                    <p class="h4 text-primary mb-sm text-uppercase">Ответственность перед Вами</p>
                                    <p class="mb-lg white-text">НА ВЕСЬ ПРОЕКТ МЫ ДАЁМ ГАРАНТИЮ 365 ДНЕЙ
                                        И даже после окончания этого срока мы поможем по всем вопросам.
                                        ЦЕНИМ ЛОЯЛЬНОСТЬ КАЖДОГО КЛИЕНТА</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="push-top6em">
                        <div class="col-md-6 col-sm-6">
                            <div class="feature-box feature-box-style-5">
                                <div class="col-md-4 col-sm-3">
                                    <img class="img-responsive" src="/img/video_security/smart_system/icons/5.png" alt=""/>
                                </div>
                                <div class="feature-box-info col-md-8 col-sm-9" style="padding-left: 0">
                                    <p class="h4 text-primary mb-sm text-uppercase">Уникальные системы охраны. Такого не делает никто!</p>
                                    <p class="mb-lg white-text">Если ночью сработает сигнализация Вы посмотрите фото на почте телефона и будете
                                        ТОЧНО ЗНАТЬ ЧТО И ГДЕ ПРОИЗОШЛО.
                                        Если это кошка на фото, то можно спать спокойно …</p>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="feature-box feature-box-style-5">
                                <div class="col-md-4 col-sm-3">
                                    <img class="img-responsive" src="/img/video_security/smart_system/icons/6.png" alt=""/>
                                </div>
                                <div class="feature-box-info col-md-8 col-sm-9" style="padding-left: 0">
                                    <p class="h4 text-primary mb-sm text-uppercase">Адекватные цены</p>
                                    <p class="mb-lg white-text">Уникальность наших решений в правильной настройке оборудования, а не в закупке всего самого дорогого.
                                        Соберем лучшую систему охраны в рамках бюджета !</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="button" class="btn push-bot1_618em btn-lg btn-primary push-top4em call_modalcallback_button" data-toggle="modal" data-target="#callbackModal">Позаботиться о своей безопасности</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
        <div id="video-example">
            <div class="container">

                <h4 class="h1 text-center push-top push-bot1_618em">Смотри, что еще умеет наше оборудование !</h4>

                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <p class="h3 text-center">Тайм компрессор</p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="360" height="202" src="https://www.youtube.com/embed/-rXfHhZ6fIc" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <p class="h3 text-center">Обработка тревог</p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="360" height="202" src="https://www.youtube.com/embed/y-ZBslISTuk" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <p class="h3 text-center">Поиск с интеллектом</p>
                        <div class="embed-responsive embed-responsive-16by9">
                           <iframe width="360" height="202" src="https://www.youtube.com/embed/wxicoCU9nRE" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-sm-4">
                        <p class="h3 text-center">Надёжность записи</p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="360" height="202" src="https://www.youtube.com/embed/Dlt-onwPQ9Q" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <p class="h3 text-center">Интерактивная карта</p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="360" height="202" src="https://www.youtube.com/embed/8H_VFL3YhGk" allowfullscreen></iframe>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <p class="h3 text-center">Видеодетектор</p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="360" height="202" src="https://www.youtube.com/embed/7JNf73pbQjA" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4">
                        <p class="h3 text-center">Видеоархив</p>
                        <div class="embed-responsive embed-responsive-16by9">
                            <iframe width="360" height="202" src="https://www.youtube.com/embed/JihXaetkhP0" allowfullscreen></iframe>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center push-top alternative-font font-size2_2em push-top">Современные технологии видеонаблюдения</p>
                    </div>
                    <div class="col-md-12">
                        <div class="text-center">
                            <button type="button" class="btn push-bot1_618em btn-lg btn-primary call_modalcallback_button" data-toggle="modal" data-target="#callbackModal">Снизить риск воровства на 70-100%</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
		
		
        <div id="getting">
            <div class="container">
                <h3 class="h1 text-center white-text push-bot1_618em push-top">Уникальная система видеонаблюдения:
                    <span class="text-primary">что Вы получаете?</span></h3>
                <div class="row">
                    <div class="col-md-6  col-sm-6">
                        <div class="white-text margin-right2em">
                            <p class="h3 text-right push-top4em appear-animation" data-appear-animation="fadeInLeft">Установка цифровых камер .02</p>

                            <p class="h3 text-right push-top4em appear-animation" data-appear-animation="fadeInLeft">Яркое светодиодное освещение

                                для фотофиксации и отпугивания .04</p>

                            <p class="h3 text-right push-top4em appear-animation" data-appear-animation="fadeInLeft">Помощь в обучении .06</p>

                        </div>
                    </div>
                    <div class="position-absolute right47_5pr">
                        <img src="/img/video_security/gettingtree.png" alt=""/>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <div class="white-text margin-left2em">
                            <p class="h3 appear-animation" data-appear-animation="fadeInRight">01. Расчет сметы и фото проект

                                размещения оборудования</p>

                            <p class="h3 push-top3_2em appear-animation" data-appear-animation="fadeInRight">03. Установка ИК датчиков контроля

                                периметра</p>

                            <p class="h3 push-top3_7em appear-animation" data-appear-animation="fadeInRight">05. Настройка просмотра через Internet</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 col-md-offset-4">
                        <div class="angle-border padding2em white-text push-top6em">
                            <p class="h3 text-center">Рассчитываем проект системы безопасности <span class="text-primary">всего за 1 день!</span></p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                        <div class="text-center push-top12em padding10px white-text border-radius5px grey-opacity-bg">
                            <p class="h1 bold">Акция!</p>

                            <p class="h3">Установка и настройка Wi Fi сети в Подарок!
                                Оборудование от мирового лидера Mikrotik.
                                Wi-Fi который НИКОГДА не "зависает" !</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center push-top4em push-bottom14em push-bottom7em">
                            <button type="button" class="btn btn-lg btn-primary call_modalcallback_button"  data-toggle="modal" data-target="#callbackModal">Получить Wi-Fi в подарок</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="who_work">
            <div class="container">
                <h2 class="h1 text-center push-top push-bot1_618em">Как мы работаем?

                    <span class="text-primary">7 дней от проекта до монтажа:</span></h2>
                <div class="row appear-animation" data-appear-animation="fadeInUp">
                    <div class="col-md-4 col-sm-4">
                        <div class="text-center"><img src="/img/video_security/who_works_icons/1.png" alt=""/>

                            <p class="text-center h4">Оставляете заявку на сайте или связываетесь с нами по телефону</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="text-center"><img src="/img/video_security/who_works_icons/2.png" alt=""/>

                            <p class="text-center h4">Выезд нашего специалиста на объект для осмотра</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="text-center"><img src="/img/video_security/who_works_icons/3.png" alt=""/>

                            <p class="text-center h4">Получаете подробный проект системы наблюдения и/или охраны</p>
                        </div>
                    </div>
                </div>
                <div class="row push-top appear-animation" data-appear-animation="fadeInUp">
                    <div class="col-md-4 col-sm-4">
                        <div class="text-center"><img src="/img/video_security/who_works_icons/4.png" alt=""/>

                            <p class="text-center h4">Выплачиваете аванс</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="text-center"><img src="/img/video_security/who_works_icons/5.png" alt=""/>

                            <p class="text-center h4">Монтируем оборудование</p>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-4">
                        <div class="text-center"><img src="/img/video_security/who_works_icons/6.png" alt=""/>
                            <p class="text-center h4">Проверяете результаты оплаты и выплачиваете остаток</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="report">
            <div class="container">
                <h5 class="h1 text-center push-bot1_618em">Отзывы наших клиентов</h5>

                <div class="row">
                    <div class="col-md-7 col-sm-7">
                        <blockquote class="testimonial">
                            <p>От работы этой фирмы моя семья в полном восторге. Уже пользуемся их услугами больше года
                                и ни разу не пожалели. Стоит, конечно, дорого, но защита лучше всего! Решили установить
                                себе видеонаблюдение после того, как нас обчистили. Зато теперь я спокойна со всех сторо
                                н дом, как на ладони и все записывается в любое время суток! Также огромное преимущество
                                , что камеры срабатывают на любое движение и днем и ночью! Работает без сбоев и качестве
                                нно! Спасибо Вам большое, наилучших пожеланий!
                            </p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                            <div class="img-thumbnail img-thumbnail-small">
                                <img class="img-responsive" src="/img/video_security/testimonials/EMtkBfdBreI100x133.jpg" alt="">
                            </div>
                            <p><strong>Валерия</strong></p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5">
                        <blockquote class="testimonial">
                            <p>Перечислю ряд преимуществ, почему обратился именно в Snapix. Во-первых, это - тщательное
                                консультирование по телефону и в самом офисе. Во-вторых удобное расположение в центре го
                                рода. В-третьих, мне всё показали, рассказали и научили пользоваться! В общем, приобрёл
                                6 камер в подъезд и 2 камеры на улицу. Ребята молодцы, всё быстро установили.
                            </p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                            <div class="img-thumbnail img-thumbnail-small">
                                <img class="img-responsive"  src="/img/video_security/testimonials/pdtYwaKXpQk100x100.jpg" alt="">
                            </div>
                            <p><strong>Анатолий</strong></p>
                        </div>
                    </div>
                </div>
                <div class="row push-top">
                    <div class="col-md-5 col-sm-5">
                        <blockquote class="testimonial">
                            <p>Пришлось обратиться в Вашу фирму по просьбе отца, нужны были видеокамеры для дачи, а в эт
                                ом вопросе я вообще ничего не разбираюсь. Менеджер Евгений помог на все 150%. Очень приз
                                нательна за помощь, консультацию и доброжелательность!!! Все работает, папа остался дово
                                лен :)
                            </p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                            <div class="img-thumbnail img-thumbnail-small">
                                <img class="img-responsive"  src="/img/video_security/testimonials/kQbQqZjrwVI100x133.jpg" alt="">
                            </div>
                            <p><strong>Дарья</strong></p>
                        </div>
                    </div>
                    <div class="col-md-7 col-sm-7">
                        <blockquote class="testimonial">
                            <p>Сейчас такие времена, что волей, не волей хочется обезопасить себя и свою семью. Для этог
                                о мы поставили Цветную уличную камера видеонаблюдения. Да и просто, смотреть, кто пришел
                                . Так как мы живем в частном доме, забор и калитка, высокие и если кто позвонил в калитк
                                у, из окна дома, не чего не видно. Подключили мы, камеру к обычному телевизору, который
                                стоит на кухне. А, так как я большую часть времени провожу именно там, мне постоянно вид
                                но, что происходит на улице, около двора и дети под присмотром. Это очень удобно!!!
                            </p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                            <div class="img-thumbnail img-thumbnail-small">
                                <img class="img-responsive"  src="/img/video_security/testimonials/vQNYbA1iF_g100x150.jpg" alt="">
                            </div>
                            <p><strong>Александра</strong></p>
                        </div>
                    </div>
                </div>
                <div class="row push-top">
                    <div class="col-md-7 col-sm-7">
                        <blockquote class="testimonial">
                            <p>В данную компанию пришлось обратиться, так как до этого нашла установку видеонаблюдения ч
                                ерез авито. Заказала-приехали, поставили, потом через неделю все перестало работать, зво
                                нила много раз, телефон у установщиков был постоянно выключенным.. Звонила около 3 месяц
                                ев. В итоге нашла эту компанию через яндекс, объяснила ситуацию - ребята с snapix приеха
                                ли все исправили, как выяснилось блоки питания оказались подобраны не правильно (заменил
                                и блоки питания), не хватало мощности, Теперь все хорошо. Спасибо!!!!!!!!!!!
                            </p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                            <div class="img-thumbnail img-thumbnail-small">
                                <img class="img-responsive"  src="/img/video_security/testimonials/zzeWcWEGHFI100x133.jpg" alt="">
                            </div>
                            <p><strong>Евгения</strong></p>
                        </div>
                    </div>
                    <div class="col-md-5 col-sm-5">
                        <blockquote class="testimonial">
                            <p>Добрый день! Хотел бы выразить благодарность Вашей компании и всем сотрудникам за внимате
                                льное отношение ко мне как к покупателю: и проконсультировали и посоветовали, всё слажен
                                о и четко, и самое главное порадовали цены. Спасибо Вам.
                            </p>
                        </blockquote>
                        <div class="testimonial-arrow-down"></div>
                        <div class="testimonial-author">
                            <div class="img-thumbnail img-thumbnail-small">
                                <img class="img-responsive"  src="/img/video_security/testimonials/z_e87a44a2100x64.jpg" alt="">
                            </div>
                            <p><strong>Александр</strong></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="solve_problems">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 col-md-offset-1">
                        <p class="white-text h2 angle-border padding10px text-center">
                            Для любой задачи охраны ЕСТЬ РЕШЕНИЕ ! Современные технологии сделают БЕЗОПАСНЫМ ЛЮБОЙ ОБЪЕКТ!
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="text-center push-top2em">
                            <button type="button" class="btn btn-lg btn-primary call_modalcallback_button" data-toggle="modal" data-target="#callbackModal">Решить проблему безопасности</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="callback_block">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 col-md-offset-3">
                        <p class="h3 left-line push-top push-bot1_618em">
                            Желаете получить бесплатную консультацию по вопросам безопасности Вашей недвижимости или бизнеса?
                        </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4 visible-lg visible-md">
                        <img src="/img/video_security/front_man.png" class="push-top-10em img-responsive position-absolute appear-animation" data-appear-animation="fadeInUp" alt=""/>
                    </div>
                    <div class="col-md-4 col-md-offset-3">
                        <div class="padding10px dark-black-opacity-bg border-radius5px border-primary">
                            <p class="text-center white-text font-size1_2em">
                                <strong>Закажите обратный звонок, и мы свяжемся с Вами в любое удобное время!</strong>
                            </p>

                            <form class="form-horizontal" id="callback_form">
                                <!-- role="form"-->
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <i class="fa fa-user fa-2x form_icon"></i>
                                        <input type="text" id="callback_name" class="form-control border-primary input-lg" placeholder="Ваше Имя">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <i class="fa fa-phone fa-2x form_icon"></i>
                                        <input type="tel" class="form-control input-lg border-primary" id="callback_tel" placeholder="Ваш контактный телефон">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <button type="button" id="callback_button" class="btn btn-default btn-block btn-lg btn-primary">Заказать обратный звонок</button>
                                    </div>
                                </div>
                                <p class="text-center"><small>Ваши данные в безопасности и не передаются третьим лицам.</small></p>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include_once($_SERVER["DOCUMENT_ROOT"] . "/engine/parts/footer.php"); ?>

<!--modal callback-->
<div class="modal fade" id="callbackModal"  tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content push-bottom">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <p class="modal-title h4" id="myModalLabel">Оставьте заявку</p>
            </div>

            <form id="callback_modal_form" class="contactForm_modal">
                <div class="modal-body">
                    <div class="row">
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Введите Ваше имя</label>
                                <input data-msg-required="Пожалуйста, введите Ваше имя." type="text" class="form-control" id="client_name">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-12">
                                <label>Введите Ваш номер телефона</label>
                                <input data-msg-required="Пожалуйста, введите Ваш телефон." type="tel" class="form-control" id="client_phone">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" id="button_callback_modal" class="btn btn-primary pull-left" data-loading-text="Loading...">Заказать звонок</button>
                    <button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
                </div>
            </form>
        </div>

        <div class="alert alert-success hidden" id="contactSuccess">
            <strong>Успех!</strong> Ваше сообщение было отправлено!
        </div>

        <div class="alert alert-danger hidden" id="contactError">
            <strong>Ошибка!</strong> Что-то пошло не так и Ваше сообщение не отправлено
        </div>

    </div>
</div>