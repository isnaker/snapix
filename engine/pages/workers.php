<?php
/**
 * Created by PhpStorm.
 * User: Denis
 * Date: 06.07.2016
 * Time: 15:10
 */

include_once($_SERVER['DOCUMENT_ROOT']."/engine/App.php");
$app = new App();

if(isset($_GET['id'])){
    $id=$_GET['id'];
    $stmt = $app->db->connector->prepare("SELECT *  FROM `workers` WHERE `id`='".$id."' LIMIT 1");
    $stmt->execute();
    $stmt->setFetchMode(PDO::FETCH_OBJ);
    $worker = $stmt->fetch();
    ?>


    <!DOCTYPE html>
    <html>
    <head>

        <!-- Basic -->
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">

        <title><?php print $worker->name;?></title>

        <meta name="keywords" content="HTML5 Template" />
        <meta name="description" content="Porto - Responsive HTML5 Template">
        <meta name="author" content="okler.net">

        <!-- Favicon -->
        <link rel="shortcut icon" href="img/favicon.ico" type="image/x-icon" />
        <link rel="apple-touch-icon" href="img/apple-touch-icon.png">

        <!-- Mobile Metas -->
        <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

        <!-- Web Fonts  -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

        <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/css.php"); ?>

        <!-- Head Libs -->
        <script src="vendor/modernizr/modernizr.min.js"></script>
        <style>
            html #header.header-semi-transparent, html #header.header-semi-transparent-light {
                min-height: 85px !important;
                width: 100%;
                background: rgba(0, 0, 0, 0.89);
                position: absolute;
                top: 0;
            }
            div.main { margin-top: 85px}

            .thumb-info-caption .thumb-info-caption-text, .thumb-info-caption p {
                height: 160px;
            }
        </style>
    </head>
    <body>

    <div class="body">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/engine/parts/header.php'); ?>
        <div role="main" class="main">

            <section class="page-header">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <ul class="breadcrumb">
                                <li><a href="/">Главная</a></li>
                                <li><a href="/workers.php">Сотрудники</a></li>
                                <li class="active"><?php print $worker->name;?></li>
                            </ul>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <h1>Сотрудники</h1>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <!-- <div class="owl-carousel owl-theme" data-plugin-options='{"items": 1, "margin": 10}'>
                            <div>
									<span class="img-thumbnail">
										<img alt="" height="300" class="img-responsive" src="img/team/<?php print $worker->img; ?>">
									</span>
                            </div>
                            <div>
									<span class="img-thumbnail">
										<img alt="" height="300" class="img-responsive" src="img/team/team-9.jpg">
									</span>
                            </div>
                        </div>-->
                        <img alt="" height="300" class="img-responsive img-thumbnail" src="/img/team/<?php print $worker->img; ?>">
                    </div>
                    <div class="col-md-8">
                        <?php $splitname = explode(" ",$worker->name); ?>
                        <h2 class="mb-none"><?php print $splitname[0]; ?> <strong><?php print $splitname[1]; ?></strong></h2>
                        <h4 class="heading-primary"><?php print $worker->post; ?></h4>

                        <hr class="solid">

                        <!--<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur pellentesque neque eget diam posuere porta. Quisque ut nulla at nunc <a href="#">vehicula</a> lacinia. Proin adipiscing porta tellus, ut feugiat nibh adipiscing sit amet. In eu justo a felis faucibus ornare vel id metus. Vestibulum ante ipsum primis in faucibus.</p>

                        <ul class="list list-icons">
                            <li><i class="fa fa-check"></i> Fusce sit amet orci quis arcu vestibulum vestibulum sed ut felis.</li>
                            <li><i class="fa fa-check"></i> Phasellus in risus quis lectus iaculis vulputate id quis nisl.</li>
                            <li><i class="fa fa-check"></i> Iaculis vulputate id quis nisl.</li>
                        </ul>-->
                        <?php print $worker->description; ?>

                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <hr>
                    </div>
                </div>
                <div class="row mt-xlg">
                    <?php $skills=json_decode($worker->skills);

                    foreach($skills as $item) {?>

                        <div class="col-md-3">
                            <div class="circular-bar">
                                <div class="circular-bar-chart" data-percent="<?php print $item->proc; ?>" data-plugin-options='{"barColor": "<?php print $item->color; ?>"}'>
                                    <strong><?php print $item->name; ?></strong>
                                    <label><span class="percent"><?php print $item->proc; ?></span>%</label>
                                </div>
                            </div>
                        </div>

                        <?php }?>
                    <!--<div class="col-md-3">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="75" data-plugin-options='{"barColor": "#E36159"}'>
                                <strong>HTML/CSS</strong>
                                <label><span class="percent">75</span>%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="85" data-plugin-options='{"barColor": "#0088CC", "delay": 300}'>
                                <strong>Design</strong>
                                <label><span class="percent">85</span>%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="60" data-plugin-options='{"barColor": "#2BAAB1", "delay": 600}'>
                                <strong>WordPress</strong>
                                <label><span class="percent">60</span>%</label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="circular-bar">
                            <div class="circular-bar-chart" data-percent="95" data-plugin-options='{"barColor": "#353535", "delay": 900}'>
                                <strong>Photoshop</strong>
                                <label><span class="percent">95</span>%</label>
                            </div>
                        </div>
                    </div>-->
                </div>
            </div>

            <section class="parallax section section-parallax section-center" data-stellar-background-ratio="0.5" style="background-image: url(img/parallax-transparent.jpg);">
                <div class="container">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="owl-carousel owl-theme nav-bottom rounded-nav mt-lg mb-none" data-plugin-options='{"items": 1, "loop": false}'>
                                <div>
                                    <div class="col-md-12">
                                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                                            <blockquote>
                                                <p>Joe Doe is the smartest guy I ever met, he provides great tech service for each template and allows me to become more knowledgeable as a designer.</p>
                                            </blockquote>
                                            <div class="testimonial-author">
                                                <p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="col-md-12">
                                        <div class="testimonial testimonial-style-6 testimonial-with-quotes mb-none">
                                            <blockquote>
                                                <p>He provides great tech service for each template and allows me to become more knowledgeable as a designer.</p>
                                            </blockquote>
                                            <div class="testimonial-author">
                                                <p><strong>John Smith</strong><span>CEO & Founder - Okler</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <div class="container">
                <div class="row">
                    <div class="col-md-12">

                        <h4 class="mt-xlg mb-none text-uppercase">My <strong>Work</strong></h4>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.</p>

                        <div class="row">

                            <ul class="portfolio-list">
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="portfolio-item">
                                        <a href="portfolio-single-project.html">
												<span class="thumb-info">
													<span class="thumb-info-wrapper">
														<img src="img/projects/project.jpg" class="img-responsive" alt="">
														<span class="thumb-info-title">
															<span class="thumb-info-inner">Random Chars</span>
															<span class="thumb-info-type">Website</span>
														</span>
														<span class="thumb-info-action">
															<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
														</span>
													</span>
												</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="portfolio-item">
                                        <a href="portfolio-single-project.html">
												<span class="thumb-info">
													<span class="thumb-info-wrapper">
														<img src="img/projects/project-1.jpg" class="img-responsive" alt="">
														<span class="thumb-info-title">
															<span class="thumb-info-inner">Full House</span>
															<span class="thumb-info-type">Website</span>
														</span>
														<span class="thumb-info-action">
															<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
														</span>
													</span>
												</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="portfolio-item">
                                        <a href="portfolio-single-project.html">
												<span class="thumb-info">
													<span class="thumb-info-wrapper">
														<img src="img/projects/project-2.jpg" class="img-responsive" alt="">
														<span class="thumb-info-title">
															<span class="thumb-info-inner">The Fly</span>
															<span class="thumb-info-type">Logo</span>
														</span>
														<span class="thumb-info-action">
															<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
														</span>
													</span>
												</span>
                                        </a>
                                    </div>
                                </li>
                                <li class="col-md-3 col-sm-6 col-xs-12">
                                    <div class="portfolio-item">
                                        <a href="portfolio-single-project.html">
												<span class="thumb-info">
													<span class="thumb-info-wrapper">
														<img src="img/projects/project-3.jpg" class="img-responsive" alt="">
														<span class="thumb-info-title">
															<span class="thumb-info-inner">Run Away</span>
															<span class="thumb-info-type">Brand</span>
														</span>
														<span class="thumb-info-action">
															<span class="thumb-info-action-icon"><i class="fa fa-link"></i></span>
														</span>
													</span>
												</span>
                                        </a>
                                    </div>
                                </li>
                            </ul>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/footer.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/scripts.php"); ?>
    </body>
    </html>



<?php }
else{
    $workers = $app->db->connector->query("SELECT * FROM `workers`");
    $workers->setFetchMode(PDO::FETCH_OBJ);
?>

<!DOCTYPE html>
<html lang="ru-RU">
<head>

    <!-- Basic -->
    <meta charset="utf-8">
    <meta name="google-site-verification" content="roI4WhDGEBeE6qTZ8QqUF1LCAb2Ck_xvxCopHxZjl3c" />
    <meta name="keywords" content="вебстудия создать сайт цена создание для бизнеса лендинг визитка продающий одностраничник интернет магазин"/>
    <meta name="description" content="Создание сайтов для бизнеса. Создание продающих сайтов и готовых решений для бизнеса в Коломне и Москве." />

    <?php include_once($_SERVER['DOCUMENT_ROOT']."/mtags.php"); ?>

    <title>Создание сайтов для бизнеса. Создание продающих сайтов. Вебстудия Snapix</title>

    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="76x76" href="/img/apple-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon-16x16.png">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="theme-color" content="#ffffff">

    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <!-- Web Fonts  -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700,800%7CShadows+Into+Light" rel="stylesheet" type="text/css">

    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/css.php"); ?>
    <!-- Head Libs -->
    <script src="vendor/modernizr/modernizr.min.js"></script>
    <style>
        html #header.header-semi-transparent, html #header.header-semi-transparent-light {
            min-height: 85px !important;
            width: 100%;
            background: rgba(0, 0, 0, 0.89);
            position: absolute;
            top: 0;
        }
        div.main { margin-top: 85px}

        .thumb-info-caption .thumb-info-caption-text, .thumb-info-caption p {
            height: 160px;
        }
    </style>
</head>
<body>

<link rel="stylesheet" href="https://cdn.callbackkiller.com/widget/cbk.css">
<script type="text/javascript" src="https://cdn.callbackkiller.com/widget/cbk.js?wcb_code=fc70afff955b43b13d5ae1622fd5e25c" charset="UTF-8" async></script>


<div class="body">
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/engine/parts/header.php'); ?>
    <div role="main" class="main">

        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="#">Главная</a></li>
                            <li class="active">Сотрудники</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1>Сотрудники</h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="container">
            <h2>Наша <strong>Команда</strong></h2>

            <ul class="nav nav-pills sort-source" data-sort-id="team" data-option-key="filter">
                <li data-option-value="*" class="active"><a href="#">Показать всех</a></li>
                <li data-option-value=".development"><a href="#">Разработчики</a></li>
                <li data-option-value=".marketing"><a href="#">Маркетологи</a></li>
            </ul>
            <hr>

            <div class="rw">

                <ul class="team-list sort-destination" data-sort-id="team">
                    <?php foreach($workers as $item){?>
                    <li class="col-md-3 col-sm-6 col-xs-12 isotope-item <?php print $item->class;?>">
								<span class="thumb-info thumb-info-hide-wrapper-bg mb-xlg">
									<span class="thumb-info-wrapper">
										<div>
                                            <img src="img/team/<?php print $item->img; ?>" class="img-responsive" alt="">
											<span class="thumb-info-title">
												<span class="thumb-info-inner"><?php print $item->name; ?></span>
												<span class="thumb-info-type"><?php print $item->post; ?></span>
											</span>
                                        </div>
									</span>
									<span class="thumb-info-caption">
										<span class="thumb-info-caption-text"><?php print $item->brif; ?></span>
										<span class="thumb-info-social-icons">
											<a target="_blank" href="https://www.vk.com/<?php print $item->vkid;?>"><i class="fa fa-vk"></i><span>Вконтакте</span></a>
										</span>
									</span>
								</span>
                    </li>
                    <?php }?>
                </ul>
            </div>

        </div>

    </div>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/footer.php"); ?>
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/scripts.php"); ?>
</body>
</html>
<?php }?>