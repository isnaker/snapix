        <section class="page-header">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <ul class="breadcrumb">
                            <li><a href="/">Главная</a></li>
                            <li><a href="/services">Наши услуги</a></li>
                            <li class="active">Продвижение сайтов</li>
                        </ul>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="h2"><?php print $title ?></h1>
                    </div>
                </div>
            </div>
        </section>

        <div class="back-head">
            <div class="container">
                <div class="col-md-7" style="height: 610px;">
                    <h1 class="text-center" style="margin-top: 167px;">Продвижение сайтов <br/>в топ 10</h1>
                    
                </div>
                <div class="col-md-5 text-center">
                    <div class="row" style="margin-left: -42px">
                    <div class="h-form">Оставьте заявку</div>
                    <div class="t-form">и получите консультацию<br />по продвижению вашего<br />сайта в интернете</div>
                    <button class="btn btn-primary push-top push-bottom" data-toggle="modal" data-target="#myModal">
                        Заказать продвижение!
                    </button>
                    </div>
                </div>
                <div class="col-md-12 text-center push-top">
                    <p class="text-center"><span class="alternative-font"> Продвижение сайтов</span> - привлечение потенциальных клиентов из интернета при помощи поисковых систем.</p>
                    <a class="btn btn-primary">Заказать продвижение сайта.</a>


                </div>
            </div>
        </div>
        <hr class="tall">
        <div class="container">
            <div class="col-md-4 clearfix">
                <img class="img-circle img-responsive" src="/img/seo_1.png" alt="продвижение сайтов в топ 10">
            </div>
            <div class="col-md-8">
                <h2><strong>Стратегии</strong> под цели бизнеса:</h2>
                <ul>
                    <li>Вывод сайта в топ</li>
                    <li>Привлечение трафика</li>
                    <li>Генерация лидов (заявок, звонков, регистраций)</li>
                    <li>Специальная программа для молодого сайта</li>
                </ul>
            </div>
        </div>
        <hr class="tall">
        <div class="container">
        <div class="col-md-8">
            <h2><strong>Преимущества</strong> поискового продвижения:</h2>
            <ul>
                <li>Низкая стоимость контакта</li>
                <li>Лояльность и доверие пользователей</li>
                <li>Высокая конвертация посетителей сайта в клиентов</li>
                <li>Имиджевый эффект при выходе в топ</li>
            </ul>
        </div>
        <div class="col-md-4">
            <img class="img-responsive" src="/img/seo_2.png" alt="Заказать продвижение сайта">
        </div>
        </div>

        <div class="container">
            <h2>Зачем выходить в <strong>Топ</strong>?</h2>
            <div class="col-md-4">
                <table class="text-center">
                    <thead>
                    <tr>
                        <th width="80">Позиция</th>
                        <th>Средний процент клика</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>1</td>
                        <td>42,1 %</td>
                    </tr>
                    <tr>
                        <td>2</td>
                        <td>11,9 %</td>
                    </tr>
                    <tr>
                        <td>3</td>
                        <td>8,5 %</td>
                    </tr>
                    <tr>
                        <td>4</td>
                        <td>6,1 %</td>
                    </tr>
                    <tr>
                        <td>5</td>
                        <td>4,9 %</td>
                    </tr>
                    <tr>
                        <td>6</td>
                        <td>4,1 %</td>
                    </tr>
                    <tr>
                        <td>7</td>
                        <td>3,4 %</td>
                    </tr>
                    <tr>
                        <td>8</td>
                        <td>3,0 %</td>
                    </tr>
                    <tr>
                        <td>9</td>
                        <td>2,8 %</td>
                    </tr>
                    <tr>
                        <td>10</td>
                        <td>3,0 %</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div class="col-md-8">
                <h2>ВАМ <strong>НУЖНО SEO</strong>, ЕСЛИ ВЫ ХОТИТЕ</h2>
                <ul>
                    <li>Увеличить число целевых посетителей, т.е. ваших потенциальных клиентов</li>
                    <li>Получать больше звонков/заказов с сайта</li>
                    <li>Увеличить объем продаж</li>
                    <li>Повысить узнаваемость компании</li>
                    <li>Ощутить превосходство, оставив всех конкурентов позади</li>
                </ul>
            </div>
        </div>
        <hr class="tall">
        <div class="container">
            <h2 class="text-center">От чего зависит стоимость продвижения?</h2>
            <div class="col-md-4">
                <ul>
                    <li>Техническое состояние сайта</li>
                    <li>Конкурентность тематики</li>
                </ul>
            </div>
            <div class="col-md-4">
                <ul>
                <li>Количество продвигаемых запросов</li>
                <li>Регион продвижения</li>
                </ul>
            </div>
                <div class="col-md-4">
                <ul>
                <li>Текущая видимость сайта</li>
                </ul>
            </div>
            <hr class="clearfix tall">
        </div>

        <div class="container">
            <h2 class="center"><strong>Что входит в продвижение сайта?</strong></h2>
            <div class="col-lg-6">
                <h3>Аудит сайта</h3>
                <ul>
                    <li>Базовый экспресс-аудит сайта</li>
                    <li>Базовый аудит юзабилити сайта</li>
                    <li>Технический аудит сайта</li>
                </ul>
            </div>
            <div class="col-lg-6">
                <h3>Внешняя оптимизация</h3>
                <ul>
                    <li>Написание уникальных тематических статей</li>
                    <li>Размещение статей и ссылок на тематических площадках</li>
                    <li>Оптимизация внешних факторов ранжирования страниц</li>
                    <li>Мониторинг позиций сайта и последующая корректировка хода продвижения сайта</li>
                    <li>Регулярный контроль за появлением нежелательных ссылок</li>
                    <li>Постоянный контроль качества и индексации ссылок</li>
                </ul>
            </div>
        </div>
        <div class="container">
            <div class="col-lg-6">
                <h3>Внутренняя оптимизация</h3>
                <ul>
                    <li>Составление семантического ядра</li>
                    <li>Оптимизация контента на сайте</li>
                    <li>Оптимизация Title, Description, Keywords</li>
                    <li>Внутренняя перелинковка сайта</li>
                    <li>Ежедневный мониторинг состояния сайта</li>
                    <li>Отслеживание уникальности текстового содержимого сайта</li>
                </ul>
            </div>
            <div class="col-lg-4">
                <h3 class="text-center" style="margin-top: 1.5em">
                    РЕГУЛЯРНАЯ ОТЧЕТНОСТЬ И ТЕХНИЧЕСКАЯ ПОДДЕРЖКА ПО ВОПРОСАМ ПРОДВИЖЕНИЯ
                </h3>
            </div>
        </div>

        <section class="call-to-action featured footer">
            <div class="container">
                <div class="row">
                    <div class="center">
                        <h3>Хотите <strong>завоевать</strong> интернет?
                            <button class="btn btn-primary btn-lg push-top push-bottom" data-toggle="modal" data-target="#myModal">
                                Заказать продвижение!
                            </button>
                            <span class="arrow hlb hidden-xs hidden-sm hidden-md appear-animation rotateInUpLeft appear-animation-visible" data-appear-animation="rotateInUpLeft" style="top: -22px;"></span></h3>
                    </div>
                </div>
            </div>
        </section>
