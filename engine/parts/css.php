<!-- Vendor CSS -->
<link rel="stylesheet" href="/vendor/bootstrap/css/bootstrap.min.css">
<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/vendor/simple-line-icons/css/simple-line-icons.min.css">
<link rel="stylesheet" href="/vendor/owl.carousel/assets/owl.carousel.min.css">
<link rel="stylesheet" href="/vendor/owl.carousel/assets/owl.theme.default.min.css">
<link rel="stylesheet" href="/vendor/magnific-popup/magnific-popup.min.css">
<!-- Theme CSS -->
<link rel="stylesheet" href="/css/theme.css">
<link rel="stylesheet" href="/css/theme-elements.css">
<link rel="stylesheet" href="/css/theme-blog.css">
<link rel="stylesheet" href="/css/theme-shop.css">
<link rel="stylesheet" href="/css/theme-animate.css">
<!-- Skin CSS -->
<link rel="stylesheet" href="/css/skins/skin-corporate-hosting.css">
<!-- Theme Custom CSS -->
<link rel="stylesheet" href="/css/custom.css">