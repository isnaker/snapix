<div class="slider-container rev_slider_wrapper" style="height: 700px;">
    <div id="revolutionSlider" class="slider rev_slider" data-plugin-revolution-slider data-plugin-options='{"gridwidth": 800, "gridheight": 700}'>
        <ul>
            <li data-transition="fade">
                <div class="rs-background-video-layer"
                     data-forcerewind="on"
                     data-volume="mute"
                     data-videowidth="100%"
                     data-videoheight="100%"
                     data-videomp4="/video/firework.mp4"
                     data-videopreload="preload"
                     data-videoloop="none"
                     data-forceCover="1"
                     data-aspectratio="16:9"
                     data-autoplay="true"
                     data-autoplayonlyfirsttime="false"
                     data-nextslideatend="true"
                ></div>
                <div class="tp-caption"
                     data-x="center" data-hoffset="-160"
                     data-y="center" data-voffset="-95"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="создание сайтов"></div>
                <div class="tp-caption top-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-95"
                     data-start="500"
                     style="z-index: 5"
                     data-transform_in="y:[-300%];opacity:0;s:500;">Мы - Snapix. И мы</div>
                <div class="tp-caption"
                     data-x="center" data-hoffset="160"
                     data-y="center" data-voffset="-95"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="продвижение сайтов"></div>
                <div class="tp-caption main-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-45"
                     data-start="1500"
                     data-whitespace="nowrap"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;"
                     style="z-index: 5"
                     data-mask_in="x:0px;y:0px;">УВЕЛИЧИВАЕМ ПРОДАЖИ</div>
                <div class="tp-caption bottom-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="5"
                     data-start="2000"
                     style="z-index: 5"
                     data-transform_in="y:[100%];opacity:0;s:500;">минимум в 3 раза с помощью интернета</div>
                <a class="tp-caption btn btn-lg btn-primary btn-slider-action hidden"
                   data-hash
                   data-hash-offset="85"
                   href="#home-intro"
                   data-x="center" data-hoffset="0"
                   data-y="center" data-voffset="80"
                   data-start="2200"
                   data-whitespace="nowrap"
                   data-transform_in="y:[100%];s:500;"
                   data-transform_out="opacity:0;s:500;"
                   style="z-index: 5"
                   data-mask_in="x:0px;y:0px;">Узнать, как мы это делаем!</a>

            </li>
            <li data-transition="fade">
                <img src="img/backgrounds/business_background.jpg"
                     alt="разработка сайтов"
                     data-bgposition="center center"
                     data-bgfit="cover"
                     data-bgrepeat="no-repeat"
                     class="rev-slidebg">
                <div class="tp-caption"
                     data-x="center" data-hoffset="-150"
                     data-y="center" data-voffset="-95"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[-300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="реклама сайта"></div>
                <div class="tp-caption top-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-95"
                     data-start="500"
                     style="z-index: 5"
                     data-transform_in="y:[-300%];opacity:0;s:500;">используйте силу</div>
                <div class="tp-caption"
                     data-x="center" data-hoffset="150"
                     data-y="center" data-voffset="-95"
                     data-start="1000"
                     style="z-index: 5"
                     data-transform_in="x:[300%];opacity:0;s:500;"><img src="img/slides/slide-title-border.png" alt="создание интернет магазина"></div>
                <div class="tp-caption main-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="-45"
                     data-start="1500"
                     data-whitespace="nowrap"
                     data-transform_in="y:[100%];s:500;"
                     data-transform_out="opacity:0;s:500;"
                     style="z-index: 5"
                     data-mask_in="x:0px;y:0px;">DIGITAL MARKETING</div>
                <div class="tp-caption bottom-label"
                     data-x="center" data-hoffset="0"
                     data-y="center" data-voffset="5"
                     data-start="2000"
                     style="z-index: 5"
                     data-transform_in="y:[100%];opacity:0;s:500;">и получите новых клиентов уже сегодня!</div>
                <a class="tp-caption btn btn-lg btn-primary btn-slider-action hidden"
                   data-hash
                   data-hash-offset="85"
                   href="#home-intro"
                   data-x="center" data-hoffset="0"
                   data-y="center" data-voffset="80"
                   data-start="2200"
                   data-whitespace="nowrap"
                   data-transform_in="y:[100%];s:500;"
                   data-transform_out="opacity:0;s:500;"
                   style="z-index: 5"
                   data-mask_in="x:0px;y:0px;">Узнать подробнее</a>
                <div style="opacity: 0.7"
                     class="tp-dottedoverlay tp-opacity-overlay"></div>
            </li>
        </ul>
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-md-12 center">
            <img src="/img/logo.png" alt="Snapix" class="mt-xlg">
            <hr>
            <h2 class="word-rotator-title mb-sm"><strong>Самые
                                    <span class="word-rotate" data-plugin-options='{"delay": 2000, "animDelay": 300}'>
                                        <span class="word-rotate-items">
                                            <span>работающие</span>
                                            <span>качественные</span>
                                            <span>инновационные</span>
                                        </span>
							        </span>
                </strong> услуги в сфере развития бизнеса в Интернете.</h2>
            <p class="lead">С 2009 года мы предоставляем услуги по разработке и продвижению сайтов,
                комплексному развитию бизнеса в интернете и техническому сопровождению.</p>
        </div>
    </div>
    <div class="row mt-xl">
        <div class="counters counters-text-dark">
            <div class="col-md-3 col-sm-6">
                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="300">
                    <i class="fa fa-user"></i>
                    <strong data-to="90" data-append="+">0</strong>
                    <label>Довольных клиентов</label>
                    <p class="text-color-primary mb-xl">Они сделали верный выбор.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="600">
                    <i class="fa fa-desktop"></i>
                    <strong data-to="250" data-append="+">0</strong>
                    <label>Выполненных проектов</label>
                    <p class="text-color-primary mb-xl">И будет еще!</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="900">
                    <i class="fa fa-ticket"></i>
                    <strong data-to="326" data-append="%">0</strong>
                    <label>Повышение продаж</label>
                    <p class="text-color-primary mb-xl">Да, так бывает.</p>
                </div>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="counter appear-animation" data-appear-animation="fadeInUp" data-appear-animation-delay="1200">
                    <i class="fa fa-clock-o"></i>
                    <strong data-to="4000" data-append="+">0</strong>
                    <label>Привлекли лидов</label>
                    <p class="text-color-primary mb-xl">Для наших друзей - самых горячих!</p>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="container">
    <div class="row mt-xl">
        <div class="col-md-12 center">
            <h2 class="mt-xl mb-none">Наши <strong>Услуги</strong></h2>
            <div class="divider divider-primary divider-small divider-small-center mb-xl">
                <hr>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="tabs tabs-bottom tabs-center tabs-simple mt-xl mb-xl">
                <ul class="nav nav-tabs">
                    <li>
                        <a href="#sites" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured icon-bulb icons"></i>
													</span>
												</span>
											</span>
                            <h1 class="mb-none h6 index-h2 pb-none">Сайты</h1>
                        </a>
                    </li>
                    <li>
                        <a href="#optimization" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured icon-moustache icons"></i>
													</span>
												</span>
											</span>
                            <h2 class="mb-none index-h2 h6 pb-none">Продвижение</h2>
                        </a>
                    </li>
                    <li>
                        <a href="#context" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured icon-puzzle icons"></i>
													</span>
												</span>
											</span>
                            <h2 class="mb-none index-h2 h6 pb-none">Контекстная реклама</h2>
                        </a>
                    </li>
                    <li>
                        <a href="#complex" data-toggle="tab">
											<span class="featured-boxes featured-boxes-style-6 p-none m-none">
												<span class="featured-box featured-box-primary featured-box-effect-6 p-none m-none">
													<span class="box-content p-none m-none">
														<i class="icon-featured icon-rocket icons"></i>
													</span>
												</span>
											</span>
                            <p class="mb-none pb-none">Бизнесу</p>
                        </a>
                    </li>
                </ul>
                <div class="tab-content">
                    <div class="tab-pane" id="sites" aria-expanded="true">
                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs small">
                                <li>
                                    <a href="#landings" data-toggle="tab">Создание Landing page</a>
                                </li>
                                <li class="">
                                    <a href="#shops" data-toggle="tab">Создание интернет-магазинов</a>
                                </li>
                                <li class="">
                                    <a href="#corporate" data-toggle="tab">Создание корпоративных сайтов</a>
                                </li>
                                <li class="">
                                    <a href="#webservices" data-toggle="tab">Создание веб-сервисов</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="landings">
                                    <div class="col-md-6">
                                        <img src="/img/services/landings.jpg" class="img-responsive" alt="продвижение сайтов">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Создание Landing page</h4>
                                        <p>Лендинг пейдж (посадочная страница или landing page) -
                                            это возможность показать посетителю уникальное товарное
                                            предложение (УТП), описание компании, сертификаты, дипломы,
                                            награды, отзывы клиентов, контакты и легко осуществить заказ
                                            с помощью всего одной страницы.</p>
                                        <p>Посадочная страница разрабатывается как основной или
                                            дополнительный сайт компании. Лэндинги отлично подходят
                                            для проведения рекламной кампании на конкретный товар
                                            или услугу с уникальной (низкой) ценой и привлечения
                                            на сайт целевой аудитории из контекстной рекламы
                                            (Яндекс.Директ, Google AdWords), социальных сетей
                                            или E-mail рассылок.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="shops">
                                    <div class="col-md-6">
                                        <img src="/img/services/shops.jpg" class="img-responsive" alt="создание сайтов">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Создание интернет-магазинов</h4>
                                        <p>Интернет-магазин – это современный торговый канал, дающий возможность реализовывать товары через интернет.

                                            Как по части презентационных возможностей, так и в том, что касается удобства работы с товарным ассортиментом – интернет-магазин является полнофункциональным магазином, с той лишь разницей, что его "витрина" расположена в Интернете.

                                            Интернет-магазин обычно имеет достаточно большой объем и сложную структуру. Как правило, на сайте представлен каталог товаров с их подробным описанием, условиями поставки и ценами. От прочих сайтов, интернет-магазин главным образом отличается программными модулями, позволяющими автоматизировать процесс купли-продажи через интернет.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="corporate">
                                    <div class="col-md-6">
                                        <img src="/img/services/corporate.jpg" class="img-responsive"  alt="раскрутка сайтов">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Создание корпоративных сайтов</h4>
                                        <p>Корпоративный сайт — содержит полную информацию о компании-владельце, услугах/продукции, событиях в жизни компании. Отличается от сайта-визитки и представительского сайта полнотой представленной информации, зачастую содержит различные функциональные инструменты для работы с контентом (поиск и фильтры, календари событий, фотогалереи, корпоративные блоги, форумы). Может быть интегрирован с внутренними информационными системами компании-владельца (КИС, CRM, бухгалтерскими системами). Может содержать закрытые разделы для тех или иных групп пользователей — сотрудников, дилеров, контрагентов и пр.</p>
                                        <p>
                                            информационный портал — это очень большой веб-ресурс, который предоставляет исчерпывающую информацию по определённой тематике. Порталы похожи на тематические сайты, но дополнительно содержат средства взаимодействия с пользователями и позволяют пользователям общаться в рамках портала (форумы, чаты) — это среда существования пользователя.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="webservices">
                                    <div class="col-md-6">
                                        <img src="/img/services/webservices.jpg" class="img-responsive" alt="интернет-магазины">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Создание веб-сервисов</h4>
                                        <p>
                                            Веб-сервис — это сайт, оказывающий какую-либо услугу сам по
                                            себе, а не просто информирующий о компании, которая этими
                                            услугами занимается.</p>
                                        <p>Разработка сложных веб-сервисов, которые обладают длинным перечнем функционала, требует очень тщательного и подробного проектирования, чтобы получившийся в результате продукт был удобен и интуитивно понятен пользователям.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="optimization">
                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a href="#seo_consult" data-toggle="tab">Консультации специалиста</a>
                                </li>
                                <li>
                                    <a href="#seo_audit" data-toggle="tab">SEO аудит сайта</a>
                                </li>
                                <li class="">
                                    <a href="#seo_optimization" data-toggle="tab">SEO - оптимизация</a>
                                </li>
                                <li class="">
                                    <a href="#seo_promotion" data-toggle="tab">SEO - продвижение</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="seo_consult">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-consult.jpg" class="img-responsive" alt="создание лендингов">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Консультации специалиста</h4>
                                        <p>Если у Вас возникли вопросы по качеству продвижения сайта в
                                            интернете, нужен совет специалиста или свежий взгляд на
                                            раскрутку, то команда Snapix поможет вам! Аналитика сайта,
                                            консультация специалиста, оценка проведенных работ
                                            СЕО-компаний - абсолютно БЕСПЛАТНО! </p>
                                        <p>Только конструктивная критика, полезные советы
                                            и актуальные методики. Нам не важно, являетесь
                                            Вы директором бизнеса или оптимизатор сайта,
                                            мы готовы помогать всем.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="seo_audit">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-audit.jpg" class="img-responsive" alt="seo-разработка">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>SEO аудит сайта</h4>
                                        <p>Цель аудита – повышение позиций и увеличение количества
                                            переходов на сайт. Мы проводим анализ сайта компании и
                                            даем рекомендации, как улучшить работу ресурса. Такая
                                            диагностика необходима для создания или изменения
                                            алгоритма эффективного продвижения в сети.</p>
                                        <p> Вы узнаете,
                                            как нужно модифицировать сайт, чтобы он оперативно и
                                            корректно попадал в поисковые системы. </p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="seo_optimization">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-optimize.jpg" class="img-responsive" alt="создание сайтов">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>SEO - оптимизация</h4>
                                        <p>SEO оптимизация сайта под поисковые запросы (или просто SEO
                                            от Search Engine Optimization) включает в себя целый
                                            комплекс специальных мероприятий, которые направлены на
                                            достижение сайтом высоких позиций в выдаче поисковых систем.</p>
                                        <p>В результате работы SEO-специалистов, о Ваших товарах/услугах,
                                            да и в целом о сайте, узнает как можно большее количество
                                            пользователей сети Интернет.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="seo_promotion">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-promotion.jpg" class="img-responsive"  alt="лендинг пейдж">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>SEO - продвижение</h4>
                                        <p>SEO оптимизация сайта под поисковые запросы (или просто SEO
                                            от Search Engine Optimization) включает в себя целый
                                            комплекс специальных мероприятий, которые направлены на
                                            достижение сайтом высоких позиций в выдаче поисковых систем.</p>
                                        <p>В результате работы SEO-специалистов, о Ваших товарах/услугах,
                                            да и в целом о сайте, узнает как можно большее количество
                                            пользователей сети Интернет.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="context">
                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a href="#context_about" data-toggle="tab">Преимущества контекстной рекламы </a>
                                </li>
                                <li>
                                    <a href="#direct" data-toggle="tab">Яндекс.Директ</a>
                                </li>
                                <li class="">
                                    <a href="#adwords" data-toggle="tab">Google Adwords</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="context_about">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-consult.jpg" class="img-responsive" alt="снапикс">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Преимущества контекстной рекламы</h4>
                                        <p>Вникаем в бизнес, изучаем конкурентов, разрабатываем портреты целевой аудитории.
                                            Создаём и ведём рекламные кампании.
                                            Проводим детальный аудит сайта и существующей рекламы.
                                            Интервьюируем представителей аудитории.
                                            Определяем инструменты продвижения,
                                            строим медиа-план. Согласуем систему отчетности,
                                            настраиваем аналитику.
                                            Создаем контент. Пишем крутые рекламные тексты, создаем баннеры.
                                            Проводим эксперименты.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="direct">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-audit.jpg" class="img-responsive" alt="создание лендингов">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Яндекс.Директ </h4>
                                        <p>Яндекс.Директ - российская система, позволяющая размещать рекламные
                                            объявления в поисковой машине «Яндекс» и в Рекламной Сети
                                            Яндекса (РСЯ) — на сайтах, где объявления показываются с
                                            помощью автоматического определения смысла страничек.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="adwords">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-optimize.jpg" class="img-responsive" alt="посадочная страница">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Google AdWords </h4>
                                        <p>Google AdWords — система для показа контекстных объявлений
                                            на поисковой машине Google, второй по популярности в России,
                                            и в сети сайтов, где объявления также показываются с помощью
                                            автоматического определения смысла страничек.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane" id="complex">
                        <div class="tabs tabs-bottom tabs-center tabs-simple">
                            <ul class="nav nav-tabs">
                                <li>
                                    <a href="#complex_business" data-toggle="tab">Ведение бизнеса</a>
                                </li>
                                <li>
                                    <a href="#complex_landing" data-toggle="tab">Landing +</a>
                                </li>
                                <li>
                                    <a href="#complex_shop" data-toggle="tab">Интернет-магазин +</a>
                                </li>
                                <li>
                                    <a href="#complex_top" data-toggle="tab">Всё включено ++</a>
                                </li>
                            </ul>
                            <div class="tab-content">
                                <div class="tab-pane" id="complex_business">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-consult.jpg" class="img-responsive" alt="страница приземления">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Комплексное ведение бизнеса</h4>
                                        <p>Мы интегрируемся в бизнес-процессы вашей компании и развиваем
                                            все аспекты деятельности - прорабатываем ваше представление на интернет-рынке,
                                            создаем и внедряем необходимые решения для увеличения ваших продаж и повышения
                                            статуса компании.</p>
                                        <p>Комплексное ведение включает разработку и доработку сайтов, ведение рекламных
                                            кампаний, продвижение сайта и рекомендации по улучшению продаж.</p>
                                        <p>Имеется возможность переключить Ваш отдел продаж на наших специалистов - вы
                                            получаете готовые заявки на ваши товары или услуги с разработанных нами сайтов.</p>
                                    </div>

                                </div>
                                <div class="tab-pane" id="complex_landing">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-consult.jpg" class="img-responsive" alt="посадочная страница">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Landing +</h4>
                                        <p>Мы разрабатываем и полностью оптимизируем лендинг для Вашей ниши,
                                            включая настройку рекламных кампаний и проработку поведенческих факторов.</p>
                                        <p>Приводим клиентов под ключ и отвечаем за качество и количество заявок с
                                            каналов продвижения. </p>
                                        <p>Имеется возможность переключить Ваш отдел продаж на наших специалистов - вы
                                            получаете готовые заявки на ваши товары или услуги с разработанных нами сайтов.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="complex_shop">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-consult.jpg" class="img-responsive" alt="страница приземления">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Интернет-магазин +</h4>
                                        <p>Мы разрабатываем и полностью оптимизируем интернет-магазин для Вашей ниши,
                                            включая настройку рекламных кампаний и проработку поведенческих факторов.</p>
                                        <p>Мы прорабатываем внешний вид карточек товара и изучаем поведение пользователей,
                                            постоянно внедряя улучшения и повышая количество заказов.</p>
                                        <p>Имеется возможность переключить Ваш отдел продаж на наших специалистов - вы
                                            получаете готовые заявки на ваши товары или услуги с разработанных нами сайтов.</p>
                                    </div>
                                </div>
                                <div class="tab-pane" id="complex_top">
                                    <div class="col-md-6">
                                        <img src="/img/services/seo-consult.jpg" class="img-responsive" alt="создать сайт">
                                    </div>
                                    <div class="col-md-6">
                                        <h4>Все включено ++</h4>
                                        <p>Мы включаемся в бизнес-процессы на
                                            правах партнеров и полностью берем на себя ведение Ваших интересов в
                                            интернете - разрабатываем, продвигаем и рекламируем сайты по самому
                                            высшему уровню, позволяя Вашему бизнесу выйти в ТОП интернет-сегмента.</p>
                                        <p>Имеется возможность переключить Ваш отдел продаж на наших специалистов - вы
                                            получаете готовые заявки на ваши товары или услуги с разработанных нами сайтов.</p>
                                        <p><small><strong>Примечание:</strong>
                                                в данный момент мы имеем ограничения по заявкам на эту услугу в связи со слишком большим
                                                потоком клиентов. Мы имеем право отказаться в сотрудничестве без объяснения причин.</small></p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="call-to-action call-to-action-primary call-to-action-front mb-none">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action-content align-left pb-md mb-xl ml-none">
                    <h2 class="text-color-light mb-none mt-none">
                        Поднимаем <strong>продажи</strong> Вашего сайта, приводим в бизнес новых <strong>клиентов</strong>,
                        а также возвращаем старых!
                    </h2>
                </div>
                <div class="call-to-action-btn">
                    <button data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary btn-primary-scale-2 mr-md">Хочу этого!</button>
                    <span class="mr-md text-color-light hidden-xs">не откладывай!<span class="arrow arrow-light hlb" style="top: -88px; right: -47px;"></span></span>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="section section-default mt-none mb-xl" id="howwework">
    <div class="home-concept">
        <div class="container">
            <p class="h2 heading"><strong>Как мы это делаем?</strong></p>
            <div class="row center">
                <span class="sun"></span>
                <span class="cloud"></span>
                <div class="col-md-2">
                    <div class="process-image" data-appear-animation="bounceIn">
                        <img src="img/analyze.png" alt="seo сайта" />
                        <strong class="text-color-dark mb-xs">Анализируем Вашу нишу</strong>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="200">
                        <img src="img/count.png" alt="создание сайта с нуля" />
                        <strong class="text-color-dark mb-xs">Определяем стратегию разработки сайта</strong>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
                        <img src="img/discuss.png" alt="создание и продвижение сайтов" />
                        <strong class="text-color-dark mb-xs">Выбираем стратегию продвижения</strong>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="process-image" data-appear-animation="bounceIn" data-appear-animation-delay="400">
                        <img src="img/decision.png" alt="создание интернет сайтов" />
                        <strong class="text-color-dark mb-xs">Обсуждаем количество привлеченных клиентов в сутки</strong>
                    </div>
                </div>
                <div class="col-md-2">

                    <div class="project-image" style="margin-left: 5%">
                        <div id="fcSlideshow" class="fc-slideshow">
                            <ul class="fc-slides">
                                <li><img class="img-responsive" src="img/item1.jpg" alt="разработка интернет магазина" /></li>
                                <li><img class="img-responsive" src="img/item2.jpg" alt="создание веб сайтов" /></li>
                                <li><img class="img-responsive" src="img/item3.jpg" alt="поисковое продвижение сайтов" /></li>
                            </ul>
                        </div>
                        <strong class="text-color-dark pt-xl">Начинаем отгружать клиентов !</strong>
                    </div>
                </div>
            </div>
            <div class="row center">
                <div class="col-md-12">
                    <div style="margin: 0 0 150px 0">
                        <h2 class="mt-none mb-none">Почему наши сайты так хороши?</h2>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-4">
                <div class="feature-box feature-box-style-2 reverse mb-xl appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="300">
                    <div class="feature-box-icon">
                        <i class="fa fa-bolt"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Современный дизайн</h4>
                        <p class="mb-lg">Используем все современые наработки и приемы в сфере дизайна
                            и следим за новыми тенденциями.</p>
                    </div>
                </div>
                <div class="feature-box feature-box-style-2 reverse mt-xl appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="600">
                    <div class="feature-box-icon">
                        <i class="fa fa-heart"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Ориентиры на клиента</h4>
                        <p class="mb-lg">С трепетом подходим к требованиям наших клиентов. Даем гарантию и особые условия работы.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <img src="img/services/development.png"
                     class="img-responsive mockup-landing-page mb-xl mt-xl appear-animation hidden-xs"
                     alt="Snapix" data-appear-animation="fadeInDown" data-appear-animation-delay="300">
            </div>
            <div class="col-md-4">
                <div class="feature-box feature-box-style-2 mb-xl appear-animation" data-appear-animation="fadeInRight" data-appear-animation-delay="300">
                    <div class="feature-box-icon">
                        <i class="fa fa-puzzle-piece"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Умные технологии</h4>
                        <p class="mb-lg">Делаем свою работу по современным методикам и
                            постоянно внедряем инновации, оставляя конкурентов позади. </p>
                    </div>
                </div>
                <div class="feature-box feature-box-style-2 mt-xl appear-animation" data-appear-animation="fadeInRight" data-appear-animation-delay="600">
                    <div class="feature-box-icon">
                        <i class="fa fa-cloud-download"></i>
                    </div>
                    <div class="feature-box-info">
                        <h4 class="mb-sm">Постоянное развитие</h4>
                        <p class="mb-lg">Осуществляем постоянную поддержку и развитие проектов.
                            Оперативно предлагаем свежие решения.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12 center">
            <h2 class="mt-xl mb-none">Оптимизация под все <strong>мобильные устройства</strong></h2>
            <p class="lead mb-xl">63% пользователей посещают интернет с помощью<br> мобильных телефонов или планшетов.</p>
            <hr class="invisible">
        </div>
    </div>
    <div class="row">
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6 reverse mb-none mt-xl appear-animation" data-appear-animation="fadeInLeft" data-appear-animation-delay="300">
                <div class="feature-box-icon">
                    <i class="fa fa-mobile text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="mb-sm">100% Адаптивные сайты</h4>
                    <p class="mb-lg">Мы разрабатываем сайты, идеально выглядящие на всех возможных устройствах.</p>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <img alt="Responsive" data-appear-animation="fadeInUp" class="hidden-xs img-responsive appear-animation fadeInUp" src="img/responsive-ready.png" style="margin-bottom: -1px;">
        </div>
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6 mb-none mt-xl appear-animation" data-appear-animation="fadeInRight" data-appear-animation-delay="300">
                <div class="feature-box-icon">
                    <i class="fa fa-eye text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="mb-sm">Дизайн и удобство</h4>
                    <p class="mb-lg">Мы продумываем дизайн и все элементы с учетом мобильных устройств.
                        Сайтом всегда удобно пользоваться.</p>
                </div>
            </div>
        </div>
    </div>
</div>
<section class="section section-default mt-none">
    <div class="container">
        <div class="row">
            <div class="col-md-7">
                <h2 class="text-right">Опытная <strong>команда</strong> разработчиков и маркетологов</h2>
                <p class="text-right">
                    Мы занимаемся разработкой и продвижением сайтов с 2009 года.
                    С 2012 года мы занимаемся комплексным маркетингом в сфере интернет и развитием бизнеса.
                    Наши сотрудники - профессионалы с многолетним стажем работы, имеющие за плечами
                    учебу в престижных ВУЗах России, сертификаты ведущих на рынке компаний
                    (Яндекс, 1С, Бизнес Молодость) и опыт работы в самых горячих проектах.
                </p>
            </div>
            <div class="col-md-4">
                <img class="hidden-xs img-responsive appear-animation" src="img/services/crew.png" alt="Snapix" data-appear-animation="fadeInRight">
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row mt-none mb-xl">
        <div class="col-md-4 col-md-offset-1">
            <img class="img-responsive mt-xl appear-animation" src="img/services/support.png" alt="поисковое продвижение сайтов" data-appear-animation="fadeInLeft">
        </div>
        <div class="col-md-7">
            <h2 class="mt-xl">Качественная и компетентная <strong>поддержка</strong></h2>
            <p>Наши клиенты всегда могут рассчитывать на поддержку в решении любых проблем с
                их проектами - от простой консультации о работе сайта или сервиса до глобального
                сопровождения всего бизнеса!<br>Мы своих не бросаем. </p>
        </div>
    </div>
</div>
<section class="call-to-action call-to-action-primary mb-xl">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action-content align-left pb-md mb-xl ml-none">
                    <h2 class="text-color-light mb-none mt-xl">Все еще сомневаетесь? <strong>Свяжитесь с нами.</strong></h2>
                    <p class="lead mb-xl">Нам есть, что Вам предложить уже сейчас!</p>
                </div>
                <div class="call-to-action-btn">
                    <button data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary btn-primary-scale-2 mr-md">Связаться!</button>
                    <span class="arrow arrow-light hlb" style="top: -88px; right: -87px;"></span>
                </div>
            </div>
        </div>
    </div>
</section>
<div class="container">
    <div class="row">
        <div class="col-md-12 center">
            <h2 class="mt-xl mb-none">Наши <strong>особые достижения</strong></h2>
            <p class="lead mb-xl">Есть кое-что, что мы делаем, как все. Но есть и другое...</p>
            <hr class="invisible">
        </div>
    </div>
    <div class="row mb-xl">
        <div class="col-md-3">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Повышали продажи на 326% всего за 4 месяца.</strong></h4>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Выводили сайт из под фильтра Яндекса.</strong></h4>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Писали свой движок для сайта. И не один.</strong></h4>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Синхронизировали сайты с 1С-предприятие.</strong></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-xl">
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">40 лидов в первый день запуска контекстной рекламы. Без лендинга.</strong></h4>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Три дня ночевали в офисе у одного из клиентов. Искали идеи. И нашли.</strong></h4>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Работали с иностранными заказчиками.</strong></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row mb-xl">
        <div class="col-md-6">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">Участвовали в разработке крупного
                            сервиса, которым пользуются тысячи человек.</strong></h4>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="feature-box feature-box-style-6">
                <div class="feature-box-icon">
                    <i class="fa fa-check text-color-primary"></i>
                </div>
                <div class="feature-box-info">
                    <h4 class="pt-xs"><strong class="semi-bold">
                            Получили 8 тысяч подписчиков ВКонтакте за первый месяц с минимальными вложениями.</strong></h4>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 center">
            <hr>
            <h2><span class="alternative-font font-size-md mt-xl">Мы так работаем.</span></h2>
        </div>
    </div>
</div>
<section class="section section-text-light section-center mt-xl mb-none"  style="background-image: url(img/slides/slide-bg-medium.jpg); background-repeat: no-repeat; background-position: center center; background-size: cover">
    <div class="container">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                <h2><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs mr-xs"></i><i class="fa fa-star font-size-xs"></i><br><strong>Отзывы о нас</strong></h2>
                <div class="owl-carousel owl-theme nav-bottom rounded-nav mb-none" data-plugin-options='{"items": 1, "loop": false}'>
                    <div>
                        <div class="col-md-12">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                <blockquote>
                                    <p>Выражаем свою благодарность Агентству интернет маркетинга Snapix
                                        за качественную настройку рекламных кампаний Яндекс.Директ. В первый день запуска
                                        получили 40 заявок на свои услуги, при этом потратив 1500 рублей из бюджета.</p>
                                    <p>Надеемся на продолжение успешного сотрудничества, а также на дольейшее увеличение
                                        достигнутых показателей совместной работы.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>ООО Осаго52</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-md-12">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                <blockquote>
                                    <p>Спасибо за быстро и качественно выполненный лендинг!
                                        Меньше, чем за неделю ребята создали отличный продающий сайт, который
                                        уже начал окупаться! Правда думал что цена будет чуть меньше, а потом
                                        посмотрел - по качеству если, то цена даже ниже, чем могла бы быть. Не пожалел.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>Владимир Кудряшов</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div>
                        <div class="col-md-12">
                            <div class="testimonial testimonial-style-2 testimonial-with-quotes mb-none">
                                <blockquote>
                                    <p>После того, как к сайту прицепили каталог и пришлось потратить много
                                        времени на его заполнение сайт резко стал выходить на первые строчки
                                        в поисковиках. Пошли заказы, работы существенно прибавилось. В общем в
                                        своем городе я теперь почти как монопилист - другие опоздали, видимо, с
                                        сайтом.</p>
                                </blockquote>
                                <div class="testimonial-author">
                                    <p><strong>Алексей, "Корея-Авто"</strong></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="call-to-action call-to-action-default call-to-action-in-footer mt-none no-top-arrow">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="call-to-action-content align-left pb-md mb-xl ml-none">
                    <h2 class="mb-xs mt-xl">Присоединяйтесь к успеху вместе с командой <strong>Snapix!</strong></h2>

                </div>
                <div class="call-to-action-btn">
                    <a data-toggle="modal" data-target="#callbackModal" class="btn btn-lg btn-primary popup-with-zoom-anim"><i class="fa fa-cark mr-xs"></i>Начать сотрудничество</a>
                    <p><span class="alternative-font font-size-sm mt-xs text-color-primary">У нас много бонусов для новых клиентов :)</span></p>
                </div>
            </div>
        </div>
    </div>
</section>