<footer id="footer">
    <div class="container">
        <div class="row">
            <div class="footer-ribbon">
                <span>Мы на связи</span>
            </div>

            <div id="footerForm">
            <div class="col-md-10 col-md-offset-2">
                <p class="h1 strong mb-none">Быстрая заявка на услуги</p>
                <p class="h4 strong mt-none">Вы хотите заказать...</p>
            </div>
            <div class="col-md-4 col-md-offset-2">

                <div class="rounded_checkbox">
                    <div class="roundedTwo icon">
                        <input type="checkbox" value="Продвижение сайта" id="roundedTwo1" name="check" />
                        <label for="roundedTwo1"></label>
                    </div>
                    <p class="heading">Продвижение сайта</p>
                </div>
                <div class="rounded_checkbox">
                    <div class="roundedTwo icon">
                        <input type="checkbox" value="Создание сайта" id="roundedTwo2" name="check" />
                        <label for="roundedTwo2"></label>
                    </div>

                    <p class="heading">Создание сайта</p>
                </div>
                <div class="rounded_checkbox">
                    <div class="roundedTwo icon">
                        <input type="checkbox" value="Аудит сайта" id="roundedTwo3" name="check" />
                        <label for="roundedTwo3"></label>
                    </div>
                    <p class="heading">Аудит сайта</p>
                </div>
                <div class="rounded_checkbox">
                    <div class="roundedTwo icon">
                        <input type="checkbox" value="Контекстная реклама" id="roundedTwo4" name="check" />
                        <label for="roundedTwo4"></label>
                    </div>
                    <p class="heading">Контекстная реклама</p>
                </div>
            </div>
            <div class="col-md-5">

                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-laptop" aria-hidden="true"></i></span>
                                <input
                                    data-label="Сайт"
                                    type="text"
                                    class="form-control input"
                                    placeholder="Ваш сайт" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon" id="basic-addon1"><i class="fa fa-user" aria-hidden="true"></i></span>
                                <input
                                    data-label="Имя клиента"
                                    type="text"
                                    class="form-control input"
                                    placeholder="Ваше имя" aria-describedby="basic-addon1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-phone" aria-hidden="true"></i></span>
                            <input
                                data-label="Телефон"
                                type="tel"
                                class="form-control input required"
                                placeholder="Ваш телефон" aria-describedby="basic-addon1">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="input-group">
                            <span class="input-group-addon" id="basic-addon1"><i class="fa fa-envelope-o" aria-hidden="true"></i></span>
                            <input
                                data-value="E-mail"
                                type="text"
                                class="form-control input"
                                placeholder="Ваш e-mail" aria-describedby="basic-addon1">
                        </div>
                    </div>
                </div>

                    <div class="form-group mt-md">
                        <textarea
                            data-label="Сообщение"
                            class="form-control input"
                            rows="" placeholder="Ваше сообщение"></textarea>
                    </div>
                <button type="button" class="btn btn-block btn-lg btn-3d btn-secondary mr-xs mb-sm sender">Отправить</button>
                <div class="alert hidden"></div>
            </div>
            </div>
        </div>
        <hr>
        <div class="row">
            <div class="col-md-3">

                <?php
                    if (isset($last_project)){ ?>
                        <h4>Последний проект</h4>
                        <a href="/works/<?=$last_project->id ?>">
                            <?php if ($last_project->image != "") { ?>
                                <img alt="<?= $last_project->name ?>" src="/img/projects/miniatures/<?= $last_project->image ?>" class="img-responsive img-thumbnail">
                            <?php } else { ?>
                                <img class="img-responsive" src="/img/holder.png" alt="нет изображения">
                            <?php } ?>
                        </a>
                        <p class="h3 color-white strong mt-lg text-center"><?= $last_project->name ?></p>
                  <?php  }
                ?>
            </div>
            <div class="col-md-4 col-md-offset-1">
                <div class="contact-details" itemscope itemtype="http://schema.org/Organization">
                    <span itemprop="name" style="display: none;">Snapix</span>
                    <h4>Контакты</h4>
                    <ul class="contact">
                        <li><p><i class="fa fa-map-marker"></i> <strong>Адрес:</strong><span class="adr" itemprop="address"><?php App::address(); ?></span></p></li>
                        <li><p><i class="fa fa-phone"></i> <strong>Телефон:</strong><span class="tel" itemprop="tel"> <?php App::phone(); ?></span></p></li>
                        <li><p><i class="fa fa-envelope"></i> <strong>Email:</strong> <a rel="nofollow" href="mailto:mail@snapix.ru" class="email" itemprop="email">mail@snapix.ru</a></p></li>

                        <li>
                            <p><strong class="color-white">Реквизиты компании:</strong></p>
                        </li>
                        <li><p>ОГРН 315502200009590</p></li>
                        <li><p>ИНН 502239822409</p></li>
                    </ul>
                </div>
            </div>
            <div class="col-md-4">
                <h4>Мы в соцсетях</h4>
                <div id="vk_groups"></div>
            </div>
        </div>
    </div>
    <div class="footer-copyright">
        <div class="container">
            <div class="row">
                <div class="col-md-1">
                    <a href="/" class="logo">
                        <img alt="Snapix" class="img-responsive" src="/img/logo-dark.png">
                    </a>
                </div>
                <div class="col-md-7">
                    <p>© Маркетинговое агентство Snapix, 2009 - <?php echo date("Y"); ?>. Все права защищены.</p>
                </div>
                <div class="col-md-4">
                    <nav id="sub-menu">
                        <ul>
                            <li><a href="/sitemap">Карта сайта</a></li>
                            <li><a href="/contacts">Контакты</a></li>
                        </ul>
                    </nav>
                </div>
                <!--LiveInternet counter--><script type="text/javascript">document.write("<a href='//www.liveinternet.ru/click' target=_blank><img src='//counter.yadro.ru/hit?t22.6;r" + escape(document.referrer) + ((typeof(screen)=="undefined")?"":";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?screen.colorDepth:screen.pixelDepth)) + ";u" + escape(document.URL) +";h"+escape(document.title.substring(0,80)) +  ";" + Math.random() + "' border=0 width=88 height=31 alt='' title='LiveInternet: показано число просмотров за 24 часа, посетителей за 24 часа и за сегодня'><\/a>")</script><!--/LiveInternet-->
            </div>
        </div>
    </div>
</footer>
<?php  include_once($_SERVER['DOCUMENT_ROOT'] . '/engine/parts/modals.php'); ?>