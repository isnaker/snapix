<header id="header" class="header-narrow header-semi-transparent" data-plugin-options='{"stickyEnabled": true, "stickyEnableOnBoxed": true, "stickyEnableOnMobile": true, "stickyStartAt": 1, "stickySetTop": "1"}'>
<div class="header-body">
<div class="header-container container">
<div class="header-row">
<div class="header-column">
    <div class="header-logo">
        <a href="/">
            <img alt="Snapix" width="142"  src="/img/logo-dark.png">
        </a>
    </div>
</div>
<div class="header-column">
<div class="header-row">
<div class="header-nav header-nav-stripe">
<button class="btn header-btn-collapse-nav" data-toggle="collapse" data-target=".header-nav-main">
    <i class="fa fa-bars"></i>
</button>
<ul class="header-social-icons social-icons hidden-xs">
    <li class="social-icons-facebook"><noindex><a href="https://vk.com/snapix" rel="nofollow" target="_blank" title="vkontakte"><i class="fa fa-vk"></i></a></noindex></li>
</ul>
<div class="header-nav-main header-nav-main-square header-nav-main-effect-2 header-nav-main-sub-effect-1 collapse">
<nav>
<ul class="nav nav-pills" id="mainNav">
<li>
    <a href="/">Главная</a>
</li>
<li id="link-services" class="dropdown dropdown-mega">
    <a  class="dropdown-toggle" href="#">
        Наши услуги
    </a>
    <ul class="dropdown-menu">
        <li>
            <div class="dropdown-mega-content">
                <div class="row">
                    <div class="col-md-4">
                        <h2 class="dropdown-mega-sub-title title_menu">Разработка веб-сайтов</h2>
                        <ul class="dropdown-mega-sub-nav">
                            <li><a data-id="#link-services" href="/landing">Landing Page (Одностраничный сайт)</a></li>
                            <li><a data-id="#link-services" href="/shop">Создание Интернет-магазинов</a></li>
                            <li><a data-id="#link-services" href="/support">Поддержка сайтов</a></li>
                        </ul>
                    </div>
                    <div class="col-md-4">
                        <h2 class="dropdown-mega-sub-title title_menu">Продвижение</h2>
                        <ul class="dropdown-mega-sub-nav">
                            <li><a data-id="#link-services" href="/seo">SEO - продвижение сайтов</a></li>
                            <li><a href="/context">Настройка контекстной рекламы</a></li>
                            <li><a href="/kalkulyator-konteksta">Калькулятор контекста</a></li>
                        </ul>
                    </div>
                    <div class="col-md-3 ">
                        <h2 class="dropdown-mega-sub-title title_menu">Поддержка</h2>
                        <ul class="dropdown-mega-sub-nav">
                            <li><a data-id="#link-services" href="/videonablyudenie-v-kolomne">Видеонаблюдение</a></li>
                            <li><a data-id="#link-services" href="/remont">Ремонт компьютерной техники</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </li>
    </ul>
</li>
<li id="link-portfolio" class="dropdown">
    <a href="/works">Наши работы</a>
</li>
<!-- <li id="link-portfolio" class="dropdown">
    <a href="/store">Купить готовый сайт</a>
</li> -->
    <!--<li id="link-portfolio" class="dropdown">
        <a href="/workers.php">Сотрудники</a>
    </li>-->
<li>
    <a href="/contacts">Контакты</a>
</li>

    <li><a href="tel:<?= $app->phone ?>"><?= $app->phone ?></a></li>

<li>
    <button data-toggle="modal" data-target="#callbackModal" class="btn btn-primary text-uppercase mt-md">
        <strong>Заказать звонок</strong></button>
</li>

</ul>
</nav>
</div>
</div>
</div>
</div>
</div>
</div>
</div>
</header>