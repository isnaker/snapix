<div class="modal fade" id="callbackModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Заказать обратный звонок</h4>
            </div>
            <div class="modal-body">
                <p>Оставьте нам Ваш контактный телефон и наши менеджеры свяжутся с Вами в самое ближайшее время!</p>
                <div class="form-group">
                    <label>Ваш телефон:</label>
                    <input class="form-control" type="tel">
                </div>
                <button class="btn btn-primary btn-block btn-lg sender">Заказать звонок</button>
                <div class="alert text-center hidden mt-md"></div>
              </div>
            <div class="modal-footer">
                <div class="">
                    <small><i class="fa fa-lock fa-4x pull-left"></i> Ваши данные защищены и не передаются третьим лицам. </small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="remontModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Вызвать мастера</h4>
            </div>
            <div class="modal-body">
                <p>Оставьте нам Ваш контактный телефон и наш специалист перезвонит вам в самое ближайшее время!</p>
                <div class="form-group">
                    <label>Ваш телефон:</label>
                    <input class="form-control" type="tel">
                </div>
                <button class="btn btn-primary btn-block btn-lg sender">Заказать звонок</button>
                <div class="alert text-center hidden mt-md"></div>
            </div>
            <div class="modal-footer">
                <div class="">
                    <small><i class="fa fa-lock fa-4x pull-left"></i> Ваши данные защищены и не передаются третьим лицам. </small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="orderShop1CSyncModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Заказать синхронизацию интернет-магазина с 1С</h4>
            </div>
            <div class="modal-body">
                <p>Оставьте нам Ваш контактный телефон и наши менеджеры свяжутся с Вами в самое ближайшее время!</p>
                <div class="form-group">
                    <label>Ваш телефон:</label>
                    <input class="form-control" type="tel">
                </div>
                <button class="btn btn-primary btn-block btn-lg sender">Оставить заявку</button>
                <div class="alert text-center hidden mt-md"></div>
              </div>
            <div class="modal-footer">
                <div class="">
                    <small><i class="fa fa-lock fa-4x pull-left"></i> Ваши данные защищены и не передаются третьим лицам. </small>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="orderSupportModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Заказать обратный звонок</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="h6 text-center">Выбранный тарифный план:</p>
                            <div class="pricing-table princig-table-flat spaced no-borders">
                                <div class="plan"><h3 data-value="selectedPlan"></h3> </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p>Оставьте нам Ваш контактный телефон и наши менеджеры свяжутся с Вами в самое ближайшее время!</p>
                            <div class="form-group">
                                <label>Ваш телефон:</label>
                                <input class="form-control" type="tel">
                            </div>
                            <button class="btn btn-primary btn-block btn-lg sender">Оставить заявку</button>
                            <div class="alert text-center hidden mt-md"></div>
                        </div>
                    </div>
                </div>

              </div>
            <div class="modal-footer">
                <div class="text-center">
                    <small><i class="fa fa-lock"></i> Ваши данные защищены и не передаются третьим лицам. </small>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="orderLandingModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title" id="defaultModalLabel">Заказать лендинг</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-6">
                            <p class="h6 text-center">Выбранный тариф:</p>
                            <div class="pricing-table princig-table-flat spaced no-borders">
                                <div class="plan"><h3 data-value="selectedPlan"></h3> </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <p>Оставьте нам Ваш контактный телефон и наши менеджеры свяжутся с Вами в самое ближайшее время!</p>
                            <div class="form-group">
                                <label>Ваш телефон:</label>
                                <input class="form-control" type="tel">
                            </div>
                            <button class="btn btn-primary btn-block btn-lg sender">Оставить заявку</button>
                            <div class="alert hidden text-center mt-md"></div>
                        </div>

                    </div>
                </div>

              </div>
            <div class="modal-footer">
                <div class="text-center">
                    <small><i class="fa fa-lock"></i> Ваши данные защищены и не передаются третьим лицам. </small>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade" id="orderShopModal" tabindex="-1" role="dialog" aria-labelledby="defaultModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title text-center" id="defaultModalLabel">Заказать интернет-магазин</h4>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-7">
                            <p class="h6">Вы выбрали:</p>
                                <div class="well" data-value="selectedPlan"></div>
                        </div>
                        <div class="col-md-5">
                            <p class="mt-xlg">Оставьте нам Ваш контактный телефон и наши менеджеры свяжутся с Вами в самое ближайшее время!</p>
                            <div class="form-group">
                                <label>Ваш телефон:</label>
                                <input class="form-control" type="tel">
                            </div>
                            <button class="btn btn-primary btn-block btn-lg sender">Оставить заявку</button>
                            <div class="alert hidden text-center mt-md"></div>
                        </div>

                    </div>
                </div>

              </div>
            <div class="modal-footer">
                <div class="text-center">
                    <small><i class="fa fa-lock"></i> Ваши данные защищены и не передаются третьим лицам. </small>
                </div>
            </div>
        </div>
    </div>
</div>


