<!DOCTYPE html>
<html lang="ru-RU">
<head>
    <!-- Basic -->
    <?php include_once($_SERVER['DOCUMENT_ROOT']."/engine/parts/mtags.php"); ?>
    <title><?php if (isset($title)) { print $title; } ?></title>
    <meta charset="utf-8">
    <meta name="google-site-verification" content="roI4WhDGEBeE6qTZ8QqUF1LCAb2Ck_xvxCopHxZjl3c" />
    <meta name="keywords" content="<?php if (isset($keywords)) { print $keywords; } ?>"/>
    <meta name="description" content="<?php if (isset($description)) { print $description; } ?>" />
    <!-- Favicon -->
    <link rel="apple-touch-icon" sizes="57x57" href="/img/favicon/apple-icon-57x57.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/img/favicon/apple-icon-60x60.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/img/favicon/apple-icon-72x72.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/img/favicon/apple-icon-76x76.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/img/favicon/apple-icon-114x114.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/img/favicon/apple-icon-120x120.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/img/favicon/apple-icon-144x144.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/img/favicon/apple-icon-152x152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/img/favicon/apple-icon-180x180.png">
    <link rel="icon" type="image/png" sizes="192x192"  href="/img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/img/favicon/favicon-16x16.png">
    <link rel="manifest" href="/img/favicon/manifest.json">
    <meta name="msapplication-TileColor" content="#ffffff">
    <meta name="msapplication-TileImage" content="img/favicon/ms-icon-144x144.png">
    <meta name="theme-color" content="#ffffff">
    <!--open graph-->
    <meta property="og:title" content="Snapix">
    <meta property="og:description" content="создание и продвижение сайтов">
    <meta property="og:image" content="http://www.snapix.ru/img/logo.png">
    <meta property="og:type" content="profile">
    <meta property="og:url" content="http://www.snapix.ru">
    <!-- Mobile Metas -->
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <!-- Web Fonts  -->
    <?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/css.php"); ?>
    <!-- Current Page CSS -->
    <link rel="stylesheet" href="/vendor/rs-plugin/css/settings.css" media="screen">
    <link rel="stylesheet" href="/vendor/rs-plugin/css/layers.css" media="screen">
    <link rel="stylesheet" href="/vendor/rs-plugin/css/navigation.css" media="screen">
    <link rel="stylesheet" href="/vendor/circle-flip-slideshow/css/component.css" media="screen">
    <?php if (isset($add_css)){
        foreach ($add_css as $item){ ?>
            <link rel="stylesheet" href="<?php print $item ?>">
     <?php   }
    } ?>
</head>
<body>
    <div class="body">
        <?php include_once($_SERVER['DOCUMENT_ROOT'] . '/engine/parts/header.php'); ?>
        <div role="main" class="main">
            <?php if (isset($includable)){
                foreach ($includable as $item){
                    include_once($_SERVER['DOCUMENT_ROOT'].$item);
                }
            } ?>
        </div>
    </div>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/footer.php"); ?>
<?php include_once($_SERVER['DOCUMENT_ROOT'] . "/engine/parts/scripts.php"); ?>

<?php if (isset($add_js)){
    foreach ($add_js as $item){ ?>
        <script type="text/javascript" src="<?= $item ?>"></script>
    <?php   }
} ?>

    <script>
        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

        ga('create', 'UA-78682752-1', { 'userId': 'USER_ID' });
        ga('require', 'displayfeatures');
        ga('require', 'linkid');
        ga('send', 'pageview');

        /* Accurate bounce rate by time */if (!document.referrer ||
            document.referrer.split('/')[2].indexOf(location.hostname) != 0)
            setTimeout(function(){
                ga('send', 'event', 'Новый посетитель', location.pathname);
            }, 15000);
    </script>
   <!-- Yandex.Metrika counter -->
<script type="text/javascript">
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter22886983 = new Ya.Metrika({
                    id:22886983,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true,
                    webvisor:true,
                    trackHash:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/22886983" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


    <!-- Rating@Mail.ru counter -->
    <script type="text/javascript">
        var _tmr = window._tmr || (window._tmr = []);
        _tmr.push({id: "2693142", type: "pageView", start: (new Date()).getTime()});
        (function (d, w, id) {
            if (d.getElementById(id)) return;
            var ts = d.createElement("script"); ts.type = "text/javascript"; ts.async = true; ts.id = id;
            ts.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//top-fwz1.mail.ru/js/code.js";
            var f = function () {var s = d.getElementsByTagName("script")[0]; s.parentNode.insertBefore(ts, s);};
            if (w.opera == "[object Opera]") { d.addEventListener("DOMContentLoaded", f, false); } else { f(); }
        })(document, window, "topmailru-code");
    </script><noscript><div style="position:absolute;left:-10000px;">
            <img src="//top-fwz1.mail.ru/counter?id=2693142;js=na" style="border:0;" height="1" width="1" alt="Рейтинг@Mail.ru" />
        </div></noscript>
    <!-- //Rating@Mail.ru counter -->
</body>
</html>