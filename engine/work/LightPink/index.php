<?php
if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strstr($_SERVER['HTTP_USER_AGENT'],'Android'))
{
  $ios = 'ok';
}
?>
<!DOCTYPE html >
<html>
    <head>
        <title>Главная</title>
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="./img/shIco.png"/>
        <link rel="stylesheet" href="./css/style.css" />
		<script src="js/jquery.js" type="text/javascript"></script>
        <script>
            $(function(){
                //alert("ok");
            });
        </script>
    </head>
    <body>
        <div class="headBar" <?if($ios=='ok'){echo'style="padding-right: 20px;"';}?>></div>
        <div class="centerBlock">
            <div class="headBlock">
                <div class="HeadUzor"></div>
                <div style="position: absolute; left: 320px; top: 90px; width: 370px; height: 37px; background: #a78d76;" class="maska"></div>
                <img style="position: absolute; left: 10px; top: 50px;" src="./img/testMenu.png"/>
                <div class="butM1">
                    <div class="butM1Text">
                        Каталог<br />причёсок
                    </div>
                    <div class="disMenu">
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                    </div>
                </div>
                <div class="butM2"></div>
                <div class="butM3"></div>
                <div class="butM4" onclick="location.href='./stylists.php'"></div>
                <div class="butM5"></div>
                <div class="butM6"></div>
                <div class="butM7"></div>
                <div class="logo" onclick="location.href='./'">Логотип</div>
            </div>
            <div class="bodyBlock">
                <div class="leftBlockBody">
                    <div class="columnMenu">
                        <img class="blockUzorUp" src="./img/blockUzor.png" alt="img"/>
                        <img class="blockUzorD" src="./img/blockUzorD.png" alt="img"/>
                        <div class="punktMenu">
                            <img class="imgLeftMemu" src="./img/blogging_1.png" alt="img"/>
                            <div class="textLeftMemu" style="margin-top: 20px;">Оставить<br /> заявку</div>
                        </div>
                        <div class="punktMenu">
                            <img class="imgLeftMemu" src="./img/icon-info_1.png" alt="img"/>
                            <div class="textLeftMemu" style="margin-top: 20px;">Советы<br /> невестам</div>
                        
                        </div>
                        <div class="punktMenu">
                        
                        </div>
                    </div>
                </div>
                <div class="sladerBlock">
                    <div class="slader">
                        <img style="position: absolute;" src="./img/slader/02.jpg"/>
                    </div>
                    <div class="maska">
                        <div class="sladLB"><img src="./img/sladLeft.png" alt="img"/></div>
                        <div class="sladRB"><img src="./img/sladRight.png" alt="img"/></div>
                    </div>
                </div>
                <div class="rightBlockBody">
                    <div class="columnMenu">
                        <img class="blockUzorUp" src="./img/blockUzor.png" alt="img"/>
                        <img class="blockUzorD" src="./img/blockUzorD.png" alt="img"/>
                        <div class="punktMenu">
                        
                        </div>
                        <div class="punktMenu">
                        
                        </div>
                        <div class="punktMenu">
                        
                        </div>
                    </div>
                </div>
            </div>
            <div class="contentBlock">
                <div class="contentBox">
                    <img class="uzor_1" src="./img/uzor_1.png"/>
                    <img class="uzor_2" src="./img/uzor_2.png"/>
                    <div class="stilistBox"></div>
                    <div class="stilistBox"></div>
                    <div class="stilistBox"></div>
                    <div class="stilistBox"></div>
                    <div class="stilistBox"></div>
                    <div class="stilistBox"></div>
                </div>
            </div>
        </div>
    </body>
</html>