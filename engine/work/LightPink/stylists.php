<?php
if(strstr($_SERVER['HTTP_USER_AGENT'],'iPhone') || strstr($_SERVER['HTTP_USER_AGENT'],'iPod') || strstr($_SERVER['HTTP_USER_AGENT'],'Android'))
{
  $ios = 'ok';
}
?>
<!DOCTYPE html >
<html>
    <head>
        <title>Cтилисты</title>
        <meta charset="utf-8" />
        <link rel="shortcut icon" href="./img/shIco.png"/>
        <link rel="stylesheet" href="./css/style.css" />
		<script src="js/jquery.js" type="text/javascript"></script>
        <script>
            $(function(){
                //alert("ok");
            });
        </script>
    </head>
    <body>
        <div class="headBar" <?if($ios=='ok'){echo'style="padding-right: 20px;"';}?>></div>
        <div class="centerBlock">
            <div class="headBlock">
                <div class="HeadUzor"></div>
                <div style="position: absolute; left: 320px; top: 90px; width: 370px; height: 37px; background: #a78d76;" class="maska"></div>
                <img style="position: absolute; left: 10px; top: 50px;" src="./img/testMenu.png"/>
                <div class="butM1">
                    <div class="butM1Text">
                        Каталог<br />причёсок
                    </div>
                    <div class="disMenu">
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                        <div class="blockMenuP"></div>
                    </div>
                </div>
                <div class="butM2"></div>
                <div class="butM3"></div>
                <div class="butM4" onclick="location.href='./stylists.php'"></div>
                <div class="butM5"></div>
                <div class="butM6"></div>
                <div class="butM7"></div>
                <div class="logo" onclick="location.href='./'">Логотип</div>
            </div>
            <style>
.contentBlockSt {
    position: relative;
    margin-top: 100px;
    float: left;
    width: 1000px;
    height: 400px;
}
.contentBoxSt {
    position: relative;
    float: left;
    margin-top: 10px;
    margin-left: 11px;
    padding-top: 14px;
    margin-bottom: 20px;
    width: 978px;
    background: #915f68;
}
.uzor_1St {
    position: absolute;
    left: 60px;
    top: -75px;
    height: 72px;
}
.uzor_2St {
    position: absolute;
    right: 60px;
    top: -75px;
    height: 72px;
}
.stilistHead {
    position: absolute;
    left: 345px;
    top: -50px;
    height: 50px;
}
.stilistBlock {
    position: relative;
    float: left;
    width: 948px;
    height: 280px;
    margin: 0px 15px 15px 15px;
    background: #c1909b;
    box-shadow: 1px 1px 10px 1px rgba(0,0,0,0.3);
    color: white;
}
.stilAv {
    position: absolute;
    left: 10px;
    top: 10px;
    width: 170px;
    border: 2px solid #e1b0bb;
    box-shadow: 1px 1px 10px 1px rgba(0,0,0,0.3);
}
.hTextSt {
    position: absolute;
    left: 199px;
    top: 22px;
    font-size: 22px;
    font-family: Georgia, 'Times New Roman', Times, serif;
}
.imagesBlockSt {
    position: absolute;
    left: 196px;
    top: 60px;
    width: 741px;
    height: 197px;
    background: #e1b0bb;
    box-shadow: 1px 1px 10px 1px rgba(0,0,0,0.3);
}
.rightStLikeBlock {
    position: absolute;
    right: 18px;
    top: 22px;
    font-size: 22px;
    cursor: pointer;
    opacity: 0.8;
}
.rightStLikeBlock:hover {
    opacity: 1;
}
.rightStLikeBlock>span  {
    position: relative;
    float: right;
    margin-right: 10px;
    font-family: Georgia, 'Times New Roman', Times, serif;
}
.rightStLikeBlock>img {
    position: relative;
    float: right;
    width: 27px;
}
.imagesBlockSt>img {
    position: relative;
    float: left;
    margin-left: 5px;
    margin-top: 5px;
    height: 187px;
    box-shadow: 1px 1px 2px 1px rgba(0,0,0,0.3);
    cursor: pointer;
}
.imagesBlockSt>img:hover {
    box-shadow: 1px 1px 6px 1px rgba(0,0,0,0.3);
    outline: 1px solid rgba(255,255,255,0.7);
}
            </style>
            <div class="contentBlockSt">
                <div class="contentBoxSt">
                    <img class="stilistHead" src="./img/Stilists.png" alt="img"/>
                    <img class="uzor_1St" src="./img/uzor_1.png" alt="img"/>
                    <img class="uzor_2St" src="./img/uzor_2.png" alt="img"/>
                    <div class="stilistBlock">
                        <img class="stilAv" src="./img/timthumb.jpg"/>
                        <div class="hTextSt">Екатерина Андреева — топ-стилист</div>
                        <div class="rightStLikeBlock"><img src="./img/star-icon.png" alt="img"/><span>15 отзывов</span></div>
                        <div class="imagesBlockSt">
                            <img src="img/a1.jpg" alt="img"/>
                            <img src="img/a2.jpg" alt="img"/>
                            <img src="img/a3.jpg" alt="img"/>
                            <img src="img/a4.jpg" alt="img"/>
                            <img src="img/a5.jpg" alt="img"/>
                        </div>
                    </div>
                    <div class="stilistBlock">
                    </div>
                    <div class="stilistBlock">
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>