<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <!--<link rel="icon" href="favicon.ico" type="image/x-icon"/> 
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/> -->
    <style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Noto+Serif:400,700,400italic,700italic&subset=latin,cyrillic-ext);
    
    a {color: white; text-decoration: none;}
    
    body {
        
        font-family: 'Noto Serif', serif;
        min-width: 1000px;
        margin: 0;
        background: black;
    }
    
    #container {
      
    }
    
    .line {
        position: relative;
        width: 100%; 
        display: block;
        background: -moz-linear-gradient(top,  rgba(0,0,0,0.65) 0%, rgba(0,0,0,0.65) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.65)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
        }
    
    .logo {
        padding: 3px 0 5px 0;
        width: 90%;
        margin: 0 auto;
    }
    
    .topMenu {
        display: inline-block; 
        margin: 8px 0 0 0; 
        float: right;
        text-transform: uppercase;
        font-size: 0.9em;
        }
    
    .topMenu a {
        padding: 12px 20px;
        text-align: center;
        background: -moz-linear-gradient(top,  rgba(0,0,0,0.65) 0%, rgba(0,0,0,0.65) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(0,0,0,0.65)), color-stop(100%,rgba(0,0,0,0.65))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(0,0,0,0.65) 0%,rgba(0,0,0,0.65) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#a6000000', endColorstr='#a6000000',GradientType=0 ); /* IE6-9 */
        
    }
    
    .content {
        margin: 50px auto 0 auto;
        position: relative;
        width: 90%;
        
        max-width: 1200px;
        display: block;
    }
    
    .enter-form {

        float: right;
        display: inline-block;

    }
    .enter-form p {
        background: white;
        padding: 5px 8px;
        font-size: 0.85em;
        margin: 4px 0;
    }
    
    .enter-form p:hover {
        cursor: pointer;
        background: #FFFFCA;
    }
    
    #heading p {
        padding: 0; 
        background: #FFFFBF; 
        display: inline-block;
        clear: both;
        padding: 3px 8px;
        margin: 2px 0 20px 0;
        font-size: 0.9em;
        }
    
    h1 {
        margin: 0;
        margin-left: -5px;
        border-radius: 5px;
        text-shadow: 0 1px 1px white;
        padding: 3px 16px;
        text-transform: uppercase; 
        display: inline-block;     
        background: -moz-linear-gradient(top,  rgba(255,255,255,0.89) 0%, rgba(255,255,255,0.89) 1%, rgba(255,255,255,0.89) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0.89)), color-stop(1%,rgba(255,255,255,0.89)), color-stop(100%,rgba(255,255,255,0.89))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e3ffffff', endColorstr='#e3ffffff',GradientType=0 ); /* IE6-9 */
        }
    
    h1:first-letter { text-transform: uppercase; }
    
    .site-description {
        width: 40%;
        padding: 15px;
        background: -moz-linear-gradient(top,  rgba(255,255,255,0.89) 0%, rgba(255,255,255,0.89) 1%, rgba(255,255,255,0.89) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0.89)), color-stop(1%,rgba(255,255,255,0.89)), color-stop(100%,rgba(255,255,255,0.89))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(255,255,255,0.89) 0%,rgba(255,255,255,0.89) 1%,rgba(255,255,255,0.89) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e3ffffff', endColorstr='#e3ffffff',GradientType=0 ); /* IE6-9 */
        border-radius: 5px;
        box-shadow: 0 1px 5px black;
    }
    
    .site-description a {
        color: #004080;
        text-shadow: 0 1px 4px white;
        display: block;
        text-align: center;
        font-size: 0.8em;
        margin: 5px 0;
        padding: 2px 0;
        border-radius: 5px;
    }
    
    .site-description a:hover {
        background: #623100;
        color: white;
        text-shadow: 0 1px 4px black;
    }
    
    .footer {
        position: fixed;
        text-align: center;
        bottom: 0;
        display: block;
        width: 100%;
        background: #0C0C0C;
        padding: 5px 0;
    }
    .footer a {
        margin: 0 1%;
        font-size: 0.9em;
    }
    
    #category {
	    padding-right: 0;
    }
    #category span {
	    background: #c6c6c6;
	    padding: 0 3px;
	    border-radius: 5px 0 0 5px;
    }
    
    #category span:hover {
    	cursor: pointer;
    	color: white;
    	text-shadow: 0 1px 1px black;
	    background: rgb(44,83,158); /* Old browsers */
background: -moz-linear-gradient(top,  rgba(44,83,158,1) 0%, rgba(44,83,158,1) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(44,83,158,1)), color-stop(100%,rgba(44,83,158,1))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(44,83,158,1) 0%,rgba(44,83,158,1) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(44,83,158,1) 0%,rgba(44,83,158,1) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(44,83,158,1) 0%,rgba(44,83,158,1) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(44,83,158,1) 0%,rgba(44,83,158,1) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#2c539e', endColorstr='#2c539e',GradientType=0 ); /* IE6-9 */

    }
    
    .left-column {
	    width: 20%;
	    margin-right: 2%;
	    margin-top: 86px;
	    height: 400px;
	    float: left;
	    display: inline-block;
	    background: -moz-linear-gradient(top,  rgba(255,255,255,0.8) 0%, rgba(255,255,255,0.8) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0.8)), color-stop(100%,rgba(255,255,255,0.8))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ccffffff', endColorstr='#ccffffff',GradientType=0 ); /* IE6-9 */

    }
    
    .search {
    	width: 60%; 
    	height: 30px;
    	float: left; 
    	display: inline-block;
    	margin: 7px 0 0px 0;
    	padding: 0;
    	border-bottom: white solid 1px;
    	}
    
    .search span {
    	width: 10%;
    	background: white;
	    padding: 1px 1%;
	    float: left;
	    display: inline-block;
	    border-radius: 4px 0 0 4px;
    }
    
    .search input { 
    	float: left;
    	width: 86%; 
    	padding:  6px 0.5% 5px 0.5%; 
    	margin: 0px;
    	border-collapse: collapse;
    	border: none;
    	display: inline-block;
    	border-radius: 0 4px 4px 0;
    	 }
    
    /*		CATALOG ITEM STYLES		*/
    
    .catalog-items {float: left; display: inline-block; margin-top: 30px; max-width: 78%}
    
    .item {
    	float: left;
	    display: inline-block;
	    width: 190px;
	    max-height: 90px;
	    overflow-y: hidden;
	    background: white;
	    padding: 10px;
	    margin: 0 15px 10px 0px;
	    font-size: 0.8em;
	    text-shadow: 0px 1px 1px white;
	    color: black;
	    border-radius: 5px;
    }
    
    .item:hover {
background: -moz-linear-gradient(top,  rgba(255,255,255,0.8) 0%, rgba(255,255,255,0.8) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,0.8)), color-stop(100%,rgba(255,255,255,0.8))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(255,255,255,0.8) 0%,rgba(255,255,255,0.8) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ccffffff', endColorstr='#ccffffff',GradientType=0 ); /* IE6-9 */

    }
    </style>
<title>Антиквар</title>
</head>

<body>
<div id="container">
<img src="img/back.jpg" style="position: absolute; top: 0; left: 0; min-width: 1000px; width: 100%;" />

<div class="line">
    <div class="logo">
        <img src="img/logo.png" />
       <div class="topMenu">
            <a class="top-menu" href="#">Главная</a>
            <a class="top-menu" href="catalog">Каталог</a>
            <a class="top-menu" href="#">Магазин</a>
            <a class="top-menu" href="#">Контакты</a>
        </div> 
    </div>
</div>

<div class="content">
<div class="left-column">
	<div class="left-column-header">Категории</div>	
		<div class="left-column-item">Монеты</div>
		<div class="left-column-item">Боны</div>
		<div class="left-column-item">Марки</div>
		<div class="left-column-item">Оружие</div>
</div>


<h1 id="category">Все предметы <span>&darr;</span></h1>
<div class="search"><span>ПОИСК:</span><input id="searchform"></div>

<div class="catalog-items">
<?php

	$database = "snapixru_ant";
	$db = mysql_connect('localhost','snapixru_ant','111111') or die("Could not connect: " . mysql_error());
	mysql_select_db($database, $db);
	
	mysql_query("SET NAMES 'cp1251'");
	mysql_query("SET CHARACTER SET 'cp1251'");
    mysql_query("SET SESSION collation_connection = 'cp1251'");
    //
    //
    function getAllItems() {
	    mysql_query("SET NAMES 'utf8'");
	    mysql_query("SET CHARACTER SET 'utf8'");
	    mysql_query("SET SESSION collation_connection = 'utf8'");
	    //
	    $ResultCategories = mysql_query("SELECT * FROM `categories`");
	    $ResultSubCategories = mysql_query("SELECT * FROM `subcategories`");
	    $ResultItems = mysql_query("SELECT * FROM `items`");
	    $ResultHealth = mysql_query("SELECT * FROM `health`");
	    //
	    if ((!$ResultCategories) || (!$ResultSubCategories) || (!$ResultItems)){
	        echo 'ERROR MATHAFAKA'.mysql_error(); 
	        break;
	    } else {
	        $i = 0;
	        
	        //  ITEMS
	        while ($row = mysql_fetch_object($ResultItems)) {
	            //
	            $json['data']['items'][$i] = $row;
	            //
	            $i++;
	        }
	        
	        //	CATEGORIES
	        $i=0;
	        while ($row = mysql_fetch_object($ResultCategories)){
	            $json['data']['categories'][$i] = $row;
	            $i++;
	        }
	        
	        //	SUBCATEGORIES
	        $i=0;
	        while ($row = mysql_fetch_object($ResultSubCategories)){
	            $json['data']['subcategories'][$i] = $row;
	            $i++;
	        }
	        
	        //	HEALTH
	        $i=0;
	        while ($row = mysql_fetch_object($ResultHealth)){
	            $json['data']['health'][$i] = $row;
	            $i++;
	        }
	        return ($json);
	    }
	}
	
	$AllItems = getAllItems();
	
	
	// PRINTING ALL LOTS
	for ($i =0; $i < count($AllItems['data']['items']); $i++){
		$item = $AllItems['data']['items'][$i];
		
		if ( strlen($item->images) > 0 ) { $itemImg = '<img width= 80 src="'.$item->images.'"/>'; }
	
		print "<a class='item' href='catalog?itemid=". $item->id ."'> Лот № ".$item->id. $itemImg.
		"<div class='item-name'>Название: ". $item->name."</div>". 
		"<div class='item-year'>Год: ".$item->year."</div>".
		"<div class='item-comment'>Описание: ".$item->comment."</div>"
		."</a>";
		
		$item = "";
	}
?>
</div>
<hr style="clear: both; visibility: hidden">
</div>

<div class="footer">
    <a class="bottom-menu" href="#">Главная</a>
    <a class="bottom-menu" href="#">Каталог</a>
    <a class="bottom-menu" href="#">Магазин</a>
    <a class="bottom-menu" href="#">Контакты</a>
</div>

</div>
</body>
</html>