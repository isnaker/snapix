<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
	<head>
		<meta http-equiv="content-type" content="text/html; charset=UTF-8" />
		<link href="css/style.css" rel="stylesheet" type="text/css"/>
		<title>ant</title>
        <script type="text/javascript"  src="js/jQuery.js"></script>
        <script type="text/javascript" src="js/ajaxupload.js"></script>
		<script type="text/javascript">
	        var theItems = [];
            var Categories = [];
            var SubCategories = [];
            var Health = [];
            //
           function getItems(){
    
                var request = {
                    request: 'getAllItems'
                };
            
                request = JSON.stringify(request);
            
                var xmlhttp;
                if (window.XMLHttpRequest){
                    xmlhttp = new XMLHttpRequest();
                }
                else{
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            
                xmlhttp.onreadystatechange = function()
                {
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        var resp = xmlhttp.responseText;
                        resp = JSON.parse(resp);
                        
                        if (resp.response === 'OK') {
                            theItems = [];
                            theItems.length = 0;
                            console.log(resp);
                            try {
                                theItems = resp.data.items;
                                Health = resp.data.health;
                                Categories = resp.data.categories;
                                SubCategories = resp.data.subcategories;
                            } catch (e) {
                                console.log('orderList Parsing Error', e);
                            }
                            showItems();
                                                      
                        } else {
                            alert('Неизвестная ошибка');
                        }
                        
                    }
                }
                xmlhttp.open("POST", "commands.php", true);
                xmlhttp.send(request);
            }
            
            function addItems(){
                if ($('#visible').prop('checked')) {visibleChecked = 1;} else {visibleChecked = 0;}
                var request = {
                    request: 'addItems',
                    addItems: {
	                    category_id: $("#category").val(),
	                    subcategory_id: $('#subcategory').val(),
	                    name: $('#name').val(),
	                    year: $('#year').val(),
	                    health: $('#health').val(),
	                    price: $('#price').val(),
	                    count: $('#count').val(),
	                    comment: $('#comment').val(),
                        //images: $('#imgLoadFile').val(),
	                    visible: visibleChecked
                    }
                };
                
                //alert($('#imgLoadFile').val());
			    
			    var xmlhttp;
			    if (window.XMLHttpRequest){
			        xmlhttp = new XMLHttpRequest();
			    }
			    else{
			        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
			    }
			
			    xmlhttp.onreadystatechange = function()
			    {
			
			        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
			        {
			            var resp = xmlhttp.responseText;
			            resp = JSON.parse(resp);
			
			            if (resp.response === 'OK') {
			  
			                try {
			                    
			                } catch (e) {
			                    console.log('orderList Parsing Error', e);
			                }
			            } else {
			                alert('Неизвестная ошибка');
			            }
			        }
			    };
			    xmlhttp.open("POST", "commands.php", true);
			    xmlhttp.send(request);
            }
            //
            function showItems(){
                //console.log(theItems);
                //
                for (i=0;i<theItems.length;i++){
                    //alert(theItems.length);
                    var bodyBoxPole = '<div class="boxPole">';
                    bodyBoxPole += '<div class="namePole">'+theItems[i].name+'</div>';
                    if (SubCategories[theItems[i].subcategory_id-1].name != ''){
                        SubCategoriesPole = ' / '+SubCategories[theItems[i].subcategory_id-1].name;
                    }
                    bodyBoxPole += '<div class="CategoriesPole">'+Categories[theItems[i].category_id - 1].name+SubCategoriesPole+'</div>';
                    
                    //$("#content").append('<div class="Pole">'+theItems[i].year+'</div>');
                    //$("#content").append('<div class="Pole">'+ Health[theItems[i].health-1].comment+'</div>');
                    //$("#content").append('<div class="Pole">'+theItems[i].price+'</div>');
                    //$("#content").append('<div class="Pole">'+theItems[i].count+'</div>');
                    //$("#content").append('<div class="Pole">'+theItems[i].comment+'</div>');
                    //$("#content").append('<div class="Pole">'+theItems[i].date_created+'</div>');
                    //$("#content").append('<div class="Pole">'+theItems[i].date_updated+'</div>');
                    if (theItems[i].images == ''){
                        imgPole = './img/white.jpg';
                    } else {
                        imgPole = './img/'+theItems[i].images;
                    }
                    bodyBoxPole += '<div class="imgPole"><img src="'+imgPole+'"/></div>';
                    bodyBoxPole += '</div>';
                    $("#content").append(bodyBoxPole);
                    //$("#content").append('<div class="Pole">'+theItems[i].visible+'</div>');
                }
                
            }
            $(function() {
                
            	$("#addItems").click(function() {

                    var CategoriesName = '<option value="" selected></option>';
                    for (i=0;i<Categories.length;i++){
                        CategoriesName += '<option value="'+Categories[i].id+'">'+Categories[i].name+'</option>';
                    }
                    $('#category').html(CategoriesName);

                    var SubCategoriesName = '<option value="" selected>Нет</option>';
                    for (i=0;i<SubCategories.length;i++){
                        SubCategoriesName += '<option value="'+SubCategories[i].id+'">'+SubCategories[i].name+'</option>';
                    }
                    $('#subcategory').html(SubCategoriesName);

                    var yearSor = '<option value="none" selected>Не известно</option>';
                    for (i=2013;i>0;i--){
                        yearSor += '<option value="'+i+'">'+i+' Г</option>';
                    }
                    $('#year').html(yearSor);

                    var healthSor = '<option value="none" selected>Не известно</option>';
                    for (i=0;i<Health.length;i++){
                        healthSor += '<option value="'+Health[i].id+'">'+Health[i].comment+' = '+Health[i].name+'</option>';
                    }
                    $('#health').html(healthSor);

            		$("#BlackBox, #FormBox").css("display","block");
                    
            	});
                
                $("#HeadForm button").click(function() {
            		$("#BlackBox, #FormBox").css("display","none");
            	});
                $("#images").change(function() { 
                    $(".images").text($("#images").val());
                    $.post('upimages.php', {
                        images: $("#images").val()
                    },function(data){
                        alert(data);
                    });
                });
                // Загрузка картинки
                $('label[for=file]').append('<iframe src="none.html" id="inFile" style="display: none;"></iframe>');
                $('label[for=file]').click(function(){
                    $('#inFile').contents().find('body').html('<form action="http://snapix.ru/work/ant/engine/upload.php" method="POST" enctype="multipart/form-data"><input style="display: none;" id="file" type="file" name="file"/></form>');
                    $('#inFile').contents().find('input#file').change(function(){
                        $('#infoFile').html('Загрузка...<img class="LoadImg" src="images/Animation.gif">');
                        $('#inFile').contents().find('form').submit();
                    });
                    $('#inFile').contents().find('input#file').click();
                });
                
            });
            getItems();
		</script>
	</head>
	<body onload="">
		<div id="head" onclick="">
			<h2>Администрирование</h2>
			<span id="addItems">Добавить запись</span>
            <img src="images/Animation.gif" title="А если у нас будит такая иконка загрузки (Chaki)" style="width: 80; height: 60px; float: right; margin-top: -8px; cursor: help;" />
		</div>
		<div id="content">
            
        </div>
		<div id="GrouBox"></div>
		<div id="BlackBox"></div>
		<div id="FormBox">
            <div id="HeadForm"><span>Добавить запись</span><button>НАЗАД</button></div>
            <div style="width: 310px; border-right: 1px solid #999999; display: inline-block; float: left; position: relative; padding-top: 15px;">
    			<div style="width: 300px;"><label for="category">Категория : </label><select id="category">
    				<option value="" selected></option>
    			</select></div>
    			<div style="width: 300px;"><label for="subcategory">Под категория : </label><select id="subcategory">
    				<option value="" selected></option>
    			</select></div>
    			<div style="width: 300px;"><label for="name">Наименование : </label><input id="name" type="text" name="name" value="" /></div>
    			<div style="width: 300px;"><label for="year">Год : </label><select id="year">
    				<option value="" selected></option>
    			</select></div>
    			<div style="width: 300px;"><label for="health">Качество : </label><select id="health">
    				<option value="" selected></option>
    			</select></div>
    			<div style="width: 300px;"><label for="price">Цена : </label><input id="price" type="text" name="price" value="" /></div>
    			<div style="width: 300px;"><label for="count">Количество : </label><input id="count" type="text" name="count" value="" /></div>
    			<div style="width: 300px;"><label for="comment">Опесание : </label><textarea id="comment" name="comment"></textarea></div>
    			<div style="width: 300px;"><label for="visible">Видимость : </label><input id="visible" type="checkbox" name="visible"/></div>
            </div>
            <div style="width: 310px; display: inline-block; float: left; position: relative; padding-top: 15px;">
                <div style="width: 300px;">
                    <label for="file">Изображение : </label>
                    <input id="imgLoadFile" type="hidden" value="">
                    <div id="infoFile">Нет</div>
                </div>
    			<div style="width: 300px;">
                    <div id="imgFile">
                        <img src="./img/white.jpg"/>
                    </div>
                </div>
                <div style="width: 300px;"><input id="submit" type="button" name="submit" value="Сохранить" onclick="addItems()"/></div>
    		</div>
        </div>
	</body>
</html>