<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

	<head>
		<title>Студия интерьерного дизайна "Золотые руки" - Портфолио</title>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		
		<meta name="keywords" content="Дизайн интерьеров, Золотые руки, Коломна, дизайн, интерьер, отделка">
		<meta name="description" content="Дизайн интерьеров от мастеров своего дела. Перемены, доступные каждому!">
		
        <link rel="stylesheet" href="css/basic.css" type="text/css">
		<link rel="stylesheet" href="css/galleriffic-2.css" type="text/css">
        <link rel="stylesheet" href="style.css">
		<meta name="viewport" content="width=1024">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        		
        <script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
        <script src="js/jquery.timers.js" type="text/javascript"></script>
    	
		<script type="text/javascript" src="js/jquery.galleriffic.js"></script>
		<script type="text/javascript" src="js/jquery.opacityrollover.js"></script>
        
<script type="text/javascript">
 $(document).ready(function()
{
$("body").animate({opacity: 1},1000, function() {
 $.ajax({
                type:"POST",
                url :"gallery_imgs/1/1.php",
                data:{/*'title':title*/},
                success: function(html)
                     { 
                        $('.preview').html(html); 
                      //  $('.close').click(close);   
 
                     }
        });              
});

$('.left-side a').click(function() {
    
    var link = $(this);

   $.ajax({
                type:"POST",
                url :this + "/1.php",
                data:{/*'title':title*/},
                success: function(html)
                     { 
                        $('.preview').html(html); 
                        $('.current').removeClass('current');
                        link.addClass('current');
                     }
        });
      return false;
      
   

});


});
</script>        
        <style type="text/css">
        .preview {
            width: 76%; 
            min-height: 400px; 
            background: none; 
            float: left; 
            margin: 1% 0 0  5%; 
            text-align: left;
            }
            
        .preview h1 {
            text-transform: uppercase;
            font-size: 2.2em;
            font-weight: bold;
            text-shadow: 0 1px 3px white;
        }
        
        .preview h2 {
            display: block; 
            width: 96%; 
            margin: 2%; 
            text-transform: uppercase;
            }
        
        .slides {
            width: 96%; 
            border: silver solid 1px; 
            margin: 2%; 
            min-height: 400px; 
            height: 100%; 
            background: #FFFFD9;
            }
        
        .description {
            text-align: left; 
            font-size: 1.1em; 
            line-height: 1.5; 
            margin: 2% 4% 4% 4%;
            float: left;
            }
        
        .slideshow img {max-width: 500px; border: white solid 14px; box-shadow: 0 0 15px black }
        
        .image-desc {
            background: url(img/opac75w.png) repeat; 
            padding: 15px; 
            margin: 10px 15px 0 25px; width: 100%; text-align: justify;}
        .left-side-a p {line-height: 1; margin: 0 5px 0 10px; padding: 3px 0; text-shadow: 0 1px 1px black; display: list-item; list-style: square; position: relative; margin-left: 28px;}
.left-side-a:hover,  .selected {background: #400000;}
</style>
    </head>
    
    <body>
    <img src="img/blur.jpg" style="position: absolute; z-index: -1; top:0; left:0; width: 100%;" />    
    <div id="container">
    
    <div class="left-side"> 
    
        <img src="img/logo.png" alt="Студия интерьерного дизайна Золотые руки">
        
      <a href="gallery_imgs/1" class="left-side-a selected"><p>Гостиная-столовая в квартире</p></a> 
      <a href="gallery_imgs/2" class="left-side-a"><p>Дача в Московской области</p></a> 
      <a href="gallery_imgs/3" class="left-side-a"><p>Квартира в мансардном этаже</p></a> 
    
    </div>
    
    <?php include_once('horizontal_menu.php'); ?>
    
<div class="preview">
</div>
</div>

<?php include_once('footer.php'); ?>
    </body>
    
    </html>