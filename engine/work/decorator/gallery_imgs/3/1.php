				<h1>Квартира в мансардном этаже</h1>


				<!-- Start Advanced Gallery Html Containers -->
				<div id="gallery" class="content" style="float: left;">
			
					<div class="slideshow-container" >
						<div id="loading" class="loader"></div>
						<div id="slideshow" class="slideshow"></div>
					</div>
                    
					<div id="caption" class="caption-container"></div>
				</div>
				<div id="thumbs" class="navigation">
					<ul class="thumbs noscript">
                    
                    <?php
                    
            $dir = 'img/';
            $files = scandir($dir);
            $count = count($files)-1;
 
             for ($i=1; $i<$count; $i++)
             {
                
                echo "

                	<li>
                    <a class='thumb' href='gallery_imgs/3/img/$i.jpg' title='Title #0'>
                      <img width='75' height='55' src='gallery_imgs/3/img/$i.jpg' alt='Title #0' />
                    </a>
                      <div class='caption'>
                        <div class='image-title'>Описание</div>
                      <div class='image-desc'>
                    Основным заданием заказчиков в этом проекте было - больше простора и воздуха. Поэтому 
                    планировка в данном случаи не менялась, а основное внимание было уделено финишной 
                    декоративной отделке. Каждое помещение наделено своим  колоритом. Изюминкой проекта стали, 
                    любимые многими дизайнерами и применяемые в интерьерах уже достаточно давно декоративные 
                    полиуретановые балки. Для каждого помещения нами был выбран свой цвет и рисунок расположения 
                    балок.
                      </div>
                    </div>
                    </li>";
             }       
             ?>      
					</ul>
				</div>


		<script type="text/javascript">
			jQuery(document).ready(function($) {
				// We only want these styles applied when javascript is enabled
				$('div.navigation').css({'width' : '30%', 'float' : 'left'});
				$('div.content').css('display', 'block');

				// Initially set opacity on thumbs and add
				// additional styling for hover effect on thumbs
				var onMouseOutOpacity = 0.67;
				$('#thumbs ul.thumbs li').opacityrollover({
					mouseOutOpacity:   onMouseOutOpacity,
					mouseOverOpacity:  1.0,
					fadeSpeed:         'fast',
					exemptionSelector: '.selected'
				});
				
				// Initialize Advanced Galleriffic Gallery
				var gallery = $('#thumbs').galleriffic({
					delay:                     2500,
					numThumbs:                 15,
					preloadAhead:              10,
					enableTopPager:            true,
					enableBottomPager:         true,
					maxPagesToShow:            7,
					imageContainerSel:         '#slideshow',
					controlsContainerSel:      '#controls',
					captionContainerSel:       '#caption',
					loadingContainerSel:       '#loading',
					renderSSControls:          true,
					renderNavControls:         true,
					playLinkText:              'Play Slideshow',
					pauseLinkText:             'Pause Slideshow',
					prevLinkText:              '&lsaquo; Previous Photo',
					nextLinkText:              'Next Photo &rsaquo;',
					nextPageLinkText:          'Next &rsaquo;',
					prevPageLinkText:          '&lsaquo; Prev',
					enableHistory:             false,
					autoStart:                 false,
					syncTransitions:           true,
					defaultTransitionDuration: 900,
					onSlideChange:             function(prevIndex, nextIndex) {
						// 'this' refers to the gallery, which is an extension of $('#thumbs')
						this.find('ul.thumbs').children()
							.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
							.eq(nextIndex).fadeTo('fast', 1.0);
					},
					onPageTransitionOut:       function(callback) {
						this.fadeTo('fast', 0.0, callback);
					},
					onPageTransitionIn:        function() {
						this.fadeTo('fast', 1.0);
					}
				});
			});
		</script>
</html>