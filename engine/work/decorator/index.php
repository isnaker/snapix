<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

	<head>
		<title>Студия интерьерного дизайна "Золотые руки"</title>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
        <meta name="viewport" content="width=1024">
		<meta name="keywords" content="Дизайн интерьеров, Золотые руки, Коломна, дизайн, интерьер, отделка, студия дизайна">
		<meta name="description" content="Дизайн интерьеров от мастеров своего дела. Перемены, доступные каждому!" >
    
        <meta http-equiv="content-type" content="text/html; charset=utf-8" >
        
        

<link type="text/css" href="css/skitter.styles.css" media="all" rel="stylesheet">
<script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
<script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
<script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
<script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
<script type="text/javascript" src="js/jquery.skitter.min.js"></script>
<script src="js/jquery.timers.js" type="text/javascript"></script>


<!-- Init Plugin -->
<script type="text/javascript">
$(document).ready(function() {
		$(".box_skitter").skitter({
			animation: "randomSmart",
			dots: true, 
 			preview: true, 
 			hideTools: true
		});
	});
</script>

<script type="text/javascript">
            $(function(){
                $("#SlidePart div:eq(0)").css({"background": "#f00"});
                $("#Loading").oneTime("2s", function() {
                    
                    $("#Loading").animate({"opacity":"0"},150,function(){
                        ti = 200;
                        $(".menu_box").animate({"opacity":"1"},ti);
                        $("#SlideWindow").animate({"opacity":"1"},ti+ti);
                        $(".column-40").animate({"opacity":"1"},ti+ti);
                        $(".contact_form").animate({"opacity":"1"},ti+ti);
                        $(".services").animate({"opacity":"1"},ti);
                        $(".lyric").animate({"opacity":"1"},ti+ti);
                        $(".footer").animate({"opacity":"1"},ti+ti);
                        $("#Loading").css({"display":"none"});
                        $("#BlockMenu").everyTime(200, "BlockMenuTimer", function(i) {
                            if (i==6) {$("#BlockMenu").stopTime("BlockMenuTimer");}
                            p=i-1;
                            $(".BlockPuntMenu:eq("+p+")").animate({"margin-left": "-=250px","opacity": "1"},400);
                        }, 60);
                        
                        });
                    });
                });
        </script>
        
        <script type="text/javascript">
            $(function(){

            
            $(".button").click(function(){
	            $('.send_message').css({'display':"block", 'zIndex':'999'}).animate({'opacity':'1','left':'300'},1000);
            });
            
            $(".close").click(close);
            
            function close()
            {
	             $('#message').val('');
                 $('#user').val('');
                 $('.send_message').animate({'opacity':'0','left':'0'},600, function(){
                    $('.send_message').css({'display':"none", 'zIndex':'1'});
                    });
                    
                 $('.warnings').html('').css({'display':"none"});
                 
            };
            
            $('.send_button').click(function(){
              var user, message;
              user = $('#user').val();
              message = $('#message').val(); 
              
              if (message =='') {$('.warnings').css({'display':"inline-block"}).html('Введите Ваше сообщение!');  }
              
              else
              
              //if (user !='') and (message !='')
              {       
               $.ajax({
                type: "POST",
                url: "message.php",
                data: {'user':user, 'message':message},
                success: $('.request').css({'display':"block"})
               }); 
               close(); 
               }             
            });
    });
</script>
 <style type="text/css">
@import url(http://fonts.googleapis.com/css?family=Ubuntu+Condensed&subset=latin,cyrillic-ext);

@media only screen and (max-device-width: 1024px) { 
body { 
    background: url('img/blur-ipad.jpg') no-repeat !important;  
    background-color: #b9936f;
    } 

}
  
    html {
        background: #b9936f;  
        color: white;      
        min-height: 1200px;
        min-width: 960px;
        cursor: default;
        }

    body {
        background-attachment: fixed;
        background-color: #b9936f;
        text-align: center;
        font-family: 'Ubuntu Condensed', sans-serif;
        padding:0;
        margin: 0;
        padding: 0px 0;
        display: block;
        min-height: 1200px;
        min-width: 960px;
        }
       
    a {color: white; text-decoration: none;}
    
    .container {
        width: 95%; 
        max-width: 1280px;
        min-height: 300px; 
        background: none; 
        position: relative; 
        display: inline-block; 
        }
    
    .menu_box { 
        width: 19%; 
        height: 600px;
        float: left;
        border-radius: 0 0 0 210px  ;
        text-align: center;
        padding-top: 20px;
        opacity: 0;
        box-shadow: 0 1px 15px black ;
        border-left: white solid 4px;
        z-index: 10;
        position: relative;
        
background: -moz-linear-gradient(top,  rgba(135,38,2,0.93) 0%, rgba(127,27,2,0.83) 45%, rgba(119,31,0,0.7) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(135,38,2,0.93)), color-stop(45%,rgba(127,27,2,0.83)), color-stop(100%,rgba(119,31,0,0.7))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed872602', endColorstr='#b3771f00',GradientType=0 ); /* IE6-9 */

        }
        
    .menu_box img {margin: 25px 10% 0 9%; width: 80%; max-width: 175px}
    
    .menu_box p {margin: 5px 0 0 0; font-size: 0.8em; text-shadow: 0 1px 2px black}
  
    .head_wrap a{ color: white; font-size: 0.75em; margin: 0 1% 0 0;}
    .head_wrap img { margin-left: 120px;}


    /* TOP MENU */
    
            .right-side {
            width: 80%; 
            margin: 0 ; 
            display: inline-block; 
            float: left;
            border-radius: 0px 0px 10px 0;
            box-shadow:  0px 1px 15px black;
            z-index: 0;
            border-bottom: white solid 1px;
background: -moz-linear-gradient(top,  rgba(135,38,2,0.93) 0%, rgba(127,27,2,0.83) 45%, rgba(119,31,0,0.7) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(135,38,2,0.93)), color-stop(45%,rgba(127,27,2,0.83)), color-stop(100%,rgba(119,31,0,0.7))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed872602', endColorstr='#b3771f00',GradientType=0 ); /* IE6-9 */

            }
    
    .top-menu {float: right;}    
            
        .top-menu a {
            display: inline-block; 
            height: 100%;  
            margin: 0;
            padding: 2% 15px;
            text-transform: uppercase;
            font-size: 0.8em;
            text-shadow: 0 1px 2px black;
            }
            
        .top-menu a:hover {
            color: #FFFFE6; 
            background: url('http://www.zolotieruki.com/img/opac60r.png') repeat;
            }
            
        .top-menu a:last-child:hover { border-radius: 0 0 12px 0; }
        
        .top-menu a.current {background: url('img/opac60r.png') repeat; border-bottom: gold solid 1px }
            
    .about_design {
        width: 78%;   
        min-height: 300px; 
        margin: 15px auto 0 auto; 
        line-height: 1.5; 
        float: left; 
        
        
        }
    
    .column-40 {
        width: 45%; 
        margin: 0 0 0 30px; 
        text-align: left; 
        font-size: 0.80em; 
        color: white;
        padding: 5px 5% 3% 5%;
        border-radius: 0 200px  0  220px ;
        text-shadow: 0 1px 5px black;
        float: left;
        opacity: 0;
        border-right: white solid 4px;
        box-shadow: 0 1px 15px black;
background: -moz-linear-gradient(top,  rgba(135,38,2,0.93) 0%, rgba(127,27,2,0.83) 45%, rgba(119,31,0,0.7) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(135,38,2,0.93)), color-stop(45%,rgba(127,27,2,0.83)), color-stop(100%,rgba(119,31,0,0.7))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed872602', endColorstr='#b3771f00',GradientType=0 ); /* IE6-9 */

        }
        
    .column-40 h2 {
        margin: 0; 
        padding: 25px 0; 
        font-size: 1.7em; 
        line-height: 1.1; 
        text-transform: uppercase;
        width: 70%;
        }
    
    .lyric {
        background: url('img/opac70r.png') repeat; 
        position: relative;
        color: white;
        width: 65%;
        font-size: 0.95em; 
        line-height: 1.5; 
        float: left;
        margin: 2% 7%;
        padding: 25px 95px 25px 130px ;
        border-radius:  220px  0  220px 0;
        text-shadow: 0 1px 5px black;
        border-left: maroon solid 3px ;
        border-right: maroon solid 3px;
        opacity: 0;
        }
        
    .lyric small {display: block; margin: 15px 0 0 0;}
    
    .contact_form {
        color: black;
        float: left; 
        width: 18%; 
        position: relative; 
        height: 300px ;
        background: none;
        border-right: black solid 1px;
        line-height: 1;
        text-align: left;
        text-shadow: 1px 1px 3px white;
        opacity: 0;
        
        
        }
    .contact_form hr {width: 100%;}
    
    .contact_form h3 {
        margin: 55px 0 0 0;
        text-transform: uppercase;
        }
    
    .contact_body {width: 90%;}
    
    .contact_body small {font-size: 0.72em;}
    
    .services {
        width: 80%; 
        margin:25px auto 0 auto; 
        background: url('img/opac60b.png') repeat;
        background: none;
        display: block;
        float: right;
        font-size: 0.8em;
        border-radius: 4px;
        padding-top: 1%;
        opacity: 0;
        }
    .services a {text-decoration: none; color: white; line-height: 1.5;}
    .services li {list-style: none;}  

    .column {
        width: 27%; 
        text-align: left;
        text-transform: uppercase; 
        background: url('img/opac70b.png') repeat;
        margin: 0% 0.5% 1% 1.5% ; 
        position: relative; 
        display: inline-block; 
        float: left;
        padding: 1% 2%;
        text-shadow: 0 1px 3px black;
        border-radius:12px;
        }
    
    .column h1 {
        margin: 5px 0 10px 0;
        padding: 0 0 12px 0;
        font-size: 1.65em; 
        border-bottom: silver solid 1px;
        }
    
    .column a{ 
        display: block; 
        margin: 5px auto 5px 8px; 
        font-size: 1.1em; 
        border-bottom: dotted 1px black;
        }
        
    .column a:hover {
        border-bottom: dotted 1px white;
        }
    
    .column ul {margin: 0 0  0 12px; line-height: 1.33; padding: 0;}
    
      .footer {
        background: none; 
        width: 100%; 
        margin: 150px 0 0 0;
        text-align: center;
        padding: 0 0 25px 0;
        }
        
    .footer small {
        color: black; 
        font-weight: bold;
        text-shadow: 1px 0px 1px rgba(255,255,255,0.85); 
        margin: 5px 2%; 
        display: block;
        }
        
    .footer small a {
        padding: 2px 5px; 
        color: black; 
        background: url('img/opac60w.png')repeat;
        } 
        
    .footer small a:hover {background: white; }   
    
    .footer img.logo {height:175px; margin: -80px 0 0 12px;}
    
    .counters {margin: 0 1% 0 0; padding: 25px 0 0 13px; opacity: 0.6}
    
    textarea {width: 100%; max-width: 100%; max-height: 150px;}    
             
    /* Меню */    
        
    #BlockMenu {
        margin-left: 10%;
        margin-top: 20px;
        float: right;
        width: 28%;  
        border-radius: 5px;
        overflow: hidden;
        border: white solid 2px;
        box-shadow: 0 1px 15px black;
    }
    
    .BlockPuntMenu {
        width: 100%;
        text-align: left;
        text-transform: uppercase;
        line-height: 1.2;
        font-size: 0.9em;
        margin-left: 250px; 
        opacity: 0;
        text-shadow: 0 1px 3px black;
background: -moz-linear-gradient(top,  rgba(63,76,107,0.95) 0%, rgba(63,76,107,0.9) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(63,76,107,0.95)), color-stop(100%,rgba(63,76,107,0.9))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(63,76,107,0.95) 0%,rgba(63,76,107,0.9) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(63,76,107,0.95) 0%,rgba(63,76,107,0.9) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(63,76,107,0.95) 0%,rgba(63,76,107,0.9) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(63,76,107,0.95) 0%,rgba(63,76,107,0.9) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f23f4c6b', endColorstr='#e63f4c6b',GradientType=0 ); /* IE6-9 */

    } 
    
    .BlockPuntMenu:hover {background: #004080 }
    
    .BlockPuntMenu a {padding: 12px 0px 10px 12px; display: block;}
    
    /* Предзагрузка */
    
    #Loading {
        position: fixed;
        background: #fff;
        width: 100%;
        height: 100%;
        z-index: 999;
        
        background: rgb(255,255,255); /* Old browsers */
        background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(47%,rgba(246,246,246,1)), color-stop(100%,rgba(237,237,237,1))); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* IE10+ */
        background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(246,246,246,1) 47%,rgba(237,237,237,1) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ededed',GradientType=0 ); /* IE6-9 */ 
    }
    
    #Loading img {
        position: absolute;
        left: 50%;
        top: 50%;
        margin-left: -8px;
        margin-top: -8px;
        z-index: 1000;
    }
    #te {
        color: #000;
    }
    
    /* buttons */

a.button {
	text-decoration: none;
	border: gray solid 1px;
	cursor: pointer;
	outline: none;
	color:#111;
    font-size: 12px;
    text-align: center;
	display:inline-block;
	vertical-align:top;
	position:relative;
	font-weight:bold;
	background-color:#aaa;
	background: #aaa -moz-linear-gradient(top, rgba(255,255,255,.75), rgba(255,255,255,0));
	background: #aaa -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(255,255,255,.75)), to(rgba(255,255,255,0)));
	text-shadow:1px 1px 0 rgba(255,255,255,.67);
    border-radius: 5px;
	line-height:24px;
	height:24px;
	margin:15px 0 10px 0;
	width:100%;
	padding:0;
	/** Make the text unselectable **/
	-moz-user-select: none;
	-webkit-user-select: none;
}

a.button:before, button:before {
	border-color: #FFF rgba(255,255,255,.25) rgba(255,255,255,.25);
	left:0;
	top:0;
	height:22px;
}
a.button:after, button:after {
	border-color: transparent transparent rgba(255, 255, 255, 0.63);
	left:-1px;
	bottom:-2px;
	height:100%;
	width:100%;
}
a.button:hover, a.button:focus, button:hover, button:focus, input[type=submit]:hover, input[type=submit]:focus {
	background-image:-moz-linear-gradient(top, rgba(255,255,255,.9), rgba(255,255,255,0.2));
	background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(255,255,255,.9)), to(rgba(255,255,255,0.2)));
}
a.button:active, button:active, input[type=submit]:active {
	background-image:-moz-linear-gradient(top, rgba(75,75,75,.4), rgba(255,255,255,.4));
	background-image: -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(75,75,75,.4)), to(rgba(255,255,255,.4)));
	line-height:26px;
}

    /*  ОТПРАВКА СООБЩЕНИЯ   */

.send_message {
    display: none;
    opacity: 0;
	min-width: 500px;
	max-width: 800px;
	width: 36%;
	position: absolute;
	padding: 0 20px 15px 20px;
    margin: 0 30%;
	z-index: 0;
    top:  -100px;
	box-shadow: 0px 0px 8px black;
	text-shadow: 0px 1px 2px black;
	text-align: left;
	border-radius: 4px;
	color: white;
background: -moz-linear-gradient(top,  rgba(135,38,2,0.93) 0%, rgba(127,27,2,0.83) 45%, rgba(119,31,0,0.7) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(135,38,2,0.93)), color-stop(45%,rgba(127,27,2,0.83)), color-stop(100%,rgba(119,31,0,0.7))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(135,38,2,0.93) 0%,rgba(127,27,2,0.83) 45%,rgba(119,31,0,0.7) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ed872602', endColorstr='#b3771f00',GradientType=0 ); /* IE6-9 */

}

.send_message input[type='text'] {
	margin: 0 6px; 
	border-radius: 10px; 
	box-shadow: 0 1px 4px black inset;
	font-size: 0.85em; 
	padding:3px 8px;
	border: none; 
	}

.close {position: relative; float: right; margin: 3% 0 0 0; font-size: 0.8em}
.close:hover {text-decoration: underline; cursor: pointer}

#user {font-family: 'Ubuntu Condensed', sans-serif;}

#message {
	margin: 8px 0; 
	border-radius: 5px; 
	border: none;
	font-size: 1em; 
	padding: 1.5%;
	box-shadow: 0 1px 4px black inset;
	width: 97%;
    font-family: 'Ubuntu Condensed', sans-serif;
    max-height: 500px;
	}

.send_button {
    background: #aaa -moz-linear-gradient(top, rgba(255,255,255,.75), rgba(255,255,255,0));
	background: #aaa -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(255,255,255,.75)), to(rgba(255,255,255,0)));
    color: black;
	display: inline-block; 
    text-align: center;
	text-transform: uppercase;
    padding: 3px 8px; 
    border-radius: 4px;
    box-shadow: 0 1px 3px white;
    text-shadow: 0 1px 3px white;
	margin: 2% 40% 0 40%; 
	width: 20%;
    font-size: 0.9em;
	}
 .send_button:hover {
    box-shadow: 0 1px 2px yellow;     
    background: #aaa -moz-linear-gradient(top, rgba(255,255,255,.9), rgba(255,255,255,0));
	background: #aaa -webkit-gradient(linear, 0% 0%, 0% 100%, from(rgba(255,255,255,.9)), to(rgba(255,255,255,0))); 
    cursor: pointer;
    }
    
 .send_button:active {padding: 4px 8px 2px 8px; box-shadow: 0 1px 6px yellow;     text-shadow: 0 1px 3px black; color: yellow;}
 
 .request {
    background: #00AA55;
    border-radius: 3px; 
    margin: 3px 0; 
    padding: 3%;
    width: 94%; 
    display: block; 
    text-align: center;
    font-size: 0.9em;
    display: none;
     } 
     
   .warnings {margin: 0; background: #D90000; padding: 3px 8px; font-size: 0.85em; border-radius: 4px; display: none;}  

/** статьи */

.articles {
    float: right; 
    width: 32%; 
    min-height: 150px;
    margin: 65px 0 0 15px;
    border: white solid 1px;
    border-radius: 6px; 
    color: #006CD9; 
background: -moz-linear-gradient(top,  rgba(247,247,247,1) 0%, rgba(255,255,255,0.28) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(247,247,247,1)), color-stop(100%,rgba(255,255,255,0.28))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(247,247,247,1) 0%,rgba(255,255,255,0.28) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(247,247,247,1) 0%,rgba(255,255,255,0.28) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(247,247,247,1) 0%,rgba(255,255,255,0.28) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(247,247,247,1) 0%,rgba(255,255,255,0.28) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f7f7f7', endColorstr='#47ffffff',GradientType=0 ); /* IE6-9 */

    position: relative;
    left: 20px;
    }
.articles-head {
    font-size: 1.1em; 
    font-weight: bold;
    margin: 0px 15px 15px 17px; 
    border-bottom: white dotted 1px; 
    text-align: center; 
    text-transform: uppercase;
    padding: 3px 0;
    }
    
.articles hr {width: 80%;}
.articles-body {margin: 10px 25px 35px 25px; text-align: left;}

.articles-body a {
    color: black; 
    display: inline-block; 
    font-size: 2em; 
    line-height: 1; 
    margin: 5px 0 0 0; 
    }
.articles-body a:hover {    color: #00509F; text-shadow: 0px 0px 5px white; text-decoration: underline;}

.footer {z-index: 10; position: relative;}

</style>
</head>
	
	<body>
<img src="img/blur.jpg" style="position: fixed; top:0; left:0; width: 100%;" />    
    <!-- Loading -->
<div id="Loading"><img src="img/ui-anim_basic_16x16.gif" alt="Loading"></div>
    
 <div class="container">  
 <div class="menu_box"> 
 <p>Г. КОЛОМНА,
 УЛ. ЗАЙЦЕВА, Д. 48</p>
 <p style="font-size: 1.5em; border: white solid; border-width: 1px 0; width: 80%; margin: 5px auto">
 8 916 208 31 77  </p>
 <p>9:00 - 17:00</p>
 <p style="font-size: 1.1em;">2083177@mail.ru</p>
 <img src="img/logo.png" alt="logo">
 </div>
 <?php include_once('horizontal_menu.php'); ?>
       <?php 
           $dir = 'img/slides/';
           $files = scandir($dir);
           $count = count($files)-2;
       ?>
        
			<div class="box_skitter" style="background: white; border-radius: 3px">
				<ul> <?php  for ($i=0;$i<$count;$i++) {   echo "<li> <a href='#'> <img src='img/slides/".$files[$i+2] ."' alt='img'></a> </li>"; }  ?> </ul>

			</div>

<div class="about_design">

    <div class="column-40">
        <h2>Дизайн интерьера — мастерство создавать уют</h2>
        <p>Когда звучит фраза «<strong >дизайн интерьера</strong>», то многие представляют себе исключительно внешний лоск, супердорогую 
            отделку, замысловатую мебель. Одним словом, люди думают, что работа дизайнера по интерьерам рассчитана только 
            на внешний эффект. Давайте разберемся, так ли это?</p>
        <p>С одной стороны, дизайнер интерьера стремится сделать так, чтобы в результате его работы помещение стало красивым,
            необычным, эксклюзивным. Но, поверьте, для того, чтобы помещение «заиграло», вовсе не обязательно использование
            очень дорогих материалов и эксклюзивных предметов интерьера – самое главное это ИДЕЯ, изюминка, которую опытный 
            дизайнер всегда найдет.</p>
        <p style="text-align: right; padding-left: 45px ;">    
            <strong >Дизайн интерьера</strong > – это, в первую очередь, искусство сделать помещение удобным и индивидуальным. Настоящая гармония
            возможна только там, где оригинальность и красота вашего внутреннего мира проявляется в формах, цветах и 
            предметах, «живущих» у вас дома. А разглядеть все это, проложить мостик между внешним и внутренним миром,
            увидеть вашу мечту и воплотить ее в реальность способен лишь талантливый дизайнер!</p>
    </div>  
    
    <!-- Menu -->
    <div id="BlockMenu">
        <div class="BlockPuntMenu"><a href="index">Главная</a></div>
        <div class="BlockPuntMenu"><a href="services">услуги и цены</a></div>
        <div class="BlockPuntMenu"><a href="gallery">Портфолио</a></div>
        <div class="BlockPuntMenu"><a href="about">о студии</a></div>
        <div class="BlockPuntMenu"><a href="contacts">контакты</a></div>
    </div>

    <div class="articles">
       <div class="articles-head"> Новая Статья</div>
    
        <div class="articles-body"><a href="articles/po-francuzski">Давай сделаем это по-французски!</a> </div>
     
    </div>
       
</div>


    <div class="contact_form">
        <div class="contact_body">
        <h3>связаться</h3>
        <hr />
        <small>Вы можете написать нам сообщение, используя эту форму</small><hr />
       <small>Также Вы можете связаться с нами по телефону или электронной почте</small>
        <a class="button">НАПИСАТЬ</a>
         <span class="request">Ваше сообщение было отправлено успешно!</span>
        </div>

<div class="send_message">
 <span class="close">закрыть</span>
 	<h1>Отправить сообщение</h1>
 	Ваше имя:<input id="user" name="user" type="text"><hr>
 	Ваше сообщение:<textarea id="message" name="message" cols="1" rows="10" title="Ваше сообщение"></textarea>
    <p class="warnings"></p>
 	<a class="send_button">отправить</a>
</div>
    </div>
    


    <div class="services">
    
    <div class="column">
    <h1>Наши Услуги</h1>    
            <a href="services">Интерьерный дизайн</a>
            <a href="services">Авторское сопровождение</a>
            <a href="services">Нанесение декоративных материалов</a>
            <a href="services">Продажа декоративных материалов</a>
    </div> 
    
    
    <div class="column">
    <h1>декоративные отделочные материалы</h1>
            <a href="#">Фрески и фотопанно</a>
            <a href="#">Декоративные штукатурки</a>
            <a href="#">Облицовочная плитка под кирпич</a>
            <a href="#">Флоковое покрытие</a>
            <a href="#">Блестки</a>
            <a href="#">Декоративные краски</a>
            <a href="#">Полиуретановая лепнина</a>
    </div>
         
    <div class="column">
     <h1>О студии</h1> 
            <a href="about">О нас</a>
            <a href="#">Акции</a>
            <a href="#">Новости</a>
    </div>  
    
    

   
   
    
    <div class="column">
    <h1>Контакты</h1>
        <ul>
            <li>г. Коломна, ул. Зайцева,  д. 48 </li>
            <li>8 916 208 31 77</li>
            <li>2083177@mail.ru</li>
        </ul>
            <a href="contacts">СХЕМА ПРОЕЗДА</a>
    </div> 
    
    
    </div>
    
        <div class="lyric">..."- Обрати-ка внимание на этого механика, - сказала Уикка.<br /> Они сидели у окна, и 
        Брида увидела, что механик открыл капот автомобиля и стоит перед ним совершенно неподвижно. 
        <br />- Видишь, он ни к чему даже не прикасается. Только смотрит. Он так давно занимается своим делом, 
        что знает – автомобиль будет говорить с ним на своем собственном языке. И сейчас он использует не разум, а чувство. 
        <br /><br />
        В этот миг механик, словно что-то уловил в шуме двигателя, шевельнулся и стал копаться в нем. <br />- Видишь, 
        он определил, в чем неисправность, - сказала Уикка. – Он не тратил времени даром, потому что канал связи 
        между ним и машиной отлажен безупречно. И так же поступают все <a href="#">мастера своего дела</a>, которых я знаю." 
        <small>Пауло Коэльо. "Брида"</small>
    </div> 

</div>
        
<?php include_once('footer.php'); ?>     
        
        
	</body>
	
</html>