<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

	<head>
		<title>Студия интерьерного дизайна "Декоратор" - Отделочные материалы</title>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<meta name="keywords" content="Дизайн интерьеров, Золотые руки, Коломна, дизайн, интерьер, отделка">
		<meta name="description" content="Дизайн интерьеров от мастеров своего дела. Перемены, доступные каждому!">
		<meta name="viewport" content="width=1024">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="style.css">

        <script src="js/jquery-1.5.2.min.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function()
        { 
            $("body").animate({opacity: 1},1000);

        });     
        </script> 
        
        <style type="text/css">
            .materials {margin: 1% 5%; position: relative; display: block; float: left; width: 70%}
            
            .materials-nav {
                z-index: 2;
                position: relative;
                border-radius: 5px;
                border: white solid 3px;
                box-shadow: 0 1px 10px black;
                float: left;
                width: 90%;
                margin: 0 5%;
                display: block; background: rgb(226,226,226); /* Old browsers */
                background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(219,219,219,1) 50%, rgba(209,209,209,1) 51%, rgba(254,254,254,1) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(219,219,219,1)), color-stop(51%,rgba(209,209,209,1)), color-stop(100%,rgba(254,254,254,1))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* IE10+ */
                background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(219,219,219,1) 50%,rgba(209,209,209,1) 51%,rgba(254,254,254,1) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#fefefe',GradientType=0 ); /* IE6-9 */
            }
            
            .materials-nav a { 
                color: black;
                text-shadow: 0 1px 1px white;
                line-height: 2.5; 
                text-transform: uppercase; 
                width: 33.3%; 
                text-align: center; 
                float: left; 
                padding: 0; 
                margin: 0; 
                display: inline-block;
                }
            
            .materials-nav a:hover {
                background: rgb(226,226,226); /* Old browsers */
                background: -moz-linear-gradient(top,  rgba(226,226,226,1) 0%, rgba(196,196,196,1) 50%, rgba(206,206,206,1) 50%, rgba(232,232,232,1) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(226,226,226,1)), color-stop(50%,rgba(196,196,196,1)), color-stop(50%,rgba(206,206,206,1)), color-stop(100%,rgba(232,232,232,1))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(196,196,196,1) 50%,rgba(206,206,206,1) 50%,rgba(232,232,232,1) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(196,196,196,1) 50%,rgba(206,206,206,1) 50%,rgba(232,232,232,1) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top,  rgba(226,226,226,1) 0%,rgba(196,196,196,1) 50%,rgba(206,206,206,1) 50%,rgba(232,232,232,1) 100%); /* IE10+ */
                background: linear-gradient(to bottom,  rgba(226,226,226,1) 0%,rgba(196,196,196,1) 50%,rgba(206,206,206,1) 50%,rgba(232,232,232,1) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e2e2e2', endColorstr='#e8e8e8',GradientType=0 ); /* IE6-9 */
            }
            
            .selected, .selected:hover {
                background: rgb(196,196,196); /* Old browsers */
                background: -moz-linear-gradient(top,  rgba(196,196,196,1) 0%, rgba(160,160,160,1) 50%, rgba(183,183,183,1) 51%, rgba(211,211,211,1) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(196,196,196,1)), color-stop(50%,rgba(160,160,160,1)), color-stop(51%,rgba(183,183,183,1)), color-stop(100%,rgba(211,211,211,1))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top,  rgba(196,196,196,1) 0%,rgba(160,160,160,1) 50%,rgba(183,183,183,1) 51%,rgba(211,211,211,1) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top,  rgba(196,196,196,1) 0%,rgba(160,160,160,1) 50%,rgba(183,183,183,1) 51%,rgba(211,211,211,1) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top,  rgba(196,196,196,1) 0%,rgba(160,160,160,1) 50%,rgba(183,183,183,1) 51%,rgba(211,211,211,1) 100%); /* IE10+ */
                background: linear-gradient(to bottom,  rgba(196,196,196,1) 0%,rgba(160,160,160,1) 50%,rgba(183,183,183,1) 51%,rgba(211,211,211,1) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c4c4c4', endColorstr='#d3d3d3',GradientType=0 ); /* IE6-9 */
            }
            
            h1 {
                text-transform: uppercase;
                font-size: 2.2em;
                font-weight: bold;
                text-shadow: 0 1px 3px white;
                text-align: center;
                }
            
            .materials-description {
                position: relative;
                z-index: 1;
                float: left;
                margin: -7px 0 0 0;
                padding: 25px 0 0 0;
                width: 99%;
                min-height: 500px; 
                border: white solid 5px; 
                border-radius: 10px;
                box-shadow: 0 1px 15px black;
                background: rgb(238,238,238); /* Old browsers */
                background: -moz-linear-gradient(top,  rgba(238,238,238,1) 0%, rgba(238,238,238,1) 100%); /* FF3.6+ */
                background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(238,238,238,1)), color-stop(100%,rgba(238,238,238,1))); /* Chrome,Safari4+ */
                background: -webkit-linear-gradient(top,  rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%); /* Chrome10+,Safari5.1+ */
                background: -o-linear-gradient(top,  rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%); /* Opera 11.10+ */
                background: -ms-linear-gradient(top,  rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%); /* IE10+ */
                background: linear-gradient(to bottom,  rgba(238,238,238,1) 0%,rgba(238,238,238,1) 100%); /* W3C */
                filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#eeeeee',GradientType=0 ); /* IE6-9 */
                }
            .images {width: 95%; float: left; margin: 5% 2.5%; position: relative; display: block; text-align: center;}
            .images img { margin: 2%;}
            
            li {list-style: square; width: 90%; margin-left: 3%;}
        </style>
  </head>
 
<body>
<img src="img/blur.jpg" style="position: absolute; z-index: -1; top:0; left:0; width: 100%;" />    
    <div id="container">
    
    <div class="left-side"><img src="img/logo.png" alt="Студия интерьерного дизайна Декоратор"/></div>
    <?php include_once('horizontal_menu.php'); ?>
    
        <div class="materials">
        <h1>отделочные материалы</h1>
        
        <div class="materials-nav">
            <a href="#" class="selected">Ламинат</a>
             <a href="#">фрески</a>
            <a href="#">облицовочная плитка</a>
        </div>
        
        <div class="materials-description">
            <div class="materials-laminat">
                <p style="width: 95%; margin: 2.5%;"></p>
                
                <div class="images">
                
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-1.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-2.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-3.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-4.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-5.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-6.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-7.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-8.png" />
                <img src="http://www.polstrimp.ru/images/index/main-catalogue-9.png" />
                
                </div>
                
                <p style=" bottom: 0; display: block; width: 95%; margin: 0 2.5%;">Преимущества ламината Euroflooring:</p>
                <ul>
                    <li>бесклеевой замок с повышенной устойчивостью к нагрузке на разрыв (90 кг/кв. см)</li>
                    <li>износостойкость и ударопрочность, высокая устойчивость к истиранию и царапинам</li>
                    <li>устойчивость к ультрафиолетовым лучам и воздействию бытовых химических средств</li>
                    <li>антистатическое покрытие — на нем не скапливается пыль, поверхность легко очищается, что делает ламинат пригодным для эксплуатации даже аллергиками</li>
                    <li>быстрота и легкость монтажа — покрытие готово к эксплуатации сразу после укладки, не требует полировки и покрытия лаком</li>
                    <li>пригодность к повторной укладке — пол легко разобрать и уложить в другом помещении, а также можно заменить отдельные панели</li>
                </ul>


            </div>
            
        </div>
        </div>
    </div>
    
    <?php include_once('footer.php'); ?>

    </body>
    </html>