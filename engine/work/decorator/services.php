<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>

	<head>
		<title>Студия интерьерного дизайна "Золотые Руки" - Наши услуги</title>
		<!--[if lt IE 9]>
			<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
		<![endif]-->
		<meta name="keywords" content="Дизайн интерьеров, Золотые руки, Коломна, дизайн, интерьер, отделка">
		<meta name="description" content="Дизайн интерьеров от мастеров своего дела. Перемены, доступные каждому!">
		
        <meta name="viewport" content="width=1024">
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="style.css">
        <noscript><style type="text/css">body {opacity:1 !important}</style></noscript>
		
        <script src="js/jquery.js" type="text/javascript"></script>
        <script type="text/javascript">
        $(document).ready(function()
        { 
            $("body").animate({opacity: 1},1000, function(){ $(this).find('.service').animate({'opacity':'1'},1100, function(){ $('.service').find('.opis').animate({'opacity':'1'},800); }); });      
        });     
        </script>        
        <style type="text/css">
        
        .about {
            width: 17%; 
            float: left; 
            margin-top: 5%; 
            line-height: 1.3;
            border-left: gray solid 1px;
            padding: 0 2% 0 2%;
            text-shadow: 1px 1px 2px white;
background: -moz-linear-gradient(top,  rgba(255,255,255,1) 0%, rgba(255,255,255,0) 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(255,255,255,1)), color-stop(100%,rgba(255,255,255,0))); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); /* IE10+ */
background: linear-gradient(to bottom,  rgba(255,255,255,1) 0%,rgba(255,255,255,0) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#00ffffff',GradientType=0 ); /* IE6-9 */

            }
        
        .services {
            width: 54%; 
            background: none; 
            float: left; 
            margin:1% 0; 
            padding: 0 0 0 5%;
            text-shadow: 1px 1px 2px white;}
    
  .service {
    width: 90%; 
    display: inline-block; 
    margin: 1% 0;
    padding: 0 ; 
    background: url('img/opac60w.png') repeat;
    border-radius: 3px;
    opacity: 0;
    } 
 .service h2 {padding: 0 10px; display: block; background: none; font-size: 1.3em;}   
 .service ul {margin: 12px 0 12px 27px; }
 
 .service ul li {margin: 0; line-height: 1; }

.service h2 {margin: 15px 0 0 0; }
.service h3 {margin: 0 0 0 10px; font-size: 0.9em; color: #3C3C3C;}


.opis {opacity: 0; background: url('img/opac75w.png') repeat; padding: 2% 2% 4% 2%; margin:0 1% 1% 1%;}

.footer {
        float: left;
        }
        
.left-side p {margin: 10px auto; width: 85%; color: white; font-size: 0.9em; text-shadow: 0 1px 1px black }
</style>

    </head>
    
    <body>
    <img src="img/blur.jpg" style="position: absolute; z-index: -1; top:0; left:0; width: 100%;" />    
    <div id="container">
    
    <div class="left-side"> 
    
    <img src="img/logo.png"  alt="Золотые руки"/>
    <div class="akcii">Акции</div>
    <p>Студия интерьерного дизайна "Декоратор" является официальным партнером 
    французских фирм-производителей отделочных материалов.</p>
    
    </div>
    
    
    
    <?php include_once('horizontal_menu.php'); ?>
    

<div class="services">

<h1>Наши услуги:</h1>

    
<div class="service">

<h2>Пакет услуг "Эскизный проект" – 350 руб./кв. м.</h2>
<hr />

<div class="opis">
Альбом дизайна интерьера - эскизный включает в себя:
<ul>
<li>	план перепланировки помещения и/или схема расположения мебели и сантехники с экспликацией помещений;</li>
<li>	план пола с обозначением напольных покрытий; </li>
<li>	план потолка с обозначением уровней, декоративных потолочных элементов; </li>
<li>	подбор концептуально–колористического решения; </li>
<li>	3-d визуализация основных помещений (один вид на каждое помещение)  </li>
<li>сопроводительная записка</li>
</ul>
<hr />
<strong>Примечание</strong>: в схемах размеры не указываются, ведомость отделки не выполняется
</div>

</div>

<div class="service">

<h2>Пакет услуг "Полный проект" – 550 руб./кв. м</h2>
<hr />
<div class="opis">
Альбом дизайна интерьера - полный  включает в себя:
<ul>
<li>	обмерочный чертеж; </li>
<li>	план перепланировки помещения и/или схема расположения мебели и сантехники с экспликацией помещений;</li>
<li>	план пола с обозначением напольных покрытий и  размеров; </li>
<li>	план потолка с обозначением уровней, декоративных потолочных элементов и размеров; </li>
<li>	план размещения электроустановочных изделий с размерами; </li>
<li>	план привязки выключателей к источникам света; </li>
<li>	план расположения «теплого пола»; </li>
<li>	развертки стен; </li>
<li>	подбор концептуально–колористического решения; </li>
<li>	3Д – визуализация основных помещений(два-три вида на каждое помещение);</li>
<li>	сопроводительная записка</li>
</ul>
<hr />
<strong>Примечание</strong>: ведомость отделки в пакет не входит.
</div>

</div>

<h1>Дополнительные услуги:</h1>

<div class="service">

<h2>Ведомость отделки  - 170 руб./кв. м.</h2>
<hr />
<div class="opis">
<ul>
<li>	название                       </li>
<li>	тип материала                  </li>
<li>	фабрика производитель          </li>
<li>	артикул                        </li>
<li>	количество                     </li>
<li>	мебель корпусная               </li>
<li>	 мягкая мебель                 </li>
<li>	 кухонный гарнитур             </li>
<li>	столы                          </li>
<li>	стулья                         </li>
<li>	светильники                    </li>
<li>	выключатели и розетки          </li>
<li>	санфаянс                       </li>
<li>	аксессуары                     </li>
</ul>
Ведомость отделки отличается от Пояснительной записки тем, что в ней указывается точное название фабрики, артикул товара, его точное количество.       В пояснительной записке таких точных данных нет.
Ведомость отделки выполняется только в полном дизайн-проекте: 
</div>
</div>

<div class="service">
<h2>Авторское сопровождение проекта – 1500 руб./выезд</h2>
<h3>каждый последующий час + 300 руб.</h3>
<hr />
<div class="opis">
<strong>Примечание</strong>: один выезд не более четырех часов (не считая времени на дорогу)
<ul>
<li>	работа с исполнителями проекта, выезды на объект, наблюдение за отделочными работами; </li>
<li>	корректировка чертежей в случае необходимости;                                         </li>
<li>	работа с заказчиком по подбору предметов интерьера, картин, аксессуаров и т.п.;        </li>
<li>	выезд для подбора мебели и отделочных материалов.                                      </li>
</ul>
Это работа дизайнера по пресечению  ненужных идей строителей и других участников процесса. Дизайнер следит за тем, чтобы Вы получили свое помещение в том виде, в котором задумывали.
</div>
</div>
</div>

<div class="about"> <p>За годы работы мы поняли, что результат реализации дизайн-проекта без сопровождения автора, 
который должен обладать правом решающего голоса, бывает значительно далек от начальной задумки того самого автора.</p>
 <p> Помимо этого все нервные и физические усилия по координации различных специалистов, реализующих дизайн- проект 
 ложатся на плечи заказчика. </p> <p> Поэтому у нас Вы можете не только заказать дизайн-проект  интерьера, но и найдете
  весь спектр  сопутствующих услуг, необходимых для его успешной реализации. <p></div>
   
</div>

 <?php include_once('footer.php'); ?>
    </body>
    
    </html>