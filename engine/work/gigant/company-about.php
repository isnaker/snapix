<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
    <style type="text/css">
    .container {padding: 0;}
    .back {width: 100%; position: relative; z-index: 1; max-width: 1200px;}
    
    .text {
        z-index: 2; 
        position: relative; 
        height: 800px; 
        overflow: hidden; 
        margin: -40px 0 0 0; 
        max-width: 100%; 
        width: 100%;
        }
        
    .company-about {
        width: 28%; 
        background: url('img/opac75w.png') repeat; 
        margin: 40px 30px 0 64%; 
        padding: 15px 18px;
        text-align: center;
        border-radius: 5px;
        top: 0;
        z-index: 500;
        position: absolute;
        }
    .company-about h1 {margin: 5px 0; text-transform: uppercase; font-weight: bold;}
    
    .company-about p {margin: 10px auto 0 auto; font-size: 0.9em; width: 80%; }
    
    .company-about a {
        border:  silver solid 1px; 
        padding: 10px 0 ;
        width: 45%;
        margin: 5px auto 0px auto; 
        color: black;
        text-decoration: none;
        display: inline-block;
        background: #396bad; /* Old browsers */
        background: -moz-linear-gradient(top,  #396bad 0%, #4f85bb 100%); /* FF3.6+ */
        background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#396bad), color-stop(100%,#4f85bb)); /* Chrome,Safari4+ */
        background: -webkit-linear-gradient(top,  #396bad 0%,#4f85bb 100%); /* Chrome10+,Safari5.1+ */
        background: -o-linear-gradient(top,  #396bad 0%,#4f85bb 100%); /* Opera 11.10+ */
        background: -ms-linear-gradient(top,  #396bad 0%,#4f85bb 100%); /* IE10+ */
        background: linear-gradient(to bottom,  #396bad 0%,#4f85bb 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#396bad', endColorstr='#4f85bb',GradientType=0 ); /* IE6-9 */
        color: white;
        text-shadow: 0 1px 2px black;
        border-radius: 4px;
        }
        
    .company-about a:hover { 
text-shadow: 0 1px 5px black;
background: #499bea; /* Old browsers */
background: -moz-linear-gradient(top,  #499bea 0%, #4f62d1 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#499bea), color-stop(100%,#4f62d1)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #499bea 0%,#4f62d1 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #499bea 0%,#4f62d1 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #499bea 0%,#4f62d1 100%); /* IE10+ */
background: linear-gradient(to bottom,  #499bea 0%,#4f62d1 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#499bea', endColorstr='#4f62d1',GradientType=0 ); /* IE6-9 */
box-shadow: 0 0 2px blue;
}
    </style>

    
    
<title>Транспортная компания Гигант</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">
<img class="back" src="images/about_company_truck.jpg" />

<div class="company-about">

    <h1>О компании</h1>
    
    <hr />
    
    <p>Наша компания существует с 1999 года и изначально осуществляла доставку туристов экскурсионными автобусами, 
    а с середины 2000-х годов мы активно развиваем направление грузовых перевозок.</p>
    <p><strong>Транспортная компания «Гигант»</strong>, расположенная в г. Воскресенск Московской области, 
    предоставляет целый комплекс услуг по перевозке грузов.</p>
    
    <p><strong>Транспортная компания «Гигант»</strong>- надежная транспортная компания, готовая предложить оперативные грузовые 
    перевозки по доступным ценам. Ответственный подход к выполнению каждого заказа демонстрирует степень нашего 
    уважения к каждому клиенту.</p>
    <hr style="margin: 15px 0 10px 0;" />
    <a href="vacancy.php">Вакансии</a>
    <a href="contacts.php">Контакты</a>
</div>

</div>

<hr style="margin: 0 0 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>

</body>
</html>