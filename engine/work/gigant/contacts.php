<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
    <script type="text/javascript">
    function initialize()
    {
        var gigant = new google.maps.LatLng(55.250261,38.754112);
        var map;
		var opts = {
     		 zoom: 12,
     		 center: gigant,
     		 mapTypeId: google.maps.MapTypeId.ROADMAP,
             draggableCursor: 'default',
             disableDefaultUI: true
	         }

        map = new google.maps.Map(document.getElementById("map"), opts);
        var image = 'images/icon.png';
        var marker = new google.maps.Marker({
        map: map,
        position: gigant,
        icon: image
      });
      
      marker.setAnimation(google.maps.Animation.BOUNCE);
      
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
<title>Транспортная компания Гигант</title>

<style type="text/css">

#map {width: 100%; height: 450px; margin-top: 15px; background: white; box-shadow: 0 1px 25px rgba(0,0,0,0.66) }

#contacts_text {
    position: relative; 
    margin-top: -430px; 
    margin-left: -20px; 
    width: 300px; 
    padding: 15px;
    color: black;
    text-shadow: 0 1px 1px white;
    border: #004A95 solid 4px;
    border-radius: 4px;
    background: rgb(238,238,238); /* Old browsers */
    background: -moz-linear-gradient(top,  rgba(238,238,238,1) 0%, rgba(204,204,204,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(238,238,238,1)), color-stop(100%,rgba(204,204,204,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(238,238,238,1) 0%,rgba(204,204,204,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#eeeeee', endColorstr='#cccccc',GradientType=0 ); /* IE6-9 */
    }
#contacts_text h2 {color: #004A95}

#contacts_text p {margin: 7px 0 5px 5px;}
</style>

</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">

<h1>Контакты</h1>

<div id="map"></div>

    <div id="contacts_text">
        <h2>Наш адрес:</h2>
        <p>140200, Московская обл., Воскресенский р-н, г. Воскресенск, ул. Гаражная, д.1Г </p>
        <h2>Наши телефоны:</h2>
        <p>8 (495) 556 45 65 </p>
        <h2>E-mail:</h2>
        <p>mail@<strong>tkgigant.com</strong></p>
    </div>

</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>

</body>
</html>