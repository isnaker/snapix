<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    <script type="text/javascript" src="js/menu.js"></script>
    
    <style type="text/css">
    
    .side_menu {
        width: 22%;
        display: inline-block;
        float: left;
        margin-left: 3%;

    }
    
    .side_menu a {
        text-decoration: none; 
        font-size: 1em; 
        padding: 2px 0 2px 4px; 
        font-weight: bold; margin: 2px 0;
        color: #004E9B;
        text-shadow: 0 1px 1px white;
        display: list-item;
        list-style: square;
        }
    
    .side_menu a:hover {
        background: #004E9B;
        color: white;
        text-shadow: 0 1px 1px black;
        border-radius: 3px;
    }
    
    </style>
    
<title>Транспортная компания Гигант</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>
<img style="position: relative; max-width: 1200px; width: 100%; z-index: 0; margin-top: -50px;" src="images/gruzoperevozki_back.png" />
<div class="text">



<div class="article-main-text">
<h1>Грузоперевозки</h1>

<h3>Вам необходимо выполнить перевозку груза какого-либо вида? </h3>

<p>
<strong>Транспортная компания «ГИГАНТ»</strong> выполняет перевозки грузов различных типов с 2003 года. Перевозки выполняются автомобильным транспортом по Москве и области, между Москвой и другими городами России.
Перевозка грузов начинается с подбора подходящего автомобиля. Вы можете подобрать автомобиль сами или проконсультироваться с диспетчерами транспортной компании «ГИГАНТ» по телефону.
</p>

Основными характеристиками грузов являются:
<ul>
<li>Размер (объём)</li>
<li>Масса</li>
<li>Хрупкость</li>
<li>Ценность</li>
</ul>

<p>
Грузы могут быть также однородными (сыпучие материалы, коробки, доски) и неоднородными (мебель, оборудование), что может иметь значение при выполнении такелажных 
работ, которые полезны более при погрузке хрупкого и неоднородного груза.
</p>

<p>
<strong>Транспортная компания «Гигант»</strong> осуществляет перевозку грузов <a href="samosvalami.php">самосвалами</a>, <a href="polupricepami.php">полуприцепами</a>, <a href="tonarami.php">тонарами</a> и <a href="autopoezdami.php">автопоездами</a>.</p>

<p>
Автомобильные грузоперевозки включают в себя транспортировку по месту назначения самых различных грузов. Мы располагаем собственным автопарком седельных тягачей и большим штатом водителей (граждан РФ).
 Благодаря работе без посредников за время присутствия на рынке перевозок мы сумели завоевать репутацию надежного и профессионального партнера. Мы всячески способствуем появлению тесных связей с 
 постоянными заказчиками, но с не меньшим вниманием относимся и к новым клиентам.
 </p>
 
 <p>
Наше основное правило – это аккуратность и точность оказания услуг, поскольку мы отлично понимаем важность соблюдения сроков поставки в условиях нынешней конкуренции на рынке транспортных перевозок.
 
 </p>
 
 </p>
Схема работы нашей организации построена таким образом, чтобы грузоперевозки были осуществлены в минимальный срок, без плановых и внеплановых простоев. Располагая собственной ремонтной базой, 
мы выпускаем на линию только исправные машины, которые гарантированно доставят груз в любую точку России в кратчайшие сроки.
Абсолютно весь водительский состав, работающий на линиях, имеет подготовку и солидный опыт международных автоперевозок. Все водители постоянно проходят инструктаж и плановые курсы повышения
 квалификации, изучая новую технику и обучаясь грамотной работе с товаросопроводительной документацией.
</p>
</div>

<div class="side_menu">
    <a href="samosvalami.php">Перевозки самосвалами</a>
    <a href="polupricepami.php">Перевозки полуприцепами</a>
    <a href="tonarami.php">Перевозки тонарами</a>
    <a href="autopoezdami.php">Перевозки автопоездами</a>
</div>

<? include_once('gruzoperevozki_template_text.php') ?>
</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>

</body>
</html>