<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link rel="icon" href="favicon.ico" type="image/x-icon"/> 
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>

    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
    <script type="text/javascript">
    $(document).ready(function()
    {
       $('.service-outer').mouseover(function(){
        var item = $(this).find('.service-description');
        var height = parseInt($(this).css('height'));
        var item_height = parseInt(item.css('height'));

        item.stop().animate({'top':height-item_height-40}, 200);
        $(this).stop().animate({'backgroundPositionX':'100%'},500);
        
       }).mouseout(function(){
         $('.service-description').stop().animate({'top':'150px'},200);
         $(this).stop().animate({'backgroundPositionX':'0%'},500);
       }); 
       
       		$(".box_skitter").skitter({
			animation: "randomSmart",
			dots: true, 
 			preview: true, 
 			hideTools: true,
            interval: 5000
		});
       
    });
    </script>
<title>Транспортная компания Гигант</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="slides-outer">
<div class="box_skitter">
    <ul>
        <li> <a href='#'> <img src='slides/mainpage/4.jpg' alt='img'/></a> </li>
        <li> <a href='#'> <img src='slides/mainpage/1.jpg' alt='img'/></a> </li>
        <li> <a href='#'> <img src='slides/mainpage/2.jpg' alt='img'/></a> </li>
        <li> <a href='#'> <img src='slides/mainpage/3.jpg' alt='img'/></a> </li>
    </ul>
</div>
</div>

<div class="text">

<div class="container-100">
<div class="company-text">
<h1 class="header">О компании</h1>
<p>
Мы рады приветствовать Вас на сайте <strong>Транспортной компании «Гигант»</strong>! </p><p>
 Наша компания существует с 1999 года 
и изначально осуществляла доставку туристов   экскурсионными автобусами, а с середины 2000-х годов мы активно развиваем 
направление грузовых перевозок.<br /></p><p>
<strong>ТК «Гигант»</strong>, расположенная в <strong>г. Воскресенск Московской области</strong>, предоставляет целый комплекс услуг по 
перевозке грузов. 
</p><p>
<strong>Транспортная компания «Гигант»</strong> - надежная транспортная компания, готовая предложить оперативные грузовые
 перевозки по доступным ценам. Ответственный подход к выполнению каждого заказа демонстрирует степень нашего уважения к 
 каждому клиенту.   </p>

<a class="why" href="company-about.php">Узнать, почему мы - лучшие!</a>
</div>

<div class="services">

<h1 class="header">наши услуги</h1>
<p style="margin: 10px 0 7px 12px; font-size: 0.9em;">
Наша компания предоставляет широкий спектр услуг - от грузоперевозок и шиномонтажных работ до аренды специализированной
техники и продажи строительных добавок для бетонных смесей.</p>
<div class="service-outer gruzoperevozki">
    <div class="service">
        <p class="service-head">Грузоперевозки</p>
        <div class="service-description">Грузоперевозки самосвалами, прицепами, тонарами...
            <a href="gruzoperevozki.php">Подробнее...</a></div>
    </div>
</div>

<div class="service-outer spectehnika">
    <div class="service">
        <p class="service-head">Спецтехника</p>
        <div class="service-description">Погрузчики, вакуумный пылесос
            <a href="spectehnika.php">Подробнее...</a></div>
    </div>
</div>

<div class="service-outer remont">
    <div class="service">
        <p class="service-head">Ремонт</p>
        <div class="service-description">Грузовых и легковых автомобилей
            <a href="remont.php">Подробнее...</a></div>
    </div>
</div>

<div class="service-outer shinomontage">
    <div class="service">
        <p class="service-head">Шиномонтаж</p>
        <div class="service-description">Спецтехники, грузовых и легковых автомобилей
            <a href="shinomontage.php">Подробнее...</a></div>
    </div>
</div>

<div class="service-outer uborochnaja">
    <div class="service">
        <p class="service-head">Уборочная техника</p>
        <div class="service-description">Мойщики дорог, подметальные и поломоечные машины
            <a href="uborochnaja_tehnika.php">Подробнее...</a></div>
    </div>
</div>

<div class="service-outer dobavki" style="width: 98.5%; background-repeat: repeat-x;">
    <div class="service">
        <p class="service-head">Добавки для цементной промышленности и производства сухих смесей</p>
        <div class="service-description">Модифицирующие добавки в бетоны и растворы
            <a href="#">Подробнее...</a></div>
    </div>
</div>
</div>



</div>

<hr style="margin: 15px 0;" />

<!--<div class="left-side"></div>
<div class="center-side"><img src="img/center.png" style="margin: 2%; width: 96%;" /></div>
<div class="right-side"></div>

<hr style="margin: 35px 0; clear: both;" /> -->
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>
</div>

</body>
</html>