<?php
// AUTO MENU
$link = $_SERVER['SCRIPT_NAME'];
$link = substr($link,0,-4);
$pieces = explode("/", $link);
$link = $pieces[count($pieces)-1];
?>
<script type="text/javascript">
$('document').ready(function(){
   $('a[href="<?php echo $link ?>.php"]').parent().addClass('current'); 
});
</script>


<div class="menu_container">
<div id="menu">    
<ul class="menu">        
<li>
<a href="index.php"><span>Главная</span></a>        
</li>                

<li>
<a href="company-about.php" class="parent"><span>Компания</span></a>
<div>                
<ul >                    
<li><a href="company-about.php"><span>О нас</span></a></li>                    
<li><a href="vacancy.php"><span>Вакансии</span></a></li>                
</ul>            
</div>        
</li>                

<li><a href="gruzoperevozki.php" class="parent"><span>Грузоперевозки</span></a>            
<div>
<ul>                    
<li><a href="samosvalami.php"><span>Самосвалами (1 - 10 т)</span></a></li>         
<li><a href="polupricepami.php"><span>Полуприцепами</span></a></li>                    
<li><a href="tonarami.php"><span>Тонарами</span></a></li>                    
<li><a href="autopoezdami.php"><span>Автопоездами</span></a></li>                
</ul>            
</div>        
</li>                

<li><a href="spectehnika.php" class="parent"><span>Спецтехника</span></a>            
<div>                
<ul >                    
<li><a href="pogruzchiki_frontalnie.php"><span>погрузчики фронтальные</span></a></li>                    
<li><a href="pogruzchiki_vilochnie.php"><span>погрузчики вилочные</span></a></li>                   
<li><a href="pogruzchiki_merlo.php"><span>погрузчики MERLO</span></a></li>                    
<li><a href="snegouborochnaja_tehnika.php"><span>снегоуборочная техника</span></a></li>                    
<li><a href="vacuumniy_pilesos.php"><span>вакуумный пылесос<small style="text-transform: lowercase; margin-top: 4px;  line-height: 1; font-size: 0.8em; display: block;">для уборки территорий цементных заводов и прилегающих к ним территорий</small> </span></a></li>                    
<li><a href="polivalnaja_tehnika.php"><span>поливальная техника</span></a></li>                    
<li><a href="uborochnaja_tehnika.php"><span>уборочная техника</span></a></li>                
</ul>            
</div>        
</li>                

<li><a href="remont.php" class="parent"><span>Ремонт</span></a>            
<div>                
<ul >                    
<li><a href="remont_belazov.php"><span>ремонт БелАЗов</span></a></li>                    
<li><a href="remont_gruzovikov.php"><span>ремонт грузовых машин</span></a></li>                    
<li><a href="remont_spectehniki.php"><span>ремонт спецтехники</span></a></li>                
</ul>            
</div>        
</li>                

<li><a href="shinomontage.php" class="parent"><span>шиномонтаж</span></a>            
<div>                
<ul >                    
<li><a href="shinomontage_gruzovih.php"><span> ремонт и шиномонтаж колес грузового транспорта, спецтехники, БелАЗов</span></a></li>                    
<li><a href="shinomontage_legkovih.php"><span>легкового транспорта</span></a></li>                
</ul>            
</div>        
</li>                

<li class="last"><a href="contacts.php"><span>Контакты</span></a></li>    
</ul>    
</div>
</div>