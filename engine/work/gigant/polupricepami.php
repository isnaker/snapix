<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
<title>Транспортная компания Гигант</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">

<h1>Полуприцепами</h1>
<div class="article-text">
Транспортная компания «Гигант» осуществляет доставку грузов полуприцепами. 

Наша компания осуществляет перевозку любых тарных грузов автотягачами MAN и Scania в полуприцепах. 
Полезный объем полуприцепов находится в пределах 82-140 куб. метров, в зависимости от модели.

Уточнить стоимость и дополнительные условия можно по телефону 8 (***) ******
</div>

<img src="images/gruzoperevizki-2.jpg" style="float: right; max-width: 400px;" />
<? include_once('gruzoperevozki_template_text.php') ?>
</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>