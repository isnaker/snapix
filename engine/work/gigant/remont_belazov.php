<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
<title>Транспортная компания Гигант | Ремонт БелАзов</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>
    <img style="width: 100%; position: relative; margin-top: -40px; z-index: 3" src="img/our_crew.jpg"/>
<div class="text">

<h1>Ремонт БелАзов</h1>
Ремонт БелАзов
ОАО "Гигант" - компания с многолетним опытом в сфере оказания услуг по ремонту узлов, механизмов и агрегатов к автомобильной техники БелАЗ, а также поставке запасных частей к ним.
В своей работе по ремонту мы используем только оригинальные запасные части производства ОАО "Белорусского автомобильного завода" Республики Беларусь. Опытные специалисты нашего сервисного центра, прошедшие подготовку и аттестацию по ремонту, обслуживанию и эксплуатации карьерной техники, гарантируют для Вас надежный, качественный и своевременный ремонт узлов и агрегатов с гарантией завода изготовителя.
Ориентировочный срок выполнения ремонтных работ от 1 дня до 1 месяца с момента согласования сторонами дефектной ведомости и калькуляции ремонтных работ и материалов.
1.	Диагностика, дефектовка и полная переборка автомобиля;
2.	Капитальный ремонт основных узлов и агрегатов: ДВС; ГМП; Шасси; главной передачи и т.д;
3.	Замена вышедших из строя деталей;
4.	Жестяные и кузовные работы, покраска;
5.	Полная замена электропроводки и электрооборудования;
6.	Полная замена всех технических жидкостей и масел;
7.	Капитальный ремонт кабины.



<?php include_once('remont_template_text.php'); ?>
</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>