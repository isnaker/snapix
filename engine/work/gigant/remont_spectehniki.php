<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
<title>Транспортная компания Гигант | Ремонт спецтехники</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">

<h1>Ремонт спецтехники</h1>

Наши сервис-бригады оснащены всем необходимым инструментом и транспортом, что позволяет в кратчайшие сроки реагировать на заявки клиентов и проводить техническое обслуживание или устранять неисправности. Прибытие специалиста гарантировано в течении 24 часов с момента обращения, большинство неполадок устраняются на месте.
К услугам наших клиентов:
<ul>
<li> Все виды технического обслуживания техники;</li>
<li> Предпродажная подготовка;</li>
<li> Обучение персонала клиента;</li>
<li> Консультации по эксплуатации, ремонту и техническому обслуживанию техники;</li>
<li> Оперативная поставка запасных частей и расходных материалов;</li>
<li> Установка дополнительного оборудования на технику;</li>
<li> Оперативный ремонт любой сложности;</li>
<li> Текущий ремонт узлов и агрегатов на месте работы машины;</li>
</ul>


<?php include_once('remont_template_text.php'); ?>
</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>