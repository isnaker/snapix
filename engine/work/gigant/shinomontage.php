<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    <style type="text/css">

        .text h1 {
            background: white;
            display: inline-block;
            padding: 2px 8px;
            float: left;
            margin-top: -100px;
            position: relative;
            z-index: 5}
    </style>
<title>Транспортная компания Гигант | Шиномонтаж</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>
    <img style="width: 100%; position: relative; margin-top: -40px; z-index: 3" src="img/tire_service.jpg"/>
<div class="text">

<h1>Шиномонтаж</h1>


<b>ОАО «Гигант»</b> предлагает все виды услуг по шиномонтажу грузовых автомобилей. Современное 
технологическое оборудование позволяет нам качественно, быстро и по приемлемым ценам проводить работы по
 грузовому шиномонтажу.Мы осуществляем также вулканизацию, накачку шин и балансировку колес.

Любой грузовик периодически требует проведения шиномонтажа. Для данного вида автомобилей этот вид сервиса 
как нельзя актуален: полная замена колес для большегрузных машин весьма затратна, а менять резину на них приходится
довольно часто. Это неудивительно, грузовые шины в условиях дальних перевозок быстро изнашиваются. Экономия в данном 
случае ведет к неоправданному риску, ставит под угрозу безопасность людей, грузов и, безусловно, репутацию перевозчика.


</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>