<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
<title>Транспортная компания Гигант | Шиномонтаж грузовых автомобилей</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">

<h1>Шиномонтаж грузовых автомобилей</h1>



Компания «Гигант» предлагает широкий комплекс услуг, связанных с обслуживанием автомобильных колес. 
Здесь всегда можно провести качественный, надежный и, по возможности, недорогой шиномонтаж грузовых авто. 

В круг работ, выполняемых нашими специалистами, входят также нарезка, ремонт и восстановление шин. Последний 
аспект очень важен для экономии средств на обслуживании колес тяжелой автомобильной техники. Достаточно сказать,
 что более половины шин, устанавливаемых на машинах грузоперевозчиков во всем мире,  восстановленные. 
 Использование профессионального оборудования и высококачественных материалов гарантия высокого качества
  ремонтных работ и надежного результата.

Цены, предлагаемые компанией на услуги проведения квалифицированного, технически грамотного и профессионального 
осуществляемого грузового шиномонтажа авто, представляются нам вполне приемлемыми и для крупных транспортных фирм, 
и для индивидуальных предпринимателей.


</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>