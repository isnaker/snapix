<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
<title>Транспортная компания Гигант | Шиномонтаж легковых автомобилей</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">

<h1>Шиномонтаж легковых автомобилей</h1>

	Мы считаем, что фраза «легковой шиномонтаж» знакома абсолютно любому автолюбителю. Колесо появилось примерно тогда, 
    когда и само человечество, и не переставало развиваться вплоть до наших дней. Наши деды для того, чтоб починить колесо,
     использовали уголок и кувалду, ну или же самый простой метод – метод наезда на него автомобилем. 
     
     После того, как обод
      был обжат, нужно было еще 2 человека, чтоб разбортировать колесо. Да и в то время никто даже и не мог представить, 
      что колеса вообще могут не иметь камеры. В магазинах конечно продавалось некое оборудование для вулканизации, но и 
      работа на нем было очень трудоемкой, и требовались огромные усилия и куча времени для достижения нужной цели. 
      
На данный момент мы успели далеко продвинуться в области шиномонтажа, и с современным оборудованием сделать 
монтаж \ демонтаж, накачать \ подкачать шины не прилагая никаких усилий и в считанные минуты. 
Классифицировать оборудование для легкого шиномонтажа можно по монтажной стойке, которая может быть отклоняемой и не о
тклоняемой. Также имеет место режим работы, который может быть автоматический и полуавтоматический. 
Наша компания предоставит вам огромный выбор оборудования для легкового шиномантажа на ваш вкус, в зависимости от ваших
 потребностей.



</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>