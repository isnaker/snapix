<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    <script type="text/javascript">
        $(document).ready(function()
        {
            $('.service-outer').mouseover(function(){
                var item = $(this).find('.service-description');
                var height = parseInt($(this).css('height'));
                var item_height = parseInt(item.css('height'));

                item.stop().animate({'top':height-item_height-40}, 200);
                $(this).stop().animate({'backgroundPositionX':'100%'},500);

            }).mouseout(function(){
                        $('.service-description').stop().animate({'top':'150px'},200);
                        $(this).stop().animate({'backgroundPositionX':'0%'},500);
                    });

        });
    </script>
    <style type="text/css">
        .text { margin-top: -100px; position: relative; z-index: 10;}
        .text h1 {background: white; display: inline-block; padding: 2px 5px; float: left;}
        .article-main-text {
            background: url('img/opac75w.png') repeat;
            padding: 10px 15px;
            margin-bottom: 70px;
        }

        /*   LINKS   */
        .pogruzchiki_frontalnie {
            background: url('img/spectehnika_front_pogruzchik.jpg') no-repeat;
            background-position: 0px -30px;
        }

        .pogruzchiki_vilochnie {
            background: url("img/spectehnika_vilochniy_pogruzchik.jpg")  no-repeat;
            background-position: 0px -25px;
        }

        .pogruzchiki_merlo {
            background: url("img/spectehnika_merlo.jpg")  no-repeat;
            background-position: 0px -25px;
        }

    </style>
<title>Транспортная компания Гигант | Спецтехника</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>
<img style="width: 100%; position: relative; margin-top: -40px;" src="slides\mainpage\2.jpg"/>
<div class="text">

<h1>Спецтехника</h1>
<div class="article-main-text">
Транспортная компания «Гигант» располагает большим парком специализированной техники для выполнения самых различных производственных, строительных и других задач. 
Среди них – самосвалы MAN и SCANIA, Погрузчики Merlo, вилочные и фронтальные погрузчики, уборочная и поливальная техника.
</div>

<div class="links_to">

    <div class="service-outer pogruzchiki_frontalnie">
        <div class="service">
            <p class="service-head">погрузчики фронтальные</p>
            <div class="service-description"><a href="pogruzchiki_frontalnie.php">Подробнее...</a></div>
        </div>
    </div>

    <div class="service-outer pogruzchiki_vilochnie">
        <div class="service">
            <p class="service-head">погрузчики вилочные</p>
            <div class="service-description"><a href="pogruzchiki_vilochnie.php">Подробнее...</a></div>
        </div>
    </div>

    <div class="service-outer pogruzchiki_merlo">
        <div class="service">
            <p class="service-head">погрузчики MERLO</p>
            <div class="service-description"><a href="pogruzchiki_merlo.php">Подробнее...</a></div>
        </div>
    </div>

    <div class="service-outer snegouborochnaja_tehnika">
        <div class="service">
            <p class="service-head">снегоуборочная техника</p>
            <div class="service-description"><a href="snegouborochnaja_tehnika.php">Подробнее...</a></div>
        </div>
    </div>

    <div class="service-outer vacuumniy_pilesos">
        <div class="service">
            <p class="service-head">вакуумный пылесос</p>
            <div class="service-description">
                <small style="text-transform: lowercase; margin-top: 4px;  line-height: 1; font-size: 0.8em; display: block;">
                    для уборки территорий цементных заводов и прилегающих к ним территорий
                </small>
                <a href="vacuumniy_pilesos.php">Подробнее...</a></div>
        </div>
    </div>

    <div class="service-outer uborochnaja_tehnika">
        <div class="service">
            <p class="service-head">уборочная техника</p>
            <div class="service-description"><a href="uborochnaja_tehnika.php">Подробнее...</a></div>
        </div>
    </div>

</div>

</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>