<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    
<title>Транспортная компания Гигант | Уборочная техника</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">

<h1>Уборочная техника</h1>
<div class="article-main-text">
Транспортная компания «Гигант» обладает несколькими единицами специальной уборочной техники.
 
Среди них – уборочная машина Scandia.
Подметально-уборочная машина элеваторного типа. Две нижние боковые вертикальные щетки сметают грязь и мусор к 
центру и отправляют ее на транспортер с помощью нижней горизонтальной щетки. Автоматическая система слежения за 
уровнем очищаемой поверхности (увеличивает срок службы щеток). 

Система орошения щеток. Боковые скребки для разбивания твердых образований на поверхности дороги. 
Выгрузка по транспортеру в кузов грузовой машины (монтируется к грузовикам импортного производства и производства стран СНГ).
Встроенная автоматическая мойка высокого давления для очистки дорожных знаков, мытья автомашин и спецтехники (шланг 15 метров).
</div>
</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>