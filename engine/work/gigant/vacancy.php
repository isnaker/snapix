<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name="author" content="Snapix" />
    <meta name="viewport" content="width=1024">
    
    <link type="text/css" href="menu.css" rel="stylesheet" />
    <link type="text/css" href="skitter.styles.css" media="all" rel="stylesheet"/>
    <link type="text/css" href="main.css" media="all" rel="stylesheet"/>
    <script type="text/javascript" src="js/jquery-1.5.2.min.js"></script>
    <script type="text/javascript" src="js/jquery-1.6.3.min.js"></script>
    
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/jquery.animate-colors-min.js"></script>
    <script type="text/javascript" src="js/jquery.skitter.min.js"></script>
    <script src="js/jquery.timers.js" type="text/javascript"></script>
    
    <script type="text/javascript" src="js/menu.js"></script>
    <style type="text/css">
        .back {width: 100%; position: absolute; float: left; z-index: -1; max-width: 1200px;}
        .text {
            z-index: 2; 
            background: white; 
            position: relative; 
             overflow: hidden; margin: -40px 0 0 0; max-width: 100%; width: 100%;}
        
        .vacancies-body {display: block; background: none; height: 460px;}
        
        .vacancy-text {
        width: 30%; 
        float: left;
        margin: 70px 30px 20px 30px; 
        padding: 15px 18px 25px 18px;
        text-align: center;
        background: url('img/opac75w.png') repeat; 
        font-size: 0.9em;
        border: white solid 1px;
        }
        
        .vacancy-text h1 {margin: 0; text-transform: uppercase; font-weight: bold;}
    
        .vacancy-text p {margin-left: 20px;}
        
        .vacancy-text ul {margin: 20px 30px 0 30px; text-align: left;}
        
        .vacancy-list {
            display: block; 
            height: 400px;
            clear: both; 
            margin-top: 5px;
            
            background: #ffffff; /* Old browsers */
background: -moz-linear-gradient(top,  #ffffff 0%, #f3f3f3 50%, #ededed 51%, #ffffff 100%); /* FF3.6+ */
background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,#ffffff), color-stop(50%,#f3f3f3), color-stop(51%,#ededed), color-stop(100%,#ffffff)); /* Chrome,Safari4+ */
background: -webkit-linear-gradient(top,  #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* Chrome10+,Safari5.1+ */
background: -o-linear-gradient(top,  #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* Opera 11.10+ */
background: -ms-linear-gradient(top,  #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* IE10+ */
background: linear-gradient(to bottom,  #ffffff 0%,#f3f3f3 50%,#ededed 51%,#ffffff 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#ffffff', endColorstr='#ffffff',GradientType=0 ); /* IE6-9 */
}
        
        .vacancy-list h1 {text-align: center;}
    </style>
<title>Транспортная компания Гигант</title>
</head>

<body>
<a style="visibility: hidden;" href="http://apycom.com/"></a>
<div class="blue-line"></div>
<div class="yellow-line"></div>

<div id="container">
<?php 
include_once('heading.php');
include_once('menu.php'); 
?>

<div class="text">
<img class='back' src="images/happy_trucker.jpg" />

<div class="vacancies-body">

<div class="vacancy-text">

<h1>Вакансии</h1>
<hr style="margin: 5px 0 10px 0; " />
<p>Многопрофильность и множество направлений деятельности нашей компании требуют большое количество специалистов. 
Мы постоянно ищем мастеров своего дела, желающих присоединиться к нашей команде!
Со своей стороны обеспечиваем:
</p>
<ul>
    <li>	Устройство по трудовому кодексу</li>
    <li>	Стабильную высокую «белую» зарплату</li>
    <li>	Работу в крупной, динамичной компании</li>
    <li>	Адекватное руководство</li>
    <li>	Команду профессионалов и единомышленников</li>
</ul>

</div>

</div>
<div class="vacancy-list">
<h1>Компании требуются:</h1>
<hr style="margin: 10px auto; width: 80%;" />
    <p style="margin: 0 auto; text-align: center; color: gray">К сожалению, на данный момент у нас нет открытых вакансий</p>
</div>
</div>

<hr style="margin: 35px 0; clear: both;" />
</div>

<?php include_once('bottom-menu.php'); ?>
<div class="yellow-line"></div>
<div class="blue-line"></div>
<?php include_once('footer.php'); ?>


</body>
</html>