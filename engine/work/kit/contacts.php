<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">

<head>
<link rel="stylesheet" type="text/css" href="style.css">
<link rel="stylesheet" type="text/css" href="kit_contacts.css">
<meta name="viewport" content="width=device-width, initial-scale=0.66"/>
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<meta name="author" content="Snaker" />
<meta name="keywords" content="купить автозапчасти, запчасти для китайских автомобилей, кит-авто, китайские автозапчасти, тюнинг, ремонт, Коломна, Воскресенск" />
<meta name="description" content="Компания Кит-Авто. Запчасти для китайских автомобилей. Обслуживание, тюнинг и ремонт. Московская область, Коломна, Воскресенск ." />	

<style type="text/css">
#hour {
    display: inline-block;
    font-size: 72px; 
    padding: 0 28px 5px 28px; 
    margin: 15px 0;   
    color: white;
    text-shadow: 0 1px 4px black;  
    background: rgb(249,198,103); /* Old browsers */
    background: -moz-linear-gradient(top,  rgba(249,198,103,1) 0%, rgba(247,150,33,1) 100%); /* FF3.6+ */
    background: -webkit-gradient(linear, left top, left bottom, color-stop(0%,rgba(249,198,103,1)), color-stop(100%,rgba(247,150,33,1))); /* Chrome,Safari4+ */
    background: -webkit-linear-gradient(top,  rgba(249,198,103,1) 0%,rgba(247,150,33,1) 100%); /* Chrome10+,Safari5.1+ */
    background: -o-linear-gradient(top,  rgba(249,198,103,1) 0%,rgba(247,150,33,1) 100%); /* Opera 11.10+ */
    background: -ms-linear-gradient(top,  rgba(249,198,103,1) 0%,rgba(247,150,33,1) 100%); /* IE10+ */
    background: linear-gradient(to bottom,  rgba(249,198,103,1) 0%,rgba(247,150,33,1) 100%); /* W3C */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9c667', endColorstr='#f79621',GradientType=0 ); /* IE6-9 */
    box-shadow: 0 1px 4px black;
    }
      
    
</style>

	<title>Кит-Авто | Контакты</title>
    
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script> 
    <script type="text/javascript" src="js/onhead.js"></script> 
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false&language=ru"></script>
    <script type="text/javascript">
    function initialize()
    {
        var map;
		var opts = {
     		 zoom: 12,
     		 center: new google.maps.LatLng(55.074817,38.764422),
     		 mapTypeId: google.maps.MapTypeId.ROADMAP,
             draggableCursor: 'default'
	         }

        map = new google.maps.Map(document.getElementById("map"), opts);
        
        var kit = new google.maps.LatLng(55.073687,38.798497);
        var image = 'images/map_drago.png';
        var marker = new google.maps.Marker({
        map: map,
        position: kit,
        icon: image
      });
    }
    google.maps.event.addDomListener(window, 'load', initialize);
    </script>
</head>


<body>

    <?php include_once("heading.php"); ?>  
    <?php include_once("menu.php"); ?> 
    <?php include_once("sidebar.php"); ?>  
    
    <div id="doc">
            <div id="road">
            
                        <div id="map-outer">
                <div id="map"></div>
            </div>
            
            <div id="text">
                <p class="tile">Наш адрес:</p>
                <p class="text">Московская обл, г. Коломна, <br /> пр. Станкостроителей,<br /> ТК "КОНТИНЕНТ", пав. 4а</p>
                <p class="tile">Телефоны:</p>
                <p class="text" style="font-size: 1.2em;">8 (910) 489 08 98<br/>8 (916) 968-43-44</p>
                <p class="tile">Наша почта:</p>
                <p class="text" style="font-size: 1em;">Kit-Auto878<strong>@yandex.ru</strong></p>
            </div>
            
                <div id="hours">
                   <span style="display:block">Часы работы:</span>
                    <div id="hour"> 9:00  -  18:00</div>
                    <img src="images/logo.png" />
                </div>
                   
    </div>
</div>    
    
    <div id="footer">
<img src="images/onhead2.png" alt="Кит-Авто лого"/>
<p>2013 Кит-Авто. Сделано в Snapix.</p>
</div>
<?php //include_once('counters.php'); ?>
    


</body>
</html>