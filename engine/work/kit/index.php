<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>

	<meta name="viewport" content="width=1120"/>
	<meta http-equiv="content-type" content="text/html, charset=UTF-8"/>
	<meta name="author" content="Snaker" />
    <meta name="keywords" content="Кит-авто, запчасти для китайских автомобилей коломна, запчасти для автомобилей китайского производства марок, запчасти Chery,запчасти Geely,запчасти Great Wall, запчасти Lifan,автозапчасти для тюнинга китайских автомобилей,запчасти ЧЕРИ, запчасти ГРЕЙТ ВОЛ,запчасти лифан, Коломна, Воскресенск" />
    <meta name="description" content="Компания Кит-Авто. Запчасти для китайских автомобилей. Обслуживание, тюнинг и ремонт. Московская область, Коломна, Воскресенск ." />	

    <link rel="icon" href="favicon.ico" type="image/x-icon"/> 
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" href="kit_main.css"/>
	<title>Кит-Авто | Главная</title>
    
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script> 
    <script type="text/javascript" src="js/onhead.js"></script>   
</head>
<body>


    <?php include_once("heading.php"); ?>     
    <?php include_once("menu.php"); ?> 
    <?php include_once("sidebar.php"); ?>     

    <div id="doc">
   
    <h1>Кит-Авто.</h1> 
    <h2>Запчасти для китайских <br/>автомобилей, джипов и грузовиков.</h2>
    
    <p>Компания <strong>Кит-Авто</strong> основана <strong>в 2007 году</strong> и расположена в г. Коломна.
    Сегодня китайские автомобили все больше завоевывают рынок и каждый день доказывают свою надежность,
    комфортабельность, удобство управления, и, при этом, доступность.</p>
    <p>За время своей работы мы получили <strong>множество положительных отзывов</strong> от
    наших клиентов и зарекомендовали себя как  надежный<br/> и ответственный поставщик автозапчастей 
    для китайских автомобилей.</p>
    
    
    <a href="book.php">Мнения о нас &gt;&gt;</a>
        
 
      <br/><br/>      
      
    Мы постоянно расширяем  ассортимент запчастей  в наших магазинах и на 
    сегодняшний день готовы предложить 
    Вам <strong>более 100 000</strong>  наименований
    комплектующих. <br/><br/>
    Мы рады сотрудничеству с <strong>каждым 
    клиентом</strong> <br/>и стараемся 
    оперативно <strong>решать Ваши 
    задачи </strong>по поиску нужных Вам  
    запчастей!  
           
    <div id="features">Сотрудничая с нами Вы получаете:
    
    <img style="float: left;" src="images/ok.png" alt="OK" />
    
        <div style="float: left;">
        <ul>
        <li>Хорошие цены на китайские автозапчасти</li> 
        <li>Запчасти для грузовиков и автобусов</li>
        <li>Ремонт, обслуживание, тюнинг</li>
        <li class="r">Грузоперевозки до 3 тонн</li>
        <li class="r">Программы дисконтных скидок </li>
        <li class="r">Удобные условия работы </li>
        <li class="r">Заинтересованное, квалифицированное	обслуживание нашего персонала! </li> 
        </ul>
        </div>
    </div>


<div id="bottomtxt" style="width: 100%;  clear: left; margin-top: 20px;">
Наши менеджеры <strong>всегда помогут Вам</strong> подобрать нужную деталь и ответят на любые<br/>  сопутствующие вопросы. <br/><br/>
<strong>Всегда в наличии:</strong>
<ul>
<li>Расходные материалы для проведения ТО </li>
<li>Детали подвески, кузовные элементы, оптика</li>
<li>Детали крепежей и аксессуаров и многие другие запчасти <strong>Chery</strong>, запчасти <strong>Faw</strong></li>
<li> Запчасти <strong>Great Wall</strong> и других автомобилей.<br/></li>
</ul>
Наш адрес:<br/><b> г. Коломна, пр. Станкостроителей, ТК "КОНТИНЕНТ", пав. 4а</b>
<hr />
Вы можете связаться с нами по телефонам:
<p class="phone">8 (910) 489 08 98</p>
<p class="phone">8 (916) 968-43-44</p>

<br/>
или по e-mail:  <b>Kit-Auto878@yandex.ru</b><br/><br/>
<b>Мы будем рады видеть Вас в числе наших клиентов!</b>
</div>

    </div>
<?php //include_once('counters.php'); ?>
    </div>
</div>
<div id="footer">
<img src="images/onhead2.png" alt="Кит-Авто лого"/>
<p>2013 Кит-Авто. Сделано в Snapix.</p>
</div>

</body>
</html>