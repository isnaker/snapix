<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://vwww.w3.org/1999/xhtml" xml:lang="ru" lang="ru">
<head>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/> 
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" type="text/css" href="style.css"/>
    <link rel="stylesheet" type="text/css" href="kit_main.css"/>
	<meta name="viewport" content="width=device-width, initial-scale=0.66"/>
	<meta http-equiv="content-type" content="text/html, charset=UTF-8"/>
	<meta name="author" content="Snaker" />
    <meta name="keywords" content="Кит-авто, запчасти для китайских автомобилей коломна, запчасти для автомобилей китайского производства марок, запчасти Chery,запчасти Geely,запчасти Great Wall, запчасти Lifan,автозапчасти для тюнинга китайских автомобилей,запчасти ЧЕРИ, запчасти ГРЕЙТ ВОЛ,запчасти лифан, Коломна, Воскресенск" />
    <meta name="description" content="Компания Кит-Авто. Запчасти для китайских автомобилей. Обслуживание, тюнинг и ремонт. Московская область, Коломна, Воскресенск ." />	
	<title>Кит-Авто | Каталог</title>
    
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script> 
    <script type="text/javascript" src="js/onhead.js"></script>   
    <script type="text/javascript">
    
    $(document).ready(function() {

$('#doc a').click(function() {
    
   var itm = $(this);
   var link = $(this).attr("href");
   var str = link.substr(11, link.length);

   $.ajax({
                type:"get",
                url : "katalog_items/" + str + ".php",
                data:{/*'title':title*/},
                success: function(html)
                     { 

                        $('.output').html(html); 
                        $('#doc .current').removeClass('current');
                        itm.addClass('current');
                     }
        });
   return false;

});
});
</script>  
    
    
    </script>
    <style type="text/css">
.output {clear: left; display: block;}
.output img {}

.marks {display: block;  text-align: center}

.mark {     
			display: inline-block;
            border: 1px solid rgba(235,235,235,0.66);
            margin: 5px;
            padding: 6px;
            background: white;
            border-radius: 5px;
            }
            
.mark img {max-width: 80px;
            max-height: 80px;
            box-shadow: none;
}

#doc .current { 
	box-shadow: 0 2px 15px orange;
	border-radius: 5px; 
	border: #f0c388 solid 1px }

.mark:hover {	
                cursor: pointer;
                
                box-shadow: 0px 2px 8px rgba(0,0,0,0.5)
                }
.default_item_text {font-size: 0.85em; display: block; text-align: center; color: gray}

</style>
    
</head>
<body>

    <?php include_once("heading.php"); ?>  
    <?php include_once("menu.php"); ?> 
    <?php include_once("sidebar.php"); ?>    
    <?php 
    
    $item_in_array = false;
    if ($_GET)
        { 
            $array_items = array('faw','dongfeng','chery','byd','brilliance','lifan','greatwall','jmc','hafei','geely','foton');
            $item = $_GET['id'];
            
            for ($i=0; $i<count($array_items); $i++)
            {
                if ($item == $array_items[$i]) $item_in_array = true;
            }
        }
        else $item = "";
    ?>
    <div id="doc">
    
        <h2>Каталог товаров</h2>
        <hr />  

        <div class="marks">
        <a class="mark <?php if ($item == 'faw')        {echo "current";} ?>" href="katalog?id=faw"><img src="images/logos/FAW-logo.jpg" width="128" height="128" /></a>
        <a class="mark <?php if ($item == 'dongfeng')   {echo "current";} ?>" href="katalog?id=dongfeng"><img src="images/logos/DongFeng-logo.jpg" width="128" height="128" /></a>
        <a class="mark <?php if ($item == 'chery')      {echo "current";} ?>" href="katalog?id=chery"><img src="images/logos/Chery-logo.gif" width="350" height="350" /></a>
        <a class="mark <?php if ($item == 'byd')        {echo "current";} ?>" href="katalog?id=byd"><img src="images/logos/byd_139.jpg" width="640" height="438" /></a>
        <a class="mark <?php if ($item == 'brilliance') {echo "current";} ?>" href="katalog?id=brilliance"><img src="images/logos/Brilliance logo.png" /></a>
        <a class="mark <?php if ($item == 'lifan')      {echo "current";} ?>" href="katalog?id=lifan"><img src="images/logos/lifan_logo.jpg" width="350" /></a>
        <a class="mark <?php if ($item == 'greatwall')  {echo "current";} ?>" href="katalog?id=greatwall"><img src="images/logos/wall_5032.jpg" width="358" height="282" /></a>
        <a class="mark <?php if ($item == 'jmc')        {echo "current";} ?>" href="katalog?id=jmc"><img src="images/logos/JMC_MOTORS.gif" width="250" height="113" /></a>
        <a class="mark <?php if ($item == 'hafei')      {echo "current";} ?>" href="katalog?id=hafei"><img src="images/logos/hafei-logo-3.jpg" width="652" height="724" /></a>
        <a class="mark <?php if ($item == 'geely')      {echo "current";} ?>" href="katalog?id=geely"><img src="images/logos/Geely-1-logo.jpg" width="128" height="123" /></a>
        <a class="mark <?php if ($item == 'foton')      {echo "current";} ?>" href="katalog?id=foton"><img src="images/logos/Foton-logo.jpg" width="128" height="128" /> </a>
        <hr style="display: block; clear: left"/>
        </div>
        
        <div class="output">
        <?php
           if ($item != "")
           { 
            if ($item_in_array)
            {
              echo "<h1>$item</h1>";
              include_once("katalog_items/$item.php");
            }
            else echo '<p class="default_item_text">Неверный запрос, попробуйте еще раз</p>';
        }
        else
        echo '<p class="default_item_text">Выберите марку автомобиля для просмотра информации о ней</p>' ?>
        </div>


    </div>

  </div>
</div>

<div id="footer">
<img src="images/onhead2.png"/>
<p>2013 Кит-Авто. Сделано в Snapix.</p>
</div>

</body>
</html>