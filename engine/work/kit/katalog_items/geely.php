<img src="images/logos/Geely-1-logo.jpg" width="100" />
<img src="images/c_geely.jpg" width="600" />

<p><strong>Geely Holding Group</strong> является крупным промышленно-коммерческим объединением, основная деятельность которого 
заключается в производстве автомобилей и автомобильного оборудования. Для организации экспортных поставок автомобилей 
<strong>Geely</strong>, в июле 2002 была образована дочерняя компания <strong>Geely Holding Group</strong> — предприятие <strong>Geely International Corporation</strong> 
(Международная Корпорация Джили). Эта компания была наделена полномочиями официального экспортёра автомобилей <strong>Geely</strong>.
<br />   <br />   
Объединение <strong>Geely Holding Group</strong> было учреждено 6 ноября 1986. За более чем двадцатилетнюю историю своего развития 
предприятие достигло существенных успехов в производстве автомобилей, мотоциклов, двигателей, коробок передач и 
прочих автомобильных деталей, декоративных отделочных материалов, а также в сферах высшего образования, туризма, 
строительства и недвижимости.
<br />   <br />   
С момента выхода на автомобильный рынок в 1996 году, компания <strong>Geely</strong> быстро стала основным китайским брендом, 
выпускающим бюджетные автомобили, во многом благодаря гибкому подходу к работе и непрерывному внедрению новых идей.
</p>
<a href="../prices/geely.htm" class="a_price" target="_blank">Просмотреть каталог</a>