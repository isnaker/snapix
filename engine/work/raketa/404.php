<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="viewport" content="width=1024"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/style.css" />
    <meta name="keywords" content=""/>
    <meta name="description" content="" />

    <title>Ракета</title>
    <script type="text/javascript"> </script>
</head>

<body>
<header>
    <div id="nav">
        <img id="logo" src="images/logo.png" alt="Ракета логотип">
        <a class="nav-item" href="index.php" id="a-index">Главная</a>
        <a class="nav-item" href="egrul.php" id="a-egrul">ЕГРЮЛ</a>
        <a class="nav-item" href="egrp.php" id="a-egrp">ЕГРП</a>
        <a class="nav-item" href="bankgarant.php" id="a-bankgarant">Банковская гарантия</a>
        <a class="nav-item" href="audit.php" id="a-audit">Аудит</a>
        <a class="nav-item" href="buhoutsource.php" id="a-buhoutsource">Бухгалтерский аутсорсинг</a>
        <a class="nav-item" href="useful.php" id="a-useful">Полезное</a>
        <a class="nav-item" href="contacts.php" id="a-contacts">Контакты</a>

        <div id="head-contacts">
            <img src="images/star.png" alt="star">
            <span class="contact-line">+7 (495) 233-57-22</span>
            <span class="contact-line">info@12041961.ru</span>
        </div>
    </div>

</header>

<div id="site-content">
    <article>
       <p> <h1 style="font-size: 76px">404</h1>
        <h2>Вы попали куда-то не туда.</h2>
        </p>
    </article>
</div>

<img id="informburo" src="images/informburo.png" alt="информбюро">

<?php include_once('footer.php'); ?>

</body>

</html>