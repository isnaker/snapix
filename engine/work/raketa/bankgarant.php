<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="viewport" content="width=1024"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="description" content="Ракета. Выдача банковских гарантий – это финансовое искусство, которое под силу
    только настоящему мастеру. Совершить высший пилотаж по предоставлению банковской гарантии на всей территории России.">

    <meta name="keywords" content="банковская гарантия, выдача банковских гарантий, предоставление банковской гарантии,
    получение банковской гарантии, предоставим банковскую гарантию.">
    <meta name="robots" content="all" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />

    <title>Ракета. Безукоризненное предоставление банковских гарантий. Срочная выдача банковских гарантий в Москве. Официально предоставим банковскую гарантию. Визитка в подарок! </title>

    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/jScrollPane.css" />
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/overlay.js"></script>
    <script type="text/javascript" src="js/jScrollPane.js"></script>
    <script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            $('.scroll-pane').jScrollPane({showArrows:true,scrollbarWidth:19,dragMaxHeight:43});
        });
    </script>
    <style type="text/css">
        #nav .current {
            background:rgb(85,182,204);
        }
        #site-content p:first-child {
            margin-top: 5px;
        }
        #site-content p{
            width: 98%;
        }
        #site-content h4 {
            margin: 0;
        }
        ul li {
            list-style-image: url("images/marker-blue.jpg") ;
        }
    </style>
    <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css" />
    <![endif]-->
</head>

<body>
<header>
    <div id="nav">
        <img id="logo" src="images/logo.png" alt="Ракета логотип">
        <a class="nav-item" href="index.php" id="a-index">Главная</a>
        <a class="nav-item" href="egrul.php" id="a-egrul">ЕГРЮЛ</a>
        <a class="nav-item" href="egrp.php" id="a-egrp">ЕГРП</a>
        <a class="nav-item current" href="bankgarant.php" id="a-bankgarant">Банковская гарантия</a>
        <a class="nav-item" href="audit.php" id="a-audit">Аудит</a>
        <a class="nav-item" href="buhoutsource.php" id="a-buhoutsource">Бухгалтерский аутсорсинг</a>
        <a class="nav-item" href="useful.php" id="a-useful">Полезное</a>
        <a class="nav-item" href="contacts.php" id="a-contacts">Контакты</a>

        <div id="head-contacts">
            <img src="images/star.png" alt="star">
            <span class="contact-line">+7 (495) 233-57-22</span>
            <span class="contact-line" style="margin-right: -4px;">info@12041961.ru</span>
        </div>
    </div>

</header>

<div id="site-content" class="jScrollPaneContainer">
    <div class="scroll-wrap">
        <div class="scroll-pane">

            <h4>Что такое банковская гарантия и с чем её едят?</h4>
            <p>Банковская гарантия – это эффективный инструмент обеспечения исполнения денежных обязательств заёмщика
                перед кредитором, где в роли гаранта может выступать, как банк, так и другая кредитная, либо страховая
                организация, выдающая банковскую гарантию.  Гарант является поручителем, в случае невыполнении заёмщиком
                (принципалом) своего денежного обязательства перед кредитором (бенефициаром), ответственность за своего
                клиента ложится на гаранта. Банковская гарантия – беззалоговый рентабельный механизм обеспечения
                финансовых обязательств, поскольку позволяет сохранить оборотные активы. Выдача банковской гарантии
                осуществляется на срок, указанный в договоре.</p>

                <h4>Основные виды банковской гарантии:</h4>
                <ul>
                    <li>Обеспечение исполнения контракта, в частности тендерные контракты</li>
                    <li>Платежная гарантия</li>
                    <li>Таможенные гарантии</li>
                    <li>Гарантия возврата авансового платежа</li>
                    <li>Гарантия обеспечения кредита</li>
                </ul>
                <p><b>Мы работаем в белую.</b> Полностью исключаем любые мошеннические схемы. Процедура предоставления
                    банковской гарантии проходит проверку в ЦБ, после подтверждения о выдаче – вся информации
                    отображается в общедоступном реестре. Никакой предоплаты – денежные перечисления идут напрямую
                    на расчетный счёт банка. Официально оформленные документы выдаются лично в банке.</p>
                <p><b>Наше преимущество</b> не в самом низком посредническом проценте, а в том, что мы стараемся максимально
                    минимизировать риски. Вся система работы точно отлажена, рассмотрение заявки с принятием решения
                    проходят в самые короткие сроки. Наши партнеры - известные ликвидные проверенные временем банки.
                    Наш комиссионный процент постоянно варьируется и зависит от многих факторов: вид банковской
                    гарантии, сумма обеспечения, срок действия гарантии и непосредственно от условий контракта.</p>

                <h4>1% - навязчивый рекламный ход.</h4>

                <p><b>Мы предлагаем</b> выгодные условия сотрудничества: рассмотрение заявки в течение рабочего дня.</p>
                <p><b>Автомат Калашникова</b> такой же надёжный, как наши отношения с клиентами и партнерами. Наша цель –
            это долгосрочное плодотворное сотрудничество, мы предоставим банковскую гарантию. Чтим и уважаем 44 ФЗ
            "О контрактной системе в сфере закупок товаров, работ, услуг для обеспечения государственных
                    и муниципальных нужд".</p>
        </div>
    </div>
</div>

<img id="informburo" src="images/informburo.png" alt="информбюро">

<?php include_once('footer.php'); ?>

</body>

</html>