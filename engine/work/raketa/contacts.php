<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xmlns="http://www.w3.org/1999/html" xml:lang="en" lang="en">
<head>
<meta name="viewport" content="width=1024"/>
<meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
<meta name="description" content="Ракета. Срочное получение документов в налоговых органах. У нас можно заказать
    официально заверенную выписку с курьерской доставкой из Единого государственного реестра юридических лиц (ЕГРЮЛ)
    или Единого государственного реестра индивидуальных предпринимателей (ЕГРИП). Пунктуальное, компетентное и
    качественное исполнение оказываемых услуг.">
<meta name="keywords" content="егрюл, егрип, егрюл срочно, выписка из егрюл, егрюл заказать, заказать выписку из
    егрюл, срочная выписка из егрюл, получить егрюл, егрюл срочно, получение выписки из егрюл, выписка из егрип,
    заказать егрип, заказать выписку из егрип, выписка из егрюл москва, получить выписку из егрип, получить егрип,
    выписка из егрюл с доставкой, егрюл официально, срочное получение выписки из егрюл.">
<meta name="robots" content="all" />
<meta http-equiv="pragma" content="no-cache" />
<meta http-equiv="cache-control" content="no-cache" />

<title>Контакты. Ракета. Since 1961. Визитка в подарок!</title>

<link rel="icon" href="favicon.ico" type="image/x-icon"/>
<link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
<link rel="stylesheet" href="css/style.css" />
<link rel="stylesheet" href="css/jScrollPane.css" />

<script type="text/javascript" src="js/jquery.js"></script>
<script type="text/javascript" src="js/jScrollPane.js"></script>
<script type="text/javascript" src="js/jquery.mousewheel.min.js"></script>
<script type="text/javascript" src="js/overlay.js"></script>
<script src="//api-maps.yandex.ru/2.0/?load=package.full&lang=ru-RU" type="text/javascript"></script>
<style type="text/css">
    #nav .current {
        background: rgb(210,74,69);
    }

    #content {
        display: inline-block;
        position: relative;
        margin: 0 auto;
        padding-top: 110px;
        width: 70%;
    }

    #site-content {
        min-height: 545px;
        padding: 105px 0 0 0;
    }

    #site-content p {
        width: 100%;
        text-align: justify;
        line-height: 1;
        font-family: "Calibri", sans-serif;
    }

    #site-content h4 {
        margin: 0;
    }

    .bubble-text, .more {
        color: rgb(64,64,64);
    }

    .red-bubble {
        background: rgb(93,175,97);
    }

    .green-bubble {
        background: rgb(231,163,68);
    }

    #map {
        width: 75%;
        height: 400px;
        display: block;
        margin: 0 auto;
    }

    #back {
        margin-left: 262px;
    }

    #magick {
        position: absolute;
        z-index: 2;
        top: 0;
        width: 100%;
    }

</style>

    <script type="text/javascript">
        function showMap(){
            $('#magick').animate({'opacity':0},450, function(){ $(this).css('display','none')});
        }

        function hideMap(){
            $('#magick').css('display','block').animate({'opacity':1},450);
        }

        var myMap;
        ymaps.ready(init);

        function init () {
            myMap = new ymaps.Map('map', {
                center:[55.77329,37.683694],
                zoom: 12
            });

            myMap.controls.add('zoomControl', { left: 5, top: 5 })
                    .add('typeSelector').add('mapTools', { left: 35, top: 5 });

            myGeoObject = new ymaps.GeoObject({
                geometry: {
                    type: "Point",
                    coordinates: [55.77329,37.683694]
                },
                properties: {
                    iconContent: 'Ракета'
                }
            }, {
                preset: 'twirl#redStretchyIcon',
                draggable: false
            });

            myMap.geoObjects.add(myGeoObject);

            $('#back').click(function(){
                hideMap();
            });
        }

    </script>
    <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css" />
    <![endif]-->
</head>

<body>
<header>
    <div id="nav">
        <img id="logo" src="images/logo.png" alt="Ракета логотип">
        <a class="nav-item " href="index.php" id="a-index">Главная</a>
        <a class="nav-item " href="egrul.php" id="a-egrul">ЕГРЮЛ</a>
        <a class="nav-item " href="egrp.php" id="a-egrp">ЕГРП</a>
        <a class="nav-item" href="bankgarant.php" id="a-bankgarant">Банковская гарантия</a>
        <a class="nav-item" href="audit.php" id="a-audit">Аудит</a>
        <a class="nav-item" href="buhoutsource.php" id="a-buhoutsource">Бухгалтерский аутсорсинг</a>
        <a class="nav-item" href="useful.php" id="a-useful">Полезное</a>
        <a class="nav-item current" href="contacts.php" id="a-contacts">Контакты</a>

        <div id="head-contacts">
            <img src="images/star.png" alt="star">
            <span class="contact-line">+7 (495) 233-57-22</span>
            <span class="contact-line">info@12041961.ru</span>
        </div>
    </div>

</header>

<div id="site-content">
    <img id="magick" src="images/magick.gif">
    <span id="back">назад</span>
    <div id="map"></div>
</div>

<div class="bubble green-bubble"><p class="bubble-text" style="font-size: 0.9em"><strong>Телефон:</strong> +7 (495) 233-57-22 <strong>E-mail:</strong> info@12041961.ru</p> </div>
<div class="bubble blue-bubble"><p class="bubble-text">Наш адрес: город-герой Москва, ул. Фридриха Энгельса, д. 23/1.</p><span class="more" onclick="showMap();">схема</span> </div>
<div class="bubble red-bubble"><p class="bubble-text">Приглашаем к нам в гости на чашечку чая. Пн-пт с 9.00 до 20.00.</p> </div>

<img id="informburo" src="images/informburo.png" alt="информбюро">

<?php include_once('footer.php'); ?>

</body>

</html>