<footer>
    <b id="citate"></b>
    <div id="social">
        <a href=""><img src="images/social_linkedin.png"/></a>
        <a href=""><img src="images/social_twitter.png"/></a>
        <a href=""><img src="images/social_facebook.png"/></a>
        <a href=""><img src="images/social_youtube.png"/></a>
    </div>

</footer>
<script type="text/javascript">
    $(document).ready(function(){
        $.ajax({
            type: "POST",
            url: "engine/citates.php",
            data: {
                action: "getCitate"
            },
            success: function(data){
                var c = JSON.parse(data);
                $('#citate').text(c.citate);
            }
        })
    });
</script>