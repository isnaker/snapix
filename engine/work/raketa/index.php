<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="viewport" content="width=1024"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/style.css" />
    <meta name="description" content="Юридическая компания Ракета оказывает для предпринимателей и частных лиц диверсифицированные деловые услуги.">
    <meta name="keywords" content="Ракета, 12041961.ru.">

    <meta name="robots" content="all" />
    <meta http-equiv="pragma" content="no-cache" />
    <meta http-equiv="cache-control" content="no-cache" />

    <title>Главная. Ракета. Since 1961. Визитка в подарок!</title>
    <script type="text/javascript" src="js/jquery.js"></script>
    <script type="text/javascript" src="js/overlay.js"></script>

    <!--[if IE]>
    <link rel="stylesheet" href="css/ie.css" />
    <![endif]-->
</head>

<body>

<header>
    <div id="nav">
        <img id="logo" src="images/logo.png" alt="Ракета логотип">
        <a class="nav-item current" href="index.php" id="a-index">Главная</a>
        <a class="nav-item" href="egrul.php" id="a-egrul">ЕГРЮЛ</a>
        <a class="nav-item" href="egrp.php" id="a-egrp">ЕГРП</a>
        <a class="nav-item" href="bankgarant.php" id="a-bankgarant">Банковская гарантия</a>
        <a class="nav-item" href="audit.php" id="a-audit">Аудит</a>
        <a class="nav-item" href="buhoutsource.php" id="a-buhoutsource">Бухгалтерский аутсорсинг</a>
        <a class="nav-item" href="useful.php" id="a-useful">Полезное</a>
        <a class="nav-item" href="contacts.php" id="a-contacts">Контакты</a>

        <div id="head-contacts">
            <img src="images/star.png" alt="star">
            <span class="contact-line">+7 (495) 233-57-22</span>
            <span class="contact-line" style="margin-right: -4px;">info@12041961.ru</span>
        </div>
    </div>

</header>

<div id="site-content">
    <article>
        <p>12 апреля 1961 года Юрий Алексеевич Гагарин на борту космического корабля-спутника «Восток» совершил первый в
        мире пилотируемый полет в космическое пространство, став космонавтом номер 1. Это поистине величайшее событие
        в истории нашей страны и всего человечества, олицетворяющее собой прорыв инженерной мысли и смелость
        первоиспытателей. Поехали!</p>

        <strong>СКОРОСТЬ + НАДЁЖНОСТЬ + <img style="margin-bottom: -11px; position: relative; display: inline-block" src="images/smile.png"/> <span style="font-size: 28px;position: relative;top: 2px;">=</span> РАКЕТА</strong>

        <p>Товарищи, горячо приветствуем Вас на сайте юридической компании Ракета! Вот уже более десяти лет мы существуем
        и развиваемся на российском рынке. За это время наша компания смогла выработать эффективную схему работы,
        накопить мощный коммерческий опыт, понять и простить конкурентов, наладить стойкие деловые контакты.
        Всё это позволило нам не только занять лидирующие позиции, но и, что очень важно, улучшить показатели по
        обслуживанию заказчиков. Мы высоко ценим лояльность клиента, поэтому тактичность, оперативность,
        качественное оказание услуг, индивидуальный подход на всех этапах – это наши приоритеты. Надеемся,
        Вы будете довольны нашей работой, в противном случае… деньги Вам всё равно никто не вернёт. Шутка)</p>

    </article>
</div>

<img id="informburo" src="images/informburo.png" alt="информбюро">

<?php include_once('footer.php'); ?>

</body>

</html>