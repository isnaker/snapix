$(document).ready(function(){

        //tryin to get cookie
        var _cookie = getCookie('userHideOverlay');

        // if no cookie or expired then show overlay
        if (_cookie == undefined){
            var date = new Date( new Date().getTime() + 1000*3600*2 );
            document.cookie="userHideOverlay=true; path=/; expires="+date.toUTCString();
            showOverlay();
        }

    // bind functions
    $('#informburo').click(function(){
        showOverlay();
        $(this).unbind('click');
    });

    $(window).resize(function(){
        resizeSiteContent();
    });
});


function resizeSiteContent(){
    var contentHeight = $('#site-content').height();
    var docHeight = $(document).height();
    var headHeight = $('header').height();
    var footHeight = $('footer').height();

    var _margin = (docHeight - (headHeight+footHeight+contentHeight)) / 2;
    console.log(_margin);
    $('#site-content').css('margin-top',_margin+"px");
    $('#site-content').css('margin-bottom',_margin+"px");
}

function showOverlay(){
    var overlay = document.createElement('div');
    overlay.id = "overlay";
    overlay.innerHTML = " <div id='overlay-content'> <img src='images/close_overlay.png' id='closeOverlayButton'> " +
        "<h2 id='overlay-heading'>поздравляем! вы 1000 посетитель!</h2> "+
        "<h2 id='overlay-text'>Вам предоставляется 10% скидка!</h2> <h2>Ура, товарищи, ура!!!</h2> "+
        "</div>  <img id='mouth' src='images/mouth.png'> ";

    //set #mouth coords

    $('body').append($(overlay));
    $('#overlay').animate({'opacity':'1'},500);

    $('#closeOverlayButton').hover(function(){ $(this).animate({'marginBottom': '4px', 'marginLeft': '0px', 'width': '34px'},150); },function(){$(this).animate({'marginBottom': '8px','marginLeft': '8px','width': '30px'},150);});

    $('#closeOverlayButton').click(function(){
        hideOverlay();
    });
}

function hideOverlay(){
    $('#mouth').css('position','absolute');
    $('#overlay-content').animate({'opacity':0},400);
    $('#mouth').animate({left: '100%', top: '70%', 'width': '60px'},1000, function(){
        $('#overlay').animate({'opacity':0},500, function(){ $(this).remove(); });
    });

    $('#informburo').click(function(){
        showOverlay();
        $(this).unbind('click');
    })
}

// возвращает cookie с именем name, если есть, если нет, то undefined
function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}


function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires*1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for(var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    document.cookie = updatedCookie;
}

function deleteCookie(name) {
    setCookie(name, "", { expires: -1 })
}