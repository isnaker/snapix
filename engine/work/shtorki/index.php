<!DOCTYPE html>
<html>
<head>
    <meta name="viewport" content="width=1024"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon"/>
    <link rel="stylesheet" href="css/framework.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link href="css/jquery.nouislider.css" rel="stylesheet">
    <link rel="stylesheet" href="css/jquery.flipcountdown.css">
    <link rel="stylesheet" href="css/flexslider.css">
    <link rel="stylesheet" href="js/yoxview.css">
    <link href='http://fonts.googleapis.com/css?family=PT+Sans+Caption&subset=cyrillic-ext,latin' rel='stylesheet' type='text/css'>
    <link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">

    <meta name="keywords" content=""/>
    <meta name="description" content=""/>

    <title>Купить легальную тонировку - шторки Laitovo</title>
    <script src="https://maps.googleapis.com/maps/api/js?v=3.exp"></script>
    <script type="text/javascript" src="js/jquery-2.0.3.min.js"></script>
    <script type="text/javascript" src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript" src="js/ajaxformsender.js"></script>
    <script type="text/javascript" src="js/jquery.flipcountdown.js"></script>
    <script type="text/javascript" src="js/jquery.easing.1.3.js"></script>
    <script type="text/javascript" src="js/modernizr.js"></script>
    <script type="text/javascript" src="js/jquery.flexslider.js"></script>
    <script type="text/javascript" src="js/jquery.responsive_countdown.min.js"></script>
    <script type="text/javascript" src="js/jquery-ui-1.9.2.custom.min.js"></script>
    <script type="text/javascript" src="js/yoxview-init.js"></script>
    <script type="text/javascript" src="js/index.js"></script>
    <link rel="stylesheet" href="css/style.css" type="text/css">
</head>
<body>

<header class="collapsed">
    <div class="box1000 centered">
    <div id="logo" class="box25p">
        <img src="img/logo.png" alt=""/>
        <small>Территория комфорта</small>
    </div>
    <div class="box50p text-centered">
        <h3>8 (985) 486 32 22</h3>
    </div>
    <div class="box25p">
        <button class="button caller">Заказать звонок</button>
        <div style="font-size: 0.69em; padding-top: 0.4em">г. Коломна, ул. Кирова, д. 15Б</div>
    </div>
    <nav class="clearfix">

    </nav>
    </div>
    <img id="shadow" src="img/shadow_3.png" alt="shadow" class="centered">
</header>

  <div class="centered" id="promo">
        <h1 class="text-centered"><span>Лайтово – альтернатива тонировке!</span></h1>
        <h2 id="little_heading" class="text-centered">Каркасные автошторки на заказ для любого автомобиля</h2>
    </div>

<div class="row" style="background: rgba(0,0,0,0.36)">
    <div class="row box1000 centered">
    <div class="box5p"></div>
    <div class="box50p">
        <div class="video" id="monitor">
          <iframe id="video" width="460" height="260" style="margin-left: 20px;margin-top: 13px;" src="//www.youtube.com/embed/Kig2vDGSNm4" frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="box3p"></div>
    <div class="box33p">
        <div class="form text-centered simple" style="margin-top: -1em">
            <h3>Оставьте заявку на шторки для Вашего авто прямо сейчас!</h3>
            <input class="name" type="text" placeholder="Ваше имя">
            <input class="phone" type="tel" placeholder="Ваш телефон">
            <input class="mark" type="text" placeholder="Модель автомобиля">
            <input class="email" type="email" placeholder="Ваш e-mail">
            <select class="type">
                <option value="Передние шторки">Передние шторки</option>
                <option value="Задние шторки">Задние шторки</option>
                <option value="Весь комплект шторок">Весь комплект шторок</option>
            </select>
            <div><button class="button">Оставить заявку</button></div>
            <div class="response"></div>
        </div>
    </div>
        <div class="clearfix"></div>
  </div>
</div>

<div id="content">
    <div id="social" class="clearfix row">
        <div class="box1000 centered">
            <div class="box60p" id="video_controller">
                <div class="item current" data-title="//www.youtube.com/embed/Kig2vDGSNm4">Laitovo - защита от солнца нового поколения.</div>
                <div class="item" data-title="//www.youtube.com/embed/qNbeL8IuC28">Инструкция по установке Laitovo</div>
                <div class="item" data-title="//www.youtube.com/embed/xX8qJFlC4g8">Sonnenschutz der nächsten Generation!</div>
                <div class="item" data-title="//www.youtube.com/embed/IU-i-o9j0QQ"">Инструкция по установке бескаркасного защитного экрана</div>
                <div class="item" data-title="//www.youtube.com/embed/yoQebBHOmMU">Сдвижной Laitovo don`t look</div>
                <div class="item" data-title="//www.youtube.com/embed/EBzdSu2fQ9Q">Инструкция по установке сдвижного Laitovo don`t look</div>
            </div>
            <div class="box5p"></div>

            <div class="box33p text-centered">
        <div style="margin-top: 10px">Расскажи друзьям</div>
        <script type="text/javascript">(function() {
                if (window.pluso)if (typeof window.pluso.start == "function") return;
                if (window.ifpluso==undefined) { window.ifpluso = 1;
                    var d = document, s = d.createElement('script'), g = 'getElementsByTagName';
                    s.type = 'text/javascript'; s.charset='UTF-8'; s.async = true;
                    s.src = ('https:' == window.location.protocol ? 'https' : 'http')  + '://share.pluso.ru/pluso-like.js';
                    var h=d[g]('body')[0];
                    h.appendChild(s);
                }})();</script>
        <div class="pluso" data-background="transparent" data-options="big,round,line,horizontal,nocounter,theme=01" data-services="vkontakte,odnoklassniki,facebook,twitter"></div>
            </div>
            <div class="clearfix"></div>
       </div>
    </div>

<div class="row" id="welcome">
    <div class="box1000 centered clearfix">
        <p id="about">Laitovo – немецкий бренд производителя защитных экранов для автомобильных окон. Головной офис Laitovo находится в Гамбурге. На сегодняшний день Laitovo ведёт продажи на территории Европейского союза, ближнего Востока, центральной и восточной Азии. На территории России Laitovo имеет партнёров более чем в 50 регионах страны.
            Защитный экран Laitovo представляет собой прочный металлический каркас, обшитый износостойкой эластичной тканью, который устанавливается в оконный проём автомобиля. Предназначен для защиты водителя и пассажиров от солнечных лучей, сторонних взглядов, пыли и мелкого мусора, а также оберегает от выпадения ребёнка в открытое окно автомобиля.
            Вся продукция Laitovo запатентована и охраняется авторским правом.</p>
    </div>
</div>

<div class="row">
    <div class="box1000 centered">
        <h2 class="text-centered uppercased">Знакомство с продукцией Laitovo</h2>
            <p>Проведя в своем авто значительную часть времени, &nbsp;невольно возникает желание окружить себя&nbsp;комфортом, создать вокруг себя уют, сделать жизнь лнгче и приятней&nbsp;Защитные экраны для автомобильных окон Laitovo - &nbsp;это важный элемент, позволяющий сделать&nbsp;жизнь в автомобиле совершенние</p>

            <p><img alt="" src="http://laitovo.eu/upload/images/Banner/d77a47d902b9acf93d175fcb2bd1dfd3.png" style="width:100%"></p>

            <div class="alert alert-info fade in block">
                <p>Laitovo &nbsp;– &nbsp;немецкий &nbsp;бренд &nbsp;производителя &nbsp;защитных &nbsp;экранов &nbsp;для &nbsp;автомобильных окон.&nbsp;Головной &nbsp;офис &nbsp;Laitovo находится в Гамбурге. &nbsp;На сегодняшний день Laitovo ведёт продажи на&nbsp;территории &nbsp; Европейского &nbsp; союза, &nbsp; ближнего &nbsp; Востока, &nbsp;центральной &nbsp;и &nbsp;восточной Азии. &nbsp;Вся&nbsp;продукция Laitovo запатентована и охраняется авторским правом.</p>
            </div>

            <div class="label-success" style="padding-left:5px;">
                <h4><span style="color:#FFFFFF">Защитные экраны для автомобильных окон</span></h4>
            </div>

            <p>Защитный экран Laitovo представляет собой прочный металлический каркас, обшитый износостойкой эластичной тканью, который устанавливается в оконный проём автомобиля. Экран предназначен для защиты водителя и пассажиров от солнечных лучей, сторонних взглядов, пыли и мелкого мусора, а также оберегает ребёнка от выпадения в открытое окно автомобиля. Экраны устанавливаются на все оконные проёмы за исключением лобо- вого окна. Защитные экраны изготавливаются стандартного размера, с вырезами для боковых зеркал в нескольких типах ткани - от светлой до тёмной.</p>

            <p><img alt="" src="http://laitovo.eu/upload/images/Banner/6e85179d24bc1bfaf96c1f59892e3c70_1.png" style="width:100%"></p>

            <div class="label-success" style="padding-left:5px;">
                <h4><span style="color:#FFFFFF">Зажимы-держатели</span></h4>
            </div>

            <p><img alt="" src="http://laitovo.eu/upload/images/Banner/acc95a7a0be7302b38a1a0182be65b11.png" style="float:right; height:46px; width:150px"></p>

            <p>Зажимы держатели изготовлены из стальных изолированных пластин. Крепятся методом зажима без вмешательства в конструкцию автомобиля.</p>

            <div class="label-success" style="padding-left:5px;">
                <h4><span style="color:#FFFFFF">Дополнительный сдвижной элемент «don`t look»</span></h4>
            </div>

            <p>Данный продукт предназначен для полной изоляции салона автомобиля от проникно- вения света и сторонних взглядов при временной потребности. Дополнительный контур со сдвижным элементом легко крепится к основному экрану Laitovo с помощью специальных магнитов. Сдвижной элемент может приобретаться в комплекте с защитным экраном Laitovo или отдельно, если на а/м уже установлены защитные экраны Laitovo.</p>

            <p><img alt="" src="http://laitovo.eu/upload/images/Banner/d6207c6f1fc667e11f1129d8b1221f69.png" style="width:100%"></p>

            <div class="label-success" style="padding-left:5px;">
                <h4><span style="color:#FFFFFF">Бескаркасный защитный экран «don`t look»</span></h4>
            </div>

            <p>Бескаркасный защитный экран Laitovo don`t look представляет собой экран из износо- стойкого эластичного материала, точно повторяющего контур оконного проёма с вшитыми по всему периметру неодимовыми магнитами. Сильные неодимовые магниты позволяют без особых усилий прикрепить экран к металлическому контуру основного защитного экрана Laitovo. Бескаркасный защитный экран легко крепится и снимается. Вследствие отсутствия металлического каркаса экран удобен в хранении. Рекомендуется хранить в перчаточном ящике. В комплект входит сумка для хранения защитных экранов.</p>

            <p>&nbsp;</p>

            <p><img alt="" src="http://laitovo.eu/upload/images/Banner/914c3884d3e05f8ffbac7aa1c0128f46.png" style="width:100%"></p>

            <h4><span style="color:#3b5662">ЗАЩИТНЫЕ ЭКРАНЫ LAITOVO - ЗАЩИТА ОТ СОЛНЦА НОВОГО ПОКОЛЕНИЯ</span></h4>

            <p>Особая структура ткани с полимерным покрытием позволяет оградить салон от проникновения солнечных лучей, отражая их во внешнюю среду. Это свойство обеспечивает максимальную защиту салона от нагрева. Солнце не нагревает ни салон, ни сами защитные экраны. Одновременно данная структура мелкоячеистой ткани не ограничивает обзорность для водителя и пассажира, обеспечивая комфорт для глаз при движении автомобиля.</p>

            <p><img alt="" src="http://laitovo.eu/upload/images/Banner/00b720fffabf8fb18b71da271d6bbd8e.png" style="width:100%"></p>

            <p>Специальная мелкоячеистая структура ткани позволяет обеспечить комфортную видимость водителю и пасса- жиру в секторе 70 градусов и одновременно создать блокирующий сектор в 25 градусов для защиты от солнечных лучей.</p>

            <p>Специальное полимерное покрытие ткани отражает солнечные лучи, предотвращая нагревание.</p>


            <div class="clearfix"></div>


    </div>

<div style="background: url('img/perforation.png') repeat;">
    <div class="box1000 centered" id="advantages">
            <h2 class="text-centered">ПРЕИМУЩЕСТВА ЗАЩИТНЫХ ЭКРАНОВ LAITOVO</h2>
            <div class="row" >
                <div class="box33p">
                    <img class="flexible" alt="" src="img/advant1.png">
                    <h4>ЗАЩИТА ОТ СОЛНЦА</h4>
                    <p>Защитный экран оградит Вас от солнечных лучей.</p>
                </div>

                <div class="box33p">
                    <img class="flexible" alt="" src="img/advant2.png">
                    <h4>ЗАЩИТА ВАШЕГО РЕБЁНКА</h4>
                    <p>Laitovo оградит Вашего ребёнка от слепящего солнца и позволит свежему воздуху бесшумно проникать в салон. Ребёнка не укачает во время движения.</p>
                </div>

                <div class="box33p">
                    <img class="flexible" alt="" src="img/advant3.png">
                    <h4>ЗАЩИТА ОТ ВЕТРОВОГО ШУМА</h4>
                    <p>Специальная эластичная ткань позволяет гасить вибрации при движении с открытыми окнами.
                        Это свойство обеспечивает свободную циркуля- цию воздуха и тишину в салоне при движении
                        автомобиля.</p>
                </div>

                <div class="box33p">
                    <img class="flexible" alt="" src="img/advant4.png">
                    <h4>ЗАЩИТА ОТ ЛЮБЫХ НАСЕКОМЫХ</h4>
                    <p>Laitovo – надёжная преграда для любого москита.</p>
                </div>

                <div class="box33p">
                    <img class="flexible" alt="" src="img/advant5.png">
                    <h4>ЭКОНОМИЯ ТОПЛИВА</h4>
                    <p>Laitovo позволяют открыть окна во время движения, ограждая от солнечных лучей, ветрового шума и
                        пыли. Это даёт возможность ограничить использование кондиционера и сократить расход топлива до 15%.</p>
                </div>

                <div class="box33p">
                        <img class="flexible" alt="" src="img/advant7.png">
                        <h4>ПРОСТОТА УСТАНОВКИ И СНЯТИЯ</h4>
                        <p>Установил и машина тонирована. Снял, и можно ехать с чистыми окнами.</p>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
    </div>
</div>
</div>

    <div class="row">
        <div class="box1000 centered">
            <div class="inline-form text-centered">
                <h2>АКЦИЯ СЕЗОНА!</h2>
                <h3>При заказе передних шторок сумка в подарок!</h3>
                <h4>При заказе полного комплекта шторок – <span class="color-red">СКИДКА 10%</span> + <span style="text-decoration: underline">Сумка в подарок!</span></h4>
                <div class="countdown"></div>

                <input class="name" type="text" placeholder="Ваше имя">
                <input class="phone" type="tel" placeholder="Ваш телефон">
                <input class="email" type="email" placeholder="Ваш e-mail">
                <div style="padding-top: 1em"><button class="button">Оставить заявку</button></div>

                <div><small>Позвоните или напишите на e-mail</small></div>
                <h4 class="responder"></h4>
            </div>
        </div>
    </div>

    <div class="row" id="products">
        <div class="box1000 centered">
            <h2 class="text-centered">Каталог продукции</h2>
            <div class="row text-centered" id="catalog">
                <div class="item">
                    <img class="flexible" src="img/products/01.jpg" alt="">
                    <h3>Защитные экраны для автомобильных окон - Стандарт</h3>
                    <h4 class="price">от 2000 р. до 4500 р.</h4>
                    <button class="button">Заказать</button>
                </div>
                <div class="item">
                    <img class="flexible" src="img/products/02.jpg" alt="">
                    <h3>Защитные экраны для автомобильных окон с вырезом для курящих</h3>
                    <h4 class="price">от 2200 р. до 4700 р.</h4>
                    <button class="button">Заказать</button>
                </div>
                <div class="item">
                    <img class="flexible" src="img/products/03.jpg" alt="">
                    <h3>Защитные экраны для автомобильных окон с вырезом для бокового зеркала</h3>
                    <h4 class="price">от 2700 р. до 5200 р.</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/04.jpg" alt="">
                    <h3>Сдвижной элемент, дополнительный к защитному экрану laitovo</h3>
                    <h4 class="price">от 2000 р. до 3000 р.</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/05.jpg" alt="">
                    <h3>Бескаркасный защитный экран Laitovo don`t look</h3>
                    <h4 class="price">от 1200 р. до 2500 р.</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/06.jpg" alt="">
                    <h3>Чехлы для хранения автомобильных колёс</h3>
                    <h4 class="price">1500 р.</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/07.jpg" alt="">
                    <h3>Сумка для хранения защитных экранов laitovo</h3>
                    <h4 class="price">200 р.</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/08.jpg" alt="">
                    <h3>Запасной комплект зажимов держателей laitovo*</h3>
                    <h4 class="price">350 р</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/09.jpg" alt="">
                    <h3>Наклейка Laitovo Family</h3>
                    <h4 class="price">50 р</h4>
                    <button class="button">Заказать</button>
                </div>

                <div class="item">
                    <img class="flexible" src="img/products/10.jpg" alt="">
                    <h3>Защитный экран для автомобильных окон - Укороченный</h3>
                    <h4 class="price">от 2000 р. до 4000 р.</h4>
                    <button class="button">Заказать</button>
                </div>
                <div class="clearfix"></div>
            </div>
        </div>
    </div>

    <div class="row" style="box-shadow: 0 1px 8px rgba(0,0,0,0.23); padding-bottom: 2px; margin: 0 0 3em 0">
        <div class="box1000 centered callbackform">
            <h2 class="text-centered" style="padding-bottom: 0.1em">Позвоните нам для уточнения вашей марки автомобиля</h2>
            <h3 class="text-centered">менеджеры помогут подобрать автошторки для вашего авто</h3>

            <div class="text-centered" style="margin: 1em 0">
                <input class="model" type="text" placeholder="Модель автомобиля">
                <input class="phone" type="tel" placeholder="Ваш телефон">
                <select class="type">
                    <option value="Передние шторки">Передние шторки</option>
                    <option value="Задние шторки">Задние шторки</option>
                    <option value="Весь комплект шторок">Весь комплект шторок</option>
                </select>
                <button class="button uppercased caller">заказать звонок</button>
            </div>
            <div class="responder"></div>
        </div>
    </div>

    <div class="box1000 centered">
            <div class="box50p">
                <h2>ИНСТРУКЦИЯ ПО УСТАНОВКЕ</h2>
                <iframe width="470" height="320" class="flexible" src="//www.youtube.com/embed/qNbeL8IuC28" frameborder="0" allowfullscreen></iframe>
            </div>
            <div class="box5p"></div>
            <div class="box40p">
                <div style="height: 6em"></div>
                <p>Защитные экраны Laitovo устанавливаются легко и быстро. Вам потребуется несколько секунд на снятие
                    и несколько секунд на установку экрана.</p>

                <p>Laitovo устанавливаются в специальные металлические зажимы держатели. Зажимы держатели крепятся без
                    сверления, клея и без вмешательства в конструкцию автомобиля.
                </p>
            </div>
            <div class="clearfix"></div>
        </div>


    <div class="row" style="background: silver;  padding: 1.2em 0 2em 0; margin-top: 3em">
        <div class="box800 centered">
            <div class="flexslider">
                <ul class="slides">
                    <?php
                    $path = 'img/slides/';
                    $files = scandir($path);
                    foreach ($files as $file){
                        if (strstr($file,".jpg")){
                            print "<li><img src='$path$file' alt='галерея работ'/></li>";
                        }
                    }
                    ?>
                </ul>
            </div>

            <div style="height: 60px"></div>
            <button class="button centered"><a download class="color-white" href="files/instruction_laitovo.pdf">Скачать инструкцию</a></button>
        </div>
    </div>

    <div class="row">
        <div class="box1000 centered">
            <div class="inline-form text-centered">
                <h3>Если у Вас еще остались какие-либо вопросы - оставьте контакты и мы перезвоним Вам в ближайшее время</h3>

                <input class="name" type="text" placeholder="Ваше имя">
                <input class="phone" type="tel" placeholder="Ваш телефон">
                <input class="email" type="email" placeholder="Ваш e-mail">
                <div style="margin-top: 1em"><button class="button">Оставить заявку</button></div>

                <div><small>Позвоните или напишите на e-mail</small></div>
            </div>
        </div>
    </div>

<div  id="examples">
    <div class="box1000 centered">
        <h2 class="text-centered">ПРИМЕРЫ АВТОМОБИЛЕЙ С НАШИМИ АВТОШТОРКАМИ</h2>
        <div class="row">
            <div class="yoxview" id="gallery">
                <?php
                $path = 'img/examples/';
                $files = scandir($path);
                foreach ($files as $file){
                    if (strstr($file,".jpg")){
                        print "<a class='item' href='$path$file'><img class='flexible' src='$path$file' alt='галерея работ'/></a>";
                    }
                }
                ?>
                </div>
            <div class="clearfix"></div>
        </div>
    </div>
</div>

    <div class="row">
        <div class="box1000 centered">
            <div class="box33p">
                <h2 class="centered">Наш товар сертифицирован</h2>
                <a class="yoxview" href="img/sertificate.jpg"><img class="flexible" src="img/sertificate.jpg" alt="сертификат"></a>
                <div>Вся подукция Лайтово запатентована и охраняется авторским правом.</div>
            </div>
            <div class="box5p"></div>
            <div class="box60p" style="margin-top: 11em">
                <h2 class="centered text-centered">Отзывы наших клиентов</h2>

                <div id="testimonials">
                    <div class="testimonial current">Максим: Приобрел автошторки передние боковые стандарт 5% на хонду црв 2013г. Ездил с ними в Крым, реально спасали на стоянках от солнца, и тучь мошкары на паромной переправе. Качество исполнения, простота установки и снятия на отлично! Доставка выполнена быстро в упаковочной коробке+пакет! Стоит своих денег!</div>
                    <div class="testimonial">Калинина Ольга Александровна: Очень удобная вещь, если нужно надолго оставить машину, например, на время отпуска или просто на стоянке - салон не так сильно нагревается. Это очень удобно летом, очень уж неприятно бывает садиться в душный салон.</div>
                    <div class="testimonial">Фаустов Артем Валерьевич: Заказал на Honda Accord 2011 г. 8 поколение передние светлые шторки. С момента заказа и до момента получения прошло 4 дня. Пришли хорошо упакованы. Плюс заказал чехол для них. Качество супер. Молодцы так держать.</div>
                    <div class="testimonial">Ольга Вениаминовна : Приобрела комплект защитных экранов Laitovo с сумкой для хранения по Акции. Всем очень довольна. Сумка очень удобная можно убрать экраны в любое время . Удобно , практично , эстетично!)))Спасибо.</div>
                    <div class="testimonial">Елена: Новая продукция укороченный вариант мне очень понравилась. Очень хорошо видно боковое зеркало. Простота в установке, также можно использовать для курения. Защита от солнца. Продуктом очень довольна.</div>
                    <div class="testimonial">Таланов Михаил: Тонировать приницпиально не хотел. Ждал на рынке съёмную альтернативу. Наконец то дождался! Вчера привезли шторки Laitovo. Качество изготовления и материалов приятно порадовали. Заказал на заднее стекло 25% + треугольники 5% + задние двери 5%. 4 дня ожидания, далее контрольный прозвон по доставке. Заказал на парковку с установкой за 50р. Поставили за 15 мин. Снятие за несколько секунд. Выглядят на чёрной машине отлично! Ранее послушал совета не ставить на заднее 5% и не прогадал - 25% сзади мне кажется вполне достаточно, учитывая угол наклона стекла. Ни разу не колхоз, качество материалов не придерёшься. Результату очень рад!</div>
                    <div class="testimonial">Гришин Дмитрий Вячелавович: Когда дело шло к принятию закона о запрете тонировки передних стекол, я сразу стал искать в интернете альтернативу, поговаривали что будут двойные стекла , которые прячутся , или электро-тонировка , но все это стоило или заоблачно или в стадии разработки было, тогда было примерно года 4 назад. Потом я нашел сайт Автошторок Лайтово и заказал себе на Форд Фокус. Изготовили за 5 дней, супер.</div>
                    <div class="testimonial">Филиппов Андрей Валентинович: Спасибо за прекрасный и качественный товар, теперь Шевроле Каптива стала ещё уютнее и симпатичнее!!!</div>
                    <div class="testimonial">Алексей: Хорошая вещь , просьба сделать такие же только на магнитах и будет вообще идеально.</div>
                    <div class="testimonial">Данковцев Геннадий Павлович: Пользуюсь больше года, просто все отлично, спасибо большое! Шторки отличные, качественно сделаны. Рекомендую всем приобрести шторки.</div>
                    <div class="testimonial">Алексей: Заказал уже на второй автомобиль. Доволен на 1000%.</div>
                    <div class="testimonial">Виталий: Купил каркасные шторки на Дэу Матиз задний полу круг. Легко устанавливаются, при необходимости так же сегко демонтируются. Мне понравилось.</div>
                </div>
                <div class="clearfix" style="text-align: center">
                <span id="testimonials-prev" class="button"> < Предыдущий отзыв </span>
                <span id="testimonials-next" class="button"> Следующий отзыв > </span>
                    </div>
            </div>
            <div class="clearfix"></div>
        </div>
    </div>

    <div class="box1000 centered">
        <h2 class="text-centered">Наши контакты</h2>
    </div>
</div>
<div id="map_wrapper">
    <h2 style="font-style: italic; z-index: 5; position: absolute; font-size: 3.5em; left: 3%; text-shadow: 0 1px 2px white">Laitovo</h2>
    <h3  style="font-style: italic; z-index: 5; position: absolute; font-size: 1em; left: 3%; top: 5.8em; text-shadow: 0 1px 2px white">территория комфорта</h3>
    <h3 style="z-index: 5; position: absolute; font-size: 1em; left: 3%; top: 8em; text-shadow: 0 1px 2px white">г. Коломна, пр-к. Кирова, 15б</h3>
    <div style="z-index: 5; position: absolute; left: 3%; top: 11em" class="yoxview"> <a href="img/magaz.jpg"><img width="280" src="img/magaz.jpg"></a> </div>

    <button id="calculateRoute" class="button" style="z-index: 5; color: black; position: absolute; font-size: 1em; left: 20em; top: 3.7em; text-shadow: 0 1px 2px white">Построить маршрут</button>

    <div class="form simple" style="z-index: 5; position: absolute; margin-left: 2%; margin-top: 1%; right: 3%">
        <h3>Оставьте заявку на шторки для Вашего авто прямо сейчас!</h3>
        <input class="name" type="text" placeholder="Ваше имя">
        <input class="phone" type="tel" placeholder="Ваш телефон">
        <input class="mark" type="text" placeholder="Модель автомобиля">
        <input class="email" type="email" placeholder="Ваш e-mail">
        <select class="type">
            <option value="Передние шторки">Передние шторки</option>
            <option value="Задние шторки">Задние шторки</option>
            <option value="Весь комплект шторок">Весь комплект шторок</option>
        </select>
        <div><button class="button">Оставить заявку</button></div>
        <div class="response"></div>
    </div>
    <div id="map"></div>
</div>

</div>

<footer>
<div class="box5p"></div>
<div class="box20p">
   <img src="img/logo.png" class="flexible" alt="laitovo">
    <div><small>ИП Зернов Владимир Николаевич</small></div>
    <div><small>ОГРН 314 502 224 700 010</small></div>
    <div><small><a href="http://avtoshtorki-kolomna.ru">avtoshtorki-kolomna.ru</a></small></div>
</div>

<div class="box5p"></div>
<div class="box15p">
    Магазин работает с
    <div>ВТ-ПТ 10-19</div>
    <div>СБ, ВС 10-16</div>
    <div>ПН - выходной</div>
</div>
<div class="box20p text-centered">
    Сделано в <a href="http://www.snapix.ru">Snapix</a>
    <div id="counter">счетчик</div>
</div>
    <div class="box5p"></div>
    <div class="box33p text-right">
        <h2 style="padding-bottom: 0">8 (985) 486 32 22</h2>
        <h4>г. Коломна, ул. Кирова, д. 15Б</h4>
        <h5>avtoshtorkii@yandex.ru</h5>
    </div>

    <div class="clearfix"></div>
</footer>

<div id="overlay" class="hidden"></div>
<div id="modalform_call" class="form floating text-centered  hidden">
    <img class="closeForm" id="closeForm_call" src="img/Actions-file-close-icon.png" width="42">
    <h3>Закажите обратный звонок</h3>
    <input class="name" type="text" placeholder="Ваше имя">
    <input class="phone" type="tel" placeholder="Ваш телефон">
    <button class="button sender">Перезвоните мне!</button>
    <div class="responder"></div>
</div>

<div id="modalform_product" class="form floating text-centered  hidden">
    <img class="closeForm" id="closeForm_product" src="img/Actions-file-close-icon.png" width="42">
    <h3>Оставьте заявку</h3>
    <img width="150" id="product_id" alt="">
    <h4 id="selected_product"></h4>
    <hr>
    <input class="name" type="text" placeholder="Ваше имя">
    <input class="phone" type="tel" placeholder="Ваш телефон">

    <button class="button sender">Заказать!</button>
    <div class="responder"></div>
</div>

</body>
</html>