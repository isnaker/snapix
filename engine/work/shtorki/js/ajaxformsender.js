function sendAjax(action, _data, responder, sender){
    var _response;
    $.ajax({
        type: 'POST',
        url: './engine/engine.php',
        data: {
            action: action,
            message : _data
        },
        success: function(data){
            _response = 1;
            if (sender) $(responder).html('Ваша заявка принята!');
            $(sender).hide(500);
        },
        timeout: function(){
            _response = 0;
        },
        error: function(){
            _response = 0;
        }
    });
    return _response;
}

function IsEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
}

function showModalForm(form){
    $('#'+form).css('display','block');
    $('#'+form).animate({'opacity':1},600);
}

function hideModalForm(form){
    $('#'+form).animate({'opacity':'-0.5'},1000,function(){ $('#'+form).css('display','none')});
}

