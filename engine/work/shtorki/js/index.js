/**
 * Created by Alex on 15.10.14.
 */
$(function(){
    $(".yoxview").yoxview({lang: 'ru'});
    $('.flexslider').flexslider({slideshow: false});
    /*  $('header').click(function(){
     $(this).toggleClass('collapsed');
     }); */

    $('input[type="tel"]').mask("8 (999) 999-99-99");


    $(".countdown").ResponsiveCountdown({
        target_date:"2020/1/1 00:00:00",
        time_zone:3,target_future:true,
        set_id:0,pan_id:0,day_digits:3,
        fillStyleSymbol1:"rgba(255,255,255,1)",
        fillStyleSymbol2:"rgba(255,255,255,1)",
        fillStylesPanel_g1_1:"rgba(100,100,100,1)",
        fillStylesPanel_g1_2:"rgba(30,30,30,1)",
        fillStylesPanel_g2_1:"rgba(20,20,60,1)",
        fillStylesPanel_g2_2:"rgba(80,80,130,1)",
        text_color:"rgba(255,255,255,1)",
        text_glow:"rgba(0,0,0,1)",
        show_ss:true,show_mm:true,
        show_hh:true,show_dd:false,
        f_family:"Verdana",show_labels:true,
        type3d:"group",max_height:64,
        days_long:"days",days_short:"dd",
        hours_long:"часов",hours_short:"hh",
        mins_long:"минут",mins_short:"mm",
        secs_long:"секунд",secs_short:"ss",
        min_f_size:9,max_f_size:30,
        spacer:"circles",groups_spacing:0,text_blur:2,
        font_to_digit_ratio:0.125,labels_space:1.2
    });
    updateFormPosition('#modalform_call');
    $(window).resize(function(){updateFormPosition('#modalform_call');})

    // video control

    $('#video_controller .item').click(function(){
        var video = $(this).attr('data-title');
        $('#video').attr('src', video);
        $('#video_controller .current').removeClass('current');
        $(this).addClass('current');
    });


    // testimonials
    var testimonials = $('.testimonial');

        $('#testimonials-next').click(function(){
           var _currnt = $('.testimonial.current');
            $(_currnt).removeClass('current');
            if ($(_currnt).next('.testimonial').text() != ""){
                $(_currnt).next('.testimonial').addClass('current'); //.addClass('current');
            } else {
                var first = $('.testimonial')[0];
                $(first).addClass('current');
            }
        });

    $('#testimonials-prev').click(function(){
        var _currnt = $('.testimonial.current');
        $(_currnt).removeClass('current');
        if ($(_currnt).prev('.testimonial').text() != ""){
            $(_currnt).prev('.testimonial').addClass('current'); //.addClass('current');
        } else {
            var length = $('.testimonial').length;
            var first = $('.testimonial')[length-1];
            $(first).addClass('current');
        }
    });

    // buttons bindings
    $('.closeForm').click(function(){
        $('#overlay, .floating').addClass('hidden');
    });

    $('header .caller').click(function(){
        $('#modalform_call, #overlay').removeClass('hidden');
    });

    $('#calculateRoute').click(function(){ calculateRoute(); });

    // forms bindings
    $('.simple .button').click(function(){
        var message = "";
        var validated = true;
        var name = $(this).parent().parent().find('.name').val();
        var phone = $(this).parent().parent().find('.phone').val();
        var mark = $(this).parent().parent().find('.mark').val();
        var type = $(this).parent().parent().find('.type').val();
        var email = $(this).parent().parent().find('.email').val();
        var responder = $(this).parent().parent().find('.response');

        if (name == "") {validated = false}
        if (phone == "") {validated = false}
        if (mark == "") {validated = false}
        if (type == "") {validated = false}

        if (validated) {
            message =
                "Имя: <strong>" + name + "</strong><br>" +
                 "Телефон: <strong>" + phone + "</strong><br>" +
                 "Марка авто: <strong>" + mark + "</strong><br>" +
                 "Тип шторок: <strong>" + type + "</strong><br>" +
                 "E-mail: <strong>" + email + "</strong><br>" +
                    "<hr>";

           sendAjax('form',message,responder, this);
        } else {alert('Пожалуйста, заполните все поля!');}
    });

    $('#modalform_call .sender').click(function(){
        var message = "";
        var validated = true;
        var responder = $(this).parent().find('.responder');
        var name = $(this).parent().find('.name').val();
        var phone = $(this).parent().find('.phone').val();

        if (name == "") {validated = false}
        if (phone == "") {validated = false}
        if (validated) {
            message =
                "Имя: <strong>" + name + "</strong><br>" +
                    "Телефон: <strong>" + phone + "</strong><br>" +
                    "<hr>";
            sendAjax('call',message,responder, this);
        } else {alert('Пожалуйста, заполните все поля!');}
    });

    $('.inline-form .button').click(function(){
        var message = "";
        var validated = true;
        var responder = $(this).parent().find('.responder');
        var name = $(this).parent().parent().find('.name').val();
        var phone = $(this).parent().parent().find('.phone').val();
        var email = $(this).parent().parent().find('.email').val();

        if (name == "") {validated = false}
        if (phone == "") {validated = false}

        if (validated) {
            message =
                "Имя: <strong>" + name + "</strong><br>" +
                    "Телефон: <strong>" + phone + "</strong><br>" +
                    "E-mail: <strong>" + email + "</strong><br>" +
                    "<hr>";
            sendAjax('call',message,responder, this);
        } else {alert('Пожалуйста, заполните все поля!');}
    });

    $('.callbackform .button').click(function(){
        var message = "";
        var validated = true;
        var responder = $(this).parent().find('.responder');
        var model = $(this).parent().find('.model').val();
        var phone = $(this).parent().find('.phone').val();
        var type = $(this).parent().find('.type').val();

        if (model  == "") {validated = false}
        if (phone == "") {validated = false}
        if (type  == "") {validated = false}

        if (validated) {
            message =
                    "Модель: <strong>" + model + "</strong><br>" +
                    "Телефон: <strong>" + phone + "</strong><br>" +
                    "Тип шторок: <strong>" + type + "</strong><br>" +
                    "<hr>";
            sendAjax('form',message,responder, this);
        } else {alert('Пожалуйста, заполните все поля!');}
    });

    $('#catalog .item .button').click(function(){
        var productName = $(this).parent().find('h3').html();
        var img = $(this).parent().find('img').attr('src');

        $('#overlay').removeClass('hidden');
        $('#modalform_product').removeClass('hidden');
        $('#modalform_product .button').show();
        $('#modalform_product input').val("");
        $('#selected_product').html(productName);
        $('#product_id').attr('src', img);
        updateFormPosition('#modalform_product');
        $(window).resize(function(){updateFormPosition('#modalform_product');});
    });

    $('#modalform_product .button').click(function(){
        var message = "";
        var validated = true;
        var responder = $(this).parent().find('.responder');
        var name = $(this).parent().find('.name').val();
        var phone = $(this).parent().find('.phone').val();
        var product = $(this).parent().find('#selected_product').html();

        if (name  == "") {validated = false}
        if (phone == "") {validated = false}

        if (validated) {
            message =
                "Имя: <strong>" + name + "</strong><br>" +
                    "Телефон: <strong>" + phone + "</strong><br>" +
                    "Продукт: <strong>" + product + "</strong><br>" +
                    "<hr>";
            sendAjax('product',message,responder, this);
        } else {alert('Пожалуйста, заполните все поля!');}
    });
});

function updateFormPosition(formId){
    var width = $(window).width();
    var height = $(window).height();
    var leftOffset = ( width / 2 - ( $(formId).width() / 2) );
    var topOffset = ( height / 2 - ( $(formId).height() / 2) );
    $(''+formId).css('left', leftOffset+"px").css('top', topOffset+"px");
}

// google maps
var map;
var coords = new google.maps.LatLng(55.085654, 38.765603);
var userPos;
var directionsDisplay;
var directionsService = new google.maps.DirectionsService();

function initialize() {
    directionsDisplay = new google.maps.DirectionsRenderer();
    var mapOptions = {
        zoom: 18,
        center: coords ,
        disableDefaultUI: true
    };
    map = new google.maps.Map(document.getElementById('map'),
        mapOptions);
    directionsDisplay.setMap(map);

    var marker = new google.maps.Marker({
        position: coords,
        map: map,
        icon: 'img/germesPos.png',
        title: 'Laitovo'
    });

    var existmarker = new google.maps.Marker({
        map: map,
        position: new google.maps.LatLng(55.086007, 38.765741),
        icon: 'img/existPos.png',
        zIndex: google.maps.Marker.MAX_ZINDEX + 1
    });

    if(navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(function(position) {
            userPos = new google.maps.LatLng(position.coords.latitude,
                position.coords.longitude);

            var marker = new google.maps.Marker({
                map: map,
                position: userPos,
                icon: 'img/userPos.png',
                zIndex: google.maps.Marker.MAX_ZINDEX + 1
            });

        }, function() {
            handleNoGeolocation(true);
        });
    } else {
        // Browser doesn't support Geolocation
        handleNoGeolocation(false);
    }
}

function calculateRoute(){
    var start = userPos;
    var end = coords;

    var request = {
        origin:start,
        destination:end,
        travelMode: google.maps.DirectionsTravelMode.DRIVING
    };
    directionsService.route(request, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response);
        }
    });
}

google.maps.event.addDomListener(window, 'load', initialize);