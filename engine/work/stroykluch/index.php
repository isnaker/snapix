<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
    <meta name="viewport" content="width=1024"/>
    <meta http-equiv="Content-type" content="text/html;charset=UTF-8"/>
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta name="robots" content="all" />
    <style type="text/css">
        button:hover {
            box-shadow: 0 1px 15px black;
            cursor: pointer;
        }
        body {
            margin: 0;
        }
        #site-content {
            width: 1024px;
            margin: 0 auto;
        }
        #line-1 {
            background: url("images/line_1.jpg");
            background-size: contain;
            height: 894px;
        }

        #head_1, #logo {
            position: relative;
            float: left;
        }

        #head_1 {
            margin-left: 190px;
            margin-top: 10px;
        }

        #logo {
            margin-left: 15px;
            box-shadow: 0 4px 10px rgba(0,0,0,0.7);
        }

        #form_line_1 {
            background: url('images/form_line_1.png');
            height: 111px;
        }

        #form_line_1_name {
            position: relative;
            margin-top: 50px;
            margin-left: 170px;
            width: 180px;
        }

        #form_line_1_phone {
            margin-left: 75px;
            width: 180px;
        }

        #form_line_1_submit {
            background: url("images/button_1.png") no-repeat;
            width: 226px;
            height: 49px;
            border: none;
            margin-top: 35px;
            margin-right: 140px;
            float: right;
        }

        input[type="text"] {
            border: none;
            font-size: 16px;
        }

        #line-2 {
            background: url('images/line_2.jpg') no-repeat;
            height: 403px;
        }
        #line-3 {
            background: url('images/line_3.jpg') no-repeat;
            height: 732px;
        }
        #form-3 {
            background: url("images/form_3.png") no-repeat;
            width: 515px;
            height: 294px;
            position: absolute;
            margin-top: 420px;
            margin-left: 270px;
        }

        #form_3_name {
            position: absolute;
            bottom: 149px;
            left: 295px;
        }

        #form_3_phone {
            position: absolute;
            bottom: 101px;
            left: 295px;
        }

        #form_3_submit {
            background: url("images/button_2.png") no-repeat;
            width: 226px;
            height: 49px;
            border: none;
            position: absolute;
            bottom: 28px;
            left: 265px;
        }
    </style>
</head>

<body>
<div id="site-content">
    <div id="line-1">
        <img id="logo" src="images/logo.jpg">
        <img id="head_1" src="images/head_1.png" alt="">
    </div>
    <div id="form_line_1">
        <input id="form_line_1_name" type="text" placeholder="Ваше имя">
        <input id="form_line_1_phone" type="text" placeholder="Ваш телефон">
        <button id="form_line_1_submit"></button>
    </div>
    <div id="line-2"></div>
    <div id="line-3">
        <div id="form-3">
            <input id="form_3_name" type="text" placeholder="Ваше имя">
            <input id="form_3_phone" type="text" placeholder="Ваш телефон">
            <button id="form_3_submit"></button>
        </div>
    </div>
</div>

</body>
</html>
