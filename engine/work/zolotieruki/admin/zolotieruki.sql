-- phpMyAdmin SQL Dump
-- version 3.5.1
-- http://www.phpmyadmin.net
--
-- Хост: 127.0.0.1
-- Время создания: Июн 17 2013 г., 08:54
-- Версия сервера: 5.5.25
-- Версия PHP: 5.2.12

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- База данных: `zolotieruki`
--

-- --------------------------------------------------------

--
-- Структура таблицы `client_orders`
--

CREATE TABLE IF NOT EXISTS `client_orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` int(11) NOT NULL COMMENT 'номер заказа',
  `service` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'текст услуги',
  `count` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'количество услуг',
  `price` int(11) NOT NULL COMMENT 'цена за 1 услугу',
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci COMMENT='информация о заказах' AUTO_INCREMENT=16 ;

--
-- Дамп данных таблицы `client_orders`
--

INSERT INTO `client_orders` (`id`, `order_number`, `service`, `count`, `price`) VALUES
(1, 6, 'as', 1, 250),
(11, 32, 'РњРѕРЅС‚Р°Р¶ СЃРІРµС‚РёР»СЊРЅРёРєРѕРІ РїРѕС‚РѕР»РѕС‡РЅС‹С…, РЅР°СЃС‚РµРЅРЅС‹С… РІ СЂРµРµС‡РЅРѕРј(РїРѕРґРІРµСЃРЅРѕРј) РїРѕС‚РѕР»РєРµ, Р±СЂР°', 1, 350),
(8, 31, 'Р’СЂРµР·РєР° Р·Р°РјРєР° РІ РґРµСЂРµРІСЏРЅРЅСѓСЋ РґРІРµСЂСЊ', 4, 700),
(9, 31, 'Р—Р°РјРµРЅР° Р»РёС‡РёРЅРєРё Р·Р°РјРєР°', 3, 700),
(10, 31, 'Р РµРјРѕРЅС‚,Р·Р°РјРµРЅР° Р·Р°РјРєР°,Р»РёС‡РёРЅРєРё', 1, 500),
(12, 32, 'Р”РµРјРѕРЅС‚Р°Р¶ Р»СЋСЃС‚СЂ,Р±СЂР°,СЃРІРµС‚РёР»СЊРЅРёРєРѕРІ', 1, 250),
(13, 32, 'Р”РµРјРѕРЅС‚Р°Р¶ РјРѕР№РєРё,СЃРјРµСЃРёС‚РµР»СЏ,РґСѓС€РµРІРѕРіРѕ С€Р»Р°РЅРіР°', 1, 250),
(14, 33, 'Р”РµРјРѕРЅС‚Р°Р¶ РјРѕР№РєРё,СЃРјРµСЃРёС‚РµР»СЏ,РґСѓС€РµРІРѕРіРѕ С€Р»Р°РЅРіР°', 1, 250),
(15, 33, 'РЈСЃС‚Р°РЅРѕРІРєР° РјРѕР№РєРё, С‚СѓРјР±С‹ РїРѕРґ РјРѕР№РєСѓ', 1, 1000);

-- --------------------------------------------------------

--
-- Структура таблицы `group_services`
--

CREATE TABLE IF NOT EXISTS `group_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

--
-- Дамп данных таблицы `group_services`
--

INSERT INTO `group_services` (`id`, `name`) VALUES
(1, 'САНТЕХНИКА'),
(2, 'ЭЛЕКТРИКА'),
(3, 'УСЛУГИ ПЛОТНИКА');

-- --------------------------------------------------------

--
-- Структура таблицы `orders`
--

CREATE TABLE IF NOT EXISTS `orders` (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'номер заявки',
  `date_created` datetime NOT NULL COMMENT 'дата и время создания заявки',
  `date_order` datetime NOT NULL COMMENT 'на какую дату и время заявка',
  `address` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'адрес',
  `client_name` varchar(80) COLLATE utf8_unicode_ci NOT NULL COMMENT 'Имя и фамилия заказчика',
  `client_order` int(11) NOT NULL COMMENT 'текст заявки',
  `order_status` set('New','Cancelled','Done','Active') COLLATE utf8_unicode_ci NOT NULL COMMENT 'статус заявки',
  `express_order` int(1) NOT NULL COMMENT 'срочный заказ',
  `client_status` set('Normal','Loyal','Blacklist','') COLLATE utf8_unicode_ci NOT NULL COMMENT 'статус клиента в системе',
  `comment` text COLLATE utf8_unicode_ci NOT NULL COMMENT 'доп. информация',
  `approved` bit(1) NOT NULL DEFAULT b'0',
  `client_phone` text COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci AUTO_INCREMENT=34 ;

--
-- Дамп данных таблицы `orders`
--

INSERT INTO `orders` (`id`, `date_created`, `date_order`, `address`, `client_name`, `client_order`, `order_status`, `express_order`, `client_status`, `comment`, `approved`, `client_phone`) VALUES
(6, '0000-00-00 00:00:00', '0000-00-00 00:00:00', 'РќРёРєРѕР»Р°РµРІСЃРєРѕРµ РїРѕР»Рµ ', 'РџРµРІРµР» Р’РµС‚СЂРѕРІ2', 0, 'New', 1, 'Loyal', 'Р’СЃРµРіРґР° РјРµС‡С‚Р°Р» Р¶РёС‚СЊ!', b'1', '8 (916) 159 78 98'),
(5, '2030-10-20 13:00:00', '0000-00-00 00:00:00', 'РљР°Р»РёРЅРµРЅРіСЂР°Рґ', 'Р’РµРґСЊРјРёРЅ Р‘СЂР°С‚', 0, 'New', 0, 'Normal', 'РќРµС‚ Р»СѓС‡С€Рµ РјРµСЃС‚Р° С‡РµРј СЌС‚Р° СЃС‚СЂРѕРєР°)', b'0', '8(345)54 565 44'),
(7, '2013-06-09 00:00:00', '0000-00-00 00:00:00', 'Рі. РљРѕР»РѕРјРЅР°, СѓР». Р—Р°Р№С†РµРІР°, Рґ. 18', 'РЎРµСЂРіРµР№ РџРѕС‚Р°РїРѕРІ', 0, 'New', 1, 'Normal', 'РўСЂР°РЅСЃРїРѕСЂС‚РЅР°СЏ РєРѕРјРїР°РЅРёСЏ В«Р“РёРіР°РЅС‚В» СЂР°СЃРїРѕР»Р°РіР°РµС‚ Р±РѕР»СЊС€РёРј РїР°СЂРєРѕРј СЃРїРµС†РёР°Р»РёР·РёСЂРѕРІР°РЅРЅРѕР№ С‚РµС…РЅРёРєРё РґР»СЏ РІС‹РїРѕР»РЅРµРЅРёСЏ СЃР°РјС‹С… СЂР°Р·Р»РёС‡РЅС‹С… РїСЂРѕРёР·РІРѕРґСЃС‚РІРµРЅРЅС‹С…, СЃС‚СЂРѕРёС‚РµР»СЊРЅС‹С… Рё РґСЂСѓРіРёС… Р·Р°РґР°С‡.', b'0', '79852502232'),
(8, '2013-06-10 16:31:03', '0000-00-00 00:00:00', 'Рі. Р›СЋР±РµСЂС†С‹, РїСЂ-С‚ РџРѕР±РµРґС‹, Рґ. 18 РєРІ. 210', 'РђРЅРґСЂРµР№', 0, 'Cancelled', 0, 'Blacklist', 'yjwtdmdg,mfhj,jh,', b'0', '89164482756'),
(9, '2013-06-10 16:32:44', '0000-00-00 00:00:00', 'Рі. РњРѕСЃРєРІР°, СѓР». РљСЂР°СЃРЅРѕР±РѕРіР°С‚С‹СЂСЃРєР°СЏ, Рґ. 3', 'РўРµСЃС‚РѕРІ РўРµСЃС‚ РўР•СЃС‚РѕРІРёС‡', 0, 'New', 0, 'Normal', '11111111111111111111111111', b'0', ''),
(32, '2013-06-22 23:04:00', '2013-06-17 07:36:19', 'РђР»РёРєСЃР°РЅРґСЂРёС‚', 'РџСЂРёРІРµР» Р’Р°СЃ', 0, 'New', 0, 'Normal', 'РІР°РІС‹Р°РІС‹РІР°С‹РІР°С‹РІ', b'0', '4343434'),
(31, '2013-06-23 23:57:00', '2013-06-16 16:13:40', 'as', 'as', 0, 'New', 0, 'Normal', 'asdasd', b'0', '8999'),
(33, '0000-00-00 00:00:00', '2013-06-17 07:37:30', 'С„С‹РІС„', 'С„С‹', 0, 'New', 0, 'Normal', 'С„С‹РІ', b'0', '434');

-- --------------------------------------------------------

--
-- Структура таблицы `services`
--

CREATE TABLE IF NOT EXISTS `services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `price` int(11) NOT NULL,
  `group` text CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Дамп данных таблицы `services`
--

INSERT INTO `services` (`id`, `service`, `price`, `group`) VALUES
(1, 'Монтаж светильников потолочных, настенных в реечном(подвесном) потолке, бра', 350, 'ЭЛЕКТРИКА'),
(2, 'Демонтаж люстр,бра,светильников', 250, 'ЭЛЕКТРИКА'),
(3, 'Установка люстры простой на крюк, -евро', 1000, 'ЭЛЕКТРИКА'),
(4, 'Демонтаж мойки,смесителя,душевого шланга', 250, 'САНТЕХНИКА'),
(5, 'Установка мойки, тумбы под мойку', 1000, 'САНТЕХНИКА'),
(6, 'Врезка замка в деревянную дверь', 700, 'УСЛУГИ ПЛОТНИКА'),
(7, 'Замена личинки замка', 700, 'УСЛУГИ ПЛОТНИКА'),
(8, 'Ремонт,замена замка,личинки', 500, 'УСЛУГИ ПЛОТНИКА'),
(9, 'Демонтаж замка,личинки', 250, 'УСЛУГИ ПЛОТНИКА');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
