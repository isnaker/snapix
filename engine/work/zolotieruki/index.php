<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
	<meta http-equiv="content-type" content="text/html" />

	<title>Золотые руки</title>
    
    <style type="text/css">
    @import url(http://fonts.googleapis.com/css?family=Istok+Web:400,700&subset=latin,cyrillic);
    a {text-decoration: none; color: white;}
    
    body {
        margin: 0; 
        font-family: 'Istok Web', sans-serif; 
        min-width: 1280px;
        
        }
    
    .place {
        position: relative; 
        width: 100%; 
        min-height: 800px;
        height: 80%;
        display: block; 
        z-index: 2; 
        box-shadow: 0 2px 25px black; 
        background: url('img/place2.jpg') no-repeat;
        background-position: center center;
        }
    .bottom {position: absolute; height: 20%; width: 100%; display: block; bottom: 0; z-index: 1;}
    .man {
        position: absolute; 
        display: inline-block; 
        margin-top: -630px; 
        left: 15%;  
        z-index: 3; 
        -webkit-transform-style: preserve-3d;
        -moz-transform-style: preserve-3d;
        -o-transform-style: preserve-3d;
        -webkit-perspective: 1000; 
    	-moz-perspective: 1000; 
    	-o-perspective: 1000;
    }
    
    .content {width: 800px; margin:  0 auto;  min-height: 800px;}
    .flag {height: 400px; width: 150px; background: url('img/flag2.png') no-repeat; float: right; margin-right: -100px;}
    .flag a { 
        display: block; 
        text-align: center; 
        font-size: 0.8em;
        margin: 5px; 
        text-shadow: 0 1px 1px black;
        text-transform: uppercase;
         }
    .wrapper {display: block; height: 30px; width: 100%; position: relative;}
    
    .intro {}
    
    .intro-text {width: 55%; text-align: right; font-style: italic; margin-right: 3%; text-shadow: 0 1px 2px white}
    
    .intro p {display: inline-block;}
    
    .intro .help {

        font-weight: bold; 
        font-size: 1.2em; 
        text-transform: uppercase;
        text-shadow: 0 1px 2px white;
        line-height: 1.1;
        }
    .intro .help strong {display: block; font-size: 1.5em;}
    
    .marquee, .description, .call-onphone {
        position: relative;
        display: block;
        width: 50%;
        margin: 5% 25%;
        z-index: 10;
        background: -moz-radial-gradient(center, ellipse cover,  rgba(255,255,255,0.9) 0%, rgba(255,255,255,0.85) 100%); /* FF3.6+ */
        background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(255,255,255,0.9)), color-stop(100%,rgba(255,255,255,0.85))); /* Chrome,Safari4+ */
        background: -webkit-radial-gradient(center, ellipse cover,  rgba(255,255,255,0.9) 0%,rgba(255,255,255,0.85) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-radial-gradient(center, ellipse cover,  rgba(255,255,255,0.9) 0%,rgba(255,255,255,0.85) 100%); /* Opera 12+ */
        background: -ms-radial-gradient(center, ellipse cover,  rgba(255,255,255,0.9) 0%,rgba(255,255,255,0.85) 100%); /* IE10+ */
        background: radial-gradient(ellipse at center,  rgba(255,255,255,0.9) 0%,rgba(255,255,255,0.85) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#e6ffffff', endColorstr='#d9ffffff',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
}

    .marquee {
        
transform: skew(2deg,-2deg);
-webkit-transform: skew(2deg,-2deg);
-moz-transform: skew(5deg,0deg);
-o-transform: skew(5deg,0deg);
-ms-transform: skew(5deg,0deg);
        
    }

.service {
    padding: 5%; 
    position: relative; 
    display: block; 
    text-align: center;
    }
    
    .service-head { font-size: 34px; }
    
    .service-description {
        position: relative;
        background: white; 
        padding: 2px 12px; 
        display: inline-block;
        margin: 0px auto;
        box-shadow: 0 1px 2px rgba(0,0,0,0.66);
         }
    
    .description {
        padding: 15px 0;
        z-index: 100;
        }
    
    .order-button {
        background: url('img/button2.png') no-repeat;
        padding: 20px 0;
        width: 350px;
        text-align: center;
        margin: 0 auto;
        color: white;
        font-size: 1.2em;
        font-weight: bold;
        text-shadow: 0 1px 2px black;
    }
    
    .order-button:hover { 
        background: url('img/button3.png') no-repeat; 
        cursor: pointer; }
        
    .call-onphone {text-align: center; background: none; color: white}
    
    .call-onphone p {
        margin: 0;
        padding: 2px 8px 0 8px;
        position: relative; 
        display: inline-block;
        background: -moz-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%, rgba(0,0,0,0.75) 100%); /* FF3.6+ */
        background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(0,0,0,0.75)), color-stop(100%,rgba(0,0,0,0.75))); /* Chrome,Safari4+ */
        background: -webkit-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* Chrome10+,Safari5.1+ */
        background: -o-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* Opera 12+ */
        background: -ms-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* IE10+ */
        background: radial-gradient(ellipse at center,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* W3C */
        filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bf000000', endColorstr='#bf000000',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */
        }
    .phone {
        padding: 5px 8px;
        width: 70%;
        display: block;
        margin: 0 auto;
        font-size: 2em; 
        font-weight: bold;
        background: -moz-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%, rgba(0,0,0,0.75) 100%); /* FF3.6+ */
background: -webkit-gradient(radial, center center, 0px, center center, 100%, color-stop(0%,rgba(0,0,0,0.75)), color-stop(100%,rgba(0,0,0,0.75))); /* Chrome,Safari4+ */
background: -webkit-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* Chrome10+,Safari5.1+ */
background: -o-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* Opera 12+ */
background: -ms-radial-gradient(center, ellipse cover,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* IE10+ */
background: radial-gradient(ellipse at center,  rgba(0,0,0,0.75) 0%,rgba(0,0,0,0.75) 100%); /* W3C */
filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#bf000000', endColorstr='#bf000000',GradientType=1 ); /* IE6-9 fallback on horizontal gradient */

        }
        
        
        .footer {
            position: relative; 
            bottom: 0; 
            z-index: 5; 
            margin: 10px auto; 
            display: block; 
            color: gold;
            font-size: 1.1em;
            text-shadow: 0 1px 2px black;
            text-align: center;
            }
        .footer span {color: white}
        
        /*  CALC  */
        
        #calc {
            background: white;
            width: 500px;
            height: 650px;
            display: none;
            position: relative;
            float: left;
            margin-top: 100px;
            margin-right:  -500px;
            opacity: 0;
        }
        
        #hidecalc {
            position: absolute;
            background: white;
            left: -100px;
            top: -35px;
            padding: 3px 6px;
            cursor: pointer;
        }
        #calc h1 {
            position: absolute;
            padding: 3px 6px;
            top: -75px;
            left: -8px;
        }
        .FormServices {
            margin: 5px;
            width: 490px;
            height: 240px;
            background: #dddddd;
            overflow-y: scroll;
        }
        .ListGroupServices div {
            text-align: center;
            width: 31%;
            margin: 1%;
            margin-right: 5px;
            display: inline-block;
        }
        #checksum {
            position: absolute; 
            background: #00CE34; 
            color: white; 
            text-align: center; 
            min-width: 150px; 
            display: none; 
            padding: 1px 8px 0 8px; 
            top: -55px; 
            right: -20px; 
            font-size: 1.7em; 
            text-shadow: 0 1px 2px black;
            box-shadow: 0 1px 15px black inset;
            border-radius: 4px;
             }
        .service-item {font-size: 0.8em; padding: 3px 2%; text-align: left; display: block; width: 96%; margin-bottom: 2px; position: relative; float: left; }
        .service-item:hover {background: #E1E1E1; cursor: pointer; text-shadow: 0 1px 1px white;}
        
        .service-item-description {width: 78%; display: inline-block; float: left;}
        .service-price {float: left; width: 21%;}
        
        .service-selected-item { font-size: 0.8em; float: left; width: 100%; }
        .service-selected-item:hover {background: white; cursor: pointer;}
        .plusOne:hover, .minusOne:hover, .removeLine:hover {color: red;}
        
        .service-selected-item .service-item-description {width: 65%;}
        .incDecButtons {width: 7%; display: inline-block; text-align: center;  float: left;}
        .removeLine {float: right; font-size: 1.5em; width: 5%; text-align: center;}
        #showClientForm {
            position: absolute;
            right: -180px;
            background: white;
            display: none;
            padding: 4px 6px;
            cursor: pointer;
        }
        #ClientForm {
            position: absolute;
            left: 520px;
            width: 300px;
            margin-top: -230px;
            background: #eeeeee;
            z-index: 10;
            padding: 15px;
            box-shadow: 1px 1px 15px black;
            display: none;
            opacity: 0;
        }
        #ClientForm div {
            display: block;
        }
        label {
            display: inline-block;
            width: 150px;
        }
        
        input {width: 96%; padding: 5px; background: white; border-collapse: collapse; border: silver solid 1px; border-radius: 4px; }
        
        #SubmitButton {
            background: red;
            padding: 4px 7px;
            text-shadow: 0px 1px 1px black;
            text-align: center;
            color: white;
            width: 40%;
            margin: 25px auto 0 auto;
            box-shadow: 0 1px 5px black;
            border: white solid 2px;
            border-radius: 4px; 
        }
    </style>
    <script src="jQuery.js" type="text/javascript"></script>
    <script type="text/javascript">
            var Services = [];
            var GroupServices = [];
            
            var Order = [];
            //
            function getServices(){
    
                var request = {
                    request: 'get_all_services'
                    //id: orderId
                };
            
                request = JSON.stringify(request);
            
                var xmlhttp;
                if (window.XMLHttpRequest){
                    xmlhttp = new XMLHttpRequest();
                }
                else{
                    xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
                }
            
                xmlhttp.onreadystatechange = function()
                {
            
                    if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
                    {
                        var resp = xmlhttp.responseText;
                        resp = JSON.parse(resp);
                        
                        if (resp.response === 'OK') {
                            Services = [];
                            Services.length = 0;
                            GroupServices = [];
                            try {
                                Services = resp.data.services;
                                GroupServices = resp.data.group_services;
                                ShowForm();
                            } catch (e) {
                                console.log('orderList Parsing Error', e);
                            }
                            
                            console.log(Services);
                            console.log(GroupServices);
                            
                            
                        } else {
                            alert('Неизвестная ошибка');
                        }
                    }
                };
                xmlhttp.open("POST", "commands.php", true);
                xmlhttp.send(request);
    
            }
            
            function ShowForm(){
                Order.length = 0;
                $(".FormServices, .ListGroupServices, .ListServices").html("");
                $("input").val("");
                $("#checksum").html("");
                $("#checksum, #showClientForm, #ClientForm").css("display","none");
                $("#maintext").hide(500);
                $("#calc").css({"display": "block"});
                $("#calc").animate({"margin-left":  "210px","opacity": "1"},800);
                buildCalc();
                
            }
            function HideForm(){
                $("#calc").animate({"margin-left":  "0px","opacity": "0"},800,function(){
                    $("#maintext").show(500);
                    $("#calc").css({"display": "none"});
                });
            }
            
            function buildCalc(){
                for (i=0;i<GroupServices.length;i++){
                    $(".ListGroupServices").append("<div>"+GroupServices[i].name+"</div>");
                }
                
                for (i=0; i<Services.length; i++){
                    
                    line = '<div class="service-item" onclick="addServiceToCart(' + i + ');"><div class="service-item-description">' + (parseInt(i)+1)  +'. ' + Services[i].service + '</div> <span class="service-price"> Цена:  ' + Services[i].price + ' руб. </span></div>';
                    
                    $('.ListServices').append(line);
                    line = '';
                }  
            }
            
            function addServiceToCart(id){
                // var foundCollation = 0;
                //проверка, нет ли в списке совпадений
                //for (var i=0; i<Order.length; i++ ){
                //   if (Order[i].id == id) {foundCollation++; }
                //}
                    $("#checksum, #showClientForm").css("display","inline-block");
                var OrderToAdd = {
                    id: Services[id].id,
                    service: Services[id].service,
                    price: Services[id].price,
                    count: 1
                };
                
                
                
                Order.push(OrderToAdd);
                //Order[Order.length]['count'] = '1';
                updateOrdersList();
               
            }
            
            function updateOrdersList(){
                var summ = 0;
                
                 $('.FormServices').html('');
                
                for (var i=0; i<Order.length; i++ ){
                    
                    summ += parseInt(Order[i].price);
                    line = '<div class="service-selected-item"><div class="service-item-description">' + (parseInt(i)+1)  +'. ' + Order[i].service + '</div> <div class="incDecButtons"> <span class="minusOne" onclick="decCount(' + i + ');"> - </span>' + Order[i].count +  '<span class="plusOne" onclick="incCount(' + i +
                       ');"> + </span> </div>  <span class="service-price"> Цена:  ' + Order[i].price + ' руб. </span> <div class="removeLine" onclick="removeService(' + i + ');"> x </div></div>';
                    $('.FormServices').append(line);
                    line='';
                    console.log(Order[i]);
                }
                                 
                $('#checksum').text(summ);
            }
            function showClientForm(){
                $("#ClientForm").css("display","block");
                $("#calc").animate({"margin-left":  "-100px"},800);
                    $("#ClientForm").animate({"opacity": "1"},800);
                
            }
            
            function decCount(id) {
                
            }
            function incCount(id) {
                
            }
            
            function submitOrder(){
                //alert(document.getElementById('client_name').value);
                  var request = {
        request: 'add_new_order',
        addOrderData: {
            client_name   : document.getElementById('client_name').value,
            address       : document.getElementById('address').value,
            order_status  : 'new',
            client_status : 'normal',
            express_order : '0',
            client_phone  : document.getElementById('client_phone').value,
            date_order    : new Date(),
            comment       : ''      
        }
    };

    request = JSON.stringify(request);
    
    //alert(request);
    
    var xmlhttp;
    if (window.XMLHttpRequest){
        xmlhttp = new XMLHttpRequest();
    }
    else{
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
    }

    xmlhttp.onreadystatechange = function()
    {

        if (xmlhttp.readyState == 4 && xmlhttp.status == 200)
        {
            var resp = xmlhttp.responseText;
            resp = JSON.parse(resp);

            if (resp.response === 'OK') {
  
                try {
                    
                } catch (e) {
                    console.log('orderList Parsing Error', e);
                }
            } else {
                alert('Неизвестная ошибка');
            }
        }
    };
    xmlhttp.open("POST", "commands.php", true);
    xmlhttp.send(request);
                
                
            }
            
    </script>
</head>
    
<body>

<div class="place">

<div class="content">

<div id="maintext">

<div class="flag">
    <img src="img/logo.png" style="width:80%; margin: 20% 10%;"  />

    <a href="#">Сделать заказ</a>
    <a href="#">Наша команда</a>
    <a href="#">Контакты</a>
</div>

<div class="wrapper"> </div>

<div class="intro"><p class="intro-text">если где-то человек увяз в быту...</p><p class="help"><strong>СКОРАЯ</strong> БЫТОВАЯ ПОМОЩЬ</p></div>

<div class="marquee">
    <div class="service">
        <div class="service-head"><small>Сервис</small> <b style="display: block;  margin-top: -15px; ">"Мастер на час"</b></div>
        <div class="service-description">электрик, сантехник, плотник...</div>
    </div>
</div>

<div class="description">
<img src="img/call-master.png" style="position: absolute; right: 0; margin-right: -100px; margin-top: -80px; width: 150px;" />
<ul>
    <li>Повесить люстру, картину</li>
    <li>установить розетку, выключатель</li>
    <li>Повесить люстру, картину</li>
        <li>Повесить люстру, картину</li>
            <li>Повесить люстру, картину</li>
</ul>

</div>

<div class="order-button" onclick="getServices();">Вызвать мастера онлайн</div>
<div class="call-onphone"><p>или по телефону</p> <div class="phone">8 (916) 208 31 77</div></div>
</div>


<!----- Калькулятор -->
<div id="calc" >
    <div id="checksum">0</div>
    <div id="showClientForm" onclick="showClientForm();"> ОФОРМИТЬ ЗАКАЗ</div>
    <div id="hidecalc" onclick="HideForm()">Закрыть</div>
    <h1>Калькулятор услуг</h1>
    <div class="FormServices"></div>
    <div class="ListGroupServices"></div>
    
    <div class="ListServices">
    </div>
    <div id="ClientForm">
        <div><label>Ваше имя : </label><input id="client_name" type="text" /></div>
        <div><label>Ваш адрес : </label><input id="address" type="text" /></div>
        <div><label>Ваш тел.: </label><input id="client_phone" type="text" /></div>
        <div><div id="SubmitButton" onclick="submitOrder();">Отправить</div></div>
    </div>
</div>
</div>
</div>

<img class="bottom" src="img/bottom.jpg" />
<img id="man" class="man" src="img/man1.png" />

<div class="footer">E-mail: <span>mail@zolotieruki.com</span></div>
</body>


</html>