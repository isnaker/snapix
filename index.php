<?php
/**
 * Created by PhpStorm.
 * User: Denis
 * Date: 04.07.2016
 * Time: 15:09
 */


include_once($_SERVER['DOCUMENT_ROOT']."/engine/classes/App.php");

$app = new App();

if (isset($_GET['path'])){
    switch ($_GET['path']){
        case "seo":
            $title = "Заказать seo продвижение сайта в топ 10. Seo оптимизация сайтов.";
            $keywords = "Заказать seo продвижение сайтов в топ 10 вебстудия создать сайт цена продвижение топ бизнеса комплексное seo реклама маркетинг яндекс";
            $description = "Заказать seo продвижение сайта в топ 10. Seo оптимизация сайта - привлечение потенциальных клиентов из интернета при помощи поисковых систем.";
            $includable = array("/engine/pages/seo.php");
            $add_css = array("/css/fixed_header.css","/css/page-seo.css", "https://cdn.callbackkiller.com/widget/cbk.css");
            $add_js = array("/js/page-seo.js");
            break;
        case "shop":
            $keywords = "создать интернет магазин под ключ создание разработка магазина цена сделать заказать с нуля";
            $description = "Создать интернет магазин под ключ, от профессионалов! Получите мощный интернет магазин для вашего бизнеса! Оставьте заявку или звоните по тел +7(929) 525-80-45 |Snapix.ru";
            $title = "Создать интернет магазин под ключ в Москве";
            $includable = array("/engine/pages/shop.php");
            $add_css = array("/css/fixed_header.css","/css/page-shop.css");
            break;
        case "404":
            $title = "404";
            $includable = array("/engine/pages/404.php");
            $add_css = array("/css/fixed_header.css");
            break;
        case "works":
            $title = "Наши работы. Примеры работ. Портфолио сайтов. Агентство Snapix.";
            $keywords = "примеры работ snapix";
            $description = "Примеры работ. Портфолио сайтов.";
            $categories = $app->db->get("*","projects_categories","WHERE `hidden` = '0'");
            $projects = $app->db->get("*",'projects',"ORDER BY `deployed` DESC");
            foreach ($projects as $project){
                $name = $app->db->get('name','projects_categories', "WHERE `id` = '$project->category_id' LIMIT 1");
                $project->category_name =$name->name;
            }
            $add_css = array("/css/fixed_header.css","/css/page-works.css");
            $includable = array("/engine/pages/portfolio.php");
            break;
        case "project":
            $project = $app->db->get("*, DATE_FORMAT(`deployed`, '%m.%Y') as `deployed`", "projects", " WHERE `id`='".$_GET['id']."' LIMIT 1");

            $title = $project->seo_title;
            $keywords = $project->seo_keywords;
            $description =  htmlspecialchars($project->seo_description);

            if ($project->seo_h1 == "") {$project->seo_h1 = $project->name; }

            $add_css = array("/css/fixed_header.css");
              $includable = array("/engine/pages/project.php");
            break;
        case "landing":
            $title = "Заказать лендинг пейдж (landing page) под ключ в Москве";
            $keywords = "заказать лендинг пейдж landing page создание разработка создать купить одностраничник посадочная страница лендинга сделать";
            $description = "Заказать лендинг пейдж (landing page) под ключ за 7 дней от профессионалов! Получите мощный инструмент для привлечения клиентов в Ваш бизнес! Оставьте заявку или звоните по тел: +7 (929) 525-80-45";
            $add_css = array("/css/fixed_header.css");
            $includable = array("/engine/pages/landing.php");
            break;
        case "support":
            $title = "Поддержка сайтов в Москве – техническая поддержка сайта и обслуживание интернет магазинов";
            $keywords = "поддержка сайта, поддержка сайтов москва, техническая поддержка сайта, сопровождение сайтов, абонентское обслуживание сайта, техподдержка веб-сайта, информационная поддержка, дизайн поддержка, обслуживание сайта, ведение сайта, администрирование сайта компании";
            $description = "Техническая поддержка сайта под руководством профессионалов обеспечит постоянную доступность и слаженную работу ресурса. Обращайтесь к профессионалам с опытом работы от 5 лет. Оставьте заявку или звоните по тел: +7 (929) 525-80-45";

            $add_css = array("/css/fixed_header.css","/css/page-support.css");
            $includable = array("/engine/pages/support.php");
            break;
        case "context":
            $title = "Заказать Яндекс Директ - настроить контекстную рекламу у профессионалов.";
            $keywords = "Заказать Яндекс Директ настроить настройка стоимость цена бесплатный аудит рекламные кампании купить от профессионалов создание рекламных кампаний контекстная реклама настроить";
            $description = "Заказать Яндекс Директ или настроить контекстную рекламу Вы можете у нас. Яндекс Директ от 1000 до 100 000 фраз - объявлений по цене от 9900 до 35000 рублей. Бесплатный аудит в подарок.";

            $add_css = array("/css/fixed_header.css","/css/gocontext.css","/css/slick.css","/css/slick-theme.css");
            $add_js = array('/vendor/fancybox/jquery.fancybox-1.2.1.pack.js','/js/slick.min.js','/js/context.js');
            $includable = array("/engine/pages/context.php");
            break;
        case "kalkulyator-konteksta":
            $title = "Калькулятор контекстной рекламы - рассчитать бюджет!";
            $keywords = "калькулятор контекстной рекламы рассчитать бюджет бесплатно";
            $description = "Калькулятор контекстной рекламы поможет провести предварительную оценку бюджета рекламной кампании, рассчитать простую модель для любого бизнеса, и оценить затраты.";

            $add_css = array("/css/fixed_header.css","/css/bootstrap-slider.min.css","/css/calc.css");
            $add_js = array('/js/bootstrap-slider.min.js','/js/context-calc.js');
            $includable = array("/engine/pages/kalkulyator-konteksta.php");
            break;
        case "remont":
            $title = "Ремонт компьютеров с выездом на дом в Коломне";
            $keywords = "Качественный ремонт компьютеров в Коломне и компьютерная помощь по низким ценам, с выездом на дом - от 20 минут.";
            $description = "ремонт компьютеров выезд в коломне на дом компьютерная помощь компьютера отремонтировать сделать ремонт пк ноутбука сломался ноутбук";

            $add_css = array("/css/fixed_header.css");
            //$add_js = array('/js/bootstrap-slider.min.js','/js/context-calc.js');
            $includable = array("/engine/pages/remont.php");
            break;
        case "videonablyudenie-v-kolomne":
            $title = "Заказать видеонаблюдение в Коломне";
            $keywords = "Видеонаблюдение в Коломне, установка видеонаблюдения, монтаж видеонаблюдения, монтаж, установка, обслуживание, ремонт, низкие, цены, выезд, бесплатно, проект, видеонаблюдение.";
            $description = "Видеонаблюдение в Коломне: получите проект по установке систем видеонаблюдения и обслуживанию в Коломне и соседних городах. С бесплатным выездом, по низким ценам!";

            $add_css = array("/css/fixed_header.css","/css/video_security.css");
            //$add_js = array('/js/bootstrap-slider.min.js','/js/context-calc.js');
            $includable = array("/engine/pages/video.php");
            break;
        case "contacts":
            $title = "Snapix - Контакты";
            $keywords = "контакты создание сайтов в москве настройка контекстной рекламы";
            $description = "Snapix - Контакты. Создание сайтов для бизнеса. Создание продающих сайтов и готовых решений для бизнеса в Москве.";

            $includable = array("/engine/pages/contacts.php");
            $add_css = array("/css/fixed_header.css");
          //  $add_js = array("http://maps.google.com/maps/api/js?sensor=false","/js/contacts_map.js");
            break;
        case "store":
            $keywords = "купить готовый сайт лендинг интернет магазин";
            $description = "купить готовый сайт лендинг интернет магазин в snapix";

            $add_css = array("/css/fixed_header.css","/css/page-store.css");
            if (isset($_GET['product'])){
                $product = $app->db->get('*','shop_products',"WHERE `id`='".$_GET['product']."'");
                $images = array();
                $imgs = $app->db->get('*','shop_products_images',"WHERE `product_id` = '".$product->id."'");

                if (is_array($imgs)){ $images = $imgs; } else {array_push($images, $imgs); }
                $product->images = $images;

                $title = "Купить готовый проект '".$product->name."'";
                $includable = array("/engine/pages/shop.project.php");
            } else {
                $title = "Купить готовый сайт";
                $description = "";
                $keywords = "";
                $categories = $app->db->get('*','shop_categories');
                $products = $app->db->get('*','shop_products');
                $includable = array("/engine/pages/store.php");
            }
            break;
        case "sitemap":
            $title = "Snapix - Карта сайта";
            $keywords = "контакты создание сайтов в москве настройка контекстной рекламы";
            $description = "Snapix - Контакты. Создание сайтов для бизнеса. Создание продающих сайтов и готовых решений для бизнеса в Москве.";

            $add_css = array("/css/fixed_header.css");
            $includable = array("/engine/pages/sitemap.php");
            break;
        case "alex":
            $title = "Нанять программиста для сайта. Опытный разработчик.";
            $keywords = "Разработчик, сайтов, нанять, разработка, сайтов";
            $description = "Меня зовут Александр Гуськов и я являюсь разработчиком сайтов с опытом более 8 лет. Я могу принять участие в вашем проекте на позиции fullstack-разработчика и решить множество задач.";

            $logos = scandir($_SERVER['DOCUMENT_ROOT']."/img/personal-alex/logos/");
            $includable = array('/engine/pages/personal-alex.php');
            $add_css = array("/css/fixed_header.css");
            break;
        default:
            //header('Location: /404');
            break;
    }

} else {
    // main page
    $page_class = 'mainpage';
    $title = "Создание сайтов и интернет магазинов. Реклама и продвижение сайта";
    $description = "Создание сайтов для бизнеса. Создание продающих сайтов. Разработка сайтов и интернет магазинов / SEO / Контекстная реклама / увеличение продаж / поиск клиентов..";
    $keywords = "создание сайтов, продвижение сайтов, разработка сайтов, реклама сайта, создание интернет магазина, seo сайта, интернет маркетинг, маркетинговое агенство, продающий сайт, продающий контент для сайта, контент для сайта, продающие текста, копирайтинг, текста на сайт, сайт, сайтов, сайта, сайты, интернет магазин, интернет-магазин, создание, разработка, сделать, разработать, спроектировать, дизайн, продвижение, топ, в топ, яндекс, yandex, google, mail, rambler, yahoo, гугл, гоогле, рамблер, маил, SEO, сео, продвижение в поисковике, поисковик, поисковике, поисковика, реклама, рекламировать, контекстная, рся, roi, cpa, баннерная, баннерная реклама, поиск клиентов, увеличение клиентов, посещаемость, увеличить посещаемость, увеличить продажи, повысить прибыль, найти клиентов, лиды, лидов, лид, анализ, отчет, директ, adwords, адвордс, web";
    $includable = array(
        "/engine/pages/home.php");

    $add_css = array(
        'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.css',
        '/css/slick.css',
        '/css/slick-theme.css');
     $add_js = array(
         'https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.0.47/jquery.fancybox.min.js',
         '/js/slick.min.js',
         '/js/script.js');

}


$last_project = $app->db->get('*','projects',"ORDER BY `id` DESC LIMIT 1");
include_once($_SERVER["DOCUMENT_ROOT"]  . $app->default_template);

?>