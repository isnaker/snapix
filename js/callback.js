function validate(name,phone){
    if(name.val().length > "0"){
        name.removeClass('border-red');
    }
    else{
        name.addClass("border-red");
        return false;
    }

    if(phone.val().length >6){
        phone.removeClass('border-red');
    }
    else{
        phone.addClass("border-red");
        return false;
    }
    return true;
}

$ = jQuery;

$(function(){
    $('#callback_button').click(function(){
        var name = $('#callback_name');
        var phone = $('#callback_tel');

        if(!validate(name,phone)){
            return false;
        }

        var message = "Имя: "+name.val() + "<br>\n\rТелефон: "+phone.val();
        var action = "callback";

        $.ajax({
            type:'post',
            url: '../ajax.php',
            data: {
                action: action,
                data: message
            },
            success: function(response){
                /*var resp = JSON.parse(response);
                 var stat = resp.message;*/
                $("body").prepend("<div class='alert alert-success left-margin35pr alert-dismissible zindex100 position-fixed' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Успех!</strong> Ваше сообщение было отправлено!</div>");
            },
            error: function(response){
                $("body").prepend("<div class='alert alert-danger left-margin35pr position-fixed zindex100 alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Ошибка!</strong> Что-то пошло не так и Ваше сообщение не отправлено</div>");
            }
        });

    });

    $('#get_project_button').click(function(){
        var name = $('#get_project_name');
        var phone = $('#get_project_tel');

        if(!validate(name,phone)){
            return false;
        }

        var message = "Имя: "+name.val() + "<br>\n\rТелефон: "+phone.val();
        var action = 'video_get_project';

        $.ajax({
            type:'post',
            url: '../ajax.php',
            data: {
                action: action,
                data: message
            },
            success: function(response){
                /*var resp = JSON.parse(response);
                var stat = resp.message;*/
                $("body").prepend("<div class='alert alert-success left-margin35pr alert-dismissible zindex100 position-fixed' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Успех!</strong> Ваше сообщение было отправлено!</div>");
            },
            error: function(response){
                $("body").prepend("<div class='alert alert-danger left-margin35pr position-fixed zindex100 alert-dismissible' role='alert'><button type='button' class='close' data-dismiss='alert' aria-label='Close'><span aria-hidden='true'>&times;</span></button><strong>Ошибка!</strong> Что-то пошло не так и Ваше сообщение не отправлено</div>");
            }
        });
    });


    $(".call_modalcallback_button").click(function(){
        console.log("click");
        $("#contactError").addClass("hidden");
        $("#contactSuccess").addClass("hidden");
        $("#myModalLabel").html($(this).attr("modal-heder-text"));
    });

    $('#button_callback_modal').click(function(){
        var name = $('#client_name');
        var phone = $('#client_phone');

        if(!validate(name,phone)){
            return false;
        }

        var message = "Имя: "+name.val() + "<br>\n\rТелефон: "+phone.val();
        var action = "callback";

        $.ajax({
            type:'post',
            url: '../ajax.php',
            data: {
                action: action,
                data: message
            },
            success: function(response){
                /*var resp = JSON.parse(response);
                var stat = resp.message;*/
                $("#contactSuccess").removeClass("hidden");
            },
            error: function(response){
                $("#contactError").removeClass("hidden");
            }
        });
    });
});