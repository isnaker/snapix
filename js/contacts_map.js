/**
 * Created by Alex on 30.01.2016.
 */
/*
Map Settings

Find the Latitude and Longitude of your address:
- http://universimmedia.pagesperso-orange.fr/geo/loc.htm
- http://www.findlatitudeandlongitude.com/find-address-from-latitude-and-longitude/

*/

// Map Markers
var mapMarkers = [{
    address: "Россия, Московская область, г. Коломна, ул. Огородная, д. 85",
    html: "<strong>Центральный офис</strong><br>г. Коломна, ул. Окский проспект, д. 101",
    icon: {
    image: "img/pin.png",
    iconsize: [26, 46],
    iconanchor: [12, 46]
    },
    popup: true
    }];

// Map Initial Location
var initLatitude = 55.070810;
var initLongitude =  38.800723;


// Map Extended Settings
var mapSettings = {
    controls: {
    draggable: (($.browser.mobile) ? false : true),
    panControl: true,
    zoomControl: true,
    mapTypeControl: true,
    scaleControl: true,
    streetViewControl: true,
    overviewMapControl: true
    },
    scrollwheel: false,
    markers: mapMarkers,
    latitude: initLatitude,
    longitude: initLongitude,
    zoom: 14
    };

var map = $("#googlemaps").gMap(mapSettings),
mapRef = $("#googlemaps").data('gMap.reference');

// Create an array of styles.
var mapColor = "#0088cc";

var styles = [{
    stylers: [{
    hue: mapColor
    }]
    }, {
    featureType: "road",
    elementType: "geometry",
    stylers: [{
    lightness: 0
    }, {
    visibility: "simplified"
    }]
    }, {
    featureType: "road",
    elementType: "labels",
    stylers: [{
    visibility: "off"
    }]
    }];

var styledMap = new google.maps.StyledMapType(styles, {
    name: "Styled Map"
    });

mapRef.mapTypes.set('map_style', styledMap);
mapRef.setMapTypeId('map_style');
