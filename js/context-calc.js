/**
 * Created by Alex on 24.11.2016.
 */
$(document).ready(function(){

    //IX = $("#ex1").slider();
    //IY = $("#ex2").slider();
    IZ = $("#ex3").slider();
    IA = $("#ex4").slider();
    IB = $("#ex5").slider();

    $("#ex1").on("slide", function(slideEvt) {
        $("#ex1SliderVal").text(slideEvt.value);
    });
    $("#ex2").on("slide", function(slideEvt) {
        $("#ex2SliderVal").text(slideEvt.value);
    });
    $("#ex3").on("slide", function(slideEvt) {
        $("#ex3SliderVal").text(slideEvt.value);
    });
    $("#ex4").on("slide", function(slideEvt) {
        $("#ex4SliderVal").text(slideEvt.value);
    });
    $("#ex5").on("slide", function(slideEvt) {
        $("#ex5SliderVal").text(slideEvt.value);
    });
});

$(document).on('click','#calc',function(){
    calculate();
});

function calculate(){
    //var x=$(IX).slider('getValue');
    //var y=$(IY).slider('getValue');
    var x = $('#ex123').val();
    var y = $('#ex223').val();
    var z=$(IZ).slider('getValue');
    var a=$(IA).slider('getValue');
    var b=$(IB).slider('getValue');

    console.log(x);
    console.log(y);
    console.log(z);
    console.log(a);
    console.log(b);


    var c = 100/b;//конв.оператора в соотношении
    var d = 100/z;//конв.сайта в соотношении
    var h = c * x;//формула целевых заявок
    var r = parseInt(h, 10);
    var i = r * d;//формула посетителей
    var q = parseInt(i, 10);
    var o = x * y;//формула общая маржа
    var w = a * q;//формула расходов на рекламу
    var n = o - w;//формула чистой прибыли от рекламы
    var t = o - w;//формула ROI коэффициент окупаемости
    var s = t / w;
    var g = s * 100;
    var m = parseInt(g, 10);
    var e = 30 * w;//формула ROI срок окупаемости
    var f = e / o;
    var p = parseInt(f, 10);
    $('#cz').html(r);//итог целевых заявок
    $('#ps').html(q);//итог посетителей
    $('#pribil').html(o);//итог общая маржа
    $('#vloj').html(w);//итог расходы на рекламу
    $('#pribil_chist').html(n);//итог чистой прибыли от рекламы
    $('#days').html(m);//ROI коэффициент окупаемости
    $('#prozent').html(p);//ROI срок окупаемости
}