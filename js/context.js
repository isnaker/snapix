$('.why-count').popover();
$('.why-time').popover();

$(".grafic").fancybox();

$('#slider').slick({
    arrows: true,
    dots: true,
    autoplay: true,
    prevArrow: "<img style='height:110px;width: 50px; left: -34px;' class='a-left control-c next slick-prev img-responsive hidden-xs' src='/img/left.png'>",
    nextArrow: "<img style='height:110px;width: 50px; right: -35px;' class='a-right control-c next slick-next img-responsive hidden-xs' src='/img/right.png'>"

});


$('#slider_2').slick({
    arrows: true,
    dots: true,
    autoplay: true,
    prevArrow: "<img style='height:110px;width: 50px; left: -34px;' class='a-left control-c next slick-prev img-responsive hidden-xs' src='/img/left.png'>",
    nextArrow: "<img style='height:110px;width: 50px; right: -35px;' class='a-right control-c next slick-next img-responsive hidden-xs' src='/img/right.png'>"

});


$(document).ready(function () {

    $('[data-target="#callbackModalTable"]').click(function(){
        var that = this;
        $('#uslugaform').val($(that).attr('data-label'));
    });

    $('.sender').click(function () {
        var that = this;
        var form = $($(that).attr('data-parent'));
        var inputs = $(form).find('.input');
        var message = "";
        var responder = $(form).find('.responder');
        var validated = true;


        for (var i = 0; i < inputs.length; i++) {
            if ($(inputs[i]).val() == "") {
                validated = false;
            }
        }

        if (validated) {
            for (i = 0; i < inputs.length; i++) {
                message += "<strong>" + $(inputs[i]).attr('data-label') + "</strong>: " + $(inputs[i]).val() + "<br>";
            }
            $.ajax({
                type: 'post',
                url: '/engine/Ajax.php',
                data: {
                    action: "callback",
                    data: message
                },
                success: function (response) {
                    yaCounter22886983.reachGoal('context');
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(that).addClass('hidden');
                        $(responder).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(responder).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function (response) {
                }
            });
        }
    });

    $('#headercallback .sender1').click(function () {
        var sender = $(this);
        var form = "#headercallback";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/Ajax.php',
                data: {
                    action: "callback",
                    data: message + phone
                },
                success: function (response) {
                    yaCounter22886983.reachGoal('context');
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function (response) {
                }
            });
        }
    });


    $("#owl-context").owlCarousel({

        navigation: true, // Show next and prev buttons
        slideSpeed: 300,
        paginationSpeed: 400,
        singleItem: true
        // "singleItem:true" is a shortcut for:
        // items : 1,
        // itemsDesktop : false,
        // itemsDesktopSmall : false,
        // itemsTablet: false,
        // itemsMobile : false

    });

});/**
 * Created by Alex on 24.11.2016.
 */
