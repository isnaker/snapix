
(function( $ ) {

	'use strict';

/*
	Dialog with CSS animation
	*/
	$('.popup-with-zoom-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-zoom-in'
	});

	$('.popup-with-move-anim').magnificPopup({
		type: 'inline',

		fixedContentPos: false,
		fixedBgPos: true,

		overflowY: 'auto',

		closeBtnInside: true,
		preloader: false,

		midClick: true,
		removalDelay: 300,
		mainClass: 'my-mfp-slide-bottom'
	});

	// highlight current url in menu
	$('#mainNav .active').removeClass('active');
	var loc = window.location.pathname;
	var elem = $('#mainNav').find('[href="'+loc+'"]');
//	console.log(elem);
	$(elem).parent().addClass('active');

	if ($(elem).attr('data-id') != ""){
		$($(elem).attr('data-id')).addClass('active');
	}


	if (typeof currentLink != 'undefined'){
		$(currentLink).addClass('active');
	}





	}).apply( this, [ jQuery ]);