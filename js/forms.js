$(function() {
    $('input').focusout(function() {
        $(this).parent().removeClass("has-error")
    });
    $('input').focusin(function() {
        $(this).parent().removeClass("has-error")
    });
    $('#callbackModal .sender').click(function() {
        var sender = $(this);
        var form = "#callbackModal";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "callback",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#orderSupportModal, #orderLandingModal').on('show.bs.modal', function(e) {
        var modal = $(this);
        var button = $(e.relatedTarget);
        var selectedPlan = $(button).attr('data-value');
        var plan_html = $('h3[data-value="' + selectedPlan + '"]').html();
        $(modal).find('[data-value="selectedPlan"]').html(plan_html);
    });
    $('#orderSupportModal .sender').click(function() {
        var sender = $(this);
        var form = "#orderSupportModal";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            message = "Тариф: " + $(form).find('.plan').html() + "<br>" + message + phone;
            console.log(message);
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "support_order",
                    data: message
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#head-block .sender').click(function() {
        var sender = $(this);
        var message = "Телефон: ";
        var validated = true;
        var phone = $('#head-block .form input').val();
        var alert = $('#head-block .form .alert');
        if (phone == "") {
            validated = false;
            $('#head-block .form input').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "landing_callback_headform",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#landing_consult_form .sender').click(function() {
        var sender = $(this);
        var form = "#landing_consult_form";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "landing_consult",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#orderLandingModal .sender').click(function() {
        var sender = $(this);
        var form = "#orderLandingModal";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            message = "Тариф: " + $(form).find('.plan').html() + "<br>" + message + phone;
            console.log(message);
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "landing_order",
                    data: message
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('.link-sender').click(function() {
        var val = $(this).attr('data-link');
        $('#shop_pricing_table button[data-value="' + val + '"]').trigger('click');
    });
    $('#orderShopModal').on('show.bs.modal', function(e) {
        var modal = $(this);
        var button = $(e.relatedTarget);
        var selectedPlan = $(button).attr('data-value');
        var plan_html = $('.item[data-value="' + selectedPlan + '"]').html();
        $(modal).find('[data-value="selectedPlan"]').html(plan_html);
    });
    $('#orderShopModal .sender').click(function() {
        var sender = $(this);
        var form = "#orderShopModal";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            message = "Тариф: " + $(form).find('.plan').html() + "<br>" + message + phone;
            console.log(message);
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "shop_order",
                    data: message
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    console.log(res);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#shop_consult_sidebar .sender').click(function() {
        var sender = $(this);
        var form = "#shop_consult_sidebar";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "shop_consult_sidebar",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#orderShop1CSyncModal .sender').click(function() {
        var sender = $(this);
        var form = "#orderShop1CSyncModal";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "orderShop1CSyncModal",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#remont_inline .sender').click(function() {
        var sender = $(this);
        var form = "#remont_inline";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "remont",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    $('#remontModal .sender').click(function() {
        var sender = $(this);
        var form = "#remontModal";
        var message = "Телефон: ";
        var validated = true;
        var phone = $(form).find('input[type="tel"]').val();
        var alert = $(form).find('.alert');
        if (phone == "") {
            validated = false;
            $(form).find('input[type="tel"]').parent().addClass('has-error');
        }
        if (validated) {
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "remont",
                    data: message + phone
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }
    });
    
    $('#footerForm .rounded_checkbox *').click(function () {
        $(this).toggleClass('selected');
    });

    $('#footerForm .sender').click(function () {
        var alert = $('#footerForm').find('.alert');
        var validated = true;
        var services = $('#footerForm .rounded_checkbox input.selected');
        var inputs = $('#footerForm .input');
        var message = "<h3>Заказ услуги (нижняя форма)</h3><h4>Выбранные услуги:</h4>";

        for (var i=0; i < services.length; i++){
            var service = $(services)[i];
            message += "<h6>"+ $(service).val()+"</h6>";
        }

        message += "<h3>Данные клиента:</h3>";
        for (var i=0; i < inputs.length; i++){
            var input = $(inputs)[i];
            if ($(input).val() != ""){
                message += "<br>" + $(input).attr('data-label') + ": " + $(input).val();
            }

            if ($(input).hasClass('required') && $(input).val() == "" ){
                validated = false;
                $(input).parent().addClass('has-error');
            }
        }

        if (validated){
            // send ajax
            $.ajax({
                type: 'post',
                url: '/engine/classes/Ajax.php',
                data: {
                    action: "service",
                    data: message
                },
                success: function(response) {
                    var res = JSON.parse(response);
                    if (res.result == "ok") {
                        $(alert).addClass("alert-success").removeClass("hidden").html("Ваша заявка принята!");
                    } else {
                        $(alert).addClass("alert-danger").removeClass("hidden").html("Произошла ошибка!");
                    }
                },
                error: function(response) {}
            });
        }


        console.log(message);

    });
    
});